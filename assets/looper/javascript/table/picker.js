var Picker = {
    dom : {
        links : [],
        form : null,
        input: null,
    },
    initalize : function(){
        this.dom.links = $('[data-id]');
        this.dom.form = $('#form');
        this.dom.input = $('#input');

        this.events();
    },
    events : function(){
        this.dom.links.off('click').on('click', this.linkClick.bind(this));
    },
    linkClick : function(e){
        var elem = $(e.currentTarget);
        var item = elem.data('id');
        this.dom.input.val(item);
        this.dom.form.submit();
    }
};

$(function(){
    Picker.initalize();
});
