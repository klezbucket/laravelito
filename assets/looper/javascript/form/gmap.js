var Gmap = {
    doms : [],
    maps : {},
    polygons : {},
    markers : {},
    readonly : 0,
    initialize : function(){
        this.doms = $('[data-gmap-canvas]');
        var self = this;
        
        this.doms.each(function(){
            var element = $(this);
            var startupFn = self.startup.bind(self,element);
            startupFn();
        });
    },
    isReadonly : function(mixed){
        if(mixed instanceof google.maps.Map){
            element = $(mixed.getDiv());
        }
        else{
            element = mixed;
        }

        var readonly = element.attr('data-gmap-readonly') || false;
        return readonly ? true : false;
    },
    startup : function(element){
        var name = element.attr('data-gmap-canvas');

        if(this.isReadonly(element)){
            name = name + '_' + (++Gmap.readonly);
            Gmap.rename(element, name);
        }
        
        if(typeof this.maps[name] !== 'undefined'){
            var div = this.maps[name].getDiv();
            $(div).attr('data-gmap-canvas', name);
            element.replaceWith(div);
            return name;
        }

        var latitude = element.siblings('[data-gmap-latitude="' + name + '"]');
        var longitude = element.siblings('[data-gmap-longitude="' + name + '"]');
        var zoom = element.siblings('[data-gmap-zoom="' + name + '"]');
        var type = element.siblings('[data-gmap-type="' + name + '"]');

        var opts = {
            center: { 
                lat: parseFloat(latitude.val()),
                lng: parseFloat(longitude.val())
            },
            streetViewControl: false,
            zoom: parseInt(zoom.val())
        };

        this.maps[name] = this.map(element, opts, type.val());
        return name;
    },
    rename : function(element,newName){
        var name = element.attr('data-gmap-canvas');
        var latitude = element.siblings('[data-gmap-latitude="' + name + '"]');
        var longitude = element.siblings('[data-gmap-longitude="' + name + '"]');
        var zoom = element.siblings('[data-gmap-zoom="' + name + '"]');
        var type = element.siblings('[data-gmap-type="' + name + '"]');
        var input = element.siblings('input[type="hidden"]');

        element.attr('data-gmap-canvas', newName);
        latitude.attr('data-gmap-latitude', newName);
        longitude.attr('data-gmap-longitude', newName);
        zoom.attr('data-gmap-zoom', newName);
        type.attr('data-gmap-type', newName);
        input.attr('name', newName);

        //TODO: agregar limpieza de markers
        var map = this.maps[newName] || null;
        var actPolygons = this.polygons[name] || [];

        for(var i in this.polygons[newName]){
            var polygon = this.polygons[newName][i];
            polygon.setMap(null);
        }

        this.polygons[newName] = actPolygons;
        this.polygons[name] = [];

        for(var i in this.polygons[newName]){
            var polygon = this.polygons[newName][i];
            polygon.setMap(map);
        }
        // POLYGON CLEAN
    },
    options : {
        polygon : {
            draggable: true,
            geodesic: true,              
            fillColor: '#103054',
            fillOpacity: 0.3,
            strokeWeight: 2,
            clickable: true,
            editable: true,
            zIndex: 1
        }
    },
    types : {
        polygon_map : function(map){
            var drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                drawingControl: true,
                drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: [ 'polygon' ]
                },
                polygonOptions: Gmap.options.polygon
            });

            google.maps.event.addListener(drawingManager, 'polygoncomplete', (polygon) => {
                drawingManager.setDrawingMode(null);
                var eventFn = this.events.polygon.bind(this,map,polygon);
                eventFn();
            });

            var initFn = this.init.polygon_map.bind(this,drawingManager,map);
            initFn();
        },
        marker_map : function(map){
            if(! this.isReadonly(map)){
                google.maps.event.addListener(map, 'click', (event) => {
                    var eventFn = this.events.marker.bind(this,map,event.latLng);
                    eventFn();
                });
            }

            var initFn = this.init.marker_map.bind(this,map);
            initFn();
        },
    },
    events : {
        marker : function(map,latLng){
            var name = this.getName(map);
            var markerFn = this.change.marker.bind(this,map);
            var marker;

            if(typeof this.markers[name] === 'undefined'){
                marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    draggable: !this.isReadonly(map)
                });

                this.markers[name] = marker;

                if(! this.isReadonly(map)){
                    marker.addListener('dragend', markerFn);
                }
            }
            else{
                marker = this.markers[name];
                marker.setPosition(latLng);
            }

            map.panTo(latLng);
            markerFn();
        },
        polygon : function(map,polygon){
            var name = this.getName(map);

            if(typeof this.polygons[name] === 'undefined'){
                this.polygons[name] = [];
            }

            if(polygon.getPath().getLength() < 3){
                polygon.getPath().clear();
                return;
            }

            var polygonFn = this.change.polygon.bind(this,map);
            polygon.getPath().addListener('insert_at', polygonFn);
            polygon.getPath().addListener('set_at', polygonFn);
            polygon.getPath().addListener('remove_at', polygonFn);
            polygon.getPath().addListener('dragend', polygonFn);
            this.polygons[name].push(polygon);

            polygonFn();
        }
    },
    change : {
        marker : function(map){
            var input = this.getInput(map);
            var name = this.getName(map);

            if(typeof this.markers[name] !== 'undefined'){
                var marker = this.markers[name];
                input.val(JSON.stringify(marker.position));
            }
            else{
                input.val('');
            }
        },
        polygon : function(map){
            var input = this.getInput(map);
            var name = this.getName(map);
            var coords = [];
    
            if(typeof this.polygons[name] !== 'undefined'){
                for(var i in this.polygons[name]){
                    var polygon = this.polygons[name][i];
                    coords.push(polygon.getPath().getArray());
                }
            }

            if(coords.length > 0){
                var json = JSON.stringify(coords);
                input.val(json);
            }
            else{
                input.val('');
            }
        },
    },
    init : {
        marker_map : function(map){
            var input = this.getInput(map);
            var json = input.val();

            if(json.length > 0){
                var latLng = JSON.parse(json);
                var initFn = this.events.marker.bind(this,map,latLng);
                initFn();
            }
        },
        polygon_map : function(drawingManager,map){
            var input = this.getInput(map);
            var json = input.val();

            if(json.length > 0){
                var bounds = new google.maps.LatLngBounds();
                var paths = JSON.parse(json);
                
                for(var i in paths){
                    var path = paths[i];
                    var polygonOpts = Gmap.options.polygon;
                    polygonOpts.map = map;
                    polygonOpts.paths = path;

                    if(this.isReadonly(map)){
                        polygonOpts.editable = false;
                        polygonOpts.draggable = false;
                    }
                    
                    var polygon = new google.maps.Polygon(polygonOpts);
                    var eventFn = this.events.polygon.bind(this,map,polygon);
                    eventFn();
            
                    polygon.getPath().forEach(function (path, index) {
                        bounds.extend(path);
                    });
                }
                
                map.fitBounds(bounds);
            }

            if(this.isReadonly(map)){
                return;
            }

            drawingManager.setMap(map);
        },
    },
    getInput : function(map){
        var element = $(map.getDiv());
        var name = element.attr('data-gmap-canvas');
        var input = element.siblings('[name="' + name + '"]');
        return input;
    },
    getName : function(map){
        var element = $(map.getDiv());
        var name = element.attr('data-gmap-canvas');
        return name;
    },
    map : function(dom,opts,type){
        var map = new google.maps.Map(dom[0], opts);

        if(typeof this.types[type] !== 'undefined'){
            var typeFn = this.types[type].bind(this, map);
            typeFn();
        }

        return map;
    },
    recycle : function(name){
        if(typeof this.maps[name] === 'undefined'){
            return;
        }

        delete this.maps[name];
    }
};

$(function(){
    Gmap.initialize();
});