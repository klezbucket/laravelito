var Foreign = {
    context : {},
    initialize : function(){
        var startUpFn = this.startup.bind(this);

        $('[data-foreign-url]').each(function(){
            var elem = $(this);
            startUpFn(elem);
        });
    },
    startup : function(elem){
        var url = elem.attr('data-foreign-url');
        var bearer = elem.attr('data-foreign-bearer');
        var multiple = elem.attr('multiple');
        var hiddenId = elem.attr('data-foreign-hidden');
        var clearId = elem.attr('data-foreign-clear');
        var hidden = $('input[name="' + hiddenId + '"]');
        var clear = $('#' + clearId)
        var id = elem.attr('data-foreign-id');
        
        if(hidden.length === 0){
            hidden = elem.siblings('#' + elem.attr('id') + '_autocomplete');
        }

        if(multiple){
            var name = elem.attr('name') + '[]';
            elem.attr('name',name);
        }

        var formatRepo = function formatRepo(repo) {
            return repo.text;
        };
    
        var formatRepoSelection = function formatRepoSelection(repo) {
            return repo.text;
        };
    
        elem.select2({
            ajax: {
                url: url,
                dataType: 'json',
                type: 'POST',
                delay: 250,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader ("X-Access-Token", "Bearer " + bearer);
                },
                data: function data(params) {
                    return {
                        q: params.term,
                        page: params.page,
                        context: Foreign.context || {}
                    };
                },
                processResults: function processResults(data, params) {
                    return {
                        results: data,
                        pagination: {
                            more: data.length > 0
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function escapeMarkup(markup) {
                return markup;
            },
            minimumInputLength: 0,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection,
        });

        var repo = {};
        var selection = [];
    
        elem.on('select2:select', function (e) {
            var data = e.params.data;

            repo['#' + data.id] = data.text;

            if(!multiple){
                selection = [];
            }

            selection.push(data.text.replace(/,/, ''));

            hidden.val(selection.join(','));
        });
    
        elem.on('select2:unselect', function (e) {
            var val = elem.val();
            selection = [];

            if(val instanceof Array){
                for(var i in val){
                    var id = val[i];
                    selection.push(repo['#' + id]);
                }
            }

            hidden.val(selection.join(','));
        });

        if(id){
            var ids = id.split(',');
            var rawLabels = hidden.val();
            var labels = rawLabels === '' ? '' : hidden.val().split(',');

            for(var i in ids){
                var id = ids[i];
                var option = new Option(labels[i], id, true, true);
                elem.val(id);
                elem.append(option);;

                repo['#' + id] = labels[i];
                selection.push(repo['#' + id]);
            }

            elem.val(ids);
            hidden.val(selection.join(','));
        }

        if(elem.hasClass('is-invalid')){
            var span = elem.siblings('span.select2').find('span.select2-selection');

            span.css({
                borderColor : '#b76ba3',
                paddingRight : 'calc((3em + 2.25rem)/4 + 1.75rem)',
            });
        }

        clear.click(function(){
            selection = [];
            hidden.val(null);
            elem.val(null).trigger('change');
        }).css({
            color: '#b76ba3'
        });
    }
};

$(function(){
    Foreign.initialize();
});