var RowsForm = {
    dom : {
        tables : [],
        rows : {},
        clones : {},
        empty : {},
    },
    emptyRows : {},
    data : {},
    attrs : {},
    lang : {},
    validations : {},
    initialize : function(){
        var self = this;

        this.dom.tables = $('.rows-form table');
        this.dom.tables.each(function(){
            var table = $(this);
            var id = table.attr('id');
            table.find('abbr').detach();
            
            if(typeof self.dom.rows[id] === 'undefined'){
                var emptyFn = self.setEmpty.bind(self);
                emptyFn(table);

                self.dom.rows[id] = table.find('tbody tr');
                var cloneFn = self.setClone.bind(self);
                cloneFn(table);

                var langFn = self.setLang.bind(self);
                langFn(table,JSON.parse($('[data-rowsform-lang="' + id + '"]').val()));

                var emptyRowsFn = self.setEmptyRows.bind(self);
                emptyRowsFn(table,$('[data-rowsform-empty-rows="' + id + '"]').val());

                var validationsFn = self.setValidations.bind(self);
                validationsFn(table,JSON.parse($('[data-rowsform-validations="' + id + '"]').val()));

                var attrsFn = self.setAttrs.bind(self);
                attrsFn(table,JSON.parse($('[data-rowsform-attrs="' + id + '"]').val()));

                var dataFn = self.setData.bind(self);
                dataFn(table,JSON.parse($('[data-rowsform-data="' + id + '"]').val()));
            }

            var append = table.find('[data-toggle="rowsform-append"]');
            append.off('click').on('click', self.appendBtnClick.bind(self,table));
            delete self.data[id];
            delete self.validations[id];
        });
    },
    appendBtnClick  : function(table, e){
        e.preventDefault();

        var index = table.find('tbody tr:not([data-empty])').length;
        this.renderRow(table, index);
    },
    rindex : function(table,tr,index){
        var id = table.attr('id');
        var elems = tr.find('[data-name]');
        var self = this;

        elems.each(function(){
            var elem = $(this);
            var orig = elem.data('name');
            var name = id + '[' + orig + '][' + index + ']';

            elem.attr('name', name);
            self.fixIds(tr,index);

            for(var i in self.widgets.rename){
                var fn = self.widgets.rename[i].bind(self,elem,index,id);
                fn();
            }
        });

        return tr;
    },
    getClone : function(table,index,data = {},attrs = {}){
        var id = table.attr('id');
        var clone = this.dom.clones[id].clone();
        this.applyData(clone,data);
        this.applyAttrs(clone,attrs);

        return this.rindex(table,clone,index);
    },
    fixIds : function(tr,index){
        var bearers = tr.find('[data-id]');
        bearers.each(function(){
            var bearer = $(this);
            var id = bearer.attr('data-id');
            bearer.attr('id', id + '_' + index);
        });
    },
    applyAttrs : function(tr,attrs){
        for(var field in attrs){
            var attributes = attrs[field];
            var elem = tr.find('[data-name="' + field + '"]:last');

            for(var attr in attributes){
                elem.attr(attr, attributes[attr]);
            }
        }
    },
    applyData : function(tr,data){
        for(var field in data){
            var value = data[field];
            var elem = tr.find('[data-name="' + field + '"]:last');
            Form.inputValue(elem,value);
        }
    },
    renderRow : function(table,index,data = {},attrs = {}){
        var clone = this.getClone(table,index,data,attrs);
        var tbody = table.find('tbody');
        this.renderValidations(table,clone,index);
        this.setEvents(table,clone,index);
        tbody.append(clone);
        clone.fadeIn();
        this.setEmpty(table);
    },
    resolvValidation : function(table, messages, field){
        var id = table.attr('id');
        var config = this.lang[id].fields[field] || null;

        if(config){
            for(var i in messages){
                var path = messages[i].split('.');
                var conf = config;

                for(var j in path){
                    var part = path[j];
                    conf = conf[part] || 'error';
                }

                messages[i] = typeof conf === 'string' ? conf : 'error';
            }
        }

        return messages;
    },
    renderValidations: function(table,tr,index){
        var id = table.attr('id');
        var myValidations = this.validations[id] || [];
        var validations = myValidations[index] || {};

        for(var field in validations){
            var messages  = this.resolvValidation(table, validations[field], field);

            for(var i in messages){
                var elem = tr.find('[data-name="' + field + '"]:last');
                var message = messages[i];

                Form.validation(elem,message);
            }
        }
    },
    setEmptyRows : function(table, rows){
        var id = table.attr('id');
        this.emptyRows[id] = rows;
    },
    setLang : function(table, lang = {}){
        var id = table.attr('id');
        this.lang[id] = lang;
    },
    setValidations : function(table, validations = []){
        var id = table.attr('id');
        this.validations[id] = validations;
    },
    setAttrs : function(table, attrs = []){
        var id = table.attr('id');
        this.attrs[id] = attrs;
    },
    getAttrs : function(table,j){
        var id = table.attr('id');
        var attrs = this.attrs[id];
        var rows = [];

        for(var field in attrs){
            for(var j in attrs[field]){
                var value = attrs[field][j];

                if(typeof rows[j] === 'undefined'){
                    rows[j] = {};
                }

                rows[j][field] = value;
            }
        }

        return rows;
    },
    setData : function(table, data = {}){
        var id = table.attr('id');
        this.data[id] = data;

        if(this.data[id].length === 0){
            var emptyRows = parseInt(this.emptyRows[id] || 0);

            for(var i = 0;i < emptyRows;i++){
                this.renderRow(table, i);
            }

            if(emptyRows === 0){
                this.emptyTable(table);
            }
        }
        else{
            var rows = this.parseData(data);
            var attrs = this.getAttrs(table,j);

            for(var j in rows){
                var row = rows[j];
                this.renderRow(table,j,row,attrs[j] || {});
            }
        }
    },
    parseData : function(data){
        var rows = [];

        for(var field in data){
            for(var j in data[field]){
                var value = data[field][j];

                if(typeof rows[j] === 'undefined'){
                    rows[j] = {};
                }

                rows[j][field] = value;
            }
        }

        return rows;
    },
    setEmpty : function(table){
        var id = table.attr('id');
        var empty = table.find('[data-empty]');

        if(empty.length > 0){
            this.dom.empty[id] = empty.detach();
        }
    },
    setClone : function(table){
        var self = this;
        var id = table.attr('id');
        var rows = self.dom.rows[id] || [];

        rows.each(function(){
            var tr = $(this);
            var elems = tr.find('[name]').filter(':not([data-name])');

            elems.each(function(){
                var elem = $(this);
                var name = elem.attr('name');
                var newName = id + '[' + name + '][]';
                elem.attr('name', newName);
                elem.attr('data-name', name);

                for(var i in self.widgets.pre){
                    var preFn = self.widgets.pre[i].bind(self, elem);
                    preFn();
                }
            });

            var bearers = tr.find('[id]');
            bearers.each(function(){
                var bearer = $(this);
                var id = bearer.attr('id');
                bearer.attr('data-id',id);
            });
        });

        this.dom.clones[id] = rows.detach();
    },
    setEvents : function(table,tr,index){
        var deleteBtn = tr.find('[data-toggle="rowsform-delete"]');
        var id = table.attr('id');
        deleteBtn.off('click').on('click', this.deleteBtnClick.bind(this,table,tr));

        for(var i in this.widgets.events){
            var fn = this.widgets.events[i].bind(this,tr,index,id);
            fn();
        }
    },
    widgets : {
        sindex : [
            'foreign', 'gmap'
        ],
        pre : {
            foreign : function(elem){
                if(elem.hasClass('select2-hidden-accessible')){
                    elem.select2('destroy');
                    elem.off('select2:select');
                    elem.off('select2:unselect');
                }
            }
        },
        rename : {
            picture : function(elem){
                var dropzone = elem.parent().find('.picture[dropzone-for]');
                
                if(dropzone.length > 0){
                    Picture.rename(dropzone,elem.attr('name'));
                }
            },
            gmap : function(elem){
                var gmap = elem.parent().find('[data-gmap-canvas]');

                if(gmap.length > 0){
                    Gmap.rename(gmap,elem.attr('name'));
                }
            }
        },
        events : {
            foreign : function(tr,index,rowsform){
                var foreigns = tr.find('[data-foreign-url]');
                var self = this;

                foreigns.each(function(){
                    var elem = $(this);
                    self.widgets.pre.foreign(elem);

                    var hidden = elem.siblings('[type="hidden"]');
                    var id = elem.attr('id');
                    var name = elem.attr('data-name');
                    var hiddenId = elem.attr('data-id') + '_autocomplete';
                    
                    var formData = self.data[rowsform] || {};
                    var autocompletes = formData[hiddenId] || [];
                    var hiddenValue = autocompletes[index] || '';
                    var foreigns = formData[name] || [];
                    var foreignId = foreigns[index] || null;

                    hidden.attr('id', id + '_autocomplete');

                    if(hidden.val() === ''){
                        hidden.val(hiddenValue);
                    }

                    elem.attr('data-select2-id', id);
                    elem.attr('aria-describedby', id + 'Help');
                    elem.attr('data-foreign-help', id + 'Help');
                    elem.attr('data-foreign-clear', id + '_autocomplete_clear');
                    elem.attr('data-foreign-hidden', id + '_autocomplete');

                    if(elem.attr('data-foreign-id') === ''){
                        elem.attr('data-foreign-id', foreignId);
                    }

                    Foreign.startup(elem);
                });
            },
            flatpickr : function(tr){
                var datetimepickers = tr.find('[data-toggle="flatpickr"]');
                datetimepickers.each(function(){
                    var elem = $(this);
        
                    flatpickr(elem[0], {
        
                    });
                });
            },
            gmap : function(tr){
                var maps = tr.find('[data-gmap-canvas]');

                maps.each(function(){
                    Gmap.startup($(this));
                });
            },
            picture : function(tr){
                var dropzones = tr.find('.picture[dropzone-for]')
                
                dropzones.each(function(){
                    Picture.startup($(this))
                });
            },
        },
    },
    deleteBtnClick : function(table,tr,e){
        e.preventDefault();

        var tbody = table.find('tbody');
        var self = this;

        if(tbody.children().length === 1){
            this.emptyTable(table);
        }

        tr.fadeOut(400, function(){
            tr.detach();
            self.sindex(table);
        });
    },
    emptyTable : function(table){
        var id = table.attr('id');
        var tbody = table.find('tbody');
        var empty = this.dom.empty[id];
        tbody.append(empty);
        empty.fadeIn();
    },
    sindex : function(table){
        var elems = table.find('tbody tr');
        var id = table.attr('id');
        var self = this;
        var index = 0;

        elems.each(function(){
            var tr = $(this);
            self.rindex(table, tr, index);

            var widgets = self.widgets.sindex;
            for(var i in widgets){
                var fnName = widgets[i];
                var fn = self.widgets.events[fnName].bind(self,tr,index,id);
                fn();
            }

            index++
        });
    }
};

$(function(){
    RowsForm.initialize();
});
