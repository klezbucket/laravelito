var Picture = {
    doms : [],
    files : [],
    inputs : [],
    mimes : [],
    sizes : [],
    clears : [],
    invalidSize : null,
    invalidMime : null,
    lang : {},
    initialize : function(){
        this.doms = $('.picture[dropzone-for]');
        var self = this;

        this.doms.each(function(){
            var startupFn = self.startup.bind(self);
            startupFn($(this));
        });
    },
    rename : function(dropzone,name){
        var group = dropzone.parent();

        dropzone.attr('dropzone-for', name);

        group.find('[file-for]').each(function(){
            $(this).attr('file-for', name);
        });

        group.find('[size-for]').each(function(){
            $(this).attr('size-for', name);
        });

        group.find('[mime-for]').each(function(){
            $(this).attr('mime-for', name);
        });

        group.find('[lang-for]').each(function(){
            $(this).attr('lang-for', name);
        });
    },
    startup : function(dropzone){
        var name = dropzone.attr('dropzone-for');
        var group = dropzone.parent();
        var input = group.find('input[name="' + name + '"][type="hidden"]');
        var clearId = input.attr('data-picture-clear');
        var clear = group.find('#' + clearId);
        var files = group.find('input[file-for="' + name + '"][type="file"]');
        var size = group.find('input[size-for="' + name + '"][type="hidden"]');
        var index = this.inputs.push(input) - 1;

        this.clears[index] = clear;
        this.resolvLang(dropzone,index);
        this.resolvMimes(dropzone,index);
        this.files.push(files);
        this.sizes.push(size.val());
        this.inputValProcess(dropzone,index,input);
        this.events(dropzone, index);
    },
    inputValProcess : function(dropzone,index,input){
        var dataurl = input.val();
        var url = input.attr('data-picture-url');

        if(url.length > 0){
            this.loadURL(dropzone,index,url)
        }
        else if(dataurl.length > 0){
            this.loadDataURL(dropzone,index,dataurl)
        }
        else{
            dropzone.find('.placeholder').show();
        }
    },
    loadURL : function(dropzone,index,url){
        try{
            var clearFn = this.clearClick.bind(this,dropzone,index);
            var self = this;
            var loader = dropzone.find('.loader');
            this.inputs[index].val('');
            loader.show();
            
            fetch(url)
                .then( response => {
                    return response.blob();
                })
                .then( blob => {
                    var reader = new FileReader();
                    
                    reader.onload = function(){ 
                        var extractDataFn = self.loadDataURL.bind(self, dropzone, index, this.result);
                        extractDataFn();
                        loader.hide();
                    };

                    reader.onerror = function(){
                        clearFn();
                    };

                    reader.readAsDataURL(blob) ;
                });
        }
        catch(e){
            console.log(e);
            clearFn();
        }
    },
    loadDataURL : function(dropzone,index,dataurl){
        try{
            var file = this.dataURLtoFile(dataurl, '');
    
            if(file instanceof File){
                this.handleFile(dropzone,index,file);
            }
        }
        catch(e){
            console.log(e);
            this.clearClick(dropzone,index);
        }
    },
    resolvMimes : function(dropzone,index){
        var self = this;
        var name = dropzone.attr('dropzone-for');
        var group = dropzone.parent();
        this.mimes[index] = [];

        group.find('input[mime-for="' + name + '"][type="hidden"]').each(function(){
            self.mimes[index].push($(this).val());
        });

    },
    resolvLang : function(dropzone,index){
        var self = this;
        var name = dropzone.attr('dropzone-for');
        var group = dropzone.parent();
        this.lang[index] = {};

        group.find('[lang-for="' + name + '"][lang-entry]').each(function(){
            var entry = $(this).attr('lang-entry');
            self.lang[index][entry] = $(this).val();
        });
    },
    filesTrigger : function(dropzone,index){
        var files = this.files[index];
        files.trigger('click');
    },
    events : function(dropzone, index){
        dropzone.off('click').on('click', this.filesTrigger.bind(this, dropzone, index)).css('cursor','pointer');
        dropzone.off('dragenter').on('dragenter', this.dropZoneDragEnter.bind(this, dropzone, index));
        dropzone.off('dragover').on('dragover', this.dropZoneDragOver.bind(this, dropzone, index));
        dropzone.off('dragleave').on('dragleave', this.dropZoneDragLeave.bind(this, dropzone, index));
        dropzone.off('drop').on('drop', this.dropZoneDrop.bind(this, dropzone, index));

        this.clears[index].off('click').on('click', this.clearClick.bind(this,dropzone,index)).css({
            color: '#b76ba3'
        });

        this.files[index].off('change').on('change', this.fileChange.bind(this,dropzone,index));
    },
    clearClick : function(dropzone,index){
        var placeholder = dropzone.find('span.placeholder');
        var preview = dropzone.find('div.preview');

        placeholder.show();
        preview.detach();

        this.inputs[index].val('');
    },
    fileChange : function(dropzone,index,e){
        for(var i in e.currentTarget.files){
            var file = e.currentTarget.files[i];

            if(file instanceof File){
                this.handleFile(dropzone,index,file);
            }
        }
    },
    dropZoneDragEnter : function(dropzone,index,e){
        e.preventDefault()
        e.stopPropagation()
        dropzone.addClass('dropzone-active');
    },
    dropZoneDragOver : function(dropzone,index,e){
        e.preventDefault()
        e.stopPropagation()
        dropzone.addClass('dropzone-active');
    },
    dropZoneDragLeave : function(dropzone,index,e){
        e.preventDefault()
        e.stopPropagation()
        dropzone.removeClass('dropzone-active');
    },
    dropZoneDrop : function(dropzone,index,e){
        e.preventDefault()
        e.stopPropagation()
        dropzone.removeClass('dropzone-active');

        for(var i in e.originalEvent.dataTransfer.files){
            var file = e.originalEvent.dataTransfer.files[i];

            if(file instanceof File){
                this.handleFile(dropzone,index,file);
            }
        }
    },
    handleFile : function(dropzone,index,file){
        if(this.validate(dropzone,index,file)){
            this.preview(dropzone,index,file);
        }
        else if(this.inputs[index].val().length === 0){
            this.clearClick(dropzone,index);
        }
    },
    preview : function(dropzone,index,file){
        var reader = new FileReader();
        var renderFn = this.render.bind(this,reader,dropzone,index,file);
        reader.onload = renderFn;
        reader.readAsDataURL(file);
    },
    render : function(reader,dropzone,index,file){
        var dataURL = reader.result;
        var placeholder = dropzone.find('span.placeholder');
        placeholder.hide();

        var preview = dropzone.find('div.preview');

        if(preview.length === 0){
            preview = this.getPreview();
            dropzone.append(preview);
        }

        preview.find('img').attr('src', dataURL);
        //preview.find('div.filename').html(file.name);
        preview.find('div.fileinfo').html(this.humanBytes(file.size));
        preview.show();

        this.inputs[index].val(dataURL);
        toastr.clear();
    },
    humanBytes : function($bytes){
        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
        for ($i = 0; $bytes >= 1000; $i++) $bytes /= 1000;
        return $bytes.toFixed(2) + ' ' + $units[$i];
    },
    dataURLtoFile: function(dataurl, filename) {
        var arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), 
            n = bstr.length, 
            u8arr = new Uint8Array(n);
            
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        
        return new File([u8arr], filename, {type: mime});
    },
    getPreview : function(){
        var preview = $('<div class="preview"></div>');
        var filename = $('<div class="filename"></div>');
        var fileinfo = $('<div class="fileinfo"></div>');
        var image = $('<img class="img-fluid" />');

        preview.append(image);
        preview.append(filename);
        preview.append(fileinfo);
        return preview;
    },
    validate : function(dropzone,index,file){
        return this.validateMime(dropzone,index,file) && this.validateSize(dropzone,index,file);
    },
    validateMime : function(dropzone,index,file){
        var mimes = this.mimes[index];
        var type = file.type;
        var valid = true;

        if(mimes.indexOf(type) === -1){
            toastr.error(this.lang[index]['invalid.mime']);
            valid = false;
        }

        return valid;
    },
    validateSize : function(dropzone,index,file){
        var max = this.sizes[index];
        var size = file.size;
        var valid = size <= max;
        
        if(! valid){
            toastr.error(this.lang[index]['invalid.size']);
        }

        return valid;
    }
};

$(function(){
    Picture.initialize();
});