$(function(){
    var $_DEPS = {
        when : function(form,master,options){
            var value = this.value(master);

            if(typeof options[value] !== 'undefined'){
                var actions = options[value];

                for(var action in actions){
                    this[action](form,actions[action]);
                }
            }
        },
        value : function(master){
            switch(master.attr('type')){
                case 'checkbox':
                    return master.is(':checked') ? 1 : 0;
            }

            return master.val();
        },
        enable : function(form,slaves){
            for(var i in slaves){
                var slave = this.resolv(form,slaves[i]);

                if(slave === null){
                    continue;
                }

                slave.closest('div.form-group').show();
            }
        },
        disable : function(form,slaves){
            for(var i in slaves){
                var slave = this.resolv(form,slaves[i]);

                if(slave === null){
                    continue;
                }

                slave.closest('div.form-group').hide();
            }
        },
        handler : function(form,master,rules,e){
            for(var rule in rules){
                this[rule](form,master,rules[rule]);
            }
        },
        resolv : function(form,name){
            var candidates = [
                '[name="' + name + '"]',
                '[name="' + name + '[]"]',
                'table#' + name
            ];

            for(var i in candidates){
                var candidate = candidates[i];
                var slave = form.find(candidate);

                if(slave.length > 0){
                    return slave;
                }
            }

            return null;
        }
    };

    if(typeof $_FORMS !== 'undefined'){
        for(var name in $_FORMS){
            var form = $('#' + name);
            var deps = $_FORMS[name].deps || {};

            for(var field in deps){
                var master = form.find('[name="' + field + '"]:last');
                master.on('change', $_DEPS.handler.bind($_DEPS,form,master,deps[field])).trigger('change');
            }
        }
    }
});
