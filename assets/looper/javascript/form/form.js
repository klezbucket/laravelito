var Form = {
    initialize : function(){
        this.inputClear();
    },
    inputClear : function(){
        $('[data-toggle="input-clear"]').click(function(){
            var elem = $(this);
            var id = elem.data('input');
            var input = $('#' + id);

            if(input.length > 0){
                if(input[0]._flatpickr){
                    input[0]._flatpickr.clear();
                    input[0]._flatpickr.close();
                }

                input.val('');
            }
        })
        .css('cursor','pointer')
        .css('color','#b76ba3');
    },
    inputValue : function(input,value){
        var type = input.attr('type');

        switch(type){
            case 'checkbox':
                if(value){
                    input.attr('checked','checked');
                }
                else{
                    input.removeAttr('checked');
                }

                break;
            default:
                input.val(value);
        }
    },
    validation : function(input, message){
        var block = input.siblings('.invalid-feedback');
        
        if(block.length === 0){
            block = $('<div class="invalid-feedback"></div>');

            var i = $('<i class="fa fa-exclamation-circle fa-fw"></i>');
            block.append(i);
            input.after(block);
        }

        var span = $('<span></span>');
        span.html(message + '. ');
        block.append(span);

        return block;
    }
};

$(function(){
    Form.initialize();
});