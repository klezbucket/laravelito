var Excel = {
    cols : {
        data : [],
        set : function(cols){
            Excel.cols.data = cols;
        },
        flush : function(){
            Excel.cols.data = [];
        },
        push : function(col){
            Excel.cols.data.push(col);
        }
    },
    data : {
        data : [],
        set : function(data){
            Excel.data.data = data;
        },
        flush : function(){
            Excel.data.data = [];
        },
        push : function(row){
            Excel.data.data.push(row);
        }
    },
    flush : function(){
        this.data.flush();
        this.cols.flush();
    },
    csv : function(name = 'file'){
        var csv = "";

        for(var i in this.cols.data){
            var col = this.cols.data[i];
            csv += col + ',';
        }

        for(var i in this.data.data){
            var row = this.data.data[i];
            csv += "\r\n";

            for(var j in this.cols.data){
                var cell = row[j] || '';
                csv += cell + ',';
            }
        }

        csv = "data:application/csv," + encodeURIComponent(csv);
        var x = document.createElement("A");
        x.setAttribute("href", csv );
        x.setAttribute("download",name + ".csv");
        document.body.appendChild(x);
        x.click();
    },
    safe : function(string){
        if(typeof string === 'undefined'){
            return "";
        }

        if(string === null){
            return "";
        }

        var str = "" + string;
        return str.replace(/,/g,'').replace(/[\n\r]/g, '');
    }
};
