<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPlatformsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'user_platforms';

    /**
     * Run the migrations.
     * @table user_platforms
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('platform_id');

            $table->softDeletes();
            $table->index(["user_id"], 'user_platform_user_FKEY');

            $table->index(["platform_id"], 'user_platform_platform_FKEY');

            $table->unique(["platform_id", "user_id"], 'user_platform_UNIQUE');
            $table->nullableTimestamps();


            $table->foreign('user_id', 'user_platform_user_FKEY')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
