<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTokensTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'user_tokens';

    /**
     * Run the migrations.
     * @table user_tokens
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('platform_id');
            $table->string('token', 191);

            $table->softDeletes();
            $table->index(["platform_id"], 'user_tokens_platform_INDEX');

            $table->index(["user_id"], 'user_tokens_user_INDEX');

            $table->unique(["token"], 'user_tokens_UNIQUE');
            $table->nullableTimestamps();


            $table->foreign('user_id', 'user_tokens_user_INDEX')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('platform_id', 'user_tokens_platform_INDEX')
                ->references('id')->on('platforms')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
