<?php
namespace App;

use App\Model\Role;
use DatabaseSeeder;
use App\Model\Platform;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        $data = [
            [ 'id' => Role::BACKEND_ADMIN, 'code' => 'ADMIN', 'platform_id' => Platform::BACKEND, 'status' => 1 ],
        ];

        DatabaseSeeder::seed('App\\Model\\Role', $data);
    }
}
