<?php
namespace App;

use DatabaseSeeder;
use App\Model\Platform;
use Illuminate\Database\Seeder;

class PlatformsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        $data = [
            [ 'id' => Platform::BACKEND, 'code' => 'BACKEND', 'status' => 1 ],
        ];

        DatabaseSeeder::seed('App\\Model\\Platform', $data);
    }
}
