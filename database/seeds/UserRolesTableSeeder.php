<?php
namespace App;

use App\Model\Role;
use App\Model\User;
use DatabaseSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        $data = [
            [ 'id' => 1, 'user_id' => User::BACKEND_ADMIN, 'role_id' => Role::BACKEND_ADMIN,'status' => 1 ],
        ];

        DatabaseSeeder::seed('App\\Model\\UserRole', $data);
    }
}
