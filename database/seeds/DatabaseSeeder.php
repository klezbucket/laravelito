<?php

use App\RolesTableSeeder;
use App\UsersTableSeeder;
use App\PlatformsTableSeeder;
use App\UserRolesTableSeeder;
use Illuminate\Database\Seeder;
use App\UserPlatformsTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PlatformsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UserRolesTableSeeder::class);
        $this->call(UserPlatformsTableSeeder::class);
    }

    /**
     * Dado una clase Model y un array de data, realiza un upsert para el seeding.
     * @param string $model
     * @param array $data
     * @return void
     */

    public static function seed(string $model,array $data): void
    {
        foreach($data as $row){
            if(! ($obj = $model::find($row['id']))){
                $model::create($row);
            }
            else{
                unset($row['id']);
                $obj->update($row);
                $obj->touch();
            }
        }
    }
}
