<?php
namespace App;

use App\Model\User;
use DatabaseSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        $data = [
            [ 'id' => User::BACKEND_ADMIN, 'email' => 'felipeklez@gmail.com', 'first_name' => 'Felipe', 'last_name' => 'Martínez',
                'telephone' => '0985714373','password' => Hash::make('12345'),'lang' => 'es','timezone' => 'America/Asuncion',
                'status' => 1 ],
        ];

        DatabaseSeeder::seed('App\\Model\\User', $data);
    }
}
