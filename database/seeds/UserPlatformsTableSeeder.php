<?php
namespace App;

use App\Model\Platform;
use App\Model\User;
use DatabaseSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserPlatformsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        $data = [
            [ 'id' => 1, 'user_id' => User::BACKEND_ADMIN, 'platform_id' => Platform::BACKEND,'status' => 1 ],
        ];

        DatabaseSeeder::seed('App\\Model\\UserPlatform', $data);
    }
}
