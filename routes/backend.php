<?php

use App\Backend\Auth\Permits;

/**
 * Todas las rutas reportaran minimamente el Request y Response involucrados.
 */

Route::middleware('web.request','web.response')->group(function(){

    /*******************
     *
     * RUTAS DEL BACKEND
     *
     *******************/

    Route::namespace('\App\Backend\Controller')->group(function(){
        /**
         * Estas rutas requieren un Usuario autenticado.
         * Se controla que el Rol del Usuario sea el adecuado para el Request especifico.
         */

        Route::middleware('web.auth','url.guard','web.locale')->group(function(){
            /**
             * Backend Barebone
             */

            LaravelitoRouter::get('BackendController@home', 'backend.home');
            LaravelitoRouter::form('BackendController@me', 'backend.me');
            LaravelitoRouter::get('BackendController@logout', 'backend.logout');


            /**
             * Modulo de usuarios
             */

            LaravelitoRouter::form('UsersController@create', 'users.create', Permits::admin());
            LaravelitoRouter::get('UsersController@fetch', 'users.fetch', Permits::admin());
            LaravelitoRouter::form('UsersController@edit', 'users.edit', Permits::admin());
            LaravelitoRouter::form('UsersController@enabled', 'users.collections.enabled', Permits::admin());
            LaravelitoRouter::form('UsersController@disabled', 'users.collections.disabled', Permits::admin());
        });

        /**
         * Estas rutas requieren un Usuario anonimo
         */

        Route::middleware('web.anon','web.locale')->group(function(){
            LaravelitoRouter::form('BackendController@login', 'backend.login');
        });
    });
});
