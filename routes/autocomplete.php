<?php

use Illuminate\Http\Request;
use App\Backend\Auth\Permits;

/****************************
 *
 *  API INTERNA DEL BACKEND
 *
 ***************************/

Route::namespace('\App\Backend\Controller')->group(function(){
    Route::middleware('api.request','api.response')->group(function(){

        /**
         * Estas rutas requieren un Usuario autenticado via token.
         */

        Route::middleware('api.auth','web.locale','url.guard')->group(function(){

            /**
             * Autocomplete
             */
            LaravelitoRouter::post('AutocompleteController@users', 'users.autocomplete', Permits::admin());
            LaravelitoRouter::post('AutocompleteController@roles', 'roles.autocomplete', Permits::admin());
            LaravelitoRouter::post('AutocompleteController@platforms', 'platforms.autocomplete', Permits::admin());
        });
    });
});
