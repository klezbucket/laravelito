<?php

use App\Backend\Auth\Permits;
use App\Backend\CRUD\Users\Callback\EditUsersCallback;
use App\Backend\CRUD\Users\Callback\CreateUsersCallback;
use App\Backend\CRUD\Users\Callback\CollectionsEnabledUsersCallback;
use App\Backend\CRUD\Users\Callback\CollectionsDisabledUsersCallback;

return [
    'create' => [
        'callback' => CreateUsersCallback::class,
        'permits' => Permits::admin()
    ],
    'fetch' => [
        'permits' => Permits::admin()
    ],
    'edit' => [
        'callback' => EditUsersCallback::class,
        'permits' => Permits::admin()
    ],
    'collections' => [
        'enabled' => [
            'callback' => CollectionsEnabledUsersCallback::class,
            'permits' => Permits::admin()
        ],
        'disabled' => [
            'callback' => CollectionsDisabledUsersCallback::class,
            'permits' => Permits::admin()
        ],
    ],
];
