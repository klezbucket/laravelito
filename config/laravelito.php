<?php

use App\Backend\Menu\BackendMenu;
use App\Backend\Form\BackendForm;
use App\Backend\Detail\BackendDetail;
use App\Backend\Table\BackendTable;
use Laravelito\View\Page\BackendPage;
use Laravelito\Core\Site\SiteResolver;
use Laravelito\View\Looper\BackendContent;
use Laravelito\View\Breadcrumb\BackendBreadcrumb;

/**
 * Configuracion Stand Alone de Laravelito.
 */

return [
    
    /**
     * Se especifica el Resolvedor de Sitios, se puede especificar una implementacion
     * de SiteResolverInterface.
     */

    'sites' => [
        'resolver' => SiteResolver::class
    ],
    
    /**
     * Laravelito soporta diversos idiomas y timezones.
     * Locale ENV hace referencia a las locales utilizadas por PHP 
     * en funciones como strfitime. Esta configuracion es dependiente
     * del Sistema Operativo.
     * 
     * En Unix-Like ver comando: locale
     */

    'locale' => [
        'available' => [ 'en', 'es' ],
        'env' => [
            'en' => 'en_US.utf8',
            'es' => 'es_PY.utf8'
        ],
    ],

    /**
     * Setea el Timezone de la Base de Datos.
     * Laravelito soporta por default MySQL.
     * Sin embargo es posible reemplazar la query ejecutada aqui, 
     * el paramero %s seria el Timezone reemplazado por sprintf.
     */

    'db_time_zone' => "SET time_zone='%s'",


    /**
     * Autenticacion.
     * Puede existir mas de un site, con login diferente.
     * El Modelo de autenticacion. Debe implementar \Laravelito\Auth\Model\AuthModelInterface
     */

    'auth' => [
        'backend' => [
            'model' => 'App\\Model\\User',
            'redirect' => 'backend.home',
            'login' => 'backend.login'
        ]
    ],

    /**
     * Tema de Laravelito.
     * Puede haber mas de un site, con tema diferente.
     */

    'theme' => [
        'backend' => 'looper'
    ],

    /**
     * Composers para menu.
     */

    'menu' => [
        'backend' => BackendMenu::class
    ],

    /**
     *  Composer para el breadcrumb.
     **/

    'breadcrumb' => [
        'backend' => BackendBreadcrumb::class
    ],

    /**
     * Composer de las Paginas.
     */

    'pages' => [
        'backend' => BackendPage::class
    ],

    /**
     * Composer de los Formularios.
     */

    'forms' => [
        'backend' => BackendForm::class
    ],

    /**
     * Composer de los Detalles.
     */

    'details' => [
        'backend' => BackendDetail::class
    ],

    /**
     * Composer de las Tablas.
     */

    'tables' => [
        'backend' => BackendTable::class
    ],
];