<?php

use App\Backend\Auth\Permits;
use App\Backend\CRUD\Placeholders\Callback\EditPlaceholdersCallback;
use App\Backend\CRUD\Placeholders\Callback\FetchPlaceholdersCallback;
use App\Backend\CRUD\Placeholders\Callback\CreatePlaceholdersCallback;
use App\Backend\CRUD\Placeholders\Callback\CollectionsEnabledPlaceholdersCallback;
use App\Backend\CRUD\Placeholders\Callback\CollectionsDisabledPlaceholdersCallback;

return [
    'create' => [
        'callback' => CreatePlaceholdersCallback::class,
        'permits' => Permits::placeholderS()
    ],
    'fetch' => [
        'callback' => FetchPlaceholdersCallback::class,
        'permits' => Permits::placeholderS()
    ],
    'edit' => [
        'callback' => EditPlaceholdersCallback::class,
        'permits' => Permits::placeholderS()
    ],
    'collections' => [
        'enabled' => [
            'callback' => CollectionsEnabledPlaceholdersCallback::class,
            'permits' => Permits::placeholderS()
        ],
        'disabled' => [
            'callback' => CollectionsDisabledPlaceholdersCallback::class,
            'permits' => Permits::placeholderS()
        ],
    ],
];
