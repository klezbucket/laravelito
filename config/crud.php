<?php

use Laravelito\CRUD\Catalog\Request\MeRequest;
use Laravelito\CRUD\Catalog\Callback\MeCallback;

return [
    'me' => [
        'edit' => [
            'callback' => MeCallback::class,
            'request' => MeRequest::class,
            'model' => 'App\\Model\\User'
        ],
    ],
    'users' => require(__DIR__ . '/crud/users.php'),
];