<?php

namespace App\Backend\Auth;

class Permits {
    /**
     * Devuelve los permisos para solo ADMIN.
     * @return array
     */

    public static function admin(): array
    {
        return [
            'ADMIN'
        ];
    }
}
