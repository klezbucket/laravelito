<?php

namespace Laravelito\Auth\Services;

use Exception;
use Illuminate\Http\Request;
use Laravelito\Core\Facades\Site;
use Illuminate\Support\Facades\Config;
use Laravelito\Auth\Model\AuthModelInterface;
use Laravelito\Core\Exceptions\UnauthorizedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Laravelito\Core\Exceptions\InternalServerException;

class AuthService {
    /** @var AuthModelInterface el Usuario autenticado. */
    private static $authorized;

    /** @var string $model clase del modelo autenticado. */
    private $model;

    /** @var string nombre de la configuracion de autenticacion. */
    private $config;

    /** @var string nombre de la ruta de login. */
    private $login;

    /** @var string nombre de la ruta de redireccion al autenticar. */
    private $redirect;

    /**
     * Devuelve una nueva instancia del autenticador, debidamente configurada.
     *
     * @return void
     * @throws InternalServerException
     */

    public function __construct()
    {
        $site = Site::resolv(request());
        $this->setup($site);
    }

    /**
     * Reconfigura el servicio.
     *
     * @param string $config
     * @return void
     * @throws InternalServerException si no hay configuracion
     * @throws InternalServerException si no el modelo no implementa AuthModelInterface
     */

    public final function setup(string $config): void
    {
        if(! ($auth = Config::get("laravelito.auth.{$config}"))){
            throw new Exception('No auth configuration found: ' . $config);
        }

        $this->model = $auth['model'] ?? null;
        $this->redirect = route($auth['redirect'] ?? null);
        $this->login = route($auth['login'] ?? null);
        $this->config = $config;

        if(! (is_subclass_of($this->model, 'Laravelito\Auth\Model\AuthModelInterface'))){
            throw new Exception('Auth model does not implements: AuthModelInterface');
        }
    }

    /**
     * Realiza la verificacion de usuario autenticado, varia si es web session, o api token.
     *
     * @param Request $request
     * @return bool
     */

    public function isAuth(Request $request): bool
    {
        if(isset(self::$authorized)){
            return true;
        }

        if($request->isXmlHttpRequest() || !($request->hasSession())){
            return $this->tokenAuth($request);
        }
        else{
            return $this->webAuth($request);
        }
    }
    /**
     * Verifica que exista el Usuario autenticado en la Sesion del Request proveido.
     *
     * @param Request $request
     * @return bool
     */

    public function webAuth(Request $request): bool
    {
        $config = $this->config();
        $session = $request->session()->all();
        $id = $session['auth'][$config]['id'] ?? null;

        if($authorized = call_user_func($this->model . '::authorized', $id)){
            self::$authorized = $authorized;
            return true;
        }

        return false;
    }
    /**
     * Verifica que exista el token recibido pertenece a un Usuario valido.
     *
     * @param Request $request
     * @return bool
     */

    public function tokenAuth(Request $request): bool
    {
        $matches = [];
        $bearer = $request->header('X-Access-Token');
        preg_match('/^Bearer\s(.*)$/', $bearer, $matches);

        if($token = $matches[1] ?? null){
            if($authorized = call_user_func($this->model . '::authorizeByToken', $token)){
                self::$authorized = $authorized;
                return true;
            }
        }

        return false;
    }

    /**
     * Devuelve el Usuario autenticado.
     * Si no esta instanciado devuelve una excepcion.
     *
     * @return AuthModelInterface
     * @throws UnauthorizedException si $user no esta instanciado.
     */

    public static function getAuthorized(): AuthModelInterface
    {
        if(isset(self::$authorized)){
            return self::$authorized;
        }

        throw new UnauthorizedException();
    }

    /**
     * Devuelve el nombre de la configuracion de autenticacion.
     *
     * @return string
     */

    public function config(): string
    {
        return $this->config;
    }

    /**
     * Devuelve el la ruta de redireccion.
     *
     * @return RedirectResponse
     */

    public function redirect(): RedirectResponse
    {
        return redirect($this->redirect);
    }

    /**
     * Devuelve el la ruta de la interface de login.
     *
     * @return RedirectResponse
     */

    public function login(): RedirectResponse
    {
        return redirect($this->login);
    }

    /**
     * Intenta realizar el login.
     *
     * @param Request $request
     * @return bool
     */

    public function authenticate(Request $request): bool
    {
        if($authorized = call_user_func($this->model . '::login', $request)){
            if($request->hasSession()){
                $auth = $request->session()->get('auth', []);
                $config = $this->config();

                $auth[$config] = $authorized->toArray();
                $request->session()->put('auth', $auth);
                $request->session()->save();
            }

            self::$authorized = $authorized;
            return true;
        }

        return false;
    }

    /**
     * Olvida la sesion.
     *
     * @param Request $request
     * @return void
     */

    public function forget(Request $request): void
    {
        $auth = $request->session()->get('auth', []);
        $config = $this->config();
        unset($auth[$config]);

        $request->session()->put('auth', $auth);
        $request->session()->save();
    }
}
