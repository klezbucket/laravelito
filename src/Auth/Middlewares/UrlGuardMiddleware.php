<?php

namespace Laravelito\Auth\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Laravelito\Routing\UrlGuard;
use Laravelito\Auth\Facades\Auth;
use Laravelito\Core\Logs\Logging;
use Laravelito\Core\Facades\Theme;
use Laravelito\Core\Facades\Router;
use Laravelito\Core\Middlewares\Middleware;
use Laravelito\Core\Exceptions\ForbiddenException;

class UrlGuardMiddleware extends Middleware {
    
    /**
     * Ejecuta la logica del middleware
     */

    public function hook(Request $request, Closure $next){
        $route = Router::me();
        Logging::section('URL-GUARD@' . $route);
        $permits = UrlGuard::get($route);

        Logging::output('Permits: ' . implode(',', $permits));

        foreach($permits as $permit){
            if(! Auth::permit($permit)){
                Logging::output('Forbidden: ' . 'User do not have the ' . $permit . ' permit!');
                return Theme::exception(new ForbiddenException());
            }
        }

        return $next($request);
    }
}