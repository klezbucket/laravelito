<?php

namespace Laravelito\Auth\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Laravelito\Core\Logs\Logging;
use Laravelito\Auth\Services\AuthService;
use Laravelito\Core\Middlewares\Middleware;

class AnonMiddleware extends Middleware {

    /** @var AuthService el servicio que gestiona la Sesion. */
    private $Auth;

    /**
     * Devuelve una instancia del Middleware
     *
     * @param AuthService $auth
     * @return void
     */

    public function __construct(AuthService $auth)
    {
        $this->Auth = $auth;
    }

    /**
     * Verifica que no exista Usuario autenticado.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function hook(Request $request, Closure $next)
    {
        Logging::section('ANON@' . $this->Auth->config());

        if(! $this->Auth->isAuth($request)){
            Logging::output('No session detected for ' . $this->Auth->config());
        }
        else{
            Logging::output('Session detected for ' . $this->Auth->config());
            return $this->Auth->redirect();
        }

        return $next($request);
    }
}
