<?php

namespace Laravelito\Auth\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Laravelito\Core\Logs\Logging;
use Laravelito\Auth\Services\AuthService;
use Laravelito\Core\Middlewares\Middleware;

class AuthMiddleware extends Middleware {

    /** @var AuthService el servicio que gestiona la Sesion. */
    private $Auth;

    /**
     * Devuelve una instancia del Middleware
     *
     * @param AuthService $auth
     * @return void
     */

    public function __construct(AuthService $auth)
    {
        $this->Auth = $auth;
    }

    /**
     * Verifica que exista un Usuario debidamente autenticado.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */

    public function hook(Request $request, Closure $next)
    {
        Logging::section('AUTH@' . $this->Auth->config());

        if($this->Auth->isAuth($request)){
            Logging::output('Authorized: ' . AuthService::getAuthorized()->me());
        }
        else{
            Logging::output('Unauthorized for ' . $this->Auth->config());
            return $this->Auth->login();
        }

        return $next($request);
    }
}
