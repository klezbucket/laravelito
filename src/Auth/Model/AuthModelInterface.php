<?php

namespace Laravelito\Auth\Model;

use Illuminate\Http\Request;
use Laravelito\Locales\Locale;

interface AuthModelInterface {
    /**
     * Proporciona el token de autenticacion.
     *
     * @param string $token
     * @return string
     */

    function bearer(): string;

    /**
     * Dado el TOKEN recibido, intenta recuperar un usuario valido.
     *
     * @param string $token
     * @return AuthModelInterface|null
     */

    static function authorizeByToken(string $token): ?AuthModelInterface;

    /**
     * Dado el Request recibido, se encarga de autenticar un usuario valido.
     *
     * @param Request $request
     * @return AuthModelInterface|null
     */

    static function login(Request $request): ?AuthModelInterface;

    /**
     * Se encarga de resolver si el ID proviedo aun resuelve un usuario autorizado y validado.
     *
     * @param int|null $id
     * @return AuthModelInterface|null
     */

    static function authorized(?int $id): ?AuthModelInterface;

    /**
     * Se encarga de devolver el nombre o denominacion del usuario autorizado.
     *
     * @return string
     */

    function me(): string;

    /**
     * Se encarga de devolver las locales del usuario autorizado.
     *
     * @return Locale
     */

    function locale(): Locale;

    /**
     * Se encarga de determinar si el usuario posee el permiso requerido.
     *
     * @param string $permit
     * @return bool
     */

    function permit(string $permit): bool;
}