<?php

namespace Laravelito\Auth\Facades;

use Laravelito\Auth\Services\AuthService;
use Laravelito\Auth\Model\AuthModelInterface;

class Auth {

    /**
     * Obtiene el usuario Autorizado.
     * 
     * @param string|null $field
     * @return mixed
     */

    public static function user(string $field = null)
    {
        $user = AuthService::getAuthorized();

        if($field){
            return $user[$field];
        }

        return $user;
    }

    /**
     * Determina si el usuario autorizado disponde del permiso requerido.
     * 
     * @param string $permit
     * @return bool
     */

    public static function permit(string $permit): bool
    {
        $user = AuthService::getAuthorized();
        return $user->permit($permit);
    }

    /**
     * Refresca la data del usuario autenticado
     *
     * @param Request $request
     * @return void
     */

    public static function refresh(): void
    {
        AuthService::getAuthorized()->refresh();
    }
}