<?php

namespace App\Backend\CRUD\Users\Callback;

use App\Model\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Callback\Create\CreateCallback;
use App\Backend\CRUD\Users\Callback\UsersCallbackTrait;

class CreateUsersCallback extends CreateCallback
{
    use UsersCallbackTrait;

    /**
     * Callback antes de guardar
     *
     * @param Request $request
     * @param Model $model
     */

    public function beforeSave(Model $model,Request $request)
    {
        parent::beforeSave($model, $request);

        DB::beginTransaction();
        $this->password($model,$request);
    }

    /**
     * Se ejecuta despues de guardar los datos del request.
     *
     * @param Model $model
     * @param Request $request
     */

    public function afterSave(Model $model, Request $request)
    {
        $this->userRoles($model,$request);
        $this->userPlatforms($model,$request);
        DB::commit();
    }

    /**
     * Hashea el password el usuario.
     *
     * @param Request $request
     * @param Model $model
     * @return void
     */

    private function password(Model $model,Request $request): void
    {
        $password = $request->get('password');
        $password = Hash::make($password);

        $request->merge([
            'password' => $password,
        ]);
    }
}
