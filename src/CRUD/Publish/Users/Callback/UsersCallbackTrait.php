<?php
namespace App\Backend\CRUD\Users\Callback;

use App\Model\UserRole;
use App\Model\UserPlatform;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

trait UsersCallbackTrait {

    /**
     * Guarda los roles del usuario.
     *
     * @param Request $request
     * @param Model $model
     * @return void
     */

    private function userRoles(Model $model,Request $request): void
    {
        $roles = explode(',', $request->get('roles'));

        DB::table('user_roles')->where('user_id', $model->id)->delete();

        foreach($roles as $role){
            if(is_numeric($role) && $role > 0){
                UserRole::alloc($model, $role);
            }
        }
    }

    /**
     * Guarda las plataformas del usuario.
     *
     * @param Request $request
     * @param Model $model
     * @return void
     */

    private function userPlatforms(Model $model,Request $request): void
    {
        $platforms = explode(',', $request->get('platforms'));

        DB::table('user_platforms')->where('user_id', $model->id)->delete();

        foreach($platforms as $platform){
            if(is_numeric($platform) && $platform > 0){
                UserPlatform::alloc($model, $platform);
            }
        }
    }
}
