<?php
namespace Laravelito\CRUD\Collection;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Laravelito\CRUD\Service\Collection\CollectionData;

trait CanCollect {
    /** @var CollectionData */
    private $collection;

    /**
     * Obtiene la lista desde el modelo.
     * Inyecta la data del realaciones especificadas.
     * Sobrecarga este metodo para un CUSTOM LIST DATA ASSOC.
     *
     * @param Builder la query de esta lista
     * @param Model el modelo solicitado
     * @param Request $request
     * @param array $assocParams los nombres de cada assoc a agregar a la data, por defecto solo busca en 'assoc'
     * @return array la data del modelo
     */

    public function listWithAssoc(Builder $query,Model $model,Request $request, array $assocParams = []): array
    {
        $this->collection = new CollectionData();
        $query->limit = null;
        $query->offset = null;
        $total = $query->count();
        $this->collection->total($total);
        
        $limit = (int) $request->get('limit');
        $offset = (int) $request->get('offset');

        if($limit > 0){
            $query->take($limit);
            $query->skip($offset);
        }

        foreach($assocParams as $param){
            $name = $request->get($param);
            $query->with($name);
        }

        $collection = $query->get();
        $count = count($collection);
        $this->collection->collection($collection);

        if($count > 0){
            $this->collection->lower(1 + $offset);
            $this->collection->upper($offset + $count);
        }
        else{
            $this->collection->lower(0);
            $this->collection->upper(0);
        }

        if($limit === 0){
            $pages = 1;
            $current = 1;
        }
        else{
            $pages = ceil($total / $limit); 
            $current = 1 + ceil($offset / $limit);
        }

        $this->collection->pages($pages);
        $this->collection->current($current);
        return $this->collection->toArray();
    }

    /**
     * Devuelve la coleccion actual.
     * @return CollectionData
     */

    public function collection(): CollectionData
    {
        return $this->collection;
    }
}