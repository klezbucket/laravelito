<?php

namespace Laravelito\CRUD\Collection;

use Laravelito\Table\Table;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Laravelito\CRUD\Collection\CanCollect;

trait Collector {
    use CanCollect;

    /**
     * Collector llena la tabla especificada con los datos listos para usarse en la vista como si fuera por CRUDService.
     * @param Table $table
     * @param Request $request
     * @param Builder $builder
     * @param Model $model
     * @return void
     */
    public function collect(Table $table,Request $request,Builder $builder,Model $model): void
    {
        $table->model($model);
        $this->listWithAssoc($builder,$model,$request);
        $table->collection($this->collection());
    }
}