<?php

namespace Laravelito\CRUD\Command\Delete;

use Illuminate\Container\Container;
use Laravelito\CRUD\Command\BaseCommand;
use Laravelito\CRUD\Callback\Delete\DeleteCallback;
use Laravelito\CRUD\Service\Delete\DeleteService;

abstract class DeleteCommand extends BaseCommand
{
    public function __construct()
    {
        $this->signature = $this->name() . ':delete {--id=}';
        $this->description = $this->description();
        parent::__construct();
    }

    public function handle()
    {
        $data = [];
        $data['id'] = $this->option('id');

        $request = $this->request();
        $request->setContainer(new Container());
        $request->setMethod('POST');
        $request->request->add($data);

        $service = new DeleteService($request, $this->model(), $this->callback());
        $service->run();

        $json = $service->json();
        echo $json->content() . "\n";
    }

    abstract public function callback(): DeleteCallback;
}
