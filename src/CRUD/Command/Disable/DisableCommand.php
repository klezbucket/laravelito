<?php

namespace Laravelito\CRUD\Command\Disable;

use Illuminate\Container\Container;
use Laravelito\CRUD\Command\BaseCommand;
use Laravelito\CRUD\Callback\Disable\DisableCallback;
use Laravelito\CRUD\Service\Disable\DisableService;

abstract class DisableCommand extends BaseCommand
{
    public function __construct()
    {
        $this->signature = $this->name() . ':disable {--id=}';
        $this->description = $this->description();
        parent::__construct();
    }

    public function handle()
    {
        $data = [];
        $data['id'] = $this->option('id');

        $request = $this->request();
        $request->setContainer(new Container());
        $request->setMethod('POST');
        $request->request->add($data);

        $service = new DisableService($request, $this->model(), $this->callback());
        $service->run();

        $json = $service->json();
        echo $json->content() . "\n";
    }

    abstract public function callback(): DisableCallback;
}
