<?php

namespace Laravelito\CRUD\Command\Enable;

use Illuminate\Container\Container;
use Laravelito\CRUD\Command\BaseCommand;
use Laravelito\CRUD\Callback\Enable\EnableCallback;
use Laravelito\CRUD\Service\Enable\EnableService;

abstract class EnableCommand extends BaseCommand
{
    public function __construct()
    {
        $this->signature = $this->name() . ':enable {--id=}';
        $this->description = $this->description();
        parent::__construct();
    }

    public function handle()
    {
        $data = [];
        $data['id'] = $this->option('id');

        $request = $this->request();
        $request->setContainer(new Container());
        $request->setMethod('POST');
        $request->request->add($data);

        $service = new EnableService($request, $this->model(), $this->callback());
        $service->run();

        $json = $service->json();
        echo $json->content() . "\n";
    }

    abstract public function callback(): EnableCallback;
}
