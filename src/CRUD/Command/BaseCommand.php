<?php

namespace Laravelito\CRUD\Command;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Request\BaseRequest;


abstract class BaseCommand extends Command {
    /**
     * Dado el modelo, crea los options con cada uno de sus $fillables para el $signature de este comando.
     *
     * @return string las opciones para el $signature
     */

    public function fieldOptions(): string
    {
        $options = '';

        foreach($this->model()->fillable as $field){
            $options .= " {--$field=}";
        }

        return $options;
    }

    /**
     * Dado el modelo, recupera la data de los options definidios por cada uno de sus $fillables.
     *
     * @return array la data recuperada de los options
     */

    public function gatherOptions(): array
    {
        $data = [];

        foreach($this->model()->fillable as $field){
            $data[$field] = $this->option($field);
        }

        return $data;
    }

    public abstract function request(): BaseRequest;
    public abstract function model(): Model;
    public abstract function name(): string;
    public abstract function description(): string;
}
