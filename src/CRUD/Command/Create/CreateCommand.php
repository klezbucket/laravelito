<?php

namespace Laravelito\CRUD\Command\Create;

use Illuminate\Container\Container;
use Laravelito\CRUD\Command\BaseCommand;
use Laravelito\CRUD\Callback\Create\CreateCallback;
use Laravelito\CRUD\Service\Create\CreateService;

abstract class CreateCommand extends BaseCommand
{
    public function __construct()
    {
        $this->signature = $this->name() . ':create' . $this->fieldOptions();
        $this->description = $this->description();
        parent::__construct();
    }

    public function handle()
    {
        $data = $this->gatherOptions();
        $request = $this->request();
        $request->setContainer(new Container());
        $request->setMethod('POST');
        $request->request->add($data);

        $service = new CreateService($request, $this->model(), $this->callback());
        $service->run();

        $json = $service->json();
        echo $json->content() . "\n";
    }
    public abstract function callback(): CreateCallback;
}
