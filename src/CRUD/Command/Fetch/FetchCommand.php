<?php

namespace Laravelito\CRUD\Command\Fetch;

use Illuminate\Container\Container;
use Laravelito\CRUD\Command\BaseCommand;
use Laravelito\CRUD\Callback\Fetch\FetchCallback;
use Laravelito\CRUD\Service\Fetch\FetchService;

abstract class FetchCommand extends BaseCommand
{
    public function __construct()
    {
        $this->signature = $this->name() . ':fetch {--id=} {--assocs=}';
        $this->description = $this->description();
        parent::__construct();
    }

    public function handle()
    {
        $data = [];
        $data['id'] = $this->option('id');
        $data['assocs'] = [];

        $assocs = explode(',', $this->option('assocs'));

        foreach($assocs as $assoc){
            if(strlen($assoc) > 0){
                $data['assocs'][] = "assoc_{$assoc}";
                $data["assoc_{$assoc}"] = $assoc;
            }
        }

        $request = $this->request();
        $request->setContainer(new Container());
        $request->setMethod('POST');
        $request->request->add($data);

        $service = new FetchService($request, $this->model(), $this->callback());
        $service->run();

        $json = $service->json();
        echo $json->content() . "\n";
    }

    public abstract function callback(): FetchCallback;
}
