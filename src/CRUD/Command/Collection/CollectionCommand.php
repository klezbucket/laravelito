<?php

namespace Laravelito\CRUD\Command\Collection;

use Illuminate\Container\Container;
use Laravelito\CRUD\Command\BaseCommand;
use Laravelito\CRUD\Callback\Collection\CollectionCallback;
use Laravelito\CRUD\Service\Collection\CollectionService;

abstract class CollectionCommand extends BaseCommand
{
    public function __construct()
    {
        $this->signature = $this->name() . ':list {--offset=} {--limit=} {--assocs=} {--q=} {--sort=}';
        $this->description = $this->description();
        parent::__construct();
    }

    public function handle()
    {
        $data = [];
        $data['offset'] = $this->option('offset') ?? 0;
        $data['limit'] = $this->option('limit') ?? 10;
        $data['assocs'] = [];

        $assocs = explode(',', $this->option('assocs'));

        foreach($assocs as $assoc){
            if(strlen($assoc) > 0){
                $data['assocs'][] = "assoc_{$assoc}";
                $data["assoc_{$assoc}"] = $assoc;
            }
        }

        $request = $this->request();
        $request->setContainer(new Container());
        $request->setMethod('POST');
        $request->request->add($data);

        $service = new CollectionService($request, $this->model(), $this->callback());
        $service->run();

        $json = $service->json();
        echo $json->content() . "\n";
    }

    abstract public function callback(): CollectionCallback;
}
