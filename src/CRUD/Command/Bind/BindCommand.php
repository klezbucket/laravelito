<?php

namespace Laravelito\CRUD\Command\Bind;

use Illuminate\Container\Container;
use Laravelito\CRUD\Command\BaseCommand;
use Laravelito\CRUD\Callback\Bind\BindCallback;
use Laravelito\CRUD\Service\Bind\BindService;

abstract class BindCommand extends BaseCommand
{
    public function __construct()
    {
        $this->signature = $this->name() . ':bind {--id=} {--rel=} {--assoc=} {--class=}';
        $this->description = $this->description();
        parent::__construct();
    }

    public function handle()
    {
        $data = [];
        $data['id'] = $this->option('id');
        $data['rel'] = $this->option('rel');
        $data['assoc'] = $this->option('assoc');
        $data['class'] = $this->option('class');

        $request = $this->request();
        $request->setContainer(new Container());
        $request->setMethod('POST');
        $request->request->add($data);

        $service = new BindService($request, $this->model(), $this->callback());
        $service->run();

        $json = $service->json();
        echo $json->content() . "\n";
    }

    abstract public function callback(): BindCallback;
}
