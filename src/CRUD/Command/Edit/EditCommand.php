<?php

namespace Laravelito\CRUD\Command\Edit;

use Illuminate\Container\Container;
use Laravelito\CRUD\Command\BaseCommand;
use Laravelito\CRUD\Callback\Edit\EditCallback;
use Laravelito\CRUD\Service\Edit\EditService;

abstract class EditCommand extends BaseCommand
{
    public function __construct()
    {
        $this->signature = $this->name() . ':edit {--id=}' . $this->fieldOptions();
        $this->description = $this->description();
        parent::__construct();
    }

    public function handle()
    {
        $data = $this->gatherOptions();
        $data['id'] = $this->option('id');

        $request = $this->request();
        $request->setContainer(new Container());
        $request->setMethod('POST');
        $request->request->add($data);

        $service = new EditService($request, $this->model(), $this->callback());
        $service->run();

        $json = $service->json();
        echo $json->content() . "\n";
    }

    abstract public function callback(): EditCallback;
}
