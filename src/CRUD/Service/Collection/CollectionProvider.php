<?php

namespace Laravelito\CRUD\Service\Collection;

use Illuminate\Support\Collection;
use Laravelito\Core\Provider\Provider;

interface CollectionProvider extends Provider {
    /**
     * Obtiene una coleccion.
     *
     * @param CollectionData|null $collection
     * @return ?CollectionData
     */

    function collection(?CollectionData $collection = null): CollectionData;
}