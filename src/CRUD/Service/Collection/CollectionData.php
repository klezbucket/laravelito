<?php

namespace Laravelito\CRUD\Service\Collection;

use Illuminate\Support\Collection;

class CollectionData {
    /** @var Collection */
    private $collection;

    /** @var int */
    private $lower;

    /** @var int */
    private $upper;

    /** @var int */
    private $total;

    /** @var int */
    private $pages;

    /** @var int */
    private $current;

    /**
     * Get/Set para la coleccion
     * 
     * @param Collection|null $collection
     * @return Collection
     */

    public function collection(?Collection $collection = null): Collection
    {
        if(isset($collection)){
            $this->collection = $collection;
        }

        return $this->collection;
    }

    /**
     * Get/Set para lower
     * 
     * @param int|null $lower
     * @return int
     */

    public function lower(?int $lower = null): int
    {
        if(isset($lower)){
            $this->lower = $lower;
        }

        return $this->lower;
    }

    /**
     * Get/Set para upper
     * 
     * @param int|null $upper
     * @return int
     */

    public function upper(?int $upper = null): int
    {
        if(isset($upper)){
            $this->upper = $upper;
        }

        return $this->upper;
    }

    /**
     * Get/Set para total
     * 
     * @param int|null $total
     * @return int
     */

    public function total(?int $total = null): int
    {
        if(isset($total)){
            $this->total = $total;
        }

        return $this->total;
    }

    /**
     * Get/Set para pages
     * 
     * @param int|null $pages
     * @return int
     */

    public function pages(?int $pages = null): int
    {
        if(isset($pages)){
            $this->pages = $pages;
        }

        return $this->pages;
    }

    /**
     * Get/Set para current
     * 
     * @param int|null $current
     * @return int
     */

    public function current(?int $current = null): int
    {
        if(isset($current)){
            $this->current = $current;
        }

        return $this->current;
    }

    /**
     * Devuelve en un array la info de esta estructura.
     * @return array
     */

    public function toArray(): array
    {
        return [
            'data' => $this->collection->toArray(),
            'lower' => $this->lower,
            'upper' => $this->upper,
            'total' => $this->total,
            'pages' => $this->pages,
            'current' => $this->current
        ];
    }
}