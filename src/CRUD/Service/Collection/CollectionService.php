<?php

namespace Laravelito\CRUD\Service\Collection;

use Exception;
use Throwable;
use Illuminate\Http\Request;
use Laravelito\CRUD\Auth\CRUDAuth;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Service\BaseService;
use Illuminate\Database\Eloquent\Builder;
use Laravelito\CRUD\Collection\CanCollect;
use Laravelito\CRUD\Context\Collection\CollectionContext;
use Laravelito\CRUD\Callback\Collection\CollectionCallback;

class CollectionService extends BaseService
{
    use CanCollect;
    
    /**
     * Callbacks a ejecutar en cada punto del proceso LIST.
     * @var CollectionCallback
     */

    private $callback;

    /**
     * @var CollectionData
     */

    private $collection;

    /**
     * Instanciador del Servicio por Contexto.
     *
     * @param Context $context
     */

    public static function instantiate(CollectionContext $context)
    {
        return new CollectionService($context->request(), $context->model(), $context->callback());
    }

    /**
     * Constructor del Servicio
     *
     * @param Request $request el Request con la data cargada
     * @param Model $model el Modelo sobre el cual realizar la operacion
     * @param CollectionCallback $callback encapsulacion de los callbacks de esta operacion
     */

    public function __construct(Request $request, Model $model, CollectionCallback $callback)
    {
        parent::__construct($request, $model);

        $this->callback = $callback;
    }

    /**
     * Realiza la operacion LIST con validacion de datos del Request.
     *
     * @return bool el estado de la operacion
     */

    final public function run(bool $dry = false): bool
    {
        try{
            $this->request->validate();
            $query = $this->callback->buildQuery($this->baseQuery(), $this->request);
            // *** HOT STUFF *** //
            $this->listWithAssoc($query, $this->model, $this->request, $this->request->get('assocs') ?? []);
            // *** HOT STUFF *** //
            $collection = $this->callback->afterFind($this->collection()->collection(), $this->model, $this->request);
            $this->collection()->collection($collection);
            $this->data = $this->collection()->toArray();
            $this->success = true;
        }
        catch(Throwable $e){
            $this->errors = $this->request->errors();
            $this->exception = $e;
            $this->success = false;
        }

        return $this->success;
    }
}
