<?php

namespace Laravelito\CRUD\Service\Create;

use Exception;
use Throwable;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Service\BaseService;
use Laravelito\CRUD\Context\Create\CreateContext;
use Laravelito\CRUD\Callback\Create\CreateCallback;

class CreateService extends BaseService
{
    /**
     * Callbacks a ejecutar en cada punto del proceso CREATE.
     */

    private $callback;

    /**
     * Instanciador del Servicio por Contexto.
     *
     * @param Context $context
     */

    public static function instantiate(CreateContext $context)
    {
        return new CreateService($context->request(), $context->model(), $context->callback());
    }


    /**
     * Constructor del Servicio
     *
     * @param Request $request el Request con la data cargada
     * @param Model $model el Modelo sobre el cual realizar la operacion
     * @param CreateCallback $callback encapsulacion de los callbacks de esta operacion
     */

    public function __construct(Request $request, Model $model, CreateCallback $callback)
    {
        parent::__construct($request, $model);

        $this->callback = $callback;
    }

    /**
     * Realiza la operacion CREATE con validacion de datos del Request.
     *
     * @return bool el estado de la operacion
     */

    final public function run(bool $dry = false): bool
    {
        try{
            $this->callback->beforeValidate($this->model, $this->request);
            $this->request->validate();
            $this->callback->beforeSave($this->model, $this->request);
            
            // *** HOT STUFF *** //
            if($dry === false){
                $this->model = $this->create($this->model, $this->request->all());
            }
            // *** HOT STUFF *** //

            $this->callback->afterSave($this->model, $this->request);
            $this->data = $this->modelData($this->model, $this->request);
            $this->success = true;
        }
        catch(Throwable $e){
            $this->errors = $this->request->errors();
            $this->exception = $e;
            $this->success = false;
        }

        return $this->success;
    }

    /**
     * Ejecuta la accion CREATE.
     * Sobrecarga este metodo para un CUSTOM CREATE.
     *
     * @param Model $model el modelo sobre el cual crear el registro
     * @param array $data la data del modelo a ser almacenada
     * @return Model el nuevo registro
     */

    public function create(Model $model, array $data): Model
    {
        return $model->create($data);
    }
}
