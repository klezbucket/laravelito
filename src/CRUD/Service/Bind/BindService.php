<?php

namespace Laravelito\CRUD\Service\Bind;

use Exception;
use Throwable;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Callback\Bind\BindCallback;
use Laravelito\CRUD\Service\BaseService;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class BindService extends BaseService
{
    /**
     * Callbacks a ejecutar en cada punto del proceso BIND.
     */

    private $callback;

    /**
     * El modelo de la relacion
     *
     * @var Model
     */

    private $relationship;

    /**
     * Constructor del Servicio
     *
     * @param Request $request el Request con la data cargada
     * @param Model $model el Modelo sobre el cual realizar la operacion
     * @param BindCallback $callback encapsulacion de los callbacks de esta operacion
     */

    public function __construct(Request $request, Model $model, BindCallback $callback)
    {
        parent::__construct($request, $model);
        $this->callback = $callback;
    }

    /**
     * Realiza la operacion BIND con validacion de datos del Request.
     *
     * @param bool $dry
     * @return bool el estado de la operacion
     */

    final public function run(bool $dry = false): bool
    {
        try{
            $this->request->validate();
            $query = $this->callback->buildQuery($this->baseQuery(), $this->request);

            // *** HOT STUFF *** //
            $this->model = $this->fetch($query, $this->request);
            // *** HOT STUFF *** //

            $this->relationship = $this->model($this->request);
            $relQuery = $this->callback->buildRelQuery($this->relationship->query(), $this->request);

            // *** HOT STUFF *** //
            $this->relationship = $this->fetch($relQuery, $this->request, 'rel');
            // *** HOT STUFF *** //

            $this->callback->beforeAssoc($this->model, $this->relationship, $this->request);

            if($dry === false){
                $this->model = $this->bind($this->model, $this->relationship, $this->request);
            }

            $this->callback->afterAssoc($this->model, $this->relationship, $this->request);
            $this->data = $this->modelDataWithAssoc($this->model, $this->request);
            $this->success = true;
        }
        catch(Throwable $e){
            $this->errors = $this->request->errors()->messages();
            $this->exception = $e;
            $this->success = false;
        }

        return $this->success;
    }

    /**
     * Ejecuta la accion BIND.
     * Cada tipo de relacion tiene su propio metodo.
     *
     * @param Model $model el registro a asociar
     * @param Model $relationship la asociacion
     * @param Model $request
     * @return Model el registro asociado
     */

    private function bind(Model $model,Model $relationship,Request $request): Model
    {
        $assoc = $this->assoc($model,$request);

        // *** HOT STUFF *** //

        if($assoc instanceof BelongsToMany){
            return $this->belongsToMany($assoc,$model,$relationship,$request);
        }

        // *** HOT STUFF *** //

        throw new Exception('Relation not supported');
    }

    /**
     * Ejecuta la accion BIND hasManyThrough.
     * Sobrecarga este metodo para un CUSTOM BIND hasManyThrough.
     *
     * @param BelongsToMany $assoc la relacion
     * @param Model $model
     * @param Model $relationship
     * @param Model $request
     * @return Model el registro asociado
     */

    public function belongsToMany(BelongsToMany $assoc,Model $model,Model $relationship,Request $request): Model
    {
        $assoc->syncWithoutDetaching($relationship);
        return $model;
    }
}
