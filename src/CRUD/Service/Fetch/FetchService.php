<?php

namespace Laravelito\CRUD\Service\Fetch;

use Exception;
use Throwable;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Service\BaseService;
use Laravelito\CRUD\Context\Fetch\FetchContext;
use Laravelito\CRUD\Callback\Fetch\FetchCallback;

class FetchService extends BaseService
{
    /**
     * Callbacks a ejecutar en cada punto del proceso FETCH.
     */

    private $callback;

    /**
     * Instanciador del Servicio por Contexto.
     *
     * @param Context $context
     */

    public static function instantiate(FetchContext $context)
    {
        return new FetchService($context->request(), $context->model(), $context->callback());
    }

    /**
     * Constructor del Servicio
     *
     * @param Request $request el Request con la data cargada
     * @param Model $model el Modelo sobre el cual realizar la operacion
     * @param FetchCallback $callback encapsulacion de los callbacks de esta operacion
     */

    public function __construct(Request $request, Model $model, FetchCallback $callback)
    {
        parent::__construct($request, $model);

        $this->callback = $callback;
    }

    /**
     * Realiza la operacion FETCH con validacion de datos del Request.
     *
     * @return bool el estado de la operacion
     */

    final public function run(bool $dry = false): bool
    {
        try{
            $this->request->validate();
            $query = $this->callback->buildQuery($this->baseQuery(), $this->request);
            // *** HOT STUFF *** //
            $this->model = $this->fetch($query,$this->request);
            // *** HOT STUFF *** //

            $this->callback->afterFind($this->model, $this->request);
            $this->data = $this->modelDataWithAssoc($this->model, $this->request, $this->request->get('assocs') ?? []);
            $this->success = true;
        }
        catch(Throwable $e){
            $this->errors = $this->request->errors();
            $this->exception = $e;
            $this->success = false;
        }

        return $this->success;
    }
}
