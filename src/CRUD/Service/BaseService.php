<?php

namespace Laravelito\CRUD\Service;

use Exception;
use Throwable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravelito\CRUD\Callback;
use Illuminate\Support\Service;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\ValidationException;
use Laravelito\Core\Exceptions\NotFoundException;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class BaseService
{
    /**
     * Request a ser gestionado.
     */

    protected $request;

    /**
     * Modelo sobre el cual operar.
     */

    protected $model;

    /**
     * Errores de validacion.
     */

    protected $errors = [];

    /**
     * Excepcion capturada.
     */

    protected $exception;

    /**
     * Estado de la operacion.
     */

    protected $success = false;

    /**
     * Data de la operacion.
     */

    protected $data = [];

    /**
     * Request y Modelo requeridos.
     */

    public function __construct(Request $request, Model $model){
        $this->request = $request;
        $this->model = $model;
    }

    /**
     * Obtiene la data de la operacion.
     * KEY=NULL VAL=NULL - Obtiene todo el array
     * KEY!=NULL VAL=NULL - Obtiene solo el valor especificado por KEY
     * KEY!=NULL VAL!=NULL - Escribe en KEY el VAL y devuelve el array
     */

    public function data($key = null, $val = null)
    {
        if(isset($key)){
            if(isset($val)){
                $this->data[$key] = $val;
            }
            else{
                return $this->data[$key] ?? null;
            }
        }

        return $this->data;
    }

    /**
     * Obtiene los errores de validacion.
     * @return array
     */

    public function errors(): array
    {
        return $this->errors;
    }

    /**
     * Obtiene la excepcion capturada.
     * @return Throwable|null
     */

    public function exception(): ?Throwable
    {
        return $this->exception;
    }

    /**
     * Obtiene el estado de la operacion.
     * @return bool
     */

    public function success(): bool
    {
        return $this->success;
    }

    /**
     * La funcion que ejecuta la operacion.
     *
     * @param bool $dry
     * @return bool el resultado de la operacion
     */

    abstract public function run(bool $dry = false):  bool;

    /**
     * Gestiona una respuesta JSON
     *
     * @return JsonResponse
     */

    public function json(): JsonResponse
    {
        return response()->json($this->payload(), $this->status());
    }

    /**
     * Obtiene el payload de la respuesta de la operacion
     *
     * @return array el payload
     */

    public function payload(): array
    {
        $payload = [];
        $payload['success'] = $this->success();

        if($payload['success']) {
            $payload['data'] = $this->data();
        }
        else{
            $payload['errors'] = $this->errors();

            if(isset($this->exception) && env('APP_DEBUG', false)){
                $payload['exception'] = $this->exception()->getMessage();
            }
        }

        return $payload;
    }

    /**
     * Obtiene el status code de la operacion.
     * 200 - Success TRUE
     * 400 - Success FALSE
     * Analizar Excepcion!
     *
     * @return int
     */

    public function status(): int
    {
        if($this->success()) {
            return 200;
        }
        else if(isset($this->exception)) {
            return $this->statusByException($this->exception);
        }
        else {
            return 400;
        }
    }

    /**
     * Obtiene el status code segun el tipo de excepcion.
     *
     * @param Throwable la exception a ser analizada
     * @return int
     */

    public function statusByException(Throwable $e): int
    {
        if($e instanceof ModelNotFoundException){
            return 404;
        }
        else if($e instanceof ValidationException){
            return 400;
        }

        return 500;
    }

    /**
     * Ejecuta la accion FETCH.
    * Sobrecarga este metodo para un CUSTOM FETCH.
    * Arroja ModelNotFoundException si no se encuentra el $id especificado.
    *
    * @param Builder $query la query a ser ejecutada
    * @param Request $request el request de donde sacar el $id
    * @return Model el registro encontrado
    * @throws NotFoundException
    */

    public function fetch(Builder $query, Request $request): Model
    {
        if(empty($query->getQuery()->wheres)){
            $key = $this->model->getKeyName();
            $id = $request->get('id');
            $query->where($key,$id);
        }
        
        if($model = $query->first()) {
            return $model;
        }

        throw new NotFoundException();
    }

    /**
    * Obtiene la asociacion solicitada en el Request.
    * Sobrecarga este metodo para un CUSTOM ASSOC LOADER.
    *
    * @param Model $model
    * @param Request $request
    * @param string $param el nombre del parametro en Request, default 'assoc'
    * @return Relation la relacion solicitada
    * @throws Exception si la relacion no existe o no instancia de Relation
    */

    public function assoc(Model $model, Request $request, $param = 'assoc'): Relation
    {
        $name = $request->get($param);
        if(method_exists($model, $name)){
            $assoc = $model->{$name}();

            if($assoc instanceof Relation){
                return $assoc;
            }

            throw new Exception("Assoc is not an instance of Relation: {$name}");
        }

        throw new Exception("Assoc not found: {$name}");
    }

    /**
     * Obtiene el modelo de la relacion solicitada en el Request
     * Sobrecarga este metodo para un CUSTOM MODEL LOADER.
     *
     * @param Request|null $request
     * @param string $param el campo en el cual buscar la clase del Modelo, default 'class'
     * @return Model el modelo solicitado
     * @throws Exception si el modelo no existe o no es instancia de Model
     */

    public function model(?Request $request = null, $param = 'class'): Model
    {
        if(! isset($request)){
            return $this->model;
        }
        
        $class = $this->request->get($param);
        
        if(! class_exists($class)){
            throw new Exception("Class not found: {$class}@{$param}");
        }

        $model = new $class;

        if(! ($model instanceof Model)){
            throw new Exception('Relationship class is not an instance of Model');
        }

        return $model;
    }


    /**
     * Obtiene la data del modelo.
     * Sobrecarga este metodo para un CUSTOM MODEL DATA.
     * @param Model el modelo solicitado
     * @param Request $request
     * @return array la data del modelo
     */

    public function modelData(Model $model,Request $request): array
    {
        $data = $model->toArray();
        return $data;
    }

    /**
     * Obtiene la data del modelo.
     * Inyecta la data de las relaciones especficadas.
     * Sobrecarga este metodo para un CUSTOM MODEL DATA WITH ASSOC.
     *
     * @param Model el modelo solicitado
     * @param Request $request
     * @param array $assocParams los nombres de cada assoc a agregar a la data, por defecto solo busca en 'assoc'
     * @return array la data del modelo
     */

    public function modelDataWithAssoc(Model $model,Request $request, array $assocParams = [ 'assoc' ]): array
    {
        $data = $this->modelData($model,$request);

        foreach($assocParams as $param){
            $name = $request->get($param);
            $assoc = $this->assoc($model,$request,$param);
            $data[$name] = $assoc->get()->toArray();
        }

        return $data;
    }

    /**
     * Obtiene la query base del modelo.
     * @return Builder
     */

    public function baseQuery(): Builder
    {
        if(method_exists($this->model, 'isForceDeleting')){
            return $this->model->withTrashed();
        }
        return $this->model->query();
    }
}
