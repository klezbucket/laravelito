<?php

namespace Laravelito\CRUD\Service\Enable;

use Exception;
use Throwable;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Service\BaseService;
use Laravelito\CRUD\Callback\Enable\EnableCallback;

class EnableService extends BaseService
{
    /**
     * Callbacks a ejecutar en cada punto del proceso DISABLE.
     */

    private $callback;

    /**
     * Constructor del Servicio
     *
     * @param Request $request el Request con la data cargada
     * @param Model $model el Modelo sobre el cual realizar la operacion
     * @param EnableCallback $callback encapsulacion de los callbacks de esta operacion
     */

    public function __construct(Request $request, Model $model, EnableCallback $callback)
    {
        parent::__construct($request, $model);

        $this->callback = $callback;
    }

    /**
     * Realiza la operacion DISABLE con validacion de datos del Request.
     *
     * @return bool el estado de la operacion
     */

    final public function run(bool $dry = false): bool
    {
        try{
            $this->request->validate();
            $query = $this->callback->buildQuery($this->baseQuery(), $this->request);

            // *** HOT STUFF *** //
            $this->model = $this->fetch($this->request,$query);
            // *** HOT STUFF *** //

            $this->callback->beforeSave($this->model, $this->request);

            // *** HOT STUFF *** //
            if($dry === false){
                $this->model = $this->enable($this->model);
            }
            // *** HOT STUFF *** //

            $this->callback->afterSave($this->model, $this->request);
            $this->data = $this->modelData($this->model, $this->request);
            $this->success = true;
        }
        catch(Throwable $e){
            $this->errors = $this->request->errors()->messages();
            $this->exception = $e;
            $this->success = false;
        }

        return $this->success;
    }

    /**
     * Ejecuta la accion ENABLE.
     * Sobrecarga este metodo para un CUSTOM ENABLE.
     *
     * @param Model $model el registro al cual ejecutar ENABLE
     * @return Model el registro habilitado
     */

    public function enable(Model $model): Model
    {
        $model->deleted_at = null;
        $model->save();
        return $model;
    }
}
