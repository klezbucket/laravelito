<?php

namespace Laravelito\CRUD\Service\Delete;

use Exception;
use Throwable;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Service\BaseService;
use Laravelito\CRUD\Callback\Delete\DeleteCallback;

class DeleteService extends BaseService
{
    /**
     * Callbacks a ejecutar en cada punto del proceso DELETE.
     */

    private $callback;

    /**
     * Constructor del Servicio
     *
     * @param Request $request el Request con la data cargada
     * @param Model $model el Modelo sobre el cual realizar la operacion
     * @param DeleteCallback $callback encapsulacion de los callbacks de esta operacion
     */

    public function __construct(Request $request, Model $model, DeleteCallback $callback)
    {
        parent::__construct($request, $model);

        $this->callback = $callback;
    }

    /**
     * Realiza la operacion DELETE con validacion de datos del Request.
     *
     * @return bool el estado de la operacion
     */

    final public function run(bool $dry = false): bool
    {
        try{
            $this->request->validate();
            $query = $this->callback->buildQuery($this->baseQuery(), $this->request);

            // *** HOT STUFF *** //
            $this->model = $this->fetch($this->request,$query);
            // *** HOT STUFF *** //

            $this->callback->beforeDrop($this->model, $this->request);

            // *** HOT STUFF *** //
            if($dry === false){
                $this->model = $this->delete($this->model, $this->request);
            }
            // *** HOT STUFF *** //

            $this->callback->afterDrop($this->model, $this->request);
            $this->data = $this->modelData($this->model, $this->request);
            $this->success = true;
        }
        catch(Throwable $e){
            $this->errors = $this->request->errors()->messages();
            $this->exception = $e;
            $this->success = false;
        }

        return $this->success;
    }

    /**
     * Ejecuta la accion DELETE.
     * Sobrecarga este metodo para un CUSTOM DELETE.
     *
     * @param Model $model el registro al cual ejecutar DELETE
     * @return Model el registro eliminado
     */

    public function delete(Model $model): Model
    {
        $model->delete();
        return $model;
    }
}
