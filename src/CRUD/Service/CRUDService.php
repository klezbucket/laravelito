<?php

namespace Laravelito\CRUD\Service;

use Throwable;
use Laravelito\Form\Form;
use Laravelito\Field\Field;
use Laravelito\Table\Table;
use Illuminate\Http\Request;
use Laravelito\Detail\Detail;
use Laravelito\CRUD\Logs\Logging;
use Laravelito\Core\Logs\Sanitize;
use Laravelito\CRUD\Auth\CRUDAuth;
use Laravelito\Field\FieldProvider;
use Laravelito\CRUD\Context\Context;
use Laravelito\Field\FieldDependences;
use Illuminate\Database\Eloquent\Model;
use Laravelito\Core\Facades\Form as FF;
use Laravelito\CRUD\Model\ModelProvider;
use Laravelito\CRUD\Request\BaseRequest;
use Laravelito\CRUD\Service\BaseService;
use Laravelito\CRUD\Service\Edit\EditService;
use Illuminate\Validation\ValidationException;
use Laravelito\CRUD\Service\Fetch\FetchService;
use Laravelito\CRUD\Service\Create\CreateService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Laravelito\CRUD\Service\Collection\CollectionService;

/**
 * Se encarga de cargar el Contexto CRUD requerido y procesarlo a partir de un Request especificado.
 */

class CRUDService {
    /** @var bool */
    private $dry = false;

    /** @var BaseService */
    private $Service;
    
    /** @var Context */
    private $Context;

    /** @var int no pasa nada */
    const IGNORE = 0;

    /** @var int success */
    const SUCCESS = 1;

    /** @var int failure */
    const FAILURE = 2;

    /** @var int invalid */
    const INVALID = 3;

    /** @var int dry */
    const DRY = 4;

    /**
     * Setter/getter para flag dry run.
     * 
     * @param bool|null $dry
     * @return bool
     */

    public function dry(?bool $dry = null): bool
    {
        if(isset($dry)){
            $this->dry = $dry;
        }

        return $this->dry;
    }

    /**
     * Analiza el resultado del proceso de formulario.
     * 
     * @param string $crudForm
     * @return int
     */

    public function afterForm(string $crudForm): int
    {
        $exception = $this->Service->exception();
        $errors = $this->Context->request()->errors();
        FF::invalid($errors,$crudForm);

        if($exception instanceof ValidationException){
            Logging::output('Errors: ' . serialize($errors));
            Logging::output('Status: invalid');
            return self::INVALID;
        }
        else{       
            Logging::exception($exception);
            Logging::output('Status: failure');
            return self::FAILURE;    
        }
    }

    /**
     * Realiza un fetch.
     * 
     * @param Request $request
     * @param string $module
     * @return int
     */

    public function getFetch(ModelProvider $provider,Request $request,string $module): int
    {
        switch($this->fetch($request,$module)){
            case self::SUCCESS:
                $provider->model($this->model());
                return self::IGNORE;
        }

        return self::FAILURE;
    }

    /**
     * CRUD Editar.
     * Se encarga de procesar el CRUD especificado.
     *
     * @param Request $request
     * @param string $module el nombre del modulo.
     * @param bool $fetch
     * @return int el estado de la operacion
     */

    public function edit(Request $request, string $module, bool $fetch = true): int
    {
        Logging::separator('EDIT');
        $crudForm = $module . '.edit';
        Logging::output('Form: ' . $crudForm);
        $form = Form::instantiate($crudForm);
        $this->Context = Context::edit($form);
        $this->Service = EditService::instantiate($this->Context);
        
        if($request->isMethod('POST')){
            self::extractRouteParams($request, $this->Context->request());  
            BaseRequest::copyData($form, $request, $this->Context->request());
            Logging::output('Data: ' . serialize(Sanitize::body($this->Context->request()->all())));
            
            if($this->Service->run($this->dry)){
                Logging::output('Model: ' . serialize(Sanitize::body($this->Service->model()->toArray())));
                Logging::output('Status: success');
                return $this->dry ? self::DRY : self::SUCCESS;
            }
            else{
                return $this->afterForm($crudForm);
            }
        }
        else if($fetch){
            return $this->getFetch($form,$request,$module);
        }     
        
        Logging::output('Status: ignore');
        return self::IGNORE;
    }

    /**
     * CRUD Crear
     * Se encarga de procesar el CRUD especificado.
     *
     * @param Request $request
     * @param string $module el nombre del modulo.
     * @return int el estado de la operacion
     */

    public function create(Request $request, string $module): int
    {
        Logging::separator('CREATE');

        if($request->isMethod('POST')){
            $crudForm = $module . '.create';
            Logging::output('Form: ' . $crudForm);
            $form = Form::instantiate($crudForm);
            $this->Context = Context::create($form);
            $this->Service = CreateService::instantiate($this->Context);
            BaseRequest::copyData($form, $request, $this->Context->request());
            Logging::output('Data: ' . serialize(Sanitize::body($this->Context->request()->all())));

            if($this->Service->run($this->dry)){
                Logging::output('Model: ' . serialize(Sanitize::body($this->Service->model()->toArray())));
                Logging::output('Status: success');
                return $this->dry ? self::DRY : self::SUCCESS;
            }
            else{
                return $this->afterForm($crudForm);
            }
        }

        Logging::output('Status: ignore');
        return self::IGNORE;
    }

    /**
     * CRUD Fetch
     * Se encarga de procesar el CRUD especificado.
     *
     * @param Request $request
     * @param string $module el nombre del modulo.
     * @return int el estado de la operacion
     */

    public function fetch(Request $request, string $module): int
    {
        Logging::separator('FETCH');

        $crudDetail = $module . '.fetch';
        Logging::output('Detail: ' . $crudDetail);
        $detail = Detail::instantiate($crudDetail);
        $this->Context = Context::fetch($detail);
        $this->Service = FetchService::instantiate($this->Context);
        self::extractRouteParams($request, $this->Context->request());
        Logging::output('Data: ' . serialize(Sanitize::body($this->Context->request()->all())));

        if($this->Service->run($this->dry)){
            $detail->model($this->model());
            Logging::output('Model: ' . serialize(Sanitize::body($this->Service->model()->toArray())));
            Logging::output('Status: success');
            return self::SUCCESS;
        }
        else{                    
            Logging::exception($this->Service->exception());  
            Logging::output('Status: failure');
            return self::FAILURE;
        }
    }

    /**
     * CRUD Collection
     * Se encarga de procesar el CRUD especificado.
     *
     * @param Request $request
     * @param string $module el nombre del modulo.
     * @param string $flvaor sabor de la coleccion.
     * @return int el estado de la operacion
     */

    public function collection(Request $request, string $module, string $flavor): int
    {
        Logging::separator('COLLECTION');
        $crudTable = $module . '.' . $flavor;
        Logging::output('Table: ' . $crudTable);
        
        $table = Table::instantiate($crudTable);
        $this->Context = Context::collection($table);
        $this->Service = CollectionService::instantiate($this->Context);
        self::extractData($request, $this->Context->request());
        Logging::output('Data: ' . serialize(Sanitize::body($this->Context->request()->all())));

        if($this->Service->run($this->dry)){  
            $table->model($this->model());
            $table->collection($this->Service->collection());
            Logging::output('Status: success');
            return self::SUCCESS;
        }
        else{
            Logging::exception($this->Service->exception());  
            Logging::output('Status: failure');
            return self::FAILURE;
        }
    }

    /**
     * Rellena el $dst especificado con los parametros de la ruta de $src
     * 
     * @param Request $src
     * @param Request $dst
     */

    public static function extractRouteParams(Request $src, Request $dst): void
    {
        $params = $src->route()->parameters();
        $dst->merge($params);
    }

    /**
     * Rellena el $dst especificado con los parametros del de $src
     * 
     * @param Request $src
     * @param Request $dst
     */

    public static function extractData(Request $src, Request $dst): void
    {
        $params = $src->all();
        $dst->merge($params);
    }

    /**
     * Expone el modelo.
     * 
     * @return Model
     */

    public function model(): Model
    {
        return $this->Service->model();
    }

    /**
     * Expone el contexto.
     * 
     * @return Context
     */

    public function context(): Context
    {
        return $this->Context;
    }

    /**
     * Expone los errores de validacion.
     *
     * @return array
     */

    public function errors(): array
    {
        return $this->Service->errors();
    }

    /**
     * Expone la exception.
     *
     * @return Throwable
     */

    public function exception(): Throwable
    {
        return $this->Service->exception();
    }
    
    /**
     * Expone la respuesta del CRUD en formato JSON.
     *
     * @return JsonResponse
     */

    public function json(): JsonResponse
    {
        return $this->Service->json();
    }
    
    /**
     * Expone la respues del CRUD.
     *
     * @return array
     */

    public function payload(): array
    {
        return $this->Service->payload();
    }
}
