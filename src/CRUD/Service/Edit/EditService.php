<?php

namespace Laravelito\CRUD\Service\Edit;

use Exception;
use Throwable;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Service\BaseService;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravelito\CRUD\Context\Edit\EditContext;
use Laravelito\CRUD\Callback\Edit\EditCallback;

class EditService extends BaseService
{
    /**
     * Callbacks a ejecutar en cada punto del proceso EDIT.
     */

    private $callback;

    /**
     * Instanciador del Servicio por Contexto.
     *
     * @param Context $context
     */

    public static function instantiate(EditContext $context)
    {
        return new EditService($context->request(), $context->model(), $context->callback());
    }

    /**
     * Constructor del Servicio
     *
     * @param Request $request el Request con la data cargada
     * @param Model $model el Modelo sobre el cual realizar la operacion
     * @param EditCallback $callback encapsulacion de los callbacks de esta operacion
     */

    public function __construct(Request $request, Model $model, EditCallback $callback)
    {
        parent::__construct($request, $model);

        $this->callback = $callback;
    }

    /**
     * Realiza la operacion EDIT con validacion de datos del Request.
     *
     * @return bool el estado de la operacion
     */

    final public function run(bool $dry = false): bool
    {
        try{

            // *** HOT STUFF *** //
            $query = $this->callback->buildQuery($this->baseQuery(), $this->request);
            $this->model = $this->fetch($query,$this->request);
            $deletedAt = $this->model->deleted_at;

            // *** HOT STUFF *** //

            $this->callback->beforeValidate($this->model, $this->request);
            $this->request->validate();
            $this->callback->beforeSave($this->model, $this->request);

            // *** HOT STUFF *** //
            if($dry === false){
                $this->model = $this->edit($this->model, $this->request->all());
            }
            // *** HOT STUFF *** //

            $this->restore($deletedAt);
            $this->callback->afterSave($this->model, $this->request);
            $this->data = $this->modelData($this->model, $this->request);
            $this->success = true;
        }
        catch(Throwable $e){
            $this->errors = $this->request->errors();
            $this->exception = $e;
            $this->success = false;
        }

        return $this->success;
    }

    /**
     * Restore de un modelo, si es softdeletes
     * @param string|null $deletedAt
     * @return void
     */

    private function restore(?string $deletedAt): void
    {
        if(method_exists($this->model, 'restore')){
            $restore = ($this->request->get('deleted_at') ?? $deletedAt) ? false : true;
            
            if($restore) {          
                $this->model->restore();
            } else {
                if(is_null($deletedAt)) {
                    $this->model->delete();
                }
            }
        }
    }

    /**
     * Ejecuta la accion EDIT.
     * Sobrecarga este metodo para un CUSTOM EDIT.
     *
     * @param Model $model el registro a editar
     * @param array $data la nueva data del modelo a ser almacenada
     * @return Model el registro editado
     */

    public function edit(Model $model, array $data): Model
    {
        $model->update($data);
        return $model;
    }
}
