<?php

namespace Laravelito\CRUD\Context;

use Laravelito\Core\Provider\Provider;

interface ContextProvider extends Provider {
    /**
     * Obtiene el nombre del contexto CRUD.
     *
     * @return string
     */

    function context(): string;
}
