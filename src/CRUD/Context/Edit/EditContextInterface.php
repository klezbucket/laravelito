<?php

namespace Laravelito\CRUD\Context\Edit;

use Laravelito\CRUD\Callback\Edit\EditCallback;

interface EditContextInterface {
    function callback(): EditCallback;
}
