<?php
namespace Laravelito\CRUD\Context\Edit;

use Laravelito\CRUD\Context\Context;
use Laravelito\CRUD\Context\Edit\EditContextInterface;

abstract class EditContext extends Context implements EditContextInterface{

}
