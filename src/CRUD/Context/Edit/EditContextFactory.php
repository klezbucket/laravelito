<?php

namespace Laravelito\CRUD\Context\Edit;

use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Context\Context;
use Laravelito\CRUD\Request\BaseRequest;
use Laravelito\CRUD\Context\Edit\EditContext;
use Laravelito\CRUD\Callback\Edit\EditCallback;

class EditContextFactory extends EditContext implements EditContextInterface {
    /** @var EditCallback */
    private $callback;

    /** @var BaseRequest */
    private $request;

    /** @var Model */
    private $model;

    /** @var array */
    private $permits;

    /**
     * Facade para instanciar un nuevo EditContext.
     *
     * @param EditCallback $callback
     * @param BaseRequest $request
     * @param Model $model
     * @param array $permits
     * @return EditFactoryContext
     */

    public static function with(EditCallback $callback, BaseRequest $request, Model $model, array $permits): EditContextFactory
    {
        return new self($callback,$request,$model,$permits);
    }

    /**
     * Constructor de la Fabrica.
     *
     * @param EditCallback $callback
     * @param BaseRequest $request
     * @param Model $model
     * @param array $permits
     */

    public function __construct(EditCallback $callback, BaseRequest $request, Model $model, array $permits)
    {
        $this->callback = $callback;
        $this->request = $request;
        $this->model = $model;
        $this->permits = $permits;
    }

    public function callback(): EditCallback
    {
        return $this->callback;
    }

    public function request(): BaseRequest
    {
        return $this->request;
    }

    public function model(): Model
    {
        return $this->model;
    }

    public function permits(): array
    {
        return $this->permits;
    }
}
