<?php

namespace Laravelito\CRUD\Context\Create;

use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Context\Context;
use Laravelito\CRUD\Request\BaseRequest;
use Laravelito\CRUD\Context\Create\CreateContext;
use Laravelito\CRUD\Callback\Create\CreateCallback;

class CreateContextFactory extends CreateContext implements CreateContextInterface {
    /** @var CreateCallback */
    private $callback;

    /** @var BaseRequest */
    private $request;

    /** @var Model */
    private $model;

    /** @var array */
    private $permits;

    /**
     * Facade para instanciar un nuevo CreateContext.
     *
     * @param CreateCallback $callback
     * @param BaseRequest $request
     * @param Model $model
     * @param array $permits
     * @return CreateFactoryContext
     */

    public static function with(CreateCallback $callback, BaseRequest $request, Model $model, array $permits): CreateContextFactory
    {
        return new self($callback,$request,$model,$permits);
    }

    /**
     * Constructor de la Fabrica.
     *
     * @param CreateCallback $callback
     * @param BaseRequest $request
     * @param Model $model
     * @param array $permits
     */

    public function __construct(CreateCallback $callback, BaseRequest $request, Model $model, array $permits)
    {
        $this->callback = $callback;
        $this->request = $request;
        $this->model = $model;
        $this->permits = $permits;
    }

    public function callback(): CreateCallback
    {
        return $this->callback;
    }

    public function request(): BaseRequest
    {
        return $this->request;
    }

    public function model(): Model
    {
        return $this->model;
    }

    public function permits(): array
    {
        return $this->permits;
    }
}
