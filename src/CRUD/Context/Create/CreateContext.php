<?php
namespace Laravelito\CRUD\Context\Create;

use Laravelito\CRUD\Context\Context;
use Laravelito\CRUD\Context\Create\CreateContextInterface;

abstract class CreateContext extends Context implements CreateContextInterface{

}
