<?php

namespace Laravelito\CRUD\Context\Create;

use Laravelito\CRUD\Callback\Create\CreateCallback;

interface CreateContextInterface {
    function callback(): CreateCallback;
}
