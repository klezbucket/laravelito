<?php
namespace Laravelito\CRUD\Context\Fetch;

use Laravelito\CRUD\Context\Context;
use Laravelito\CRUD\Context\Fetch\FetchContextInterface;

abstract class FetchContext extends Context implements FetchContextInterface{

}
