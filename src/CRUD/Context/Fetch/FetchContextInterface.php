<?php

namespace Laravelito\CRUD\Context\Fetch;

use Laravelito\CRUD\Callback\Fetch\FetchCallback;

interface FetchContextInterface {
    function callback(): FetchCallback;
}
