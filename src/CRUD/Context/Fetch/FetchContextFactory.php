<?php

namespace Laravelito\CRUD\Context\Fetch;

use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Context\Context;
use Laravelito\CRUD\Request\BaseRequest;
use Laravelito\CRUD\Context\Fetch\FetchContext;
use Laravelito\CRUD\Callback\Fetch\FetchCallback;

class FetchContextFactory extends FetchContext implements FetchContextInterface {
    /** @var FetchCallback */
    private $callback;

    /** @var BaseRequest */
    private $request;

    /** @var Model */
    private $model;

    /** @var array */
    private $permits;

    /**
     * Facade para instanciar un nuevo FetchContext.
     *
     * @param FetchCallback $callback
     * @param BaseRequest $request
     * @param Model $model
     * @param array $permits
     * @return FetchFactoryContext
     */

    public static function with(FetchCallback $callback, BaseRequest $request, Model $model, array $permits): FetchContextFactory
    {
        return new self($callback,$request,$model,$permits);
    }

    /**
     * Constructor de la Fabrica.
     *
     * @param FetchCallback $callback
     * @param BaseRequest $request
     * @param Model $model
     * @param array $permits
     */

    public function __construct(FetchCallback $callback, BaseRequest $request, Model $model, array $permits)
    {
        $this->callback = $callback;
        $this->request = $request;
        $this->model = $model;
        $this->permits = $permits;
    }

    public function callback(): FetchCallback
    {
        return $this->callback;
    }

    public function request(): BaseRequest
    {
        return $this->request;
    }

    public function model(): Model
    {
        return $this->model;
    }

    public function permits(): array
    {
        return $this->permits;
    }
}
