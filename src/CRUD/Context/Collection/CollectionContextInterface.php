<?php

namespace Laravelito\CRUD\Context\Collection;

use Laravelito\CRUD\Callback\Collection\CollectionCallback;

interface CollectionContextInterface {
    function callback(): CollectionCallback;
}
