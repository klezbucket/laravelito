<?php

namespace Laravelito\CRUD\Context\Collection;

use Laravelito\CRUD\Context\Context;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Request\BaseRequest;
use Laravelito\CRUD\Request\BackendRequest;
use Laravelito\CRUD\Context\Collection\CollectionContext;
use Laravelito\CRUD\Callback\Collection\CollectionCallback;

class CollectionContextFactory extends CollectionContext implements CollectionContextInterface {
    /** @var CollectionCallback */
    private $callback;

    /** @var BaseRequest */
    private $request;

    /** @var Model */
    private $model;

    /** @var array */
    private $permits;

    /**
     * Facade para instanciar un nuevo CollectionContext.
     *
     * @param CollectionCallback $callback
     * @param BaseRequest $request
     * @param Model $model
     * @param array $permits
     * @return CollectionFactoryContext
     */

    public static function with(CollectionCallback $callback, BaseRequest $request, Model $model, array $permits): CollectionContextFactory
    {
        return new self($callback,$request,$model,$permits);
    }

    /**
     * Constructor de la Fabrica.
     *
     * @param CollectionCallback $callback
     * @param BaseRequest $request
     * @param Model $model
     * @param array $permits
     */

    public function __construct(CollectionCallback $callback, BaseRequest $request, Model $model, array $permits)
    {
        $this->callback = $callback;
        $this->request = $request;
        $this->model = $model;
        $this->permits = $permits;
    }

    public function callback(): CollectionCallback
    {
        return $this->callback;
    }

    public function request(): BaseRequest
    {
        return $this->request;
    }

    public function model(): Model
    {
        return $this->model;
    }

    public function permits(): array
    {
        return $this->permits;
    }
}
