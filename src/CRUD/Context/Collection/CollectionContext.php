<?php
namespace Laravelito\CRUD\Context\Collection;

use Laravelito\CRUD\Context\Context;
use Laravelito\CRUD\Context\Collection\CollectionContextInterface;

abstract class CollectionContext extends Context implements CollectionContextInterface{

}
