<?php

namespace Laravelito\CRUD\Context;

use Throwable;
use Laravelito\Form\Form;
use Illuminate\Support\Str;
use Laravelito\Table\Table;
use Illuminate\Http\Request;
use Laravelito\Detail\Detail;
use Laravelito\CRUD\Logs\Logging;
use Laravelito\Core\Logs\Sanitize;
use Laravelito\CRUD\Auth\CRUDAuth;
use Laravelito\Field\FieldProvider;
use Illuminate\Support\Facades\Config;
use Laravelito\CRUD\Callback\Callback;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Request\BaseRequest;
use Laravelito\CRUD\Request\BackendRequest;
use Laravelito\CRUD\Context\ContextProvider;
use Laravelito\CRUD\Context\Edit\EditContext;
use Laravelito\CRUD\Context\Fetch\FetchContext;
use Laravelito\Core\Exceptions\NotFoundException;
use Laravelito\CRUD\Context\Create\CreateContext;
use Laravelito\CRUD\Context\Collection\CollectionContext;
use Symfony\Component\CssSelector\Exception\InternalErrorException;

abstract class Context {
    abstract public function request(): BaseRequest;
    abstract public function model(): Model;
    abstract public function permits(): array;

    /**
     * Devuelve una instancia de CollectionContext.
     * 
     * @param Table $table
     * @throws NotFoundException si no se puede cargar algun componente.
     * @return CollectionContext
     */

    public static function collection(Table $table): CollectionContext
    {
        $context = self::withTable($table,'collection');
        CRUDAuth::ensure($context);
        return $context;
    }
    /**
     * Devuelve una instancia de EditContext.
     * 
     * @param Form $form
     * @throws NotFoundException si no se puede cargar algun componente.
     * @return CreateContext
     */

    public static function edit(Form $form): EditContext
    {
        $context = self::withForm($form,'edit');
        CRUDAuth::ensure($context);
        return $context;
    }

    /**
     * Devuelve una instancia de CreateContext.
     * 
     * @param Form $form
     * @throws NotFoundException si no se puede cargar algun componente.
     * @return CreateContext
     */

    public static function create(Form $form): CreateContext
    {
        $context = self::withForm($form,'create');
        CRUDAuth::ensure($context);
        return $context;
    }

    /**
     * Devuelve una instancia de FetchContext.
     * 
     * @param Detail $detail
     * @throws NotFoundException si no se puede cargar algun componente.
     * @return FetchContext
     */

    public static function fetch(Detail $detail): FetchContext
    {
        $context = self::withDetail($detail,'fetch');
        CRUDAuth::ensure($context);
        return $context;
    }

    /**
     * Transforma el nombre de un action en su clase.
     *
     * @param string $action
     * @return string
     */

    private static function action(string $action): string
    {
        return ucfirst($action);
    }

    /**
     * Trata de cargar el Contexto dese la configuracion por $name.
     * 
     * @param Table $detail
     * @param string $action
     * @return Context context
     * @throws NotFoundException si la configuracion no existae
     */

    public static function withTable(Table $table, string $action): Context
    {
        return self::instantiate($table,$table,$action);
    }

    /**
     * Trata de cargar el Contexto dese la configuracion por $name.
     * 
     * @param Detail $detail
     * @param string $action
     * @return Context context
     * @throws NotFoundException si la configuracion no existae
     */

    public static function withDetail(Detail $detail, string $action): Context
    {
        return self::instantiate($detail,$detail,$action);
    }

    /**
     * Trata de cargar el Contexto dese la configuracion por $name.
     * 
     * @param Form $form
     * @param string $action
     * @return Context context
     * @throws NotFoundException si la configuracion no existe
     */

    public static function withForm(Form $form, string $action): Context
    {
        return self::instantiate($form,$form,$action);
    }

    /**
     * Trata de cargar el Contexto dese la configuracion por $name.
     *
     * En el archivo config/crud.php debe haber un array assoc de $name con la siguiente
     * estructura:
     *
     *  - callback: clase del callback
     *  - request: clase del request
     *  - model: clase del modelo
     *  - permits: array con los permisos requeridos por el servicio
     *
     * Si no se llegase a especificar alguna clase, se deduce del request de la siguiente manera:
     *
     * - callback: segun la especificacion del @method getClass()
     * - request: segun la especificacion del @method getClass()
     * - model: se singulariza el module como nombre de clase y se usa el namespace App\Model
     * - permits: default es vacio, por ende no requiere permisos si no se especifica
     *
     * @param ContextProvider $cp
     * @param ContextProvider $fp
     * @param string $action
     * @return Context context
     * @throws NotFoundException si la configuracion no existe
     */

    public static function instantiate(ContextProvider $cp, FieldProvider $fp, string $action): Context
    {
        $name = $cp->context();
        $key = "crud.{$name}";
        $config = Config::get($key, null);

        if(isset($config)){
            $callback = self::getCallback($cp->context(),$action,$config);
            $myRequest = self::getRequest($fp,$cp->context(),$action,$config);
            $model = self::getModel($cp->context(),$action,$config);
            $permits = self::getPermits($action,$config);
            $a = self::action($action);

            $factory = 'Laravelito\\CRUD\\Context\\' .$a . '\\' . $a . 'ContextFactory';
            return $factory::with($callback,$myRequest,$model,$permits);
        }
        else{
            throw new NotFoundException("CRUD Config not found: $key");
        }
    }

    /**
     * Carga la instancia del Contexto requerido.
     *
     * Se basa en construir el namespace de la clase del contexto a partir del $name y $action proveido.
     *
     * @param string $name
     * @param string $action
     * @return Context $context
     * @throws NotFoundException si la clase no existe
     * @throws InternalErrorException si la clase no extiende a Context
     */

    public static function loadFromClass(string $name, string $action): Context
    {
        $class = self::getClass('Context',$name);

        if(! class_exists($class)) {
            throw new NotFoundException("CRUD Context not found: $class");
        }

        $context = new $class();
        $a = self::action($action);
        $parent = 'Laravelito\\\CRUD\\Context\\' .$a . '\\' . $a . 'Context';

        if(! is_a($context,$parent,true)){
            throw new InternalErrorException("$class is not an instance of $parent");
        }

        return $context;
    }

    /**
     * Obtiene el context desde el Request.
     *
     * @param Request $request
     * @return string el nombre del contexto
     */

    public static function getContext(Request $request): string
    {
        return ucfirst(Str::camel($request->context));
    }

    /**
     * Obtiene el callback de este servicio basado en la configuracion proveida.
     *
     * @param string $context
     * @param string $action
     * @param array $config
     * @return Callback
     * @throws NotFoundException si la clase no existe
     * @throws InternalErrorException si la clase no extiende al Callback adecuado, segun su action.
     */

    public static function getCallback(string $context,string $action, array $config): Callback
    {
        $name = $context;

        if(isset($config['callback'])){
            $class = $config['callback'];
        }
        else{
            $class = self::getClass('Callback',$action);
        }

        if(! class_exists($class)){
            throw new NotFoundException("CRUD Callback not found: $class");
        }

        $callback = new $class();
        $a = self::action($action);
        $parent = 'Laravelito\\CRUD\\Callback\\' . $a . '\\' . $a . 'Callback';

        if(! is_a($callback,$parent,true)){
            throw new InternalErrorException("$class is not an instance of {$parent}");
        }

        return $callback;
    }

    /**
     * Obtiene el request de este servicio basado en la configuracion proveida.
     *
     * @param FieldProvider $provider
     * @param string $context
     * @param string $action
     * @param array $config
     * @return BaseRequest
     * @throws NotFoundException si la clase no existe
     * @throws InternalErrorException si la clase no extiende a BaseRequest adecuado, dependiendo de action
     */

    public static function getRequest(FieldProvider $provider,string $context,string $action, array $config): BaseRequest
    {
        $name = $context;

        if(isset($config['request'])){
            $class = $config['request'];
        }
        else{
            $class = self::getClass('Request',$action);
        }

        if(! class_exists($class)){
            throw new NotFoundException("CRUD Request not found: $class");
        }
        
        $myRequest = new $class($provider);

        if(! ($myRequest instanceof BaseRequest)){
            throw new InternalErrorException("$class is not an instance of BaseRequest");
        }

        return $myRequest;
    }

    /**
     * Obtiene el model de este servicio basado en la configuracion proveida.
     *
     * @param string $context
     * @param string $action
     * @param array $config
     * @return Model
     * @throws NotFoundException si la clase no existe
     * @throws InternalErrorException si la clase no extiende a Model
     */

    public static function getModel(string $context,string $action, array $config): Model
    {
        if(isset($config['model'])){
            $class = $config['model'];
        }
        else{
            $name = explode('.',$context);
            $model = ucfirst(Str::camel(Str::singular(($name[0] ?? $name))));
            $class = 'App\\Model\\' . $model;
        }

        if(! class_exists($class)){
            throw new InternalErrorException("Class $class not found");
        }

        $model = new $class();

        if(! ($model instanceof Model)){
            throw new InternalErrorException("Class $class is not an instance of Model");
        }

        return $model;
    }

    /**
     * Obtiene los permisos de la configuracion proveida.
     *
     * @param string $action
     * @return array
     */

    public static function getPermits(string $action,array $config): array
    {
        $permits = [];

        if(isset($config['permits'])){
            if(is_array($config['permits'])){
                $permits = $config['permits'];
            }
        }

        return $permits;
    }

    /**
     * Obtiene el nombre de la clase default namespaceada.
     *
     * @param string $kind tipo de clase, ej: Context
     * @param string $action
     * @return string la clase namespaceada
     */

    public static function getClass(string $kind,string $action): string
    {
        $a = self::action($action);

        $class =
            'Laravelito\\CRUD\\' . $kind . '\\' . $a . '\\' . $a . $kind;

        return $class;
    }

    /**
     * Determina si el nombre esta calificado de la forma modelo.action
     * @param string $name
     * @return array
     */

    private static function induction($name): array
    {
        return explode('.', $name);
    }
}
