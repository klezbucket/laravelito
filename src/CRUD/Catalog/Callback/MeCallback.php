<?php

namespace Laravelito\CRUD\Catalog\Callback;

use Illuminate\Http\Request;
use Laravelito\Auth\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Laravelito\CRUD\Callback\Edit\EditCallback;

class MeCallback extends EditCallback {
    /**
     * Inyector de la query.
     *
     * @param Builder $query
     * @param Request $request
     */


    public function buildQuery(Builder $query,Request $request): Builder
    {
        $query->where('id', Auth::user('id'));
        return $query;
    }

    /**
     * Hashea el password el usuario.
     *
     * @param Request $request
     * @param Model $model
     */

    public function beforeSave(Model $model,Request $request)
    {
        parent::beforeSave($model, $request);
        $password = $request->get('password');


        if(strlen($password) > 0){
            $password = Hash::make($password);
        }
        else{
            $password = $model->password;
        }

        $request->merge([
            'password' => $password
        ]);
    }

    /**
     * Se ejecuta despues de guardar los datos del request.
     *
     * @param Model $model
     * @param Request $request
     */

    public function afterSave(Model $model, Request $request)
    {
        parent::afterSave($model, $request);
        Auth::refresh();
    }
}