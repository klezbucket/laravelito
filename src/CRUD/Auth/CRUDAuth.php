<?php

namespace Laravelito\CRUD\Auth;

use Laravelito\Auth\Facades\Auth;
use Laravelito\CRUD\Context\Context;
use Illuminate\Validation\UnauthorizedException;
use Laravelito\Core\Exceptions\ForbiddenException;

class CRUDAuth {

    /**
     * Verifica permisos para acceder al modulo de CRUD.
     *
     * La verificacion se hace en el Request del context.
     *
     * @param Context $context
     * @return void
     * @throws ForbiddenException si no se posee permisos
     * @throws UnauthorizedException si no esta autorizado
     */

    public static function ensure(Context $context): void
    {
        self::permits($context);
        self::authorize($context);
    }

    /**
     * Verifica que el usuario autorizado disponga de los permisos para ejecutar el Contexto.
     *
     * @param Context $context
     * @throws ForbiddenException si la autorizacion falla.
     */

    public static function permits(Context $context)
    {
        foreach($context->permits() as $permit){
            if(! Auth::permit($permit)){
                throw new ForbiddenException();
            }
        }
    }

    /**
     * Ejecuta la autorizacion del Context Request.
     *
     * @param Request $request
     * @throws UnauthorizedException si la autorizacion falla.
     */

    public static function authorize(Context $context)
    {
        if(! $context->request()->authorize()){
            $class = get_class($context);
            throw new UnauthorizedException();
        }
    }
}
