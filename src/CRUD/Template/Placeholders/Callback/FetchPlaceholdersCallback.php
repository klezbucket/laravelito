<?php

namespace App\Backend\CRUD\Placeholders\Callback;

use Laravelito\CRUD\Callback\Fetch\FetchCallback;
use App\Backend\CRUD\Placeholders\Callback\PlaceholdersCallbackTrait;

class FetchPlaceholdersCallback extends FetchCallback
{
    use PlaceholdersCallbackTrait;
}
