<?php
namespace App\Backend\CRUD\Placeholders\Callback;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

trait PlaceholdersCallbackTrait {
    /**
     * Se ejecuta antes de validar los datos.
     *
     * @param Model $model
     * @param Request $request
     */


    public function beforeValidate(Model $model, Request $request)
    {

    }

    /**
     * Callback antes de guardar
     *
     * @param Request $request
     * @param Model $model
     */

    public function beforeSave(Model $model,Request $request)
    {
        parent::beforeSave($model, $request);

        DB::beginTransaction();
    }

    /**
     * Se ejecuta despues de guardar los datos del request.
     *
     * @param Model $model
     * @param Request $request
     */

    public function afterSave(Model $model, Request $request)
    {
        DB::commit();
    }
    
    /**
     * Inyector de la query.
     *
     * @param Builder $query
     * @param Request $request
     */

    public function buildQuery(Builder $query,Request $request): Builder
    {
        return $query;
    }
}
