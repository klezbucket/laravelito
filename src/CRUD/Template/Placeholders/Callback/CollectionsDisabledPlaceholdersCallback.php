<?php
namespace App\Backend\CRUD\Placeholders\Callback;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Laravelito\CRUD\Callback\Collection\CollectionCallback;

class CollectionsDisabledPlaceholdersCallback extends CollectionCallback {
    /**
     * Inyector de la query.
     *
     * @param Builder $query
     * @param Request $request
     */


    public function buildQuery(Builder $query,Request $request): Builder
    {
        return $query->whereNotNull('deleted_at')->orderBy('id','desc');
    }
}
