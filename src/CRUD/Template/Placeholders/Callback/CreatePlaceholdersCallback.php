<?php

namespace App\Backend\CRUD\Placeholders\Callback;

use Laravelito\CRUD\Callback\Create\CreateCallback;
use App\Backend\CRUD\Placeholders\Callback\PlaceholdersCallbackTrait;

class CreatePlaceholdersCallback extends CreateCallback
{
    use PlaceholdersCallbackTrait;
}
