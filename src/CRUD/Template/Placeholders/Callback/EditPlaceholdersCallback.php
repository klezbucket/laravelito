<?php

namespace App\Backend\CRUD\Placeholders\Callback;

use Laravelito\CRUD\Callback\Edit\EditCallback;
use App\Backend\CRUD\Placeholders\Callback\PlaceholdersCallbackTrait;

class EditPlaceholdersCallback extends EditCallback
{
    use PlaceholdersCallbackTrait;
}
