<?php

namespace Laravelito\CRUD\Facades;

use Illuminate\View\View;
use Laravelito\Core\Facades\Theme;

class CRUD {
    /**
     * Devuelve la vista CREATE
     *
     * @param string $layout el nombre del layout a extender
     * @param string $name el nombre del formulario
     * @return View
     */

    public static function create(string $layout,string $name): View
    {
        $view = Theme::controller('crud.create',$layout);
        $view->with('form', $name . '.create');

        return $view;
    }

    /**
     * Devuelve la vista FETCH
     *
     * @param string $layout el nombre del layout a extender
     * @param string $name el nombre del detail
     * @return View
     */

    public static function fetch(string $layout,string $name): View
    {
        $view = Theme::controller('crud.fetch',$layout);
        $view->with('detail', $name . '.fetch');

        return $view;
    }

    /**
     * Devuelve la vista EDIT
     *
     * @param string $layout el nombre del layout a extender
     * @param string $name el nombre del formulario
     * @return View
     */

    public static function edit(string $layout,string $name): View
    {
        $view = Theme::controller('crud.edit',$layout);
        $view->with('form', $name . '.edit');

        return $view;
    }

    /**
     * Devuelve la vista COLLECTION
     *
     * @param string $layout el nombre del layout a extender
     * @param string $name el nombre de la tabla
     * @param string $flavor la variante de la tabla
     * @return View
     */

    public static function collection(string $layout,string $name,string $flavor): View
    {
        $view = Theme::controller('crud.collection',$layout);
        $view->with('table', $name . '.' . $flavor);

        return $view;
    }

    /**
     * Devuelve la vista PICKER
     *
     * @param string $layout el nombre del layout a extender
     * @param string $name el nombre de la tabla
     * @param string $flavor la variante de la tabla
     * @return View
     */

    public static function picker(string $layout,string $name,string $flavor): View
    {
        $view = Theme::controller('crud.picker',$layout);
        $view->with('table', $name . '.' . $flavor);

        return $view;
    }
}
