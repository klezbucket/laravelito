<?php

namespace Laravelito\CRUD\Request;

use Laravelito\Form\Form;
use Laravelito\Field\Field;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Laravelito\Field\FieldProvider;
use Illuminate\Validation\Validator;
use Laravelito\Field\FieldDependences;
use Laravelito\CRUD\Request\BaseRequest;
use Laravelito\Form\Validation\Validation;
use Illuminate\Foundation\Http\FormRequest;

abstract class BaseRequest extends FormRequest {
    /** @var Validator */
    protected $validator;
    
    /** @var ?FieldProvider */
    private $provider;

    /**
     * Esta clase require un FieldProvider.
     * 
     * @param FieldProvider|null $provider
     * @return void
     */

    public function __construct(?FieldProvider $provider = null)
    {
        parent::__construct();
        $this->provider = $provider;
    }

    /**
     * Devuelve el field provider relacionado al Request.
     * 
     * @return FieldProvider|null
     */

    public function provider(): ?FieldProvider
    {
        return $this->provider;
    }

    /** 
     * Debe devolver las reglas laravel de validacion
     * @return array
     */

    public function rules(): array
    {
        if($provider = $this->provider()){
            return Validation::generate($provider);
        }

        return [];
    }

    /**
     * Determina si el Request es valido.
     *
     * @return mixed
     */

    public final function validate()
    {
        return $this->validator()->validate();
    }

    /**
     * Devuelve el validador
     * @return Validator
     */

    private function validator(): Validator {
        if(is_null($this->validator)){
            $this->validator = validator($this->all(), $this->rules(), $this->messages());
        }

        return $this->validator;
    }

    /**
     * Devuelve los errores de validacion.
     * 
     * @return array
     */

    public final function errors(): array
    {
        return $this->validator()->errors()->messages();
    }

    /**
     * Devuelve errores custom de validacion.
     */

    public function messages()
    {
        return [

        ];
    }

    /**
     * Devuelve la autorizacion del request.
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Rellena el $dst especificado con la info de $src
     * @param FieldProvider $provider
     * @param Request $src
     * @param Request $dst
     */

    public static function copyData(FieldProvider $provider,Request $src, Request $dst): void
    {
        $data = $src->all() ?? [];
        $merge = [];

        $shutdown = FieldDependences::shutdown($provider);

        foreach($provider->fields() as $field){
            $name = $field->name();
            
            if(! in_array($name, $shutdown)){
                $merge[$name] = self::copyValue($data,$field);
            }
        }

        $dst->merge($merge);
    }

    /**
     * Copia el campo desde el array especificado.
     * 
     * @param array $data
     * @param Field $field
     * @return mixed
     */

    public static function copyValue(array $data,Field $field)
    {
        $type = $field->type()->name();
        $name = $field->name();

        switch($type){
            case 'boolean':
                return isset($data[$name]) ? '1' : '0';
            case 'rows':
                return $data[$field->type()->form()->name()] ?? [];
        }

        if(is_array($data[$name] ?? null)){
            return implode(',', $data[$name]);
        }

        return $data[$name] ?? null;
    }
}
