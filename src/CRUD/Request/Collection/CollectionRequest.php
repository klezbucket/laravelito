<?php

namespace Laravelito\CRUD\Request\Collection;

use Laravelito\CRUD\Request\BackendRequest;

class CollectionRequest extends BackendRequest {
    protected $validator;

    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'limit' => 'required|numeric',
            'offset' => 'required|numeric',
        ];
    }
}
