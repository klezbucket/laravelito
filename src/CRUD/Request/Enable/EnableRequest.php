<?php

namespace Laravelito\CRUD\Request\Enable;

use Laravelito\CRUD\Request\BackendRequest;

class EnableRequest extends BackendRequest {
    protected $validator;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'ID es requerido!',
        ];
    }

    public function validate(){
        $this->validator = validator($this->all(), $this->rules(), $this->messages());
        return $this->validator->validate();
    }

    public function errors(){
        return $this->validator->errors();
    }
}
