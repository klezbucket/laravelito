<?php

namespace Laravelito\CRUD\Request;

use Laravelito\Form\Form;
use Illuminate\Support\MessageBag;
use Laravelito\Field\FieldProvider;
use Illuminate\Validation\Validator;
use Laravelito\CRUD\Request\BaseRequest;
use Laravelito\Form\Validation\Validation;
use Illuminate\Foundation\Http\FormRequest;

abstract class BackendRequest extends BaseRequest {

}
