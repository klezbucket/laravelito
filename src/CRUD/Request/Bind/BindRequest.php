<?php

namespace Laravelito\CRUD\Request\Bind;

use Laravelito\CRUD\Request\BackendRequest;

class BindRequest extends BackendRequest {
    protected $validator;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required',
            'rel' => 'required',
            'assoc' => 'required',
            'class' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'ID es requerido!',
            'rel.required' => 'REL es requerido!',
            'assoc.required' => 'ASSOC es requerido!',
            'class.required' => 'CLASS es requerido!',
        ];
    }

    public function validate(){
        $this->validator = validator($this->all(), $this->rules(), $this->messages());
        return $this->validator->validate();
    }

    public function errors(){
        return $this->validator->errors();
    }
}
