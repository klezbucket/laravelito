<?php

namespace Laravelito\CRUD\Request\Fetch;

use Laravelito\CRUD\Request\BackendRequest;

class FetchRequest extends BackendRequest {
    public function rules(): array
    {
        return [
            'id' => 'required',
        ];
    }
}
