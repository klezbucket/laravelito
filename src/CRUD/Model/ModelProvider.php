<?php

namespace Laravelito\CRUD\Model;

use Laravelito\Field\Field;
use Laravelito\Core\Provider\Provider;
use Illuminate\Database\Eloquent\Model;

interface ModelProvider extends Provider {
    /**
     * Setter/Getter para el modelo.
     *
     * @param Model|null $model
     * @return ?Model
     */
    
    public function model(?Model $model = null): ?Model;

    /**
     * Devuelve el valor de un campo.
     *
     * @param Field $field
     * @return string|null
     */

    public function value(Field $field): ?string;
}