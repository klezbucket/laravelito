<?php

namespace Laravelito\CRUD\Callback\Create;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Laravelito\CRUD\Callback\Callback;

class CreateCallback extends Callback {
    /**
     * Se ejecuta antes de validar los datos.
     *
     * @param Model $model
     * @param Request $request
     */


    public function beforeValidate(Model $model, Request $request)
    {

    }
    
    /**
     * Se ejecuta antes de guardar los datos del request.
     *
     * @param Model $model
     * @param Request $request
     */


    public function beforeSave(Model $model, Request $request)
    {

    }

    /**
     * Se ejecuta despues de guardar los datos del request.
     *
     * @param Model $model
     * @param Request $request
     */

    public function afterSave(Model $model, Request $request)
    {

    }
}
