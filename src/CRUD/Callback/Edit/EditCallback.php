<?php

namespace Laravelito\CRUD\Callback\Edit;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Laravelito\CRUD\Callback\Callback;

class EditCallback extends Callback {
    /**
     * Se ejecuta antes de validar los datos.
     *
     * @param Model $model
     * @param Request $request
     */


    public function beforeValidate(Model $model, Request $request)
    {

    }
    
    /**
     * Inyector de la query.
     *
     * @param Builder $query
     * @param Request $request
     */


    public function buildQuery(Builder $query,Request $request): Builder
    {
        return $query;
    }

    /**
     * Se ejecuta antes de guardar los datos del request.
     *
     * @param Model $model
     * @param Request $request
     */


    public function beforeSave(Model $model, Request $request)
    {

    }

    /**
     * Se ejecuta despues de guardar los datos del request.
     *
     * @param Model $model
     * @param Request $request
     */

    public function afterSave(Model $model, Request $request)
    {

    }
}
