<?php

namespace Laravelito\CRUD\Callback\Disable;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Laravelito\CRUD\Callback\Callback;

class DisableCallback extends Callback {
    /**
     * Injector de la query.
     *
     * @param Builder $query
     * @param Request $request
     */

    public function buildQuery(Builder $query,Request $request): Builder
    {
        return $query;
    }

    /**
     * Antes de deshabilitar.
     *
     * @param Model $model
     * @param Request $request
     */

    public function beforeSave(Model $model,Request $request)
    {

    }

    /**
     * Despues de deshabilitar.
     *
     * @param Model $model
     * @param Request $request
     */

    public function afterSave(Model $model,Request $request)
    {

    }
}
