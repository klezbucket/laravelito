<?php

namespace Laravelito\CRUD\Callback\Enable;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Laravelito\CRUD\Callback\Callback;

class EnableCallback extends Callback {
    /**
     * Inyctor de la query.
     *
     * @param Builder $query
     * @param Request $request
     */


    public function buildQuery(Builder $query,Request $request): Builder
    {
        return $query;
    }

    /**
     * Antes de habilitar.
     *
     * @param Model $model
     * @param Request $request
     */

    public function beforeSave(Model $model,Request $request)
    {

    }

    /**
     * Despues de habilitar.
     *
     * @param Model $model
     * @param Request $request
     */

    public function afterSave(Model $model,Request $request)
    {

    }
}
