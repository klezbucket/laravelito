<?php

namespace Laravelito\CRUD\Callback\Fetch;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Laravelito\CRUD\Callback\Callback;


class FetchCallback extends Callback {
    /**
     * Inyector de la query.
     *
     * @param Builder $query
     * @param Request $request
     */


    public function buildQuery(Builder $query,Request $request): Builder
    {
        return $query;
    }

    /**
     * Despues de encontrar el registro.
     *
     * @param Model $model
     * @param Request $request
     */

    public function afterFind(Model $model,Request $request)
    {

    }
}
