<?php

namespace Laravelito\CRUD\Callback\Unbind;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Laravelito\CRUD\Callback\Callback;

class UnbindCallback extends Callback {
    /**
     * Inyector de la query.
     *
     * @param Builder $query
     * @param Request $request
     */


    public function buildQuery(Builder $query,Request $request): Builder
    {
        return $query;
    }

    /**
     * Inyector de la query del objeto a relacionar.
     *
     * @param Builder $query
     * @param Request $request
     */


    public function buildRelQuery(Builder $relQuery,Request $request): Builder
    {
        return $relQuery;
    }

    /**
     * Se ejecuta antes de eliminar la asociacion.
     *
     * @param Model $model
     * @param Model relationship
     * @param Request $request
     */


    public function beforeDissac(Model $model,Model $relationship,Request $request)
    {

    }

    /**
     * Se ejecuta despues de eliminar la asociacion.
     *
     * @param Model $model
     * @param Model relationship
     * @param Request $request
     */

    public function afterDissac(Model $model,Model $relationship,Request $request)
    {

    }
}
