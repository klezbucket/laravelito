<?php

namespace Laravelito\CRUD\Callback\Delete;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Laravelito\CRUD\Callback\Callback;

class DeleteCallback extends Callback {
    /**
     * Inyector de la query.
     *
     * @param Builder $query
     * @param Request $request
     */

    public function buildQuery(Builder $query,Request $request): Builder
    {
        return $query;
    }

    /**
     * Antes de eliminar.
     *
     * @param Model $model
     * @param Request $request
     */

    public function beforeDrop(Model $model,Request $request)
    {

    }

    /**
     * Despues de eliminar.
     *
     * @param Model $model
     * @param Request $request
     */

    public function afterDrop(Model $model,Request $request)
    {

    }
}
