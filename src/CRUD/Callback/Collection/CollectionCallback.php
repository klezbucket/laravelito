<?php

namespace Laravelito\CRUD\Callback\Collection;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Laravelito\CRUD\Callback\Callback;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class CollectionCallback extends Callback {
    /**
     * Inyector de la query.
     *
     * @param Builder $query
     * @param Request $request
     */


    public function buildQuery(Builder $query,Request $request): Builder
    {
        return $query;
    }

    /**
     * Despues de encontrar la coleccion.
     *
     * @param Collection $data la coleccion obtenida
     * @param Model $model
     * @param Request $request
     * @return Collection
     */

    public function afterFind(Collection $data,Model $model,Request $request): Collection
    {
        return $data;
    }
}
