<?php

namespace Laravelito\CRUD\Controller;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Backend\Menu\BackendMenu;
use Laravelito\Auth\Facades\Auth;
use Laravelito\Core\Facades\Form;
use Laravelito\Core\Facades\Site;
use Laravelito\CRUD\Facades\CRUD;
use Illuminate\Routing\Controller;
use Laravelito\Core\Facades\Flash;
use Laravelito\Core\Facades\Table;
use Laravelito\Core\Facades\Theme;
use Laravelito\Core\Facades\Router;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Service\CRUDService;
use Laravelito\Auth\Services\AuthService;
use Laravelito\View\Breadcrumb\Breadcrumb;
use Symfony\Component\HttpFoundation\Response;
use Laravelito\Download\Service\DownloadService;
use Laravelito\Core\Exceptions\NotFoundException;
use Laravelito\CRUD\Core\Service\Edit\EditService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Laravelito\Core\Exceptions\InternalServerException;
use Laravelito\View\Breadcrumb\Item\LabelBreadcrumbItem;

abstract class CRUDController extends Controller {
    /** @var AuthService */
    private $Auth;

    /** @var CRUDService */
    private $CRUD;

    /** @var DownloadService */
    private $Download;

    /** @var bool */
    private $dry = false;

    /**
     * Devuelve la url de direccion o NULL e intenta inferirla.
     * @return string|null
     */

    public function redirection(): ?string
    {
        return null;
    }

    /**
     * Devuelve el nombre del module.
     * @return string
     */
    public abstract function module(): string;

    /**
     * Callback cuando se recupera un modelo.
     * @param Model $model
     * @return void
     */
    public abstract function onFetch(Model $model): void;

    /**
     * Create a new controller instance.
     *
     * @param AuthService $auth
     * @param CRUDService $crud
     * @param DownloadService $download
     * @return void
     */

    public function __construct(AuthService $auth, CRUDService $crud,DownloadService $download)
    {
        $this->Auth = $auth;
        $this->CRUD = $crud;
        $this->Download = $download;
    }

    /**
     * Permite especificar una vista custom.
     * @param Request $request
     * @return View|null
     */

    public function view(Request $request): ?View
    {
        return null;
    }

    /**
     * Setter dry run mode.
     * 
     * @param bool $dry
     * @return void
     */

    public function dry(bool $dry): void
    {
        $this->dry = $dry;
    }

    /**
     * Realiza una redireccion.
     * @return RedirectResponse
     */

    private function redirectTo(): RedirectResponse
    {
        if($url = $this->redirection()){
            return Router::redirect($url, $this->CRUD->model());
        }

        return Router::redirect($this->module() . '.fetch', $this->CRUD->model());
    }

    /**
     * CRUD CREATE
     *
     * @param Request $request
     * @param bool $dry
     * @return Response|View
     */

    public function create(Request $request)
    {
        $this->CRUD->dry($this->dry);
        switch($this->CRUD->create($request, $this->module())){
            case CRUDService::SUCCESS:
                Flash::success(t($this->module() . '.create.success'));
                return $this->redirectTo();
            case CRUDService::FAILURE:
                Flash::danger(t($this->module() . '.create.failure'));
                break;
            case CRUDService::INVALID:
                Flash::danger(t($this->module() . '.create.invalid'));
                break;
            case CRUDService::DRY:
                break;
        }
        
        return $this->view($request) ?? CRUD::create('backend',$this->module());
    }

    /**
     * CRUD FETCH
     *
     * @param Request $request
     * @return Response|View
     */

    public function fetch(Request $request)
    {
        $this->CRUD->dry($this->dry);
        switch($this->CRUD->fetch($request, $this->module())){
            case CRUDService::SUCCESS:
                $this->onFetch($this->CRUD->model());
                return $this->view($request) ?? CRUD::fetch('backend',$this->module());
            default:
                throw $this->CRUD->exception();
        }
    }

    /**
     * CRUD EDIT.
     *
     * @param Request $request
     * @param bool $dry
     * @return Response|View
     */

    public function edit(Request $request)
    {
        $this->CRUD->dry($this->dry);
        switch($this->CRUD->edit($request, $this->module())){
            case CRUDService::SUCCESS:
                Flash::success(t($this->module() . '.edit.success'));
                return $this->redirectTo();
            case CRUDService::FAILURE:
                throw $this->CRUD->exception();
            case CRUDService::INVALID:
                Flash::danger(t($this->module() . '.edit.invalid'));
                break;
            case CRUDService::DRY:
                break;
        }

        $this->onFetch($this->CRUD->model());
        return $this->view($request) ?? CRUD::edit('backend',$this->module());
    }

    /**
     * CRUD COLLECTION.
     *
     * @param Request $request
     * @param string $flavor
     * @param int $limit
     * @return Response|View
     */

    protected function collection(Request $request,string $flavor, int $limit = 10)
    {
        $this->CRUD->dry($this->dry);
        $page = ($request->query(Table::PAGE_PARAM) ?? 1);
        $offset = ($page - 1) * $limit;

        $request->merge([
            'limit' => $limit,
            'offset' => $offset
        ]);
        
        switch($this->CRUD->collection($request,$this->module(),'collections.' . $flavor)){
            case CRUDService::SUCCESS:
                return $this->view($request) ?? CRUD::collection('backend',$this->module(),'collections.' . $flavor);
            default:
                throw new InternalServerException();
        }
    }

    /**
     * CRUD PICKER.
     * Internamente es un collection. Pero pinta picker.
     *
     * @param Request $request
     * @param string $flavor
     * @param int $limit
     * @return Response|View
     */

    protected function picker(Request $request,string $flavor, int $limit = 10)
    {
        $this->CRUD->dry($this->dry);
        $page = ($request->query(Table::PAGE_PARAM) ?? 1);
        $offset = ($page - 1) * $limit;

        $request->merge([
            'limit' => $limit,
            'offset' => $offset
        ]);

        switch($this->CRUD->collection($request,$this->module(),'pickers.' . $flavor)){
            case CRUDService::SUCCESS:
                return $this->view($request) ?? CRUD::picker('backend',$this->module(),'pickers.' . $flavor);
            default:
                throw new InternalServerException();
        }
    }

    /**
     * Realiza la descarga del archivo adjunto especificado.
     * Aunque no es parte del CRUD, se asume como un fetch, aplicandose los mismos permits.
     *
     * @param Request $request
     * @param string $field
     * @return mixed
     */

    public function download(Request $request,string $field)
    {
        $this->CRUD->dry($this->dry);
        switch($this->CRUD->fetch($request, $this->module())){
            case CRUDService::SUCCESS:
                $this->Download->localModel($this->CRUD->model(), $field);
                return $this->Download->response();
            default:
                throw $this->CRUD->exception();
        }
    }

    /**
     * Devuelve el modelo.
     * @return Model
     */

    public function model(): Model
    {
        return $this->CRUD->model();
    }

    /**
     * Expone los errores de validacion.
     *
     * @return array
     */

    public function errors(): array
    {
        return $this->CRUD->errors();
    }
}
