<?php

namespace App\Model;

use App\Model\Platform;
use App\Model\BaseModel;
use Illuminate\Support\Collection;
use Laravelito\Autocomplete\AutocompleteCollection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laravelito\Autocomplete\Model\AutocompleteModelInterface;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends BaseModel implements AutocompleteModelInterface
{
    use SoftDeletes;

    /** @var int Backend */
    const BACKEND_ADMIN = 1;

    /**
     * Campos para escritura.
     */

    public $fillable = [
        'code',
        'platform_id'
    ];

    /**
     * Rol pertenece a Plataforma.
     *
     * @return BelongsTo
     */

    public function platform(): BelongsTo
    {
        return $this->belongsTo(Platform::class);
    }

    /**
     * Devuelve el rol backendAdmin
     *
     * @return Role
     */

    public static function backendAdmin(): Role
    {
        return Role::findOrFail(self::BACKEND_ADMIN);
    }

    /**
     * Resuelve el Rol por plataforma y codigo
     *
     * @param Platform $platform
     * @param string $code
     * @return ?Product
     */

    public static function resolvByCode(Platform $platform,string $code): ?Platform
    {
        return Platform::whereNull('deleted_at')
                            ->where('platform', $platform->id)
                            ->where('code',$code)
                            ->first();
    }

    /*****************************
     *
     * IMPLEMENTACION AUTOCOMPLETE
     * Contra Plataforma $platform, defecto BACKEND.
     *
     *****************************/

    private static $platform = Platform::BACKEND;

    /**
     * Setter para platform.
     *
     * @param Platform
     * @return void
     */

    public static function setPlatform(Platform $platform): void
    {
        Role::$platform = $platform->id;
    }


    /**
     * Devuelve una coleccion de objetos filtrados desde el Request.
     *
     * @param string $term
     * @param array $context
     * @param int $page
     * @return AutocompleteCollection
     */

    public static function autocomplete(string $term,array $context = [],int $page = 1): AutocompleteCollection
    {
        $limit = 10;
        $offset = ($page - 1) * $limit;
        $collection = Role::whereNull('deleted_at')
                                ->orderBy('code','ASC')
                                ->take($limit)
                                ->offset($offset)
                                ->get()
                                ->filter(function($item) use ($term) {
                                    return strlen($term) === 0 || stripos($item->description, $term) !== false;
                                })
                                ->sort(function($a, $b){
                                    return strcmp($a->description, $b->description);
                                });

        return new AutocompleteCollection($collection, 'id', 'code');
    }

    /**
     * Devuelve una coleccion de objetos filtrados desde el Request.
     *
     * @param mixed $value
     * @param mixed $parameters
     * @return bool
     */

    public static function autocompleteValidation($value,$parameters): bool
    {
        $product = Role::where('id',$value)
                                ->where('platform_id', Role::$platform)
                                ->whereNull('deleted_at')
                                ->first();

        return $product ? true : false;
    }

    /**
     * Devuelve la representacion textual del objeto.
     *
     * @param mixed $id
     * @return string
     */

    public static function autocompleteText($id): string
    {
        if($role = Role::find($id)){
            return $role->code;
        }

        return '';
    }

    /******************
     *
     * FIN AUTOCOMPLETE
     *
     ******************/

    /**
     * Obtiene la descripcion del rol.
     *
     * @return string|null
     */

    public function getDescriptionAttribute(): ?string
    {
        switch($this->id){
            case self::BACKEND_ADMIN:
                return t('roles.admin.description');
        }

        return null;
    }
}
