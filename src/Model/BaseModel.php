<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

abstract class BaseModel extends Model{
    /**
     * Dado un builder retorna la sql con reemplazo de bindings
     * @param Builder
     * @return string
     */

    public static function getFinalSql(Builder $query): string
    {
        $bindings = $query->getBindings();

        return preg_replace_callback('/\?/', function ($match) use (&$bindings, $query) {
            return $query->getConnection()->getPdo()->quote(array_shift($bindings));
        }, $query->toSql());
    }

    /**
     * Guarda el modelo y toca el updated_at.
     */

    public function touchOrFail(): void
    {
        $this->saveOrFail();
        $this->touch();
    }
}
