<?php

namespace App\Model;

use App\Model\Role;
use App\Model\User;
use App\Model\UserRole;
use App\Model\BaseModel;
use App\Model\UserCustomer;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRole extends BaseModel
{
    use SoftDeletes;

    /**
     * Campos para escritura.
     */

    public $fillable = [
        'user_id',
        'role_id',
    ];

    /**
     * Data adicional.
     */

    public $appends = [
        'description',
    ];

    /**
     * Pertenece a un User
     * @return BelongsTo
     */

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->whereNull('deleted_at');
    }

    /**
     * Pertenece a un Role
     * @return BelongsTo
     */

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class)->whereNull('deleted_at');
    }

    /**
     * Obtiene la descripcion del Role.
     * @return string
     */

    public function getDescriptionAttribute(): ?string
    {
        return Role::description($this->role_id);
    }

    /**
     * Crear el registro o lo activa si ya existe.
     * @param User $user
     * @param int $role
     * @return UserRole
     */

    public static function alloc(User $user,int $role): UserRole
    {
        $query = UserRole::where('user_id', $user->id)
                            ->where('role_id', $role);

        if(! ($userRole = $query->first())){
            $userRole = new UserRole([
                'user_id' => $user->id,
                'role_id' => $role
            ]);
        }

        $userRole->deleted_at = null;
        $userRole->touchOrFail();
        return $userRole;
    }
}
