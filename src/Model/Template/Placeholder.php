<?php

namespace App\Model;

use Illuminate\Http\Request;
use Laravelito\Locales\Locale;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravelito\Autocomplete\AutocompleteCollection;
use Laravelito\Autocomplete\Model\AutocompleteModelInterface;

class Placeholder extends BaseModel implements AutocompleteModelInterface
{
    use SoftDeletes;

    /**
     * Campos para escritura.
     */

    public $fillable = [

    ];

    /**
     * Campos ocultos para lectura.
     */

    public $hidden = [

    ];

    /*****************************
     *
     * IMPLEMENTACION AUTOCOMPLETE
     *
     *****************************/


    /**
     * Devuelve una coleccion de objetos filtrados desde el Request.
     *
     * @param string $term
     * @param array $context
     * @param int $page
     * @return Collection
     */

    public static function autocomplete(string $term,array $context = [],int $page = 1): AutocompleteCollection
    {
        $limit = 10;
        $offset = ($page - 1) * $limit;
        $collection = Placeholder::where(function($query) use ($term) {
                                    $query->where('LABELHOLDER','like','%' . $term . '%');
                                })
                                ->select('placeholders.*')
                                ->whereNull('deleted_at')
                                ->orderBy('LABELHOLDER','ASC')
                                ->take($limit)
                                ->offset($offset)
                                ->get();

        return new AutocompleteCollection($collection, 'id', 'LABELHOLDER');
    }

    /**
     * Devuelve una coleccion de objetos filtrados desde el Request.
     *
     * @param mixed $value
     * @param mixed $parameters
     * @return bool
     */

    public static function autocompleteValidation($value,$parameters): bool
    {
        $placeholder = Placeholder::where('placeholders.id',$value)->first();
        return $placeholder ? true : false;
    }

    /**
     * Devuelve la representacion textual del objeto.
     *
     * @param mixed $id
     * @return string
     */

    public static function autocompleteText($id): string
    {
        if($placeholder = Placeholder::find($id)){
            return $placeholder->LABELHOLDER;
        }

        return '';
    }

    /******************
     *
     * FIN AUTOCOMPLETE
     *
     ******************/
}
