<?php

namespace App\Model;

use App\Model\BaseModel;
use App\Model\Placeholder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Relation extends BaseModel
{
    use SoftDeletes;

    /**
     * Campos para escritura.
     */

    public $fillable = [
        'placeholder_id'
    ];

    /**
     * Pertenece a un Placeholder
     * @return BelongsTo
     */

    public function placeholdeR(): BelongsTo
    {
        return $this->belongsTo(Placeholder::class)->whereNull('deleted_at');
    }

    /**
     * Campos ocultos para lectura.
     */

    public $hidden = [

    ];

    /**
     * Allocates a new register.
     * This snippet is a direct insert example, sometimes you may want to perform an upsert based on some fields.
     *
     * @param Placeholder $placeholdeR
     * @param array $data
     * @return Relation
     */

    public static function alloc(Placeholder $placeholdeR,array $data): Relation
    {
        $relatioN = new Relation($data);
        $relatioN->placeholder_id = $placeholdeR->id;
        $relatioN->deleted_at = null;
        $relatioN->touchOrFail();
        return $relatioN;
    }

    /**
     * Shutdowns the registers not included in the request.
     * Sometimes you might want to just update a flag, replace the delete clause if needeed.
     *
     * @param Placeholder $placeholdeR
     * @param array $except
     * @return void
     */

    public static function shutdown(Placeholder $placeholdeR,array $except): void
    {
        Relation::where('placeholder_id', $placeholdeR->id)
                            ->whereNotIn('id', $except)->delete();
    }
}
