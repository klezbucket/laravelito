<?php

namespace App\Model;

use App\Model\BaseModel;
use Illuminate\Support\Collection;
use Laravelito\Autocomplete\Model\AutocompleteModelInterface;
use Laravelito\Autocomplete\AutocompleteCollection;
use Illuminate\Database\Eloquent\SoftDeletes;

class Platform extends BaseModel implements AutocompleteModelInterface
{
    use SoftDeletes;

    /** @var int Backend */
    const BACKEND = 1;

    /**
     * Campos para escritura.
     */

    public $fillable = [
        'code',
    ];

    /**
     * Resuelve la Plataforma por codigo.
     *
     * @param string $code
     * @return ?Product
     */

    public static function resolvByCode(string $code): ?Platform
    {
        return Platform::whereNull('deleted_at')->where('code',$code)->first();
    }

    /**
     * Devuelve la plataforma Backend
     *
     * @return Platform
     */

    public static function backend(): Platform
    {
        return Platform::findOrFail(self::BACKEND);
    }

    /*****************************
     *
     * IMPLEMENTACION AUTOCOMPLETE
     *
     *****************************/


    /**
     * Devuelve una coleccion de objetos filtrados desde el Request.
     *
     * @param string $term
     * @param array $context
     * @param int $page
     * @return AutocompleteCollection
     */

    public static function autocomplete(string $term,array $context = [],int $page = 1): AutocompleteCollection
    {
        $limit = 10;
        $offset = ($page - 1) * $limit;
        $collection = Platform::whereNull('deleted_at')
                                ->orderBy('code','ASC')
                                ->take($limit)
                                ->offset($offset)
                                ->get()
                                ->filter(function($item) use ($term) {
                                    return strlen($term) === 0 || stripos($item->description, $term) !== false;
                                })
                                ->sort(function($a, $b){
                                    return strcmp($a->description, $b->description);
                                });


        return new AutocompleteCollection($collection, 'id', 'code');
    }

    /**
     * Devuelve una coleccion de objetos filtrados desde el Request.
     *
     * @param mixed $value
     * @param mixed $parameters
     * @return bool
     */

    public static function autocompleteValidation($value,$parameters): bool
    {
        $product = Platform::where('id',$value)
                                ->whereNull('deleted_at')
                                ->first();

        return $product ? true : false;
    }

    /**
     * Devuelve la representacion textual del objeto.
     *
     * @param mixed $id
     * @return string
     */

    public static function autocompleteText($id): string
    {
        if($platform = Platform::find($id)){
            return $platform->code;
        }

        return '';
    }

    /******************
     *
     * FIN AUTOCOMPLETE
     *
     ******************/

    /**
     * Obtiene la descripcion de la plataforma.
     *
     * @return string|null
     */

    public function getDescriptionAttribute(): ?string
    {
        switch($this->id){
            case self::BACKEND:
                return t('platforms.backend.description');
        }
        return null;
    }
}
