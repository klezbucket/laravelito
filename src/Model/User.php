<?php

namespace App\Model;

use App\Model\Role;
use App\Model\Platform;
use App\Model\BaseModel;
use App\Model\UserToken;
use App\Model\UserPlatform;
use Illuminate\Http\Request;
use Laravelito\Locales\Locale;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Collection;
use Laravelito\Auth\Model\AuthModelInterface;
use Laravelito\Autocomplete\AutocompleteCollection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravelito\Autocomplete\Model\AutocompleteModelInterface;

class User extends BaseModel implements AuthModelInterface, AutocompleteModelInterface
{
    use SoftDeletes;

    /** @var int BACKEND OWNER */
    const BACKEND_ADMIN = 1;

    /**
     * Campos para escritura.
     */

    public $fillable = [
        'email',
        'first_name',
        'last_name',
        'password',
        'lang',
        'timezone',
        'created_at',
    ];

    /**
     * Campos ocultos para lectura.
     */

    public $hidden = [
        'password', 'pivot'
    ];

    /**
     * Usuario tiene muchos platforms.
     * @return BelongsToMany
     */

    public function platforms(): BelongsToMany
    {
        return $this->belongsToMany('App\Model\Platform', 'user_platforms')
                ->whereNull('user_platforms.deleted_at')
                ->orderBy('platforms.code', 'ASC')
                ->withTimestamps();
    }

    /**
     * Usuario tiene muchos roles.
     * @return BelongsToMany
     */

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany('App\Model\Role', 'user_roles')
                ->whereNull('user_roles.deleted_at')
                ->orderBy('roles.code', 'ASC')
                ->withTimestamps();
    }

    /**
     * Obtiene el nombre completo.
     * @return string
     */

    public function getFullNameAttribute(): string
    {
        return trim($this->first_name.' '.$this->last_name);
    }

    /**
     * Determina si el usuario puede conectarse a la plataforma.
     *
     * @param Platform $product
     * @return bool
     */

    public function platformAllowed(Platform $platform): bool
    {
        return UserPlatform::allowed($this, $platform->id);
    }

    /**
     * Devuelve los Roles del Usuario sobre la Plataforma.
     *
     * @param Platform $platform
     * @return Collection los roles del Usuario.
     */

    public function loadRoles(Platform $platform): Collection
    {
        return $this->select('roles.code')
                        ->distinct()
                        ->join('user_roles','users.id','user_roles.user_id')
                        ->join('roles','roles.id','user_roles.role_id')
                        ->where('user_roles.user_id', $this->id)
                        ->where('roles.platform_id', $platform->id)
                        ->whereNull('user_roles.deleted_at')
                        ->whereNull('roles.deleted_at')
                        ->get();
    }

    /**
     * Autentica un usuario.
     * @param string $email
     * @param string $password
     * @return ?User
     */

    public static function auth(string $email,string $password): ?User
    {
        $query = User::whereNull('deleted_at')
                            ->where('email', $email);

        if($user = $query->first()){
            if(Hash::check($password, $user->password)){
                return $user;
            }
        }

        return null;
    }

    /**************************************
     *
     * Implementacion de AuthModelInterface
     *
     **************************************/

    /**
     * Dado el TOKEN recibido, intenta recuperar un usuario valido.
     *
     * @param string $token
     * @return AuthModelInterface|null
     */

    public static function authorizeByToken(string $token): ?AuthModelInterface
    {
        if($userToken = UserToken::auth($token, Platform::backend())){
            if($user = $userToken->user()->first()){
                return $user;
            }
        }

        return null;
    }

    /**
     * Dado el Request recibido, se encarga de autenticar un usuario valido.
     *
     * @param Request $request
     * @return AuthModelInterface|null
     */

    public static function login(Request $request): ?AuthModelInterface
    {
        $email = $request->get('email');
        $password = $request->get('password');

        $query = User::whereNull('deleted_at')
                        ->where('email', $email);

        if($user = $query->first()){
            if(UserPlatform::allowed($user,Platform::BACKEND)){
                if(Hash::check($password, $user->password)){
                    UserToken::backend($user,true);
                    return $user;
                }
            }
        }

        return null;
    }

    /**
     * Se encarga de resolver si el ID proviedo aun resuelve un usuario autorizado y validado.
     *
     * @param int|null $id
     * @return AuthModelInterface|null
     */

    public static function authorized(?int $id): ?AuthModelInterface
    {
        if(! isset($id)){
            return null;
        }

        $query = User::whereNull('deleted_at')->where('id', $id);

        if($user = $query->first()){
            if(UserPlatform::allowed($user, Platform::BACKEND)){
                return $user;
            }
        }

        return null;
    }

    /**
     * Se encarga de devolver el nombre o denominacion del usuario autorizado.
     *
     * @return string
     */

    public function me(): string
    {
        return $this->full_name;
    }

    /**
     * Se encarga de devolver las locales del usuario autorizado.
     *
     * @return Locale
     */

    public function locale(): Locale
    {
        return Locale::with($this->lang, $this->timezone);
    }

    /**
     * Se encarga de determinar si el usuario posee el permiso requerido.
     *
     * @param string $permit
     * @return bool
     */

    public function permit(string $permit): bool
    {
        $roles = $this->loadRoles(Platform::backend());
        $target = explode('|', $permit);
        $array = $roles->pluck('code')->toArray();

        foreach($target as $r){
            if(in_array($r, $array)){
                return true;
            }
        }

        return false;
    }

    /**
     * Proporciona el token de autenticacion.
     *
     * @param string $token
     * @return string
     */

    public function bearer(): string
    {
        return UserToken::backend($this)->token;
    }

    /***************************
     *
     * Fin de AuthModelInterface
     *
     ***************************/


    /*****************************
     *
     * IMPLEMENTACION AUTOCOMPLETE
     *
     *****************************/


    /**
     * Devuelve una coleccion de objetos filtrados desde el Request.
     *
     * @param string $term
     * @param array $context
     * @param int $page
     * @return Collection
     */

    public static function autocomplete(string $term,array $context = [],int $page = 1): AutocompleteCollection
    {
        $limit = 10;
        $offset = ($page - 1) * $limit;
        $collection = User::where(function($query) use ($term) {
                                    $query->where('first_name','like','%' . $term . '%')
                                            ->orWhere('last_name','like','%' . $term . '%');
                                })
                                ->select('users.*')
                                ->whereNull('deleted_at')
                                ->orderBy('first_name','ASC')
                                ->orderBy('last_name','ASC')
                                ->take($limit)
                                ->offset($offset)
                                ->get();

        return new AutocompleteCollection($collection, 'id', 'full_name');
    }

    /**
     * Devuelve una coleccion de objetos filtrados desde el Request.
     *
     * @param mixed $value
     * @param mixed $parameters
     * @return bool
     */

    public static function autocompleteValidation($value,$parameters): bool
    {
        $user = User::where('users.id',$value)->first();
        return $user ? true : false;
    }

    /**
     * Devuelve la representacion textual del objeto.
     *
     * @param mixed $id
     * @return string
     */

    public static function autocompleteText($id): string
    {
        return User::findOrFail($id)->full_name;
    }

    /******************
     *
     * FIN AUTOCOMPLETE
     *
     ******************/
}
