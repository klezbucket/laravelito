<?php

namespace App\Model;

use App\Model\User;
use App\Model\Platform;
use App\Model\BaseModel;
use App\Model\UserToken;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserToken extends BaseModel
{
    use SoftDeletes;

    /**
     * Campos para escritura.
     */

    public $fillable = [
        'user_id',
        'platform_id',
        'token',
    ];

    /**
     * Cada Token representa a un unico usuario.
     *
     * @return BelongsTo
     */

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->whereNull('deleted_at');
    }

    /**
     * Cada Token se utiliza por Plataforma.
     *
     * @return BelongsTo
     */

    public function platform(): BelongsTo
    {
        return $this->belongsTo(Platform::class)
                            ->whereNull('deleted_at');
    }

    /**
     * Genera un nuevo token UUID4
     * @return string
     */

    public static function generateToken(): string
    {
        return Str::uuid();
    }

    /**
     * Autentica un token valido.
     *
     * @param string $token el token a autenticar
     * @param Platform $platform
     * @return UserToken
     */

    public static function auth(string $token,Platform $platform = null): ?UserToken
    {
        $query = UserToken::where('token', $token)
                            ->where('platform_id', $platform->id)
                            ->whereNull('deleted_at');

        return $query->first();
    }

    /**
     * Realiza Upsert de Token por Producto.
     *
     * @param User $user
     * @param Platform $platform
     * @return UserToken|null
     */

    public static function alloc(User $user,Platform $platform): UserToken
    {
        $query = UserToken::where('user_id', $user->id)
                            ->where('platform_id', $platform->id);

        if(! ($token = $query->first())){
            $token = UserToken::create([
                'user_id' => $user->id,
                'platform_id' => $platform->id,
                'token' => self::generateToken(),
            ]);
        }
        else{
            $token->token = self::generateToken();
            $token->deleted_at =  null;
            $token->touchOrFail();
        }

        return $token;
    }

    /**
     * Recupear el token para la API de backend, o lo crea si no existe.
     *
     * @param User
     * @return UserToken
     */

    public static function backend(User $user,$update = false): UserToken
    {
        $query = UserToken::where('user_id', $user->id)
                                ->whereNull('deleted_at')
                                ->where('platform_id', Platform::BACKEND);

        if(!( $token = $query->first())){
            $token = UserToken::create([
                'user_id' => $user->id,
                'platform_id' => Platform::BACKEND,
                'token' => self::generateToken(),
            ]);
        }
        else if($update){
            $token->token = self::generateToken();
        }

        $token->touchOrFail();
        return $token;
    }
}
