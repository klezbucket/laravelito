<?php

namespace App\Model;

use App\Model\User;
use App\Model\Platform;
use App\Model\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPlatform extends BaseModel
{
    use SoftDeletes;

    /**
     * Campos para escritura.
     */

    public $fillable = [
        'user_id',
        'platform_id',
    ];

    /**
     * Pertenece a un User
     * @return BelongsTo
     */

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->whereNull('deleted_at');
    }

    /**
     * Pertenece a un Platform
     * @return BelongsTo
     */

    public function platform(): BelongsTo
    {
        return $this->belongsTo(Platform::class)->whereNull('deleted_at');
    }

    /**
     * Obtiene la descripcion del Role.
     * @return string
     */

    public function getDescriptionAttribute(): ?string
    {
        return Platform::description($this->platform_id);
    }

    /**
     * Determina si el usuario esta asignado a la plataforma.
     * @param User $user
     * @param int $platform
     * @return bool
     */

    public static function allowed(User $user, int $platform): bool
    {
        $query = UserPlatform::where('user_id', $user->id)
                            ->where('platform_id', $platform)
                            ->whereNull('deleted_at');

        return $query->first() ? true : false;
    }

    /**
     * Crear el registro o lo activa si ya existe.
     * @param User $user
     * @param int $platform
     * @return UserPlatform
     */

    public static function alloc(User $user,int $platform): UserPlatform
    {
        $query = UserPlatform::where('user_id', $user->id)
                            ->where('platform_id', $platform);

        if(! ($userPlatform = $query->first())){
            $userPlatform = new UserPlatform([
                'user_id' => $user->id,
                'platform_id' => $platform
            ]);
        }

        $userPlatform->deleted_at = null;
        $userPlatform->touchOrFail();
        return $userPlatform;
    }
}
