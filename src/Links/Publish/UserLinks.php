<?php

namespace App\Backend\Links;

use Laravelito\View\Link\Link;
use Illuminate\Support\Collection;

class UserLinks {
    /**
     * Devuelve la coleccion de links
     * @return Collection
     */

    public static function provide(): Collection
    {
        return collect([
            new Link('users.fetch','users.fetch.title','fa-eye'),
            new Link('users.edit','users.edit.title','fa-pencil-alt'),
        ]);
    }
}
