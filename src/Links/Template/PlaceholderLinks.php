<?php

namespace App\Backend\Links;

use Laravelito\View\Link\Link;
use Illuminate\Support\Collection;

class PlaceholderLinks {
    /**
     * Devuelve la coleccion de links
     * @return Collection
     */

    public static function provide(): Collection
    {
        return collect([
            new Link('placeholders.fetch','placeholders.fetch.title','fa-eye'),
            new Link('placeholders.edit','placeholders.edit.title','fa-pencil-alt'),
        ]);
    }
}
