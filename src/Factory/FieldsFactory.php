<?php

namespace Laravelito\Factory;

use Laravelito\Field\Factory;
use Laravelito\Field\Generic;
use Laravelito\Field\Type\Type;
use Laravelito\Field\TypeFactory;
use Illuminate\Support\Collection;
use Laravelito\Field\ConstraintsFactory;

class FieldsFactory {
    /**
     * Transforma el array en una coleccion de field genericos.
     * @param array $config
     * @return Collection
     */

    public static function with(Collection $config): Collection
    {
        $config = $config->filter(function($item){
            return FieldsFactory::filter($item);
        })->map(function($item){
            return FieldsFactory::map($item);
        })->filter(function($item){
            return ($item instanceof Generic);
        });

        return $config;
    }

    /**
     * Filtra los campos no validos o con datos faltantes.
     * @param mixed $item
     * @return array|null
     */

    public static function filter($item): ?array
    {
        if(! is_array($item)){
            return null;
        }

        foreach(['name','type'] as $required){
            if(! isset($item[$required])){
                return null;
            }
        }

        return $item;
    }

    /**
     * Devuelve el campo generico para esta configuracion
     * @param array $config
     * @return Generic|null
     */

    public static function map(array $config): ?Generic
    {
        $type = FieldsFactory::type($config);
        $constraints = FieldsFactory::constraints($config);
        $factory = Factory::with($type,$constraints);
        
        return FieldsFactory::configure($factory, $config)->build();
    }

    /**
     * Setea los campos del field desde la configuracion
     * 
     * @param Factory $factory
     * @param array $config
     * @return Factory
     */

    public static function configure(Factory $factory,array $config): Factory
    {   
        $name = $config['name'];
        $factory->name($name);

        $label = $config['label'] ?? $name;
        $factory->label($label);

        if($placeholder = ($config['placeholer'] ?? null)){
            $factory->placeholder($placeholder);
        }

        if($help = ($config['help'] ?? null)){
            $factory->help($help);
        }

        return $factory;
    }

    /**
     * Resuelve los constraints para este campo.
     * 
     * @param array $config
     * @return Collection
     */

    public static function constraints(array $config): Collection
    {
        return ConstraintsFactory::with($config)->build();
    }

    /**
     * Resuelve el tipo de dato de este campo.
     * @param array $config
     * @return Type
     */

    public static function type(array $config): Type
    {
        $type = $config['type'] ?? null;
        return TypeFactory::with($type)->build();
    }
}