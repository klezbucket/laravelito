<?php
namespace Laravelito\Table;

use Illuminate\Support\Str;
use Laravelito\Table\Table;
use Laravelito\Table\Injector;
use Illuminate\Support\Collection;
use Laravelito\Core\Facades\Loader;
use Illuminate\Support\Facades\Config;
use Laravelito\Core\Exceptions\NotFoundException;

class BackendTable implements Injector {
    /**
     * Devuelve la Tabla requerida.
     * Implementa tu propio inyectos.
     * 
     * @param string $name
     * @return Table
     * @throws NotFoundException si el $name especificado no resuelve una tabla valida.
     */

    public function get(string $name): Table
    {
        null;
    }
}
