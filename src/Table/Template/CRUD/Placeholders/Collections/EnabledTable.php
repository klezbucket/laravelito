<?php

namespace App\Backend\Table\CRUD\Placeholders\Collections;

use Laravelito\Field\Field;
use Laravelito\Table\Table;
use Laravelito\View\Link\Link;
use App\Backend\Links\PlaceholderLinks;
use Illuminate\Support\Collection;

class EnabledTable extends Table
{
    /**
     * Obtiene la coleccion de campos.
     *
     * @return Collection of Field
     */

    public function fields(): Collection
    {
        $fields = collect();
        return $fields;
    }

    /**
     * Obtiene la coleccion de links.
     *
     * @return Collection of Link
     */

    public function links(): Collection
    {
        return PlaceholderLinks::provide();
    }

    /**
     * Obtiene el contexto que alimenta el detalle
     *
     * @return string
     */

    public function context(): string
    {
        return 'placeholders.collections.enabled';
    }
}
