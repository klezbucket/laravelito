<?php

namespace App\Backend\Table;

use Illuminate\Support\Str;
use Laravelito\Table\Table;
use Laravelito\Table\Injector;
use Illuminate\Support\Collection;
use Laravelito\Core\Facades\Loader;
use Illuminate\Support\Facades\Config;
use Laravelito\Core\Exceptions\NotFoundException;
use App\Backend\Loader\Loader as L;

class BackendTable implements Injector {
    /**
     * Devuelve la Tabla requerida.
     *
     * @param string $name
     * @return Table
     * @throws NotFoundException si el $name especificado no resuelve una tabla valida.
     */

    public function get(string $name): Table
    {
        $class = Loader::getClass($this->namespace($name), L::get($name) . 'Table', Table::class);
        return new $class($name);
    }

    /**
     * Resuelve el namespace del form.
     * Si es report el namespace es diferente, etc.
     *
     * @param string $name
     * @return string
     */

    public function namespace(string $name): string
    {
        if(preg_match('/^report\./',$name)){
            return 'App\\Backend\\Table';
        }

        return 'App\\Backend\\Table\\CRUD';
    }
}
