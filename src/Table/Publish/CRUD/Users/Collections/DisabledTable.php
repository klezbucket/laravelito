<?php

namespace App\Backend\Table\CRUD\Users\Collections;

use Laravelito\Field\Field;
use Laravelito\Table\Table;
use Laravelito\View\Link\Link;
use App\Backend\Links\UserLinks;
use Illuminate\Support\Collection;
use App\Backend\Field\Users\EmailField;
use App\Backend\Field\Users\FullNameField;
use App\Backend\Field\Users\CreatedAtField;

class DisabledTable extends Table
{
    /**
     * Obtiene la coleccion de campos.
     *
     * @return Collection of Field
     */

    public function fields(): Collection
    {
        $fields = collect();
        $fields[] = new CreatedAtField();
        $fields[] = new FullNameField();
        $fields[] = new EmailField();
        return $fields;
    }

    /**
     * Obtiene la coleccion de links.
     *
     * @return Collection of Link
     */

    public function links(): Collection
    {
        return UserLinks::provide();
    }

    /**
     * Obtiene el contexto que alimenta el detalle
     *
     * @return string
     */

    public function context(): string
    {
        return 'users.collections.disabled';
    }
}
