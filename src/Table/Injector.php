<?php

namespace Laravelito\Table;

use Laravelito\Table\Table;

interface Injector {
    /**
     * Dado el nombre del table deseado, devuelve una instancia del mismo.
     * 
     * @param string $name
     * @return Table
     */

    function get(string $name): Table;
}