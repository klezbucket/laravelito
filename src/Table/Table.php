<?php

namespace Laravelito\Table;

use Laravelito\Field\Field;
use Laravelito\Table\Injector;
use Laravelito\Core\Facades\Site;
use Illuminate\Support\Collection;
use Laravelito\Field\FieldProvider;
use Illuminate\Support\Facades\Config;
use Laravelito\Field\FieldDependences;
use Laravelito\View\Link\LinkProvider;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Context\ContextProvider;
use Laravelito\CRUD\Service\Collection\CollectionData;
use Laravelito\Core\Exceptions\InternalServerException;
use Laravelito\CRUD\Service\Collection\CollectionProvider;

abstract class Table implements FieldProvider, ContextProvider, CollectionProvider, LinkProvider {
    /** @var string nombre de la tabla */
    private $name;

    /** @var string coleccion */
    private $collection;

    /** @var Model coleccion */
    private $model;

    /**
     * Constructor de la tabla
     * @return void
     */

    public function __construct(string $name)
    {
        $this->name = $name;
    }
    
    /**
     * Obtiene el nombre del formulario para los filtros de la tabla.
     *
     * @return string|null
     */

    public function filters(): ?string
    {
        return null;
    }
    
    /**
     * Obtiene las dependencias de campos.
     * Sobreescribe este metodo para inyectar tus propias 
     *
     * @return FieldDependences
     */

    public function dependences(): ?FieldDependences
    {
        return null;
    }
    
    /**
     * Obtiene el nombre de la tabla.
     *
     * @return string
     */

    public function name(): string
    {
        return $this->name;
    }

    /**
     * Obtiene el namespace de los campos. 
     * Por defecto es el nombre mismo de la tabla hasta el primer punto.
     *
     * @return string
     */

    public function localesAt(): string
    {
        return explode('.',$this->name())[0] ?? $this->name();
    }

    /**
     * Obtiene una coleccion.
     *
     * @param CollectionData|null $collection
     * @return ?CollectionData
     */

    public function collection(?CollectionData $collection = null): CollectionData
    {
        if(isset($collection)){
            $this->collection = $collection;
        }

        return $this->collection;
    }
    
    /**
     * Setter/Getter para el modelo.
     *
     * @param Model|null $model
     * @return Model
     */
    
    public function model(?Model $model = null): ?Model
    {
        if(isset($model)){
            $this->model = $model;
        }

        return $this->model;
    }

    /**
     * Obtiene el valor del campo dado.
     *
     * @param Field $field
     * @return string|null
     */

    public function value(Field $field): ?string
    {
        $name = $field->name();
        return $this->model()->{$name} ?? null;
    }

    /**
     * Devuelve la instancia de una tabla segun su nombre, a traves del inyector.
     * @param string $name
     * @return Table
     */

    public static function instantiate(string $name): Table
    {
        if(! ($table = self::$BAG[$name] ?? null)){
            $site = Site::resolv(request());
            $class = Config::get('laravelito.tables.' . $site);
            $injector = new $class($name);

            if(! ($injector instanceof Injector)){
                throw new InternalServerException('Class provided by laravelito.tables.' . $site . ' is not an Injector instance');
            }

            $table = $injector->get($name);

            if(! ($table instanceof Table)){
                throw new InternalServerException("Class provided by {$class} is not a Table instance");
            }
        }

        self::$BAG[$name] = $table;
        return $table;
    }

    /**
     * Al instanciar una tabla se va a una bolsa de cache.
     *
     * @var array
     */

    private static $BAG = [];
}
