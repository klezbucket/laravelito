<?php

namespace Laravelito\Table;

use Laravelito\Field\Field;
use Laravelito\Table\Table;
use Laravelito\Table\Picker;
use Laravelito\Core\Facades\Site;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Laravelito\Core\Exceptions\InternalServerException;

abstract class Picker extends Table {
    /**
     * Obtiene el campo a desplegar en el picker.
     *
     * @return Field
     */

    abstract public function field(): Field;

    /**
     * Obtiene el icono a desplegar en el picker.
     *
     * @return string
     */
    
    abstract public function icon(): string;

    /**
     * Obtiene la coleccion de campos.
     * NO SE UTILIZA.
     *
     * @return Collection of Field
     */

    public function fields(): Collection
    {
        return collect();
    }

    /**
     * Obtiene la coleccion de links.
     * NO SE UTILIZA.
     *
     * @return Collection of Link
     */

    public function links(): Collection
    {
        return collect();
    }

    /**
     * Devuelve la instancia de un picker segun su nombre, a traves del inyector.
     * @param string $name
     * @return Table
     */

    public static function instantiate(string $name): Table
    {
        $table = Table::instantiate($name);

        if(!($table instanceof Picker)){
            throw new InternalServerException("Class provided by {$class} is not a Picker instance");
        }

        return $table;
    }
}
