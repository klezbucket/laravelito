<?php
namespace App\Backend\Loader;

class Loader {
    /** @var array  */

    const PREFIXES = [
        'supervisor', 'agent'
    ];

    /**
     * Devuelve el nombre de la clase a cargar, sanitizado.
     * @param string $name
     * @return string
     */

    public static function get(string $name): string
    {
        $parts = explode('.', $name);

        if(in_array($parts[0], self::PREFIXES)){
            unset($parts[0]);
        }

        return implode('.', $parts);
    }
}
