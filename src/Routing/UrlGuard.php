<?php 

namespace Laravelito\Routing;

use Laravelito\Auth\Facades\Auth;

class UrlGuard {
    /** @var array la bolsa de permisos por nombre de ruta */
    private static $BAG = [];

    /**
     * Configura la ruta especificada con los permisos.
     * Si se especifica mas de un permiso, todos deberan ser honrados por el usuario.
     * Si ya existia la ruta configurada, se pisan los permisos con los nuevos.
     * 
     * @param string $name
     * @param array $permits
     * @return void
     */

    public static function set(string $name,array $permits = []): void
    {
        self::$BAG[$name] = $permits;
    }

    /**
     * Obtiene los permisos requerido para la ruta especificada (si no se especifica se considera la actual)/
     * 
     * @param string|null $name
     * @return array
     */

    public static function get(?string $name = null): array
    {
        if(! isset($name)){
            $name = request()->route()->getName();
        }

        return self::$BAG[$name] ?? [];
    }
    
    /**
     * Determina si el usuario actual puede ingresar a la ruta especificada.
     * 
     * @param string $route
     * @return bool
     */

    public static function access(string $name): bool
    {
        $permits = self::get($name);

        foreach($permits as $permit){
            if(! Auth::permit($permit)){
                return false;
            }
        }

        return true;
    }
}