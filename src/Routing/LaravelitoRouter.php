<?php

namespace Laravelito\Routing;

use Laravelito\Locales\Facades\LocaleRouter;

class LaravelitoRouter {

    /**
     * Configura la ruta tipo formulario con todas las traducciones posibles y registra los permisos requeridos para 
     * usar con el middleware url.guard
     * 
     * Ruta formulario implica GET y POST.
     *
     * @param string $action
     * @param string $name
     * @param array $permits
     * @return void
     */

    public static function form(string $action,string $name,array $permits = []): void
    {
        UrlGuard::set($name,$permits);
        LocaleRouter::get($action,$name);
        LocaleRouter::post($action,$name);
    }

    /**
     * Configura la ruta GET con todas las traducciones posibles y registra los permisos requeridos para 
     * usar con el middleware url.guard
     *
     * @param string $action
     * @param string $name
     * @param array $permits
     * @return void
     */

    public static function get(string $action,string $name,array $permits = []): void
    {
        UrlGuard::set($name,$permits);
        LocaleRouter::get($action,$name);
    }

    /**
     * Configura la ruta POST con todas las traducciones posibles y registra los permisos requeridos para 
     * usar con el middleware url.guard
     *
     * @param string $action
     * @param string $name
     * @param array $permits
     * @return void
     */

    public static function post(string $action,string $name,array $permits = []): void
    {
        UrlGuard::set($name,$permits);
        LocaleRouter::post($action,$name);
    }
}