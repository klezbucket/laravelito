<?php

namespace Laravelito\Core\Exceptions;

use Laravelito\Core\Exceptions\AppException;

class NotFoundException extends AppException {
    /**
     * Error 404
     * 
     * @param string|null $message
     */

    public function __construct(?string $message = null)
    {
        parent::__construct(404,$message ?? t('errors.not_found'));
    }
}
