<?php

namespace Laravelito\Core\Exceptions;

use Laravelito\Core\Exceptions\AppException;

class RequestTimeoutException extends AppException {
    public $code = 408;
    
    /**
     * Error 408
     * 
     * @param string|null $message
     */

    public function __construct(?string $message = null)
    {
        parent::__construct(408,$message ?? t('errors.reques_timeout'));
    }
}
