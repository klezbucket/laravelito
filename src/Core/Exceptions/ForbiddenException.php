<?php

namespace Laravelito\Core\Exceptions;

use Laravelito\Core\Exceptions\AppException;

class ForbiddenException extends AppException {
    /**
     * Error 403
     * 
     * @param string|null $message
     */

    public function __construct(?string $message = null)
    {
        parent::__construct(403,$message ?? t('errors.forbidden'));
    }
}
