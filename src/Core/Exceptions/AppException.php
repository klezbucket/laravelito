<?php

namespace Laravelito\Core\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AppException extends HttpException {

}
