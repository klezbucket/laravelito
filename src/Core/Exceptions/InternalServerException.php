<?php

namespace Laravelito\Core\Exceptions;

use Laravelito\Core\Exceptions\AppException;

class InternalServerException extends AppException {

    /**
     * Error 500
     * 
     * @param string|null $message
     */

    public function __construct(?string $message = null)
    {
        parent::__construct(500,$message ?? t('errors.internal_error'));
    }
}
