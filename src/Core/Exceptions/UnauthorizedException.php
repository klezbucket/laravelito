<?php

namespace Laravelito\Core\Exceptions;

use Laravelito\Core\Exceptions\AppException;

class UnauthorizedException extends AppException {
    /**
     * Error 401
     * 
     * @param string|null $message
     */

    public function __construct(?string $message = null)
    {
        parent::__construct(401,$message ?? t('errors.unauthorized'));
    }
}
