<?php

namespace Laravelito\Core\Exceptions;

use Laravelito\Core\Exceptions\AppException;

class BadRequestException extends AppException {
    /**
     * Error 400
     * 
     * @param string|null $message
     */

    public function __construct(?string $message = null)
    {
        parent::__construct(400,$message ?? t('errors.bad_request'));
    }
}
