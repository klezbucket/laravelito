<?php

namespace Laravelito\Core\Providers;
    
use Laravelito\Locales\Locale;
use Laravelito\Auth\Facades\Auth;
use Laravelito\Core\Facades\Form;
use Laravelito\Core\Facades\Link;
use Laravelito\Core\Facades\Site;
use Laravelito\Core\Facades\Type;
use Laravelito\Core\Logs\Logging;
use Laravelito\CRUD\Facades\CRUD;
use Laravelito\Core\Facades\Field;
use Laravelito\Core\Facades\Flash;
use Laravelito\Core\Facades\Table;
use Laravelito\Core\Facades\Theme;
use Illuminate\Support\Facades\App;
use Laravelito\Core\Facades\Detail;
use Laravelito\Core\Facades\Picker;
use Laravelito\Core\Facades\Router;
use Laravelito\Core\Facades\Upload;
use Illuminate\Support\Facades\View;
use Laravelito\Core\Facades\Printer;
use Laravelito\Core\Facades\RowsForm;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Config;
use Laravelito\View\Page\Facades\Page;
use Illuminate\Support\ServiceProvider;
use Laravelito\CRUD\Service\CRUDService;
use Laravelito\Routing\LaravelitoRouter;
use Illuminate\Support\Facades\Validator;
use Laravelito\Auth\Services\AuthService;
use Laravelito\Form\Services\FormService;
use Laravelito\Core\Services\ThemeService;
use Laravelito\Locales\Facades\Translator;
use Laravelito\View\Composers\FormComposer;
use Laravelito\View\Composers\MenuComposer;
use Laravelito\View\Composers\PageComposer;
use Laravelito\Locales\Facades\LocaleRouter;
use Laravelito\Report\Service\ReportService;
use Laravelito\View\Composers\TableComposer;
use Laravelito\View\Composers\DetailComposer;
use Laravelito\View\Composers\LooperComposer;
use Laravelito\View\Composers\PickerComposer;
use Laravelito\Core\Facades\Vendor\Summernote;
use Laravelito\Locales\Services\LocaleService;
use Laravelito\Auth\Middlewares\AnonMiddleware;
use Laravelito\Auth\Middlewares\AuthMiddleware;
use Laravelito\Download\Service\DownloadService;
use Laravelito\View\Composers\BreadcrumbComposer;
use Laravelito\View\Composers\UserExtrasComposer;
use Laravelito\Auth\Middlewares\ControlMiddleware;
use Laravelito\Core\Middlewares\RequestMiddleware;
use Laravelito\CRUD\Core\Service\Edit\EditService;
use Laravelito\Auth\Middlewares\UrlGuardMiddleware;
use Laravelito\Console\Command\BackendFieldCommand;
use Laravelito\Core\Middlewares\ResponseMiddleware;
use Laravelito\Auth\Middlewares\TokenAuthMiddleware;
use Laravelito\Console\Command\BackendModuleCommand;
use Laravelito\Locales\Middlewares\LocaleMiddleware;
use Laravelito\Console\Command\BackendRowsFormCommand;
use Laravelito\Locales\Middlewares\LocaleRedirectMiddleware;

class LaravelitoServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */

    public function boot()
    {
        /**
         * Configuracion de Laravelito.
         */

        $this->publishes([
            __DIR__ . '/../../../config/laravelito.php' => config_path('laravelito.php'),
        ]);

        $this->publishes([
            __DIR__ . '/../../../config/crud.php' => config_path('crud.php'),
        ]);

        /**
         * Canal de Log.
         * Puedes cambiar el channel con Laravelito\Core\Logs\Logging::channel()
         */

        Config::set('logging.channels.laravelito', [
            'driver' => 'daily',
            'path' => storage_path('logs/laravelito.log'),
            'level' => 'info',
            'days' => 14,
        ]);

        Config::set('logging.channels.crud', [
            'driver' => 'daily',
            'path' => storage_path('logs/crud.log'),
            'level' => 'info',
            'days' => 14,
        ]);

        Config::set('logging.channels.report', [
            'driver' => 'daily',
            'path' => storage_path('logs/report.log'),
            'level' => 'info',
            'days' => 14,
        ]);

        Config::set('logging.channels.upload', [
            'driver' => 'daily',
            'path' => storage_path('logs/upload.log'),
            'level' => 'info',
            'days' => 14,
        ]);

        /**
         * Publicar vistas, segun el tema configurado.
         */

        $this->publishes([
            __DIR__ . '/../../../resources/views' => resource_path('views/vendor/laravelito'),
        ]);

        /**
         * Publicar assets.
         */

        $this->publishes([
            __DIR__ . '/../../../assets' => public_path('vendor/laravelito'),
        ],'public');

        /**
         * Publicar lang.
         */

        $this->publishes([
            __DIR__ . '/../../../resources/lang' => resource_path('lang'),
        ],'public');

        if (! App::runningInConsole()) {
            $locale = new LocaleService();
            $locale->fromCookie(request());
        }

        /**
         * Reglas custom de validacion
         */

        Validator::extend('foreign', 'Laravelito\\Autocomplete\\Validation\\AutcompleteValidator@validate');
        Validator::extend('upload.size', 'Laravelito\\Upload\\Validation\\UploadSizeValidator@validate');
        Validator::extend('upload.mimes', 'Laravelito\\Upload\\Validation\\UploadMimesValidator@validate');
        Validator::extend('picture', 'Laravelito\\Picture\\Validation\\PictureValidation@validate');
        Validator::extend('unsigned', 'Laravelito\\Field\\Validation\\UnsignedValidation@validate');

        /**
         * Vanilla Backend
         */

        $this->publishes([
            __DIR__ . '/../../../src/Controller/Publish' => app_path('Backend/Controller'),
        ],'public');

        $this->publishes([
            __DIR__ . '/../../../src/Auth/Publish' => app_path('Backend/Auth'),
        ],'public');

        $this->publishes([
            __DIR__ . '/../../../src/Model' => app_path('Model'),
        ],'public');

        $this->publishes([
            __DIR__ . '/../../../routes' => base_path('routes'),
        ],'public');

        $this->publishes([
            __DIR__ . '/../../../src/View/Menu/Publish' => app_path('Backend/Menu'),
        ],'public');

        $this->publishes([
            __DIR__ . '/../../../src/Loader/Publish' => app_path('Backend/Loader'),
        ],'public');

        $this->publishes([
            __DIR__ . '/../../../src/Field/Publish' => app_path('Backend/Field'),
        ],'public');

        $this->publishes([
            __DIR__ . '/../../../src/Form/Publish' => app_path('Backend/Form'),
        ],'public');

        $this->publishes([
            __DIR__ . '/../../../config/crud' => base_path('config/crud'),
        ],'public');

        $this->publishes([
            __DIR__ . '/../../../src/CRUD/Publish' => app_path('Backend/CRUD'),
        ],'public');

        $this->publishes([
            __DIR__ . '/../../../src/Links/Publish' => app_path('Backend/Links'),
        ],'public');

        $this->publishes([
            __DIR__ . '/../../../src/Detail/Publish' => app_path('Backend/Detail'),
        ],'public');

        $this->publishes([
            __DIR__ . '/../../../src/Table/Publish' => app_path('Backend/Table'),
        ],'public');

        $this->publishes([
            __DIR__ . '/../../../resources/overrides' => resource_path('views'),
        ]);

        // $this->publishes([
        //     __DIR__ . '/../../../database/migrations' => base_path('database/migrations'),
        // ]);

        $this->publishes([
            __DIR__ . '/../../../database/seeds' => base_path('database/seeds'),
        ]);

        $this->publishes([
            __DIR__ . '/../../../src/Form/MeForm.php' => app_path('Backend/Form/MeForm.php'),
        ]); 

        $this->publishes([
            __DIR__ . '/../../../src/Form/BackendForm.php' => app_path('Backend/Form/BackendForm.php'),
        ]); 

        $this->publishes([
            __DIR__ . '/../../../src/Exceptions/Publish/Handler.php' => app_path('Exceptions/Handler.php'),
        ]); 

        $this->publishes([
            __DIR__ . '/../../../src/Auth/Publish/Permits.php' => app_path('Backend/Auth/Permits.php'),
        ]); 

        /**
         * Comandos
         */
        
        if ($this->app->runningInConsole()) {
            $this->commands([
                BackendModuleCommand::class,
                BackendRowsFormCommand::class,
                BackendFieldCommand::class
            ]);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind(DownloadService::class, function($app){
            return new DownloadService();
        });

        $this->app->bind(ReportService::class, function($app){
            return new ReportService();
        });
        
        $this->app->bind(CRUDService::class, function($app){
            return new CRUDService();
        });

        $this->app->singleton(LocaleService::class, function($app){
            return new LocaleService();
        });

        $this->app->singleton(LocaleService::class, function($app){
            return new LocaleService();
        });

        $loader = AliasLoader::getInstance();
        $loader->alias('Theme', Theme::class);
        $loader->alias('Site', Site::class);
        $loader->alias('Auth', Auth::class);
        $loader->alias('LaravelitoRouter', LaravelitoRouter::class);
        $loader->alias('Translator', Translator::class);
        $loader->alias('Form', Form::class);
        $loader->alias('CRUD', CRUD::class);
        $loader->alias('Flash', Flash::class);
        $loader->alias('Detail', Detail::class);
        $loader->alias('Link', Link::class);
        $loader->alias('Table', Table::class);
        $loader->alias('Type', Type::class);
        $loader->alias('Field', Field::class);
        $loader->alias('Router', Router::class);
        $loader->alias('Printer', Printer::class);
        $loader->alias('RowsForm', RowsForm::class);
        $loader->alias('Picker', Picker::class);
        $loader->alias('Upload', Upload::class);
        $loader->alias('Summernote', Summernote::class);

        app()->make('router')->aliasMiddleware('web.request', RequestMiddleware::class);
        app()->make('router')->aliasMiddleware('web.response', ResponseMiddleware::class);
        app()->make('router')->aliasMiddleware('web.auth', AuthMiddleware::class);
        app()->make('router')->aliasMiddleware('web.anon', AnonMiddleware::class);
        app()->make('router')->aliasMiddleware('web.locale', LocaleMiddleware::class);
        app()->make('router')->aliasMiddleware('api.locale', LocaleMiddleware::class);
        app()->make('router')->aliasMiddleware('api.request', RequestMiddleware::class);
        app()->make('router')->aliasMiddleware('api.response', ResponseMiddleware::class);
        app()->make('router')->aliasMiddleware('api.auth', TokenAuthMiddleware::class);
        app()->make('router')->aliasMiddleware('url.guard', UrlGuardMiddleware::class);

        View::composer(
            'vendor.laravelito.core.menu', MenuComposer::class
        );

        View::composer(
            'vendor.laravelito.core.breadcrumb', BreadcrumbComposer::class
        );

        View::composer(
            'vendor.laravelito.core.head', PageComposer::class
        );

        View::composer(
            'vendor.laravelito.core.title', PageComposer::class
        );

        View::composer(
            'vendor.laravelito.core.form', FormComposer::class
        );

        View::composer(
            'vendor.laravelito.core.detail', DetailComposer::class
        );

        View::composer(
            'vendor.laravelito.core.table', TableComposer::class
        );

        View::composer(
            'vendor.laravelito.core.picker', PickerComposer::class
        );

        // BACKEND COMPOSERS

        View::composer(
            'backend.looper.users.extras', UserExtrasComposer::class
        );
    }
}
