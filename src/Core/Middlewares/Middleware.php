<?php

namespace Laravelito\Core\Middlewares;

use Closure;
use Exception;
use Throwable;
use Illuminate\Http\Request;
use Laravelito\Core\Logs\Logging;
use Illuminate\Support\Facades\Log;
use Laravelito\Exceptions\ExceptionReport;
use Symfony\Component\HttpFoundation\Response;

abstract class Middleware {
    /**
     * Reporta en un archivo de Log el Request recibido.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            return $this->hook($request,$next);
        }
        catch (Throwable $e){
            return $this->handleException($e,$request,$next);
        }
    }

    /**
     * Ejecuta la logica del middleware
     */

    public abstract function hook(Request $request, Closure $next);

    /**
     * Gestiona la exception capturada.
     * @param Throwable $e
     * @param Request $request
     * @param Closure $next
     * @return Response
     */

    protected function handleException(Throwable $e,Request $request,Closure $next): Response
    {
        ExceptionReport::log($e);
        
        try{
            return response()->view('errors/500', [
                'exception' => $e
            ], 500);
        }
        catch(Throwable $e2){
            throw $e;
        }
    }
}
