<?php

namespace Laravelito\Core\Middlewares;

use Closure;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Laravelito\Core\Logs\Logging;
use Laravelito\Core\Logs\Sanitize;
use Laravelito\Core\Middlewares\Middleware;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Laravelito\Core\Exceptions\InternalServerException;

class ResponseMiddleware extends Middleware {

    /**
     * Reporta en un archivo de Log la respuesta generada.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */

    public function hook(Request $request, Closure $next)
    {
        $response = $next($request);

        Logging::section('RESPONSE');

        if(method_exists($response,'status')){
            Logging::output('Status: ' . $response->status());
        }

        if($response instanceof RedirectResponse){
            Logging::output('Redirect: ' . $this->redirect($response)->getTargetUrl());
        }
        else if($response instanceof StreamedResponse){
            Logging::output('Stream');
        }
        else if($response instanceof JsonResponse){
            $json = json_encode(Sanitize::body(json_decode($this->json($response)->content(), true)));

            if(strlen($json) > 500){
                $json = substr($json,0,500) . ' CUT@500';
            }

            Logging::output('Json: ' . $json);
        }
        else if($response->original instanceof View){
            Logging::output('View: ' . $this->view($response)->name());
        }

        return $response;
    }

    /**
     * Obtiene la redireccion.
     * @param RedirectResponse
     * @return RedirectResponse
     */

    private function redirect(RedirectResponse $response): RedirectResponse
    {
        return $response;
    }

    /**
     * Obtiene la vista.
     * @param Response
     * @return View
     */

    private function view(Response $response): View
    {
        return $response->original;
    }

    /**
     * Obtiene la respuesta JSON.
     * @param Response
     * @return View
     */

    private function json(Response $response): JsonResponse
    {
        return $response;
    }
}
