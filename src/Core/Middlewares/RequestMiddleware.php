<?php

namespace Laravelito\Core\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Laravelito\Core\Logs\Logging;
use Laravelito\Core\Logs\Sanitize;
use Illuminate\Support\Facades\URL;
use Laravelito\Core\Middlewares\Middleware;

class RequestMiddleware extends Middleware {
    /**
     * Reporta en un archivo de Log el Request recibido.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function hook(Request $request, Closure $next)
    {
        Logging::section('REQUEST',25,'#');
        $proto = 'PLAIN';

        if($request->secure() || $request->server('HTTP_X_FORWARDED_PROTO') == 'https'){
            URL::forceScheme('https');
            $proto = 'SAFE';
        }

        Logging::output($proto . ' ' . $request->method() . ' ' . $request->url());

        if($contentType = $request->headers->get('Content-Type')){
            Logging::output('Content-Type: ' . $contentType);
        }

        if($request->isMethod('POST')){
            $contentType = explode(';', $contentType)[0] ?? '';
            
            switch($contentType){
                case 'application/json':
                    Logging::output('Body: ' . json_encode(Sanitize::body($request->all())));
                    break;
                case 'application/x-www-form-urlencoded':
                    Logging::output('Body: ' . http_build_query(Sanitize::body($request->all())));
                    break;
            }
        }

        return $next($request);
    }
}
