<?php

namespace Laravelito\Core\Provider;

interface Provider {
    /**
     * Obtiene el nombre del proveedor.
     *
     * @return string
     */

    function name(): string;
}