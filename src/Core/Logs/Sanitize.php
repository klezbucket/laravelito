<?php

namespace Laravelito\Core\Logs;

class Sanitize {
    /** @var array campos que no se reportan */

    private static $hidden = [ 
        'password', 'token', 'access_token'
    ];

    /**
     * Devuelve un payload seguro para guardar en el log.
     * Limpia cosas como passwords y tokens.
     * 
     * @param array $data
     * @return array
     */

    public static function body(array $data): array
    {
        $max = env('LOG_FIELD_MAX', 100);

        foreach($data as $field => $value){
            if(is_array($value)){
                $data[$field] = self::body($value);
            }
            else{
                if(in_array($field, self::$hidden)){
                    $data[$field] = 'OK';
                }
                else if(strlen($data[$field]) > $max){
                    $data[$field] = substr($data[$field], 0, env('LOG_FIELD_MAX', $max)) . ' CUT@' . $max;
                }
            }
        }
        
        return $data;
    }
}