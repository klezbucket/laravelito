<?php

namespace Laravelito\Core\Logs;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Logging {
    /** @var int */
    public static $TAB = 10;

    /** @var string */
    public static $ID;

    /** @var string */
    public static $CHANNEL = 'laravelito';

    /**
     * Changes the Log's Channel.
     * @param string $ch the new channel
     */

    public static function channel(string $ch)
    {
        self::$CHANNEL = $ch;
    }

    /**
     * Genera el hash de logging.
     * @return string
     */

    public static function id(): string
    {
        if($id = self::$ID){
            return $id;
        }

        self::$ID = substr(md5(uniqid()),0,10);
        return self::$ID;
    }

    /**
     * Writes out the message into the web log file.
     * @param string $message
     * @param int $blocks the # blocks to prepend into the message
     */

    public static function output(string $message,int $blocks = 1)
    {
        Log::channel(self::$CHANNEL)->info(self::id() . ' ' . str_repeat('#',$blocks) . ' ' . $message);
    }

    /**
     * Writes out the message into the web log file.
     * @param string $message
     * @param int $strips the arrow lenght to prepend into the message
     */

    public static function section(string $message,int $strips = 25,string $symbol = '-')
    {
        self::output(str_repeat($symbol,$strips) . '# ' . $message . ' #' . str_repeat($symbol,$strips));
    }


    /**
     * Writes out the message among # bars.
     * @param int $blocks the bar length after and before the message
     */

    public static function separator(int $blocks = 25)
    {
        $message = strtoupper(self::$CHANNEL);
        self::output(str_repeat('#',$blocks) . ' ' .  $message . ' ' . str_repeat('#',$blocks));
    }
}
