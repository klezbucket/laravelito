<?php
namespace Laravelito\Core\Site;

use Illuminate\Http\Request;
use Laravelito\Core\Site\SiteResolverInterface;

class SiteResolver implements SiteResolverInterface {
    
    /**
     * Debe devolver el nombre del Site actual, segun el Request proveido.
     *
     * @param Request $request
     * @return string
     */

    static function site(Request $request): string
    {
        return 'backend';
    }
}