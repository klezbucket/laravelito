<?php

namespace Laravelito\Core\Facades;

use Illuminate\Support\Str;
use Laravelito\Field\Field;
use Laravelito\Core\Facades\Site;
use Laravelito\Field\Type\Absent;
use Laravelito\Table\Table as TI;
use Laravelito\Core\Facades\Theme;
use Laravelito\Core\Facades\Router;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;


class Picker {
    /** @var string */
    const PAGE_PARAM = 'p';

    /**
     * Devuelve el esqueleto del table.
     *
     * @param TI $table
     * @return string
     */

    public static function layout(TI $table): string
    {
        return self::picker($table, 'table');
    }

    /**
     * Devuelve el partial para el icon
     *
     * @param TI $table
     * @return string
     */

    public static function icon(TI $table): string
    {
        return self::picker($table, 'icon');
    }

    /**
     * Devuelve el partial para el angle
     *
     * @param TI $table
     * @return string
     */

    public static function angle(TI $table): string
    {
        return self::picker($table, 'angle');
    }

    /**
     * Devuelve el partial para el body
     *
     * @param TI $table
     * @return string
     */

    public static function body(TI $table): string
    {
        return self::picker($table, 'body');
    }

    /**
     * Devuelve el override del detail o el partial proveido por el tema.
     *
     * Override example:
     *
     * @param TI $table
     * @param string $partial
     * @param Field|null $field
     * @return string
     */

    private static function picker(TI $table, string $partial, ?Field $field = null): string
    {
        $name = $table->name();
        $site = Site::resolv();
        $theme = Config::get('laravelito.theme.' . $site);

        if(isset($field)){
            $override = $site . '.' . $theme . '.picker.' . $name . '.' . $field->name() . '.' . $partial;

            if(view()->exists($override)){
                return $override;
            }
        }

        $override = $site . '.' . $theme . '.picker.' . $name . '.' . $partial;

        if(view()->exists($override)){
            return $override;
        }

        if(isset($field)){
            $override = $site . '.' . $theme . '.picker.fields.' . $field->name() . '.' . $partial;

            if(view()->exists($override)){
                return $override;
            }
        }

        return Theme::get('picker.' . $partial);
    }
}
