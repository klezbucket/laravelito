<?php

namespace Laravelito\Core\Facades;

use Illuminate\Support\Str;
use Laravelito\Core\Exceptions\NotFoundException;

class Loader {
    /**
     * Se encarga de verificar una clase y se asegura que pertenezca a $parent.
     *
     * @param string $root el namespace o al menos el prinicipio del mismo
     * @param string $path el path de la clase separado por puntos, notese que separamos por puntos y cada parte se cameliza
     * @param string $parent la clase a la cual debe pertenecer el objeto
     * @throws NotFoundException si la clase no existe o no honra a $parent
     * @return string
     */

    public static function getClass(string $root,string $path,string $parent)
    {
        $path = explode('.', $path);
        $class = $root;

        foreach($path as $p){
            $class .= '\\' . ucfirst(Str::camel($p));
        }

        if(! class_exists($class)){
            throw new NotFoundException('No such class: ' . $class);
        }

        if(! is_a($class,$parent,true)){
            throw new NotFoundException("{$class} is not a {$parent} subclass");
        }

        return $class;
    }
}
