<?php

namespace Laravelito\Core\Facades;

use Exception;
use Illuminate\Http\Request;
use Laravelito\Core\Facades\Router;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\RedirectResponse;

class Site {
    /** @var string */
    private static $site;

    /**
     * Resuelve el sitio actual desde el Request proveido.
     *
     * Implementa Laravelito\Core\Site\SiteResolverInterface y configura laravelito.sites.resolver con dicha clase
     * para sobreescribir esta funcionalidad.
     *
     * @param Request|null $request
     * @return string
     */

    public static function resolv(?Request $request = null): string
    {
        if(! isset($request)){
            $request = request();
        }

        if(! ($site = self::$site)){
            $resolver = Config::get('laravelito.sites.resolver');
            $site = call_user_func($resolver . '::site', $request);
        }

        self::$site = $site;
        return $site;
    }
}
