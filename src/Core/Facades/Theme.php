<?php

namespace Laravelito\Core\Facades;

use Illuminate\View\View;
use Laravelito\Form\Form;
use Illuminate\Http\Request;
use Laravelito\Core\Facades\Site;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * TODO: Server Side assets caching
 */

class Theme {
    /** @var array */
    private static $scripts = [];

    /** @var array */
    private static $externalScripts = [];

    /** @var array */
    private static $stylesheets = [];

    /** @var array */
    private static $externalStylesheets = [];

    /** @var array */
    private static $deferred = [];

    /** @var string */
    private static $theme;

    /** @var string */
    private static $layout;

    /**
     * Setter/getter para vistas atrasadas.
     * @param string|null $view
     * @return array
     */

    public static function deferred(?string $view = null): array
    {   
        if(isset($view)){
            if(! in_array($view, self::$deferred)){
                self::$deferred[] = $view;
            }
        }

        return self::$deferred;
    }
    
    
    /**
     * Guarda un nuevo stylesheet a ser rendereado luego.
     * @param string $stylesheet
     * @return void
     */

    public static function externalStylesheet(string $stylesheet): void
    {
        if(! in_array($stylesheet, self::$externalStylesheets)){
            self::$externalStylesheets[] = $stylesheet;
        }
    }

    /**
     * Getter para $externalStylesheets.
     * @return array
     */

    public static function externalStylesheets(): array
    {
        return self::$externalStylesheets;
    }

    /**
     * Guarda un nuevo stylesheet a ser rendereado luego.
     * @param string $stylesheet
     * @return void
     */

    public static function stylesheet(string $stylesheet): void
    {
        if(! in_array($stylesheet, self::$stylesheets)){
            self::$stylesheets[] = $stylesheet;
        }
    }

    /**
     * Getter para $stylesheets.
     * @return array
     */

    public static function stylesheets(): array
    {
        return self::$stylesheets;
    }

    /**
     * Guarda un nuevo script a ser rendereado luego.
     * @param string $script
     * @param array $attrs
     * @return void
     */

    public static function externalScript(string $script, array $attrs = []): void
    {
        if(! in_array($script, self::$externalScripts)){
            self::$externalScripts[] = $script;
        }
    }

    /**
     * Getter para $externalScripts.
     * @return array
     */

    public static function externalScripts(): array
    {
        return self::$externalScripts;
    }

    /**
     * Guarda un nuevo script a ser rendereado luego.
     * @param string $script
     * @return void
     */

    public static function script(string $script): void
    {
        if(! in_array($script, self::$scripts)){
            self::$scripts[] = $script;
        }
    }

    /**
     * Getter para $scripts.
     * @return array
     */

    public static function scripts(): array
    {
        return self::$scripts;
    }

    /**
     * Devuelve el layout actual.
     */

    public static function currentLayout(): string
    {
        return self::$layout;
    }

    /**
     * Devuelve el tema actual.
     *
     * @param Request|null $request
     * @return string
     */

    public static function resolv(?Request $request = null): string{
        if(! isset($request)){
            $request = request();
        }

        if(! ($theme = self::$theme)){
            $site = Site::resolv($request);
            $theme =  'vendor/laravelito/' . Config::get('laravelito.theme.' . $site);
        }

        self::$theme = $theme;
        return $theme;
    }

    /**
     * Devuelve el override del tema, o la vista del tema propio.
     *
     * Override example:
     *
     * resource/views/backend/looper/partials/backend/account/summary.blade.php -> override del resumen de la cuenta en la barra superior del layout
     *
     *
     * @param string
     * @return string
     */

    public static function get(string $view): string
    {
        $site = Site::resolv();
        $theme = Config::get('laravelito.theme.' . $site);
        $me = Router::me();
        $override = $site . '.' . $theme . '.' . $me . '.' . $view;
        
        if(view()->exists($override)){
            return $override;
        }

        $override = $site . '.' . $theme . '.' . $view;

        if(view()->exists($override)){
            return $override;
        }

        return self::resolv() . '.' . $view;
    }

    /**
     * Devuelve una vista.
     *
     * @param string $view
     * @return View
     */

    public static function view(string $view): View
    {
        return view(self::get($view));
    }

    /**
     * Devuelve el layout del tema actual.
     *
     * @param string $layout
     * @return string
     */

    public static function layout(string $layout): string
    {
        return self::get('layouts.' . $layout);
    }

    /**
     * Devuelve la vista de una accion del tema actual, renderea la vista controller,
     * la cual diponse del include del layout y un parcial auxiliar que renderea los
     * yields dentro de parciales.
     *
     * @param string $action
     * @param string $layout
     * @param string|null $partial
     * @return View
     */

    public static function controller(string $action,string $layout,?string $partial = null): View
    {
        self::$layout = $layout;

        if(! isset($partial)){
            $partial = $layout . '.' . 'layout';
        }

        return view(self::core('controller'),[
            'action' => $action,
            'layout' => $layout,
            'partial' => $partial,
        ]);
    }

    /**
     * Devuelve la vista de exception.
     *
     * @param HttpException $exception
     * @return Response
     */

    public static function exception(HttpException $exception): Response
    {
        return response()->view(self::core('controller'),[
            'action' => 'exception',
            'layout' => 'error',
            'partial' => 'error.layout',
            'exception' => $exception
        ], $exception->getStatusCode());
    }

    /**
     * Devuelve el asset del tema actual.
     *
     * @param string $asset
     * @return string
     */

    public static function asset(string $asset): string
    {
        $site = Site::resolv(request());
        $public = public_path() . '/' . $site . '/' . $asset;
        if(file_exists($public)){
            return asset($site . '/' . $asset);
        }

        return asset(self::resolv(request()) . '/' . $asset);
    }

    /**
     * Determina si el partial existe.
     *
     * @param string $partial
     * @return string
     */

    public static function exists(string $partial): string
    {
        $view = self::get($partial);
        return view()->exists($view);
    }

    /**
     * Devuelve el partial optional
     *
     * @param string $partial
     * @return string
     */

    public static function optional(string $partial): string
    {
        $view = self::get($partial);

        if(view()->exists($view)){
            return $view;
        }

        return Theme::core('blank');
    }

    /**
     * Devuelve el action del tema actual.
     *
     * @param string $action
     * @return string
     */

    public static function action(string $action): string
    {
        return self::get('actions.' . $action);
    }

    /**
     * Devuelve el partial del tema actual.
     *
     * @param string $partial
     * @return string
     */

    public static function partial(string $partial): string
    {
        return self::get('partials.' . $partial);
    }

    /**
     * Devuelve el partial del core.
     *
     * @param string $partial
     * @return string
     */

    public static function core(string $partial): string
    {

        return 'vendor/laravelito.core.' . $partial;
    }

    /**
     * @var string
     */

    private static $active = '';

    /**
     * Guarda/Obtiene el link activo del menu
     *
     * @param string|null $url
     * @return string
     */

    public static function active(?string $url = null): string
    {
        if(isset($url)){
            self::$active = $url;
        }

        return self::$active;
    }

}
