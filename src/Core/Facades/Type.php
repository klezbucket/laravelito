<?php

namespace Laravelito\Core\Facades;

use NumberFormatter;
use Illuminate\Support\Str;
use Laravelito\Field\Field;
use Laravelito\Locales\Locale;
use Laravelito\Field\Type\Enum;
use Laravelito\Core\Facades\Site;
use Laravelito\Core\Facades\Theme;
use Laravelito\Core\Facades\Printer;
use Illuminate\Support\Facades\Config;
use Laravelito\CRUD\Model\ModelProvider;

class Type {

    /**
     * Devuelve el Id de ayuda del par provider, field
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string
     */

    public static function id(ModelProvider $provider, Field $field): string
    {
        return str_replace('.','_',Str::camel($provider->name() . '_' . $field->name()));
    }

    /**
     * Devuelve el valor para los boolean.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function booleanValue(ModelProvider $provider,Field $field): ?string
    {            
        return t($field->type()->label($provider->value($field) ? true : false));
    }

    /**
     * Devuelve el valor para los foreign.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function foreignValue(ModelProvider $provider,Field $field): ?string
    {
        if($id = $provider->value($field)){
            return $field->type()->text($id);
        }

        return null;
    }

    /**
     * Devuelve el valor para los date.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function dateValue(ModelProvider $provider,Field $field): ?string
    {
        return Printer::dateValue($provider->value($field));
    }

    /**
     * Devuelve el valor para los datetime.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function datetimeValue(ModelProvider $provider,Field $field): ?string
    {
        return Printer::datetimeValue($provider->value($field));
    }

    /**
     * Devuelve el valor para los ago.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function agoValue(ModelProvider $provider,Field $field): ?string
    {
        return Printer::agoValue($provider->value($field));
    }

    /**
     * Devuelve el valor para los integers.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function decimalValue(ModelProvider $provider,Field $field): ?string
    {
        return Printer::decimalValue($provider->value($field), $field->type()->decimals());
    }

    /**
     * Devuelve el valor para los integers.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function integerValue(ModelProvider $provider,Field $field): ?string
    {
        return Printer::integerValue($provider->value($field));
    }

    /**
     * Devuelve el valor el tipo time.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function timeValue(ModelProvider $provider,Field $field): ?string
    {
        if($raw = $provider->value($field)){
            $fragments = explode(':',$raw);
    
            return implode(':',[
                $fragments[0],
                $fragments[1]
            ]);
        }

        return null;
    }

    /**
     * Devuelve el valor para los text.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function textValue(ModelProvider $provider,Field $field): ?string
    {
        return $provider->value($field);
    }

    /**
     * Devuelve el valor html los rich text.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function htmlValue(ModelProvider $provider,Field $field): ?string
    {
        if($value = $provider->value($field)){
            return htmlentities($value);
        }

        return null;
    }

    /**
     * Devuelve el valor para los email.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function emailValue(ModelProvider $provider,Field $field): ?string
    {
        return $provider->value($field);
    }

    /**
     * Devuelve el valor para los enum.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function enumValue(ModelProvider $provider,Field $field): ?string
    {
        if($option = $provider->value($field)){
            $enum = Enum::cast($field->type());

            if($enum->translate()){
                return t($field->type()->option($option));
            } else {
                return $enum->options()[$option];
            }
        }

        return null;
    }

    /**
     * Imprime el valor proveido
     * 
     * @param ModelProvider $provider
     * @param Field $field
     * @param string|null $value
     * @return string
     */

    public static function print(ModelProvider $provider,Field $field,?string $value): string
    {
        if(isset($value)){
            return self::type($provider,'print.string',$field);
        }

        return self::printNull($provider,$field);
    }

    /**
     * Imprime el valor de null
     * 
     * @param ModelProvider $provider
     * @param Field $field
     * @return string
     */

    public static function printNull(ModelProvider $provider,Field $field): string
    {
        return self::type($provider,'print.null',$field);
    }

    /**
     * Imprime el valor segun el tipo de dato
     * 
     * @param ModelProvider $provider
     * @param Field $field
     * @return string
     */

    public static function value(ModelProvider $provider,Field $field): string
    {
        return self::type($provider,$field->type()->name(),$field);
    }

    /**
     * Devuelve el override del link o el partial proveido por el tema.
     *
     *
     * @param ModelProvider $provider
     * @param string $partial
     * @param Field $field
     * @return string
     */

    private static function type(ModelProvider $provider,string $partial, Field $field): string
    {
        $name = $provider->name();
        $site = Site::resolv();
        $theme = Config::get('laravelito.theme.' . $site);
        $override = $site . '.' . $theme . '.types.' . $name . '.' . $field->name() . '.' . $partial;

        if(view()->exists($override)){
            return $override;
        }

        $override = $site . '.' . $theme . '.types.' .  '.' . $field->name() . '.' . $partial;

        if(view()->exists($override)){
            return $override;
        }

        return Theme::get('types'.  '.' . $partial);
    }
}