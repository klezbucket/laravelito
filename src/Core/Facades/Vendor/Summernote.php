<?php

namespace Laravelito\Core\Facades\Vendor;

use Laravelito\Field\Field;
use Laravelito\CRUD\Model\ModelProvider;

class Summernote {
    /** @var array */
    private static $MAPPING = [
        'es' => 'es-ES'
    ];

    /**
     * Mapea el idioma actual del framework a uno de los compatibles con Summernote,
     * si no existe la relacion devuelve null y por ende no deberia cargarse archivo
     * de locales (default ingles americano).
     * 
     * @return string|null
     */

    public static function locale(): ?string
    {
        $locale = app()->getLocale();
        return self::$MAPPING[$locale] ?? null;
    }
    /**
     * Devuelve el label del link de descarga.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function link(ModelProvider $provider,Field $field): ?string
    {            
        return t($field->type()->link());
    }
}