<?php

namespace Laravelito\Core\Facades;

use Illuminate\Support\Str;
use Laravelito\Core\Facades\Router;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\RedirectResponse;

class Router {
    /**
     * Devuelve el nombre del metodo actual.
     *
     * @return string
     */

    public static function method(): string
    {
        if($route = request()->route()){
            if($uses = request()->route()->getAction()['uses']){
                $uses = explode('@', $uses);

                if($method = ($uses[1] ?? null)){
                    return $method;
                }
            }
        }

        return 'error';
    }
    /**
     * Devuelve el nombre del controlador actual.
     *
     * @param bool $namespaced
     * @return string
     */

    public static function controller(bool $namespaced = false): string
    {
        if($route = request()->route()){
            if($uses = request()->route()->getAction()['uses']){
                $uses = explode('@', $uses);

                if($controller = ($uses[0] ?? null)){
                    if($namespaced){
                        return $controller;
                    }

                    $controller = explode('\\', $controller);
                    return end($controller);
                }
            }
        }

        return 'error';
    }

    /**
     * Devuelve el nombre de la ruta actual.
     *
     * @return string
     */

    public static function me(): string
    {
        if($route = request()->route()){
            $me = request()->route()->getAction()['as'];
            return preg_replace('/#.*$/','',$me);
        }

        return 'error';
    }

    /**
     * Devuelve la URL actual con los parametros pasados como querystring.
     *
     * @param array $params
     * @return string
     */

    public static function hereWithParams(array $params = []): string
    {
        $url = parse_url(self::here());
        $query = [];
        parse_str($url['query'] ?? '', $query);
        $query = array_merge($query,$params);
        $querystring = http_build_query($query);
        $full =
                $url['scheme'] . '://' .
                $url['host'] .
                $url['path'];

        if(strlen($querystring) > 0){
            $full .= '?' . $querystring;
        }

        return $full;
    }

    /**
     * Devuelve la URL actual
     *
     * @return string
     */

    public static function here(): string
    {
        return url()->full();
    }

    /**
     * Obtiene los parametros de una ruta.
     *
     * @param string $route
     * @return array
     */

    public static function params(string $route): array
    {
        $params = [];
        $matches = [];

        $url = __('routes.' . $route);
        preg_match_all('/\{(.*?)\}/', $url, $matches);

        if(isset($matches[1])){
            $params = $matches[1];
        }

        return $params;
    }

    /**
     * Deveulve una redireccion a la url especificada con los parametros del modelo especificado.
     *
     * @param string $route
     * @param Model|null $model
     * @return string
     */

    public static function route(string $route,?Model $model = null): string
    {
        $routeParams = [];

        if(isset($model)){
            $params = self::params($route);

            foreach($params as $param){
                $routeParams[$param] = Str::slug($model->{$param});
            }
        }

        return route($route, $routeParams);
    }

    /**
     * Deveulve una redireccion a la url especificada con los parametros del modelo especificado.
     *
     * @param string $route
     * @param Model|null $model
     * @return RedirectResponse
     */

    public static function redirect(string $route,?Model $model = null): RedirectResponse
    {
        return redirect(self::route($route,$model));
    }
}
