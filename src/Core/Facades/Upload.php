<?php

namespace Laravelito\Core\Facades;

use Laravelito\Field\Field;
use Laravelito\CRUD\Model\ModelProvider;


class Upload {
    /**
     * Devuelve el label del link de descarga.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function link(ModelProvider $provider,Field $field): ?string
    {            
        return t($field->type()->link());
    }

    /**
     * Devuelve la URL del link de descarga.
     *
     * @param ModelProvider $provider
     * @param Field $field
     * @return string|null
     */

    public static function url(ModelProvider $provider,Field $field): ?string
    {            
        if($model = $provider->model()){
            return $field->type()->url($model);
        }

        return '';
    }
}