<?php

namespace Laravelito\Core\Facades;

use Carbon\Carbon;
use NumberFormatter;
use Laravelito\Locales\Locale;

class Printer {
    /**
     * Imprime un decimal, alias de floatValue.
     * @param string|null $value
     * @return string|null
     */

    public static function decimalValue(?string $value,int $decimals = 2): ?string
    {

        return self::floatValue($value,$decimals);
    }

    /**
     * Imprime un float
     * @param string|null $value
     * @return string|null
     */

    public static function floatValue(?string $value,int $decimals = 2): ?string
    {
        if(is_null($value)){
            return null;
        }

        $intval = (int) $value;

        if($intval > 0){
            if(($value / $intval) == 1){
                return self::integerValue($value);
            }
        }
        else if($value == 0){
            return '0';
        }
        $formatter = new NumberFormatter(Locale::current()->envlocale(),NumberFormatter::DECIMAL);
        $formatter->setAttribute(NumberFormatter::FRACTION_DIGITS, $decimals); 
        return $formatter->format($value);
    }

    /**
     * Imprime un entero
     * @param string|null $value
     * @return string|null
     */

    public static function integerValue(?string $value): ?string
    {
        if(is_null($value)){
            return null;
        }

        $formatter = new NumberFormatter(Locale::current()->envlocale(),NumberFormatter::DECIMAL);
        $formatter->setAttribute(NumberFormatter::FRACTION_DIGITS, 0); 
        return $formatter->format($value);
    }

    /**
     * Imprime un DATE  
     * @param string|null $value
     * @return string|null
     */

    public static function dateValue(?string $value): ?string
    {
        if(is_null($value)){
            return null;
        }
        
        return strftime('%d %B %Y', strtotime($value));
    }

    /**
     * Imprime un datetime  
     * @param string|null $value
     * @return string|null
     */

    public static function datetimeValue(?string $value): ?string
    {
        if(is_null($value)){
            return null;
        }
        
        return strftime('%c', strtotime($value));
    }

    /**
     * Imprime un datetime de la forma ago.
     * @param string|null $value
     * @return string|null
     */

    public static function agoValue(?string $value): ?string
    {
        if(is_null($value)){
            return null;
        }
        
        return Carbon::parse($value)->diffForHumans();
    }
}