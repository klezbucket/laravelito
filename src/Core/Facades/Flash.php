<?php

namespace Laravelito\Core\Facades;

use Laravelito\Core\Facades\Flash;
use Illuminate\Support\Facades\Session;

class Flash {
    /** @var string */
    private $message;

    /** @var string */
    private $icon;

    /** @var string */
    private $title;

    /** @var string */
    private $type;

    /** @var string */
    const KEY = 'laravelito';

    /**
     * Crea una instancia del flash.
     *
     * @param string $type
     * @param string $message
     * @param string|null $icon
     * @param string|null $title
     * @return void
     */

    private function __construct(string $type,string $message,?string $icon = null,?string $title = null)
    {
        $this->message = $message;
        $this->icon = $icon;
        $this->title = $title;
        $this->type = $type;
    }

    /**
     * Getter para message
     *
     * @return string
     */

    public function message(): string
    {
        return $this->message;
    }

    /**
     * Getter para type
     *
     * @return string
     */

    public function type(): string
    {
        return $this->type;
    }

    /**
     * Getter para icon
     *
     * @return ?string
     */

    public function icon(): ?string
    {
        return $this->icon;
    }

    /**
     * Getter para title
     *
     * @return string|null
     */

    public function title(): ?string
    {
        return $this->title;
    }

    /**
     * Crear un nuevo success flash message y lo guarda en sesion.
     *
     * @param string $message
     * @param string|null $icon
     * @param string|null $title
     * @return Flash
     */

    public static function success(string $message,?string $icon = null,?string $title = null): Flash
    {
        $flash = new Flash('success',$message,$icon,$title);
        Session::flash(self::KEY, $flash->serialize());
        return $flash;
    }

    /**
     * Crear un nuevo danger flash message y lo guarda en sesion.
     *
     * @param string $message
     * @param string|null $icon
     * @param string|null $title
     * @return Flash
     */

    public static function danger(string $message,?string $icon = null,?string $title = null): Flash
    {
        $flash = new Flash('danger',$message,$icon,$title);
        Session::flash(self::KEY, $flash->serialize());
        return $flash;
    }

    /**
     * Crear un nuevo warning flash message y lo guarda en sesion.
     *
     * @param string $message
     * @param string|null $icon
     * @param string|null $title
     * @return Flash
     */

    public static function warning(string $message,?string $icon = null,?string $title = null): Flash
    {
        $flash = new Flash('warning',$message,$icon,$title);
        Session::flash(self::KEY, $flash->serialize());
        return $flash;
    }

    /**
     * Crear un nuevo primary flash message y lo guarda en sesion.
     *
     * @param string $message
     * @param string|null $icon
     * @param string|null $title
     * @return Flash
     */

    public static function primary(string $message,?string $icon = null,?string $title = null): Flash
    {
        $flash = new Flash('primary',$message,$icon ?? 'info',$title);
        Session::flash(self::KEY, $flash->serialize());
        return $flash;
    }


    /**
     * Devuelve un flash de session si existe.
     *
     * @param string $message
     * @param string|null $icon
     * @param string|null $title
     * @return Flash|null
     */

    public static function unserialize(): ?Flash
    {
        if(Session::has(self::KEY)){
            $data = Session::get(self::KEY);
            $flash = new Flash($data['type'],$data['message'],$data['icon'],$data['title']);
            Session::forget(self::KEY);
            return $flash;
        }

        return null;
    }

    /**
     * Devuelve un array con la data del flash
     *
     * @return array
     */

    private function serialize(): array
    {
        return [
            'icon' => $this->icon,
            'title' => $this->title,
            'message' => $this->message,
            'type' => $this->type,
        ];
    }
}
