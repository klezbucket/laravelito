<?php

namespace Laravelito\Core\Facades;

use Illuminate\Support\Str;
use Laravelito\Field\Field;
use Laravelito\Core\Facades\Site;
use Laravelito\Field\Type\Absent;
use Illuminate\Support\Collection;
use Laravelito\Core\Facades\Theme;
use Laravelito\Detail\Detail as DI;
use Illuminate\Support\Facades\Config;
use Laravelito\Field\FieldDependences;


class Detail {
    /**
     * Devuelve el esqueleto del detail.
     *
     * @param DI $detail
     * @return string
     */

    public static function layout(DI $detail): string
    {
        return self::detail($detail, 'detail');
    }

    /**
     * Devuelve el partial del before si existe, sino devuelve la plantilla en blanco
     *
     * @param DI $detail
     * @return string
     */

    public static function before(DI $detail): string
    {
        $view = self::detail($detail, 'before');

        if(view()->exists($view)){
            return $view;
        }

        return Theme::core('blank');
    }

    /**
     * Devuelve el partial del after si existe, sino devuelve la plantilla en blanco
     *
     * @param DI $detail
     * @return string
     */

    public static function after(DI $detail): string
    {
        $view = self::detail($detail, 'after');

        if(view()->exists($view)){
            return $view;
        }

        return Theme::core('blank');
    }

    /**
     * Devuelve los campos, excluyendo las dependencias.
     *
     * @param DI $detail
     * @return Collection
     */

    public static function fields(DI $detail): array
    {
        $fields = [];
        $values = [];

        foreach($detail->fields() as $field){
            $fields[$field->name()] = $field;
            $values[$field->name()] = $detail->value($field);
        }

        foreach(FieldDependences::shutdown($detail,$values) as $field){
            unset($fields[$field]);
        }

        return $fields;
    }

    /**
     * Devuelve el partial con el cargador de atributos para el field
     *
     * @param DI $detail
     * @param Field|null $field
     * @return string
     */

    public static function fieldAttributes(DI $detail, ?Field $field): string
    {
        return self::detail($detail, 'field_attributes', $field);
    }

    /**
     * Devuelve el partial especificado para un atributo del field para el par form,field
     *
     * @param DI $detail
     * @param string $partial
     * @param Field|null $field
     * @return string
     */

    public static function fieldAttribute(DI $detail, string $partial, ?Field $field): string
    {
        return self::detail($detail, 'field.attributes.' . $partial, $field);
    }

    /**
     * Devuelve el partial con el texto del field.
     *
     * @param DI $detail
     * @param Field|null $field
     * @return string
     */

    public static function fieldText(DI $detail, ?Field $field): string
    {
        return self::detail($detail, 'field_text', $field);
    }

    /**
     * Devuelve el field del par detail,field
     *
     * @param DI $detail
     * @param Field $field
     * @return string
     */

    public static function field(DI $detail, Field $field): string
    {
        if($field->type() instanceof Absent){
            return Theme::core('blank');
        }
        
        return self::detail($detail, 'field', $field);
    }

    /**
     * Devuelve el field tag del par detail,field
     *
     * @param DI $detail
     * @param Field $field
     * @return string
     */

    public static function fieldTag(DI $detail, Field $field): string
    {
        return self::detail($detail, 'field_tag', $field);
    }

    /**
     * Devuelve el partial con el cargador de atributos para el value
     *
     * @param DI $detail
     * @param Field|null $value
     * @return string
     */

    public static function valueAttributes(DI $detail, ?Field $field): string
    {
        return self::detail($detail, 'value_attributes', $field);
    }

    /**
     * Devuelve el partial especificado para un atributo del value para el par form,field
     *
     * @param DI $detail
     * @param string $partial
     * @param Field|null $field
     * @return string
     */

    public static function valueAttribute(DI $detail, string $partial, ?Field $field): string
    {
        return self::detail($detail, 'value.attributes.' . $partial, $field);
    }

    /**
     * Devuelve el partial con el texto del value.
     *
     * @param DI $detail
     * @param Field|null $field
     * @return string
     */

    public static function valueText(DI $detail, ?Field $field): string
    {
        return self::detail($detail, 'value_text', $field);
    }

    /**
     * Devuelve el value del par detail,field
     *
     * @param DI $detail
     * @param Field $field
     * @return string
     */

    public static function value(DI $detail, Field $field): string
    {
        return self::detail($detail, 'value', $field);
    }

    /**
     * Devuelve el value tag del par detail,field
     *
     * @param DI $detail
     * @param Field $field
     * @return string
     */

    public static function valueTag(DI $detail, Field $field): string
    {
        return self::detail($detail, 'value_tag', $field);
    }

    /**
     * Devuelve el partial con el cargador de atributos para el label
     *
     * @param DI $detail
     * @param Field|null $field
     * @return string
     */

    public static function labelAttributes(DI $detail, ?Field $field): string
    {
        return self::detail($detail, 'label_attributes', $field);
    }

    /**
     * Devuelve el partial con el cargador de valores por tipo de campo
     *
     * @param DI $detail
     * @param Field|null $field
     * @return string
     */

    public static function valueByType(DI $detail, Field $field): string
    {
        return self::detail($detail, 'value.types.' . $field->type()->name() , $field);
    }

    /**
     * Devuelve el partial especificado para un atributo del label para el par form,field
     *
     * @param DI $detail
     * @param string $partial
     * @param Field|null $field
     * @return string
     */

    public static function labelAttribute(DI $detail, string $partial, ?Field $field): string
    {
        return self::detail($detail, 'label.attributes.' . $partial, $field);
    }

    /**
     * Devuelve el partial con el texto del label.
     *
     * @param DI $detail
     * @param Field|null $field
     * @return string
     */

    public static function labelText(DI $detail, ?Field $field): string
    {
        return self::detail($detail, 'label_text', $field);
    }

    /**
     * Devuelve el label del par detail,field
     *
     * @param DI $detail
     * @param Field $field
     * @return string
     */

    public static function label(DI $detail, Field $field): string
    {
        return self::detail($detail, 'label', $field);
    }

    /**
     * Devuelve el label tag del par detail,field
     *
     * @param DI $detail
     * @param Field $field
     * @return string
     */

    public static function labelTag(DI $detail, Field $field): string
    {
        return self::detail($detail, 'label_tag', $field);
    }

    /**
     * Devuelve el Id de ayuda del par detail, field
     *
     * @param DI $detail
     * @param Field $field
     * @return string
     */

    public static function id(DI $detail, Field $field): string
    {
        return Str::camel($detail->name() . '_' . $field->name());
    }

    /**
     * Devuelve el ID para yielding
     *
     * @param DI $detail
     * @param string $yielding
     * @param Field|null $field
     * @return string
     */

    public static function yield(DI $detail, string $yielding, ?Field $field): string
    {
        return self::id($detail,$field) . ucfirst(Str::camel($yielding));
    }

    /**
     * Devuelve el override del detail o el partial proveido por el tema.
     *
     * Override example:
     *
     * @param DI $detail
     * @param string $partial
     * @param Field|null $field
     * @return string
     */

    private static function detail(DI $detail, string $partial, ?Field $field = null): string
    {
        $name = $detail->name();
        $site = Site::resolv();
        $theme = Config::get('laravelito.theme.' . $site);

        if(isset($field)){
            $override = $site . '.' . $theme . '.detail.' . $name . '.' . $field->name() . '.' . $partial;

            if(view()->exists($override)){
                return $override;
            }
        }

        $override = $site . '.' . $theme . '.detail.' . $name . '.' . $partial;

        if(view()->exists($override)){
            return $override;
        }

        if(isset($field)){
            $override = $site . '.' . $theme . '.detail.fields.' . $field->name() . '.' . $partial;

            if(view()->exists($override)){
                return $override;
            }
        }

        return Theme::get('detail.' . $partial);
    }
}
