<?php

namespace Laravelito\Core\Facades;

use Closure;
use Illuminate\Support\Str;
use Laravelito\Field\Field;
use Laravelito\Routing\UrlGuard;
use Laravelito\Core\Facades\Site;
use Laravelito\Core\Facades\Theme;
use Laravelito\Core\Facades\Router;
use Laravelito\View\Link\Link as LI;
use Illuminate\Support\Facades\Config;
use Laravelito\View\Link\LinkProvider;

class Link {
    /** @var string */
    private static $FLAVOR;

    /**
     * Devuelve la URL del link.
     * 
     * @param LinkProvider $provider
     * @param LI $link
     * @return string
     */

    public static function url(LinkProvider $provider, LI $link): string
    {
        $route = $link->route();

        if($link->resolv()){
            if($resolver = $link->resolver()){
                return Router::route($route, $resolver($provider->model()));
            }

            return Router::route($route, $provider->model());
        }
        else{
            return $route;
        }
    }

    /**
     * Devuelve el link tag del par provider,link
     *
     * @param LinkProvider $provider
     * @param LI $link
     * @return string
     */

    public static function tag(LinkProvider $provider, LI $link): string
    {
        return self::link($provider, 'link_tag', $link);
    }

    /**
     * Devuelve el partial del link.
     * Si no posee permisos sobre el link, imprime la plantilla en blanco.
     *
     * @param LinkProvider $provider
     * @param LI $link
     * @param string $flavor
     * @return string
     */

    public static function provide(LinkProvider $provider, LI $link, string $flavor): string
    {
        if(UrlGuard::access($link->route())){
            self::$FLAVOR = $flavor;
            return self::link($provider, 'link', $link);
        }
        else{
            return Theme::core('blank');
        }
    }

    /**
     * Devuelve el partial con el cargador de atributos para el field
     *
     * @param LinkProvider $provider
     * @param LI $link
     * @return string
     */

    public static function attributes(LinkProvider $provider, LI $link): string
    {
        return self::link($provider, 'link_attributes', $link);
    }

    /**
     * Devuelve el partial especificado para un atributo del field para el par form,field
     *
     * @param LinkProvider $provider
     * @param string $partial
     * @param LI $link
     * @return string
     */

    public static function attribute(LinkProvider $provider, string $partial, LI $link): string
    {
        return self::link($provider, 'attributes.' . $partial, $link);
    }

    /**
     * Devuelve el partial con el texto del link.
     *
     * @param LinkProvider $provider
     * @param LI $link
     * @return string
     */

    public static function text(LinkProvider $provider, LI $link): string
    {
        return self::link($provider, 'link_text', $link);
    }

    /**
     * Devuelve el partial con el icon del link.
     *
     * @param LinkProvider $provider
     * @param LI $link
     * @return string
     */

    public static function icon(LinkProvider $provider, LI $link): string
    {
        if($icon = $link->icon()){
            return self::link($provider, 'link_icon', $link);
        }
        else{
            return Theme::core('blank');
        }
    }

    /**
     * Devuelve el Id del par provider, link
     *
     * @param LinkProvider $provider
     * @param LI $link
     * @return string
     */

    public static function id(LinkProvider $provider, LI $link): string
    {
        return Str::camel($provider->name() . '_' . $link->route());
    }

    /**
     * Devuelve el ID para yielding
     *
     * @param LinkProvider $provider
     * @param string $yielding
     * @param LI $link
     * @return string
     */

    public static function yield(LinkProvider $provider, string $yielding, LI $link): string
    {
        return self::id($provider,$link) . ucfirst(Str::camel($yielding));
    }

    /**
     * Devuelve el override del link o el partial proveido por el tema.
     *
     *
     * @param LinkProvider $provider
     * @param string $partial
     * @param LI $link
     * @return string
     */

    private static function link(LinkProvider $provider, string $partial, LI $link): string
    {
        $name = $provider->name();
        $site = Site::resolv();
        $theme = Config::get('laravelito.theme.' . $site);
        $override = $site . '.' . $theme . '.links.' . self::$FLAVOR.  '.' . $name . '.' . $link->route() . '.' . $partial;

        if(view()->exists($override)){
            return $override;
        }

        $override = $site . '.' . $theme . '.links.' . self::$FLAVOR.  '.' . $name . '.' . $partial;

        if(view()->exists($override)){
            return $override;
        }

        return Theme::get('links.' . self::$FLAVOR.  '.' . $partial);
    }
}
