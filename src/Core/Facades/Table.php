<?php

namespace Laravelito\Core\Facades;

use Illuminate\Support\Str;
use Laravelito\Field\Field;
use Laravelito\Core\Facades\Site;
use Laravelito\Field\Type\Absent;
use Laravelito\Table\Table as TI;
use Laravelito\Core\Facades\Theme;
use Laravelito\Core\Facades\Router;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;


class Table {
    /** @var string */
    const PAGE_PARAM = 'p';

    /**
     * Devuelve el esqueleto del table.
     *
     * @param TI $table
     * @return string
     */

    public static function layout(TI $table): string
    {
        return self::table($table, 'table');
    }

    /**
     * Devuelve el summary de la tabla.
     *
     * @param TI $table
     * @return string
     */

    public static function summary(TI $table): string
    {
        return self::table($table, 'summary');
    }

    /**
     * Devuelve el summary de la tabla.
     *
     * @param TI $table
     * @param Field $field
     * @return string
     */

    public static function th(TI $table,Field $field): string
    {
        if($field->type() instanceof Absent){
            return Theme::core('blank');
        }
        
        return self::table($table, 'th', $field);
    }

    /**
     * Devuelve el summary de la tabla.
     *
     * @param TI $table
     * @param Field $field
     * @return string
     */

    public static function td(TI $table,Field $field): string
    {
        if($field->type() instanceof Absent){
            return Theme::core('blank');
        }
        
        return self::table($table, 'td', $field);
    }

    /**
     * Devuelve la fila.
     * Setea el modelo con los datos recibidos.
     *
     * @param TI $table
     * @param Model $data(
     * @return string
     */

    public static function tr(TI $table,Model $data): string
    {
        $values = [];

        foreach($data->toArray() as $field => $value){
            $values[$field] = $data[$field];
        }
        
        $table->model()->fill($values);
        $key = $table->model()->getKeyName();
        $table->model()->{$key} = $data[$key];
        return self::table($table, 'tr');
    }

    /**
     * Devuelve el th de las acciones.
     *
     * @param TI $table
     * @return string
     */

    public static function actionsTh(TI $table): string
    {
        return self::table($table, 'actions_th');
    }

    /**
     * Devuelve la celda con acciones.
     *
     * @param TI $table
     * @return string
     */

    public static function actionsTd(TI $table): string
    {
        return self::table($table, 'actions_td');
    }

    /**
     * Devuelve los links de paginacion before.
     *
     * @param TI $table
     * @return string
     */

    public static function beforePageLink(TI $table): string
    {
        return self::table($table, 'before_page_link');
    }

    /**
     * Devuelve un link de paginacion
     *
     * @param TI $table
     * @return string
     */

    public static function pageLink(TI $table): string
    {
        return self::table($table, 'page_link');
    }

    /**
     * Devuelve los links de paginacion.
     *
     * @param TI $table
     * @return string
     */

    public static function pageLinks(TI $table): string
    {
        return self::table($table, 'page_links');
    }

    /**
     * Devuelve el link de paginacion.
     *
     * @param int $page
     * @return string
     */

    public static function page(int $page): string
    {
        $url = Router::hereWithParams([
            self::PAGE_PARAM => $page
        ]);
        return $url;
    }

    /**
     * Devuelve los links de paginacion after.
     *
     * @param TI $table
     * @return string
     */

    public static function afterPageLink(TI $table): string
    {
        return self::table($table, 'after_page_link');
    }

    /**
     * Devuelve dots en vez de link pages.
     *
     * @param TI $table
     * @return string
     */

    public static function pageDots(TI $table): string
    {
        return self::table($table, 'page_dots');
    }

    /**
     * Devuelve el paginador de la tabla.
     *
     * @param TI $table
     * @return string
     */

    public static function pagination(TI $table): string
    {
        return self::table($table, 'pagination');
    }

    /**
     * Devuelve el partial con el cargador de valores por tipo de campo
     *
     * @param TI $table
     * @param Field|null $field
     * @return string
     */

    public static function valueByType(TI $table, Field $field): string
    {
        return self::table($table, 'value.types.' . $field->type()->name() , $field);
    }

    /**
     * Devuelve el Id de ayuda del par detail, field
     *
     * @param DI $detail
     * @param Field $field
     * @return string
     */

    public static function id(DI $detail, Field $field): string
    {
        return Str::camel($detail->name() . '_' . $field->name());
    }

    /**
     * Devuelve el ID para yielding
     *
     * @param DI $detail
     * @param string $yielding
     * @param Field|null $field
     * @return string
     */

    public static function yield(DI $detail, string $yielding, ?Field $field): string
    {
        return self::id($detail,$field) . ucfirst(Str::camel($yielding));
    }

    /**
     * Devuelve el override del detail o el partial proveido por el tema.
     *
     * Override example:
     *
     * @param TI $table
     * @param string $partial
     * @param Field|null $field
     * @return string
     */

    private static function table(TI $table, string $partial, ?Field $field = null): string
    {
        $name = $table->name();
        $site = Site::resolv();
        $theme = Config::get('laravelito.theme.' . $site);

        if(isset($field)){
            $override = $site . '.' . $theme . '.table.' . $name . '.' . $field->name() . '.' . $partial;

            if(view()->exists($override)){
                return $override;
            }
        }

        $override = $site . '.' . $theme . '.table.' . $name . '.' . $partial;

        if(view()->exists($override)){
            return $override;
        }

        if(isset($field)){
            $override = $site . '.' . $theme . '.table.fields.' . $field->name() . '.' . $partial;

            if(view()->exists($override)){
                return $override;
            }
        }

        return Theme::get('table.' . $partial);
    }
}
