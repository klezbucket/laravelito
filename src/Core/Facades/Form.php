<?php

namespace Laravelito\Core\Facades;

use Illuminate\Support\Str;
use Laravelito\Field\Field;
use Laravelito\Form\Form as FI;
use Laravelito\Core\Facades\Site;
use Laravelito\Field\Type\Absent;
use Laravelito\Core\Facades\Theme;
use Illuminate\Support\Facades\Config;
use Laravelito\Field\Type\OptionsField;
use Laravelito\Field\Constraint\Constraint;
use Symfony\Component\CssSelector\Exception\InternalErrorException;

class Form {
    /**
     * Devuelve los mensajes de errores localizados del campo.
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function errors(FI $form, Field $field): string
    {
        $name = $field->name();
        $fname = $form->name();
        $vname = $field->validationsName();
        $namespace = $form->localesAt();
        $messages = [];

        foreach(self::$INVALID[$fname][$name] ?? [] as $error){
            $rule = str_replace('validation.' , '', $error);

            if($vname){
                $messages[] = t($namespace . '.fields.' . $vname .'.' . $error);
            }
            else{
                $messages[] = t($namespace . '.fields.' . $error);
            }
        }

        return implode('. ', $messages);
    }
    /**
     * Devuelve el esqueleto del form.
     *
     * @param FI $form
     * @return string
     */

    public static function layout(FI $form): string
    {
        return self::form($form, 'form');
    }

    /**
     * Devuelve el ID para yielding
     *
     * @param FI $form
     * @param string $yielding
     * @param Field|null $field
     * @return string
     */

    public static function yield(FI $form, string $yielding, ?Field $field): string
    {
        return self::id($form,$field) . ucfirst(Str::camel($yielding));
    }

    /**
     * Devuelve el partial para grupo del campo.
     * Si implementa type Absent, entonces renderea la plantilla en blanco.
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function group(FI $form, Field $field): string
    {
        if($field->type() instanceof Absent){
            return Theme::core('blank');
        }

        return self::form($form, 'group',$field);
    }

    /**
     * Devuelve el partial para switcher
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function switcher(FI $form, Field $field): string
    {
        return self::form($form, 'switcher',$field);
    }

    /**
     * Devuelve el selected para el campo actual si el value coincide
     *
     * @param FI $form
     * @param Field $field
     * @param string $value
     * @return string
     */

    public static function selected(FI $form, Field $field, string $value): string
    {
        return strcmp($value, $form->value($field)) === 0;
    }

    /**
     * Devuelve el texto del autocomplete hidden proviedo del request.
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function autocompleteText(FI $form,Field $field): string
    {
        return
            request()->get(self::autocompleteHidden($form,$field)) ?? $field->type()->text(Form::value($form,$field)) ?? '';
    }

    /**
     * Devuelve el id del autocomplete cleaner.
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function autocompleteClear(FI $form,Field $field): string
    {
        return str_replace('.','_',self::id($form,$field) . '_autocomplete_clear');
    }

    /**
     * Devuelve el id del upload cleaner.
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function uploadClear(FI $form,Field $field): string
    {
        return str_replace('.','_',self::id($form,$field) . '_upload_clear');
    }

    /**
     * Devuelve el nombre del autocomplete hidden.
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function autocompleteHidden(FI $form,Field $field): string
    {
        return str_replace('.','_',self::id($form,$field) . '_autocomplete');
    }

    /**
     * Devuelve un hidden.
     *
     * @param FI $form
     * @param Field|null $field
     * @return string
     */

    public static function hidden(FI $form, ?Field $field): string
    {
        return self::form($form,'hidden',$field);
    }

    /**
     * Devuelve el value del para form, field actual
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function value(FI $form, Field $field): string
    {
        return $form->value($field);
    }

    /**
     * Devuelve el value en html decoded
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function htmlVal(FI $form, Field $field): string
    {
        return htmlentities($form->value($field));
    }

    /**
     * Devuelve el Id de ayuda del par form, field
     *
     * @param FI $form
     * @param Field|null $field
     * @return string
     */

    public static function id(FI $form, ?Field $field = null): string
    {
        if(isset($field)){
            return str_replace('.','_',Str::camel($form->name() . '_' . $field->name()));
        }

        return str_replace('.','_',Str::camel($form->name()));
    }

    /**
     * Devuelve el Id del par form,field
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function described(FI $form, Field $field): string
    {
        return self::id($form,$field) . 'Help';
    }

    /**
     * Devuelve el partial del before si existe, sino devuelve la plantilla en blanco
     *
     * @param FI $form
     * @return string
     */

    public static function before(FI $form): string
    {
        $view = self::form($form, 'before');

        if(view()->exists($view)){
            return $view;
        }

        return Theme::core('blank');
    }

    /**
     * Devuelve el partial del after si existe, sino devuelve la plantilla en blanco
     *
     * @param FI $form
     * @return string
     */

    public static function after(FI $form): string
    {
        $view = self::form($form, 'after');

        if(view()->exists($view)){
            return $view;
        }

        return Theme::core('blank');
    }

    /**
     * Devuelve el partial del header si existe, sino devuelve la plantilla en blanco
     *
     * @param FI $form
     * @return string
     */

    public static function header(FI $form): string
    {
        $view = self::form($form, 'header');

        if(view()->exists($view)){
            return $view;
        }

        return Theme::core('blank');
    }

    /**
     * Devuelve el partial que carga el script de dependencias, si existen.
     * Si no devuelve la plantilla en blanco.
     *
     * @param FI $form
     * @return string
     */

    public static function dependences(FI $form): string
    {
        if($dependences = $form->dependences()){
            return self::form($form, 'dependences');
        }

        return Theme::core('blank');
    }

    /**
     * Devuelve el partial que carga los assets globales del form.
     *
     * @param FI $form
     * @return string
     */

    public static function assets(FI $form): string
    {
        return self::form($form, 'assets');
    }

    /**
     * Devuelve las validaciones del par form,field
     * Si el campo es valido devuelve la plantilla vacia.
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function validations(FI $form, Field $field): string
    {
        if(self::isValid($form,$field)){
            return Theme::core('blank');
        }

        return self::form($form, 'validations', $field);
    }

    /**
     * Devuelve el help del par form,field
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function help(FI $form, Field $field): string
    {
        return self::form($form, 'help', $field);
    }

    /**
     * Devuelve el layout del field
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function field(FI $form, Field $field): string
    {
        return self::form($form, 'field', $field);
    }

    /**
     * Devuelve el las opciones del select.
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function options(FI $form, Field $field): string
    {
        if(! ($field->type() instanceof OptionsField)){
            throw new InternalErrorException('Field ' . $field->name() . ' must implement OptionsField');
        }
        return self::form($form, 'options', $field);
    }

    /**
     * Imprime las opciones del select.
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function option(FI $form, Field $field): string
    {
        return self::form($form, 'option', $field);
    }

    /**
     * Devuelve el partial del par form,field segun su tipo
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function type(FI $form, Field $field): string
    {
        return self::form($form, 'types.' . $field->type()->name(), $field);
    }

    /**
     * Devuelve el label del par form,field
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function label(FI $form, Field $field): string
    {
        return self::form($form, 'label', $field);
    }

    /**
     * Devuelve el label tag del par form,field
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function labelTag(FI $form, Field $field): string
    {
        return self::form($form, 'label_tag', $field);
    }

    /**
     * Devuelve el select del par form,field
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function select(FI $form, Field $field): string
    {
        return self::form($form, 'select', $field);
    }

    /**
     * Devuelve el input del par form,field
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function input(FI $form, Field $field): string
    {
        return self::form($form, 'input', $field);
    }

    /**
     * Devuelve el textarea del par form,field
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function textarea(FI $form, Field $field): string
    {
        return self::form($form, 'textarea', $field);
    }

    /**
     * Devuelve el partial con el cargador de atributos
     *
     * @param FI $form
     * @param Field|null $field
     * @return string
     */

    public static function attributes(FI $form, ?Field $field): string
    {
        return self::form($form, 'attributes', $field);
    }


    /**
     * Devuelve el partial con el cargador de atributos para selects
     *
     * @param FI $form
     * @param Field|null $field
     * @return string
     */

    public static function selectAttributes(FI $form, ?Field $field): string
    {
        return self::form($form, 'select_attributes', $field);
    }

    /**
     * Devuelve el partial con el cargador de atributos para el label
     *
     * @param FI $form
     * @param Field|null $field
     * @return string
     */

    public static function labelAttributes(FI $form, ?Field $field): string
    {
        return self::form($form, 'label_attributes', $field);
    }

    /**
     * Devuelve el partial con el texto del label.
     *
     * @param FI $form
     * @param Field|null $field
     * @return string
     */

    public static function labelText(FI $form, ?Field $field): string
    {
        return self::form($form, 'label_text', $field);
    }

    /**
     * Devuelve el partial con elementos extra para el label.
     *
     * @param FI $form
     * @param Field|null $field
     * @return string
     */

    public static function labelExtras(FI $form, ?Field $field): string
    {
        return self::form($form, 'label_extras', $field);
    }

    /**
     * Devuelve el partial con elementos extra para el label.
     *
     * @param FI $form
     * @param Field|null $field
     * @return string
     */

    public static function labelExtraText(FI $form, ?Field $field): string
    {
        return self::form($form, 'label_extra_text', $field);
    }


    /**
     * Devuelve el partial para el label extra segun el constraint.
     * Si no existe se devuelve la plantilla en blanco.
     *
     * @param FI $form
     * @param Constraint $constraint
     * @param Field $field
     * @return string
     */

    public static function labelExtra(FI $form, Constraint $constraint, Field $field): string
    {
        $name = $constraint->name();
        $view = self::form($form, 'label.constraints.' . $name, $field);

        if(view()->exists($view)){
            return $view;
        }

        return Theme::core('blank');
    }

    /**
     * Devuelve el partial para el label extra segun el type.
     * Si no existe se devuelve la plantilla en blanco.
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function labelExtraType(FI $form, Field $field): string
    {
        $name = $field->type()->name();
        $view = self::form($form, 'label.types.' . $name, $field);

        if(view()->exists($view)){
            return $view;
        }

        return Theme::core('blank');
    }

    /**
     * Devuelve el partial para el label extra text segun el type.
     * Si no existe se devuelve la plantilla en blanco.
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function labelExtraTextType(FI $form, Field $field): string
    {
        $name = $field->type()->name();
        $view = self::form($form, 'label.texts.' . $name, $field);

        if(view()->exists($view)){
            return $view;
        }

        return Theme::core('blank');
    }

    /**
     * Devuelve el partial especificado para un atributo del par form,field
     *
     * @param FI $form
     * @param string $partial
     * @param Field|null $field
     * @return string
     */

    public static function attribute(FI $form, string $partial, ?Field $field): string
    {
        return self::form($form, 'attributes.' . $partial, $field);
    }

    /**
     * Devuelve el partial especificado para un atributo del label para el par form,field
     *
     * @param FI $form
     * @param string $partial
     * @param Field|null $field
     * @return string
     */

    public static function labelAttribute(FI $form, string $partial, ?Field $field): string
    {
        return self::form($form, 'label.attributes.' . $partial, $field);
    }

    /**
     * Devuelve el submit del form actual
     *
     * @param FI $form
     * @return string
     */

    public static function submit(FI $form): string
    {
        return self::form($form, 'submit');
    }

    /**
     * Devuelve el override del formulario o el partial proveido por el tema.
     *
     * Override example:
     *
     * resource/views/backend/looper/form/me/submit.blade.php -> override del submit para el formulario me
     * resource/views/backend/looper/form/me/first_name/label.blade.php -> override del label del formulario me, campo first_name
     * resource/views/backend/looper/form/me/first_name/attributes/placeholder.blade.php -> override del placeholder del formulario me, campo first_name
     *
     * @param FI $form
     * @param string $partial
     * @param Field|null $field
     * @return string
     */

    private static function form(FI $form, string $partial, ?Field $field = null): string
    {
        $name = $form->name();
        $site = Site::resolv();
        $theme = Config::get('laravelito.theme.' . $site);

        if(isset($field)){
            $override = $site . '.' . $theme . '.form.' . $name . '.' . $field->name() . '.' . $partial;

            if(view()->exists($override)){
                return $override;
            }

            if(strpos($name, '.') !== false){
                $pname = current(explode('.', $name));
                $override = $site . '.' . $theme . '.form.' .$pname . '.' . $field->name() . '.' . $partial;

                if(view()->exists($override)){
                    return $override;
                }
            }
        }

        $override = $site . '.' . $theme . '.form.' . $name . '.' . $partial;

        if(view()->exists($override)){
            return $override;
        }

        if(isset($field)){
            $override = $site . '.' . $theme . '.form.fields.' . $field->name() . '.' . $partial;

            if(view()->exists($override)){
                return $override;
            }
        }

        return Theme::get('form.' . $partial);
    }

    /** @var array bolsa de invalidez de formularios */

    private static $INVALID = [];

    /**
     * Guarda los mensajes de invalidez de un formulario.
     *
     * @return void
     */

    public static function invalid(array $errors,string $name): void
    {
        self::$INVALID[$name] = $errors;
    }

    /**
     * Determina si el campo del form es valido.
     *
     * @param FI $form
     * @param Field $field
     * @return bool
     */

    public static function isValid(FI $form,Field $field): bool
    {
        $name = $field->name();
        $fname = $form->name();

        return count(self::$INVALID[$fname][$name] ?? []) === 0;
    }
}
