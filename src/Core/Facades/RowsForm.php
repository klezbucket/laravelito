<?php

namespace Laravelito\Core\Facades;

use Laravelito\Field\Field;
use Laravelito\Field\Type\Rows;
use Laravelito\Core\Facades\Site;
use Laravelito\Field\Type\Absent;
use Laravelito\Core\Facades\Theme;
use Laravelito\Form\RowsForm as FI;
use Illuminate\Support\Facades\Config;

class RowsForm {
    /**
     * Devuelve el partial para la tabla RO.
     *
     * @param FI $form
     * @return string
     */

    public static function table(FI $form): string
    {
        return self::form($form, 'table');
    }
    
    /**
     * Devuelve el partial para crear fila.
     *
     * @param FI $form
     * @return string
     */

    public static function append(FI $form): string
    {
        return self::form($form, 'append');
    }

    /**
     * Devuelve la traduccion desde el campo Rows.
     *
     * @param Rows $rows
     * @param string $path
     * @return string|null
     */

    public static function lang(Rows $rows,string $path): ?string
    {
        return t($rows->lang() . '.' . $path);
    }
    /**
     * Devuelve la cantidad de columnas de la tabla.
     *
     * @param FI $form
     * @return int
     */

    public static function cols(FI $form): int
    {
        return 1 + $form->fields()->count();
    }

    /**
     * Devuelve el partial para la fila vacia.
     *
     * @param FI $form
     * @return string
     */

    public static function empty(FI $form): string
    {
        return self::form($form, 'empty');
    }

    /**
     * Devuelve el partial de acciones de cada fila.
     *
     * @param FI $form
     * @return string
     */

    public static function actions(FI $form): string
    {
        return self::form($form, 'actions');
    }

    /**
     * Devuelve el partial de cabecera de tabla.
     *
     * @param FI $form
     * @return string
     */

    public static function thead(FI $form): string
    {
        return self::form($form, 'thead');
    }

    /**
     * Devuelve el partial del cuerpo de tabla.
     *
     * @param FI $form
     * @return string
     */

    public static function tbody(FI $form): string
    {
        return self::form($form, 'tbody');
    }

    /**
     * Devuelve el partial para cada th del thead.
     * Si implementa type Absent, entonces renderea la plantilla en blanco.
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function th(FI $form, Field $field): string
    {
        if($field->type() instanceof Absent){
            return Theme::core('blank');
        }

        return self::form($form,'th',$field);
    }

    /**
     * Devuelve el partial para cada td del tr.
     * Si implementa type Absent, entonces renderea la plantilla en blanco.
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function td(FI $form, Field $field): string
    {
        if($field->type() instanceof Absent){
            return Theme::core('blank');
        }

        return self::form($form,'td',$field);
    }

    /**
     * Devuelve el partial para cada tr del tbody.
     *
     * @param FI $form
     * @param Field $field
     * @return string
     */

    public static function tr(FI $form, Field $field): string
    {
        return self::form($form,'tr',$field);
    }

    /**
     * Devuelve el override del formulario o el partial proveido por el tema.
     *
     * @param FI $form
     * @param string $partial
     * @param Field|null $field
     * @return string
     */

    private static function form(FI $form, string $partial, ?Field $field = null): string
    {
        $name = $form->name();
        $site = Site::resolv();
        $theme = Config::get('laravelito.theme.' . $site);

        if(isset($field)){
            $override = $site . '.' . $theme . '.rowsform.' . $name . '.' . $field->name() . '.' . $partial;

            if(view()->exists($override)){
                return $override;
            }

            if(strpos($name, '.') !== false){
                $pname = current(explode('.', $name));
                $override = $site . '.' . $theme . '.rowsform.' .$pname . '.' . $field->name() . '.' . $partial;
        
                if(view()->exists($override)){
                    return $override;
                }

            }
        }

        $override = $site . '.' . $theme . '.rowsform.' . $name . '.' . $partial;

        if(view()->exists($override)){
            return $override;
        }

        if(isset($field)){
            $override = $site . '.' . $theme . '.rowsform.fields.' . $field->name() . '.' . $partial;

            if(view()->exists($override)){
                return $override;
            }
        }

        return Theme::get('rowsform.' . $partial);
    }
}
