<?php

namespace Laravelito\Core\Facades;

use Laravelito\Field\Field as FI;

class Field {
    /**
     * Devuelve el label de este field.
     * 
     * @param FI $field
     * @return string
     */

    public static function label(FI $field): string
    {
        $label = $field->label();

        if($field->translate()){
            return t($label);
        }

        return $label;
    }

    /**
     * Devuelve el placeholder de este field.
     * 
     * @param FI $field
     * @return string
     */

    public static function placeholder(FI $field): string
    {
        $placeholder = $field->placeholder();

        if($field->translate()){
            return t($placeholder);
        }

        return $placeholder;
    }

    /**
     * Devuelve el help de este field.
     * 
     * @param FI $field
     * @return string
     */

    public static function help(FI $field): string
    {
        $help = $field->help();

        if($field->translate()){
            return t($help);
        }

        return $help;
    }
}