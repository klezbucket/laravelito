<?php

namespace Laravelito\Upload\Validation;

use Laravelito\Upload\File\File;

class UploadMimesValidator {
    
    /**
     * Valida el mime del archivo subido.
     */

    public static function validate($attribute, $value, $parameters)
    {
        if($value){
            $file = new File($value);
            $mime = $file->mime();
            unset($file);

            return in_array($mime, $parameters);
        }

        return true;
    }
}