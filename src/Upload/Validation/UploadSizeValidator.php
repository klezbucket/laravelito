<?php

namespace Laravelito\Upload\Validation;

use Laravelito\Upload\File\File;

class UploadSizeValidator {
    
    /**
     * Valida el tamanho del archivo subido.
     */

    public static function validate($attribute, $value, $parameters)
    {
        if($value){
            $max = array_shift($parameters);    
            $file = new File($value);
            $size = strlen($file->content());
            unset($file);

            return $size <= $max;
        }

        return true;
    }
}