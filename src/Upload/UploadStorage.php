<?php
namespace Laravelito\Upload;

use Exception;
use Laravelito\Upload\File\File;
use Laravelito\Upload\LocalStorage;
use Illuminate\Database\Eloquent\Model;

abstract class UploadStorage {
    /**
     * Upload local, guarda el archivo en la carpeta storage del framework.
     * @param string $path
     * @param string $file
     */

    public static function local(string $path,string $file): UploadStorage
    {
        return new LocalStorage($path,new File($file));
    }

    /**
     * Upload local, guarda el archivo en la carpeta storage del framework.
     * Genera el path de forma automatica.
     * Si $file no se especifica se asume que viene del request con el mismo nombre de campo.
     *
     * @param Model $model
     * @param string $field
     * @param string|null $file
     */

    public static function localWithModel(Model $model,string $field,?string $file = null): UploadStorage
    {
        if(! isset($file)){
            $file = request()->get($field);
        }

        $path = LocalStorage::target($model,$field);
        return new LocalStorage($path,new File($file));
    }

    /**
     * Ejecuta la escritura del archivo.
     * @throws Exception si la escritura falla
     * @return void
     */

    abstract public function save(): void;
}
