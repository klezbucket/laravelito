<?php
namespace Laravelito\Upload;

use Exception;
use Laravelito\Upload\File\File;
use Laravelito\Upload\LocalStorage;
use Laravelito\Upload\Logs\Logging;
use Laravelito\Upload\UploadStorage;
use Illuminate\Database\Eloquent\Model;

class LocalStorage extends UploadStorage {
    /** @var $string */
    const MIME = 'mime';

    /** @var File*/
    private $file;

    /** @var string */
    private $path;

    /**
     * Constructor recibe donde guardar el archivo y el contenido del archivo.
     * Donde guardar el archivo es relativo a la carpeta storage del framework, pero puede hacerse override con .env
     * LOCAL_STORAGE
     *
     * @param string $path
     * @param File $file
     * @return void
     */

    public function __construct(string $path, File $file)
    {
        $this->path = LocalStorage::base($path);
        $this->file = $file;
    }

    /**
     * Ejecuta la escritura del archivo.
     * @throws Exception si la escritura falla
     * @return void
     */

    public function save(): void
    {
        Logging::separator('UPLOAD');
        Logging::output('Storage: Locale');
        Logging::output('Path: ' . $this->path);

        $directory = dirname($this->path);

        if(! file_exists($directory)){
            if(! mkdir($directory, 0777, true)){
                throw new Exception('mkdir() return false, path=' . $directory);
            }
        }
        else if(! is_dir($directory)){
            throw new Exception('Not a directory, path=' . $directory);
        }
        else if(! is_writable($directory)){
            throw new Exception('Not writable, path=' . $directory);
        }   

        if(file_put_contents($this->path, $this->file->content()) === false){
            throw new Exception('Cannot write file, path=' . $this->path);
        }

        if(file_put_contents($this->mimepath(), $this->file->mime()) === false){
            throw new Exception('Cannot write mime, path=' . $this->path);
        }
    }

    /**
     * Obtiene el mimepath de este fichero. Mismo path que el archivo con extension .mime
     * @return string
     */

    public function mimepath(): string
    {
        return $this->path . '.' . LocalStorage::MIME;
    }

    /**
     * Obtiene el path completo al archivo
     * @return string
     */

    public static function path(): string
    {
        $base = env('LOCAL_STORAGE',storage_path());
        return $base . DIRECTORY_SEPARATOR;
    }

    /**
     * Obtiene el base path de la carpeta de uploads.
     * Si se especifica path, se anexa al final con un DIR SEPARATOR.
     * 
     * @param string|null $path
     * @return string
     */

    public static function base(?string $path = null): string
    {
        $base = env('LOCAL_STORAGE',storage_path());
        
        if(isset($path)){
            $base .= DIRECTORY_SEPARATOR . $path;
        }

        return $base;
    }

    /**
     * Dado un modelo y el campo deseado, se encarga de generar la carpeta de upload y el archivo de forma automatica.
     * No crea directorios ni archivos, solo el string.
     * 
     * @param Model $model
     * @param string $field
     * @return string
     */

    public static function target(Model $model, string $field): string
    {
        $page = env('MAX_FILES_PER_DIRECTORY', 1000);
        $id = $model->id ?? 0;

        $hexa = '0x' . str_pad(dechex($id > 0 ? (
            intval(floor($id / $page)) + 1
        ) : (0)), 16, '0',STR_PAD_LEFT);

        $components = [
            $model->getTable(),
            $field,
            $hexa,
            $id
        ];

        return implode(DIRECTORY_SEPARATOR, $components);
    }

    /**
     * Obtiene el mimepath de un archivo de upload, dado el Modelo y Field requeridos.
     * 
     * @param Model $model
     * @param string $field
     * @return string
     */

    public static function mimespec(Model $model,string $field): string
    {
        return self::base(self::target($model,$field)). '.' . LocalStorage::MIME;
    }

    /**
     * Obtiene el fullpath de un archivo de upload, dado el Modelo y Field requeridos.
     * 
     * @param Model $model
     * @param string $field
     * @return string
     */

    public static function fullpath(Model $model,string $field): string
    {
        return self::base(self::target($model,$field));
    }
}
