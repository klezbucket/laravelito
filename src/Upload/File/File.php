<?php
namespace Laravelito\Upload\File;

use Exception;

class File {
    /**
     * Genera un file desde el mime y base64 separados.
     * @param string $base64
     * @param string $mime
     * @return File
     */

    public static function instantiate(string $base64, string $mime): File
    {
        return new File($mime . ';base64,' .$base64);
    }

    /**
     * Genera un objeto mime, content desde el base64 obtenido.
     *
     * @param string $base64
     * @return void
     * @throws Exception si no posee dos partes mime y contenido
     */
    public function __construct($base64)
    {
        $parts = explode(',', $base64);

        if(count($parts) < 2){
            throw new Exception('Invalid base64 uploaded file');
        }

        $mime = preg_replace('/^data:/','', $parts[0]);
        $mime = preg_replace('/;base64$/','', $mime);
        $this->content = base64_decode($parts[1]);
        $this->mime = $mime;
    }

    /**
     * Devuelve el mime del archivo
     * @return string
     */

    public function mime(): string
    {
        return $this->mime;
    }

    /**
     * Devuelve el contenido del archivo
     * @return string
     */

    public function content(): string
    {
        return $this->content;
    }
}