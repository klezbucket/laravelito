<?php

namespace App\Backend\Upload\Tables;

use Laravelito\Core\Facades\Router;
use Laravelito\Field\Upload\Upload;
use Illuminate\Database\Eloquent\Model;

class PictureUpload implements Upload {

    /**
     * Determina los mimes validos.
     * @return array
     */

    public function mimes(): array
    {
        return [
            'image/png',
            'image/jpeg',
            'image/jpg'
        ];
    }

    /**
     * Determina el size maximo en bytes del archivo a subir.
     * Devuelve null para no chequear.
     *
     * @return array
     */

    public function size(): ?int
    {
        return 1000000; // 1 MB
    }

    /**
     * Determina la URL donde recuperar el archivo.
     * @param Model $model
     * @return string|null
     */

    public function url(Model $model): ?string
    {
        if($model->picture){
            return Router::route(PRroute, $model) . '?t=' . $model->picture;
        }

        return null;
    }
}
