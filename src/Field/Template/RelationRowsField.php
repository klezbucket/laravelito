<?php

namespace App\Backend\Field\Placeholders;

use Laravelito\Field\Field;
use Laravelito\Field\Type\Rows;
use Laravelito\Field\Type\Type;
use Illuminate\Support\Collection;
use Laravelito\Core\Facades\Router;
use Laravelito\Field\Constraint\Required;
use App\Backend\Form\CRUD\Relations\RowsForm;

class RelationsRowsField extends Field {
    /**
     * Se debe establecer el label del campo
     * @return string
     */

    public function label(): string
    {
        return 'placeholders.fields.relations.label';
    }

    /**
     * Se debe establecer el nombre del campo
     * @return string
     */

    public function name(): string
    {
        return 'relations';
    }

    /**
     * Se debe establecer el tipo del campo
     * @return Type
     */

    public function type(): Type
    {
        return new Rows(new RowsForm('relations'), 'relations.rows');
    }

    /**
     * Se debe establecer una coleccion de Constaints.
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }
}
