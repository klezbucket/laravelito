<?php
namespace Laravelito\Field\Template;

use Laravelito\Field\Field;
use Illuminate\Support\Collection;
use Laravelito\Field\Constraint\Unique;
use Laravelito\Field\Constraint\Required;

trait TemplateField {
    /**
     * Constuctor
     */

    public function __construct()
    {
        
    }

    /**
     * Determina si el campo es requrido.
     * @param bool $required
     * @return Field
     */

    public function required(bool $required): Field
    {
        $this->required = $required;
    }

    /**
     * Se debe establecer el help del campo
     * @return string
     */

    public function help(): string
    {
        if($this->help){
            return "{$this->table}.fields.{$this->field}.help";
        }

        return '';
    }

    /**
     * Se debe establecer el placeholder del campo
     * @return string
     */

    public function placeholder(): string
    {
        if($this->placeholder){
            return "{$this->table}.fields.{$this->field}.placeholder";
        }

        return '';
    }

    /**
     * Se debe establecer el label del campo
     * @return string
     */

    public function label(): string
    {
        return "{$this->table}.fields.{$this->field}.label";
    }

    /**
     * Se debe establecer el nombre del campo
     * @return string
     */

    public function name(): string
    {
        return $this->field;
    }

    /**
     * Se debe establecer una coleccion de Constaints.
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();

        if($this->required){
            $constraints[] = new Required();
        }

        if($this->unique){
            $comp = [];

            foreach($this->uniqueComposite as $field){
                $comp[$field] = request()->get($field);
            }

            $constraints[] = new Unique($this->table,$this->field,request()->route()->parameter('id') ?? null,$comp);
        }

        return $constraints;
    }
}