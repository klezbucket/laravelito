<?php

namespace App\Backend\Field\Tables;

use Laravelito\Field\Field;
use Laravelito\Field\Type\Type;
use Laravelito\Core\Facades\Router;
use Laravelito\Field\Template\TemplateField;

/**
 * Made by php artisan PRarguments
 */

class PhieldField extends Field {
    use TemplateField;

    /** @var bool */
    protected $placeholder = PRplaceholder;

    /** @var bool */
    protected $help = PRhelp;

    /** @var bool */
    private $required = PRrequired;

    /** @var bool */
    private $unique = PRunique;

    /** @var string */
    private $table = 'tables';

    /** @var string */
    private $field = 'phield';

    /** @var array */
    private $uniqueComposite = PRuniqcomp;

    /**
     * Se debe establecer el tipo del campo
     * @return Type
     */

    public function type(): Type
    {
        return new \Laravelito\Field\Type\PRType(PRSignature);
    }
}
