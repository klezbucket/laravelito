<?php

namespace Laravelito\Field;

use Illuminate\Support\Collection;
use Laravelito\Field\Type\Type;

abstract class Field {
    /**
     * Se debe establecer el nombre del campo
     * 
     * @return string
     */

    public abstract function name(): string;

    /**
     * Se debe establecer el tipo del campo
     * 
     * @return Type
     */

    public abstract function type(): Type;

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public abstract function constraints(): Collection;

    /**
     * Se debe establecer el label del campo
     * 
     * @return string
     */

    public abstract function label(): string;

    /**
     * Se debe establecer el placeholder del campo
     * 
     * @return string
     */

    public function placeholder() {
        return '';
    }
    
    /**
     * Se debe establecer la ayuda del campo
     * 
     * @return string
     */

    public function help() {
        return '';
    }

    /**
     * Realiza la traduccion.
     * 
     * @return bool
     */

    public function translate(): bool
    {
        return true;
    }
    
    /**
     * Devuelve el nombre del campo para el namespace de validacion.
     * Por defecto el mismo nombre.
     * 
     * @return string|null
     */

    public function validationsName(): ?string
    {
        return $this->name();
    }
}