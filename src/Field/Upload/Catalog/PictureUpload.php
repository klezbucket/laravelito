<?php

namespace Laravelito\Field\Upload\Catalog;

use Laravelito\Core\Facades\Router;
use Laravelito\Field\Upload\Upload;
use Illuminate\Database\Eloquent\Model;

class PictureUpload implements Upload {
    /** @var array */
    private $mimes = [
        'image/png',
        'image/jpeg',
        'image/jpg'
    ]; 

    /** @var int */
    private $size = 1000000; // 1 MB

    /** @var string */
    private $route;

    /** @var string */
    private $field;

    /**
     * Constructor recibe los parametros por defecto.
     * @param string $route
     * @param string $field
     * @param array|null $mimes
     * @param int|null $size
     */

    public function __construct(string $route,string $field,?array $mimes = null,?int $size = null){
        $this->route = $route;
        $this->field = $field;

        if(isset($mimes)){
            $this->mimes = $mimes;
        }

        if(isset($size)){
            $this->size = $size;
        }
    }

    /**
     * Determina los mimes validos.
     * @return array
     */

    public function mimes(): array
    {
        return $this->mimes;
    }

    /**
     * Determina el size maximo en bytes del archivo a subir.
     * Devuelve null para no chequear.
     *
     * @return array
     */

    public function size(): ?int
    {
        return $this->size;
    }

    /**
     * Determina la URL donde recuperar el archivo.
     * @param Model $model
     * @return string|null
     */

    public function url(Model $model): ?string
    {
        $field = $this->field;

        if($model->{$field}){
            return Router::route($this->route, $model) . '?t=' . $model->{$field};
        }

        return null;
    }
}
