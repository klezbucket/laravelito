<?php

namespace Laravelito\Field\Upload;

use Illuminate\Database\Eloquent\Model;

interface Upload {
    /**
     * Determina los mimes validos.
     * @return array
     */

    public function mimes(): array;

    /**
     * Determina el size maximo en bytes del archivo a subir.
     * Devuelve null para no chequear.
     *
     * @return array
     */

    public function size(): ?int;

    /**
     * Determina la URL donde recuperar el archivo.
     * @parma Model $model
     * @return string|null
     */

    public function url(Model $model): ?string;
}
