<?php
namespace Laravelito\Field\Upload;

use Laravelito\Field\Upload\Upload;
use Laravelito\Core\Facades\Printer;
use Laravelito\Helpers\HumanReadable;

trait UploadTrait {
    /** @var Upload */
    private $upload;

    /**
     * Constructor recibe una instancia de Upload.
     * @return void
     */

    public function __construct(Upload $upload)
    {
        $this->upload = $upload;
    }
    
    /**
     * Obtiene el Upload
     * @return Upload
     */

    public function upload(): Upload
    {
        return $this->upload;
    }
    
    /**
     * Obtiene el tamanho maximo del upload
     * @return int|null
     */

    public function size(): ?int
    {
        return $this->upload->size();
    }
    
    /**
     * Obtiene los mimes validos del upload
     * @return array
     */

    public function mimes(): array
    {
        return $this->upload->mimes();
    }

    /**
     * Obtiene el mensaje de validacion de mime.
     * @return string
     */

    public function invalidMime(): string
    {
        return t('looper.upload.invalid.mime');
    }

    /**
     * Obtiene el mensaje de validacion de mime.
     * @return string
     */

    public function invalidSize(): string
    {
        return t('looper.upload.invalid.size', [
            'max' => HumanReadable::bytesToHuman($this->size()),
            'bytes' => Printer::integerValue($this->size())
        ]);
    }
}