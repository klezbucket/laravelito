<?php

namespace Laravelito\Field\Constraint;

class Min implements Constraint  {
    /** @var int */
    private $min;

    /**
     * Devuelve una instancia de este Constraint.
     * 
     * @param int $min
     * @return void
     */

    public function __construct(int $min)
    {
        $this->min = $min;
    }

    /**
     * En laravel la validacion es 'min:$min'
     * 
     * @return string
     */

    public function validates(): string
    {
        return "min:{$this->min}";
    }

    /** 
     * Devuelve el nombre formal.
     * 
     * @return string
     */

    public function name(): string
    {
        return 'min';
    }
}