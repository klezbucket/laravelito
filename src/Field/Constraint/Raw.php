<?php

namespace Laravelito\Field\Constraint;

class Raw implements Constraint  {
    /** @var string */
    private $raw;

    /**
     * Devuelve una instancia de este Constraint.
     * 
     * @param string $raw
     * @return void
     */

    public function __construct(string $raw)
    {
        $this->raw = $raw;
    }

    /**
     * Se devuelve el raw rule. 
     * 
     * @return string
     */

    public function validates(): string
    {
        return $this->raw;
    }

    /** 
     * Devuelve el nombre formal.
     * 
     * @return string
     */

    public function name(): string
    {
        return 'raw';
    }
}