<?php

namespace Laravelito\Field\Constraint;

class Foreign implements Constraint{
    /**
     * En laravel, la validacion customizada es 'foreign'.
     * 
     * @return string
     */

    public function validates(): string
    {
        return 'foreign';
    }

    /** 
     * Devuelve el nombre formal.
     * 
     * @return string
     */

    public function name(): string
    {
        return 'foreign';
    }
}