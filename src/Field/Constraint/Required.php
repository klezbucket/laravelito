<?php
namespace Laravelito\Field\Constraint;

use Laravelito\Field\Constraint\Constraint;

class Required implements Constraint {
    /**
     * En laravel la validacion es 'required'
     * 
     * @return string
     */

    public function validates(): string
    {
        return 'required';
    }

    /** 
     * Devuelve el nombre formal.
     * 
     * @return string
     */

    public function name(): string
    {
        return 'required';
    }
}