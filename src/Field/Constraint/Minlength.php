<?php

namespace Laravelito\Field\Constraint;

class Minlength implements Constraint  {
    /** @var int */
    private $minlength;

    /**
     * Devuelve una instancia de este Constraint.
     * 
     * @param int $minlength
     * @return void
     */

    public function __construct(int $minlength)
    {
        $this->minlength = $minlength;
    }

    /**
     * En laravel la validacion es 'min:$minlength'
     * 
     * @return string
     */

    public function validates(): string
    {
        return "min:{$this->minlength}";
    }

    /** 
     * Devuelve el nombre formal.
     * 
     * @return string
     */

    public function name(): string
    {
        return 'minlength';
    }
}