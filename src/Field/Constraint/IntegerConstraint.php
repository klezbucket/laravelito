<?php

namespace Laravelito\Field\Constraint;

use Laravelito\Field\Type\NumberTrait;
use Laravelito\Field\Upload\UploadTrait;
use Laravelito\Field\Constraint\Constraint;

class IntegerConstraint implements Constraint{
    use NumberTrait;
    /**
     * Inyecta la de integer y si es necesario unsigned
     * 
     * @return string
     */

    public function validates(): string
    {
        $rules = 'integer';

        if($this->unsigned){
            $rules .= '|unsigned';
        }
        
        return $rules;
    }

    /** 
     * Devuelve el nombre formal.
     * 
     * @return string
     */

    public function name(): string
    {
        return 'integer';
    }
}