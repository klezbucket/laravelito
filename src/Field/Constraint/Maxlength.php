<?php

namespace Laravelito\Field\Constraint;

class Maxlength implements Constraint  {
    /** @var int */
    private $maxlength;

    /**
     * Devuelve una instancia de este Constraint.
     * 
     * @param int $maxlength
     * @return void
     */

    public function __construct(int $maxlength)
    {
        $this->maxlength = $maxlength;
    }

    /**
     * En laravel, la validacion es 'max:$maxlength'.    
     * 
     * @return string
     */

    public function validates(): string
    {
        return "max:{$this->maxlength}";
    }

    /** 
     * Devuelve el nombre formal.
     * 
     * @return string
     */

    public function name(): string
    {
        return 'maxlength';
    }
}