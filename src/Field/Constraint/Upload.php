<?php

namespace Laravelito\Field\Constraint;

use Laravelito\Field\Upload\UploadTrait;
use Laravelito\Field\Constraint\Constraint;

class Upload implements Constraint{
    use UploadTrait;

    /**
     * Inyecta la validacion de mimes y tamanho de archivo.
     * 
     * @return string
     */

    public function validates(): string
    {
        $rules = 'upload.mimes:' . implode(',', $this->mimes());

        if($size = $this->size()){
            $rules .= '|upload.size:' . $size;
        }

        return $rules;
    }

    /** 
     * Devuelve el nombre formal.
     * 
     * @return string
     */

    public function name(): string
    {
        return 'upload';
    }
}