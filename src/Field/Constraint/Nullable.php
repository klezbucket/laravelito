<?php

namespace Laravelito\Field\Constraint;

class Nullable implements Constraint  {
    /**
     * Se devuelve el nullable rule. 
     * 
     * @return string
     */

    public function validates(): string
    {
        return 'nullable';
    }

    /** 
     * Devuelve el nombre formal.
     * 
     * @return string
     */

    public function name(): string
    {
        return 'nullable';
    }
}