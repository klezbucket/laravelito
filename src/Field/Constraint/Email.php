<?php

namespace Laravelito\Field\Constraint;

class Email implements Constraint{
    /**
     * En laravel, la validacion es 'email'.
     * 
     * @return string
     */

    public function validates(): string
    {
        return 'email';
    }

    /** 
     * Devuelve el nombre formal.
     * 
     * @return string
     */

    public function name(): string
    {
        return 'email';
    }
}