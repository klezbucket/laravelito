<?php

namespace Laravelito\Field\Constraint;

interface Constraint {
    /** 
     * Devuelve la validacion Laravel.
     * 
     * @return string
     */

    function validates(): string;

    /** 
     * Devuelve el nombre formal.
     * 
     * @return string
     */

    function name(): string;
}