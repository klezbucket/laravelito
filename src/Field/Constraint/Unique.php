<?php
namespace Laravelito\Field\Constraint;

use Laravelito\Field\Constraint\Constraint;

class Unique implements Constraint {
    /** @var string */
    private $column;

    /** @var string */
    private $table;

    /** @var int|null */
    private $id;

    /** @var array */
    private $composite;

    /**
     * Devuelve la instancia del Constraint.
     * Requiere nombre tabla e ID opcional.
     * 
     * @param string $table
     * @param string $column
     * @param mixed|null $id
     * @param array $composite
     * @return void
     */

    public function __construct(string $table, string $column = 'id',$id = null,array $composite = [])
    {
        $this->column = $column;
        $this->table = $table;
        $this->id = $id;
        $this->composite = $composite;
    }

    /**
     * En laravel la validacion es 'unique'
     * 
     * @return string
     */

    public function validates(): string
    {
        $rule = 'unique:' . $this->table . ',' . $this->column . ',';
        
        if(isset($this->id)){
            $rule .= $this->id . ',id';
        }
        else{
            $rule .= 'NULL,NULL';
        }

        foreach($this->composite as $field => $value){
            $rule .= ',' . $field . ',' . $value;
        }

        return $rule;
    }

    /** 
     * Devuelve el nombre formal.
     * 
     * @return string
     */

    public function name(): string
    {
        return 'unique';
    }
}