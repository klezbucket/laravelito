<?php

namespace Laravelito\Field\Constraint;

use Laravelito\Field\Upload\UploadTrait;

class Picture implements Constraint{
    use UploadTrait;

    /**
     * En laravel, la validacion es 'email'.
     * 
     * @return string
     */

    public function validates(): string
    {
        return 'picture';
    }

    /** 
     * Devuelve el nombre formal.
     * 
     * @return string
     */

    public function name(): string
    {
        return 'picture';
    }
}