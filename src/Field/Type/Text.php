<?php

namespace Laravelito\Field\Type;

use Illuminate\Support\Collection;
use Laravelito\Field\Type\Type;

class Text extends Type {
    /**
     * Tipo de dato Text.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'text';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }
}