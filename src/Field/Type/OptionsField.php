<?php

namespace Laravelito\Field\Type;

interface OptionsField {
    function options(): array;
}