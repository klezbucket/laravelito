<?php

namespace Laravelito\Field\Type;

use Laravelito\Field\Type\Type;
use Illuminate\Support\Collection;
use Laravelito\Field\Type\NumberTrait;

class Money extends Type {
    use NumberTrait;

    /** @var int */
    private $decimals;

    /** @var string */
    private $currency;

    /**
     * Constructor de esta clase.
     * @param int $decimals
     * @return void
     */

    public function __construct(string $currency,int $decimals = 2,bool $unsigned = true,float $step = 0.01)
    {
        $this->currency = $currency;
        $this->unsigned = $unsigned;
        $this->step = $step;
        $this->decimals = $decimals;
    }

    /**
     * Devuelve el currency
     *
     * @return string
     */

    public function currency(): string
    {
        return $this->currency;
    }

    /**
     * Tipo de dato Decimal.
     *
     * @return string
     */

    public function name(): string
    {
        return 'money';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     *
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }

    /**
     * Determina la cantidad de decimales a imprimir.
     * @return int
     */

    public function decimals(): int
    {
        return $this->decimals;
    }
}
