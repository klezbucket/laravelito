<?php

namespace Laravelito\Field\Type;

use Illuminate\Support\Collection;

abstract class Type {
    /**
     * Se establece el nombre del tipo.
     *
     * @return string
     */
    public abstract function name(): string;

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public abstract function constraints(): Collection;
}