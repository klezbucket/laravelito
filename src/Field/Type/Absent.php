<?php

namespace Laravelito\Field\Type;

use Illuminate\Support\Collection;

class Absent extends Text {
    /**
     * Tipo de dato Ausente.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'absent';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }
}