<?php

namespace Laravelito\Field\Type;

use Illuminate\Support\Collection;

class Boolean extends Text {
    /** @var string */
    private $true;

    /** @var string */
    private $false;

    /**
     * Debe los labels para cada estado booleano.
     * @param string $true
     * @param string $false
     * @return void
     */

    public function __construct(string $true = 'boolean.true',string $false = 'boolean.false')
    {
        $this->true = $true;
        $this->false = $false;
    }

    /**
     * Resuelve el label
     * @param bool $status
     * @return string
     */

    public function label(bool $status): string
    {
        return $status ? $this->true : $this->false;
    }

    /**
     * Tipo de dato Boolean.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'boolean';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }
}