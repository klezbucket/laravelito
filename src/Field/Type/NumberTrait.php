<?php

namespace Laravelito\Field\Type;

use Illuminate\Support\Collection;
use Laravelito\Field\Constraint\IntegerConstraint;

trait NumberTrait {

    /** @var bool */
    private $unsigned = false;

    /** @var float */
    private $step = 1;

    /**
     * Constructor para este trait.
     *
     * @param bool $unsigned
     * @param float $step
     * @return void
     */

    public function __construct(bool $unsigned = false,float $step = 1)
    {
        $this->unsigned = $unsigned;
        $this->step = $step;
    }

    /**
     * Devuelve el step del input number.
     * @return float
     */

    public function step(): float
    {
        return $this->step;
    }

    /**
     * Devuelve el unsigned del input number.
     * @return float
     */

    public function unsigned(): bool
    {
        return $this->unsigned;
    }

    /**
     * Agrega los constraints de tipo number.
     * @param Collection $constraints
     * @return void
     */

    public function numberConstraints(Collection $constraints): void
    {
        $constraints[] = new IntegerConstraint($this->unsigned);
    }
}
