<?php

namespace Laravelito\Field\Type;

use Laravelito\Field\Type\Map;
use Laravelito\Field\Type\Type;
use Illuminate\Support\Collection;

class PolygonMap extends Map {
    /**
     * Tipo de dato PolygonMap.
     * Permite trazar un poligono en un mapa.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'polygon_map';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }
}