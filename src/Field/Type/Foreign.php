<?php

namespace Laravelito\Field\Type;

use Laravelito\Field\Type\Text;
use Laravelito\Field\Type\Type;
use Illuminate\Support\Collection;
use Laravelito\Field\Constraint\Foreign as FC;
use Laravelito\Core\Exceptions\InternalServerException;
use Laravelito\Autocomplete\Model\AutocompleteModelInterface;

class Foreign extends Type  {
    /** @var AutocompleteModelInterface */
    private $model;
    
    /** @var string */
    private $url;

    /** @var bool */
    private $multiple;

    /**
     * @param string $url;
     * @param string $model the class model
     * @param bool $multiple
     * @throws InternalServerException if the class provided is not an AutocompleteModelInterface instance
     * @return void
     */

    public function __construct(string $url, string $model, bool $multiple = false)
    {
        $this->url = $url;

        if(! is_a($model,AutocompleteModelInterface::class,true)){
            throw new InternalServerException($model . ' is not an ' . AutocompleteModelInterface::class . ' instance');
        }

        $this->model = $model;
        $this->multiple = $multiple;
    }

    /**
     * Determina si el campo soporta multiples selecciones.
     * @return bool
     */

    public function multiple(): bool
    {
        return $this->multiple;
    }

    /**
     * Devuelve la clase del modelo de este autocomplete.
     * @return string
     */

    public function model(): string
    {
        return $this->model;
    }

    /**
     * Devuelve la URL para consulta via AJAX.
     * @return string
     */

    public function url(): string
    {
        return $this->url;
    }

    /**
     * Tipo de dato Foreign.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'foreign';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        $constraints[] = new FC();
        return $constraints;
    }

    /**
     * Devuelve la representacion textual del modelo.
     * @param mixed $id
     * @return string
     */

    public function text($id): string
    {
        $ids = explode(',', $id);
        $texts = [];

        foreach($ids as $id){
            $texts[] = str_replace(',','',call_user_func($this->model . '::autocompleteText',$id));
        }

        return implode(',', $texts);
    }
}