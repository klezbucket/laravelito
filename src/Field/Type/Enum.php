<?php

namespace Laravelito\Field\Type;

use Illuminate\Support\Collection;
use Laravelito\Field\Type\Text;
use Laravelito\Field\Type\Type;
use Laravelito\Field\Type\OptionsField;

class Enum extends Type implements OptionsField {
    /** @var array */
    private $options = [];

    /** @var bool */
    private $translate = true;

    /**
     * Debe especificar el array de opciones.
     * @param array $options;
     * @return void
     */

    public function __construct(array $options = [],?bool $translate = true)
    {
        $this->options = $options;
        $this->translate = $translate;
    }

    /**
     * Determina si los labels deben traducirse.
     */

    public function translate(): bool
    {
        return $this->translate;
    }

    /**
     * Debe especificar el array assoc de $values => $labels.
     * @return array
     */

    public function options(): array
    {
        return $this->options;
    }

    /**
     * Devuelve el label de una opcion.
     * 
     * @param string $option
     * @return string
     */

    public function option(string $option): string
    {
        if(isset($this->options[$option])){
            return $this->options[$option];
        }

        return $option;
    }

    /**
     * Tipo de dato Enum.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'enum';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }

    /**
     * Realiza cast implicito de type a enum.
     * @param Type $type
     * @return Enum
     */

    public static function cast(Type $type): Enum {
        return $type;
    }
}