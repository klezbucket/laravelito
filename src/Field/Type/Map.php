<?php

namespace Laravelito\Field\Type;

use Illuminate\Support\Collection;
use Laravelito\Field\Type\Type;

abstract class Map extends Type {
    /** @var string $apiKey */
    private $apiKey;

    /** @var float $latitude */
    private $latitude;

    /** @var float $longitude */
    private $longitude;

    /** @var int $zoom */
    private $zoom;

    /**
     * Constructor requiere un API KEY de Google Maps.
     * @param string $apiKey
     * @return void
     */

    public function __construct(string $apiKey,float $latitude,float $longitude,int $zoom)
    {
        $this->apiKey = $apiKey;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->zoom = $zoom;
    }

    /**
     * Devuelve el API KEY
     * @return string
     */

    public function apiKey(): string
    {
        return $this->apiKey ?? '';
    }

    /**
     * Devuelve la longitude
     * @return float
     */

    public function longitude(): float
    {
        return $this->longitude;
    }

    /**
     * Devuelve la latitude
     * @return float
     */

    public function latitude(): float
    {
        return $this->latitude;
    }

    /**
     * Devuelve el zoom
     * @return int
     */

    public function zoom(): int
    {
        return $this->zoom;
    }
}