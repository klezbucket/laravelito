<?php

namespace Laravelito\Field\Type;

use Laravelito\Field\Type\Type;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Laravelito\Field\Upload\UploadTrait;
use Laravelito\Field\Constraint\Upload as UC;

class Upload extends Type {
    use UploadTrait;

    /**
     * Tipo de dato Upload.
     * Permite subir un archivo en base64 al servidor.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'upload';
    }

    /**
     * Label del link de descarga.
     * 
     * @return string
     */

    public function link(): string 
    {
        return 'looper.download.link';
    }

    /**
     * URL del link de descarga.
     * 
     * @param Model $model
     * @return string|null
     */

    public function url(Model $model): ?string 
    {
        return $this->upload->url($model);
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        $constraints[] = new UC($this->upload);
        return $constraints;
    }
}