<?php

namespace Laravelito\Field\Type;

use Illuminate\Support\Collection;
use Laravelito\Field\Type\Type;

class Ago extends Type {
    /**
     * Tipo de dato Ago.
     * Es como datetime pero la vista muestra diffForHumans.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'ago';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }
}