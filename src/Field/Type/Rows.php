<?php

namespace Laravelito\Field\Type;

use Laravelito\Form\Form;
use Laravelito\Form\RowsForm;
use Laravelito\Field\Type\Type;
use Illuminate\Support\Collection;

class Rows extends Type {
    /** @var RowsForm */
    private $form;

    /** @var string */
    private $lang;

    /**
     * Constructor debe proveer un RowsForm.
     * @param RowsForm $form
     * @return void
     */

    public function __construct(RowsForm $form, string $lang = 'rowsform')
    {
        $this->form = $form;
        $this->lang = $lang;
    }

    /**
     * Devuelve el formulario.
     * @return RowsForm
     */

    public function form(): RowsForm
    {
        return $this->form;
    }

    /**
     * Tipo de dato Rows.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'rows';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }

    /**
     * Determina el path al array de i18n a utilizarse.
     * @return string
     */

    public function lang(): string
    {
        return $this->lang;
    }
}