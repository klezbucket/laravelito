<?php

namespace Laravelito\Field\Type;

use Illuminate\Support\Collection;
use Laravelito\Field\Type\Type;

class DateRange extends Type {
    /**
     * Tipo de dato DateRange.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'date_range';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }
}