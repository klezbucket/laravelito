<?php

namespace Laravelito\Field\Type;

use Illuminate\Support\Collection;
use Laravelito\Field\Type\Type;

class Time extends Type {
    /**
     * Tipo de dato Time.
     * Es como datetime pero la vista muestra diffForHumans.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'time';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }
}