<?php

namespace Laravelito\Field\Type;

use Laravelito\Field\Type\Type;
use Laravelito\Field\Type\Upload;
use Illuminate\Support\Collection;
use Laravelito\Field\Upload\UploadTrait;
use Laravelito\Field\Constraint\Upload as UC;
use Laravelito\Field\Constraint\Picture as PC;

class Picture extends Upload {
    /**
     * Tipo de dato Picture.
     * Permite subir una foto en base64 al servidor.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'picture';
    }

    /**
     * Label del link de descarga.
     * 
     * @return string
     */

    public function link(): string 
    {
        return 'looper.picture.link';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        $constraints[] = new UC($this->upload());
        $constraints[] = new PC($this->upload());
        return $constraints;
    }
}