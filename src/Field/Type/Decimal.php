<?php

namespace Laravelito\Field\Type;

use Laravelito\Field\Type\Type;
use Illuminate\Support\Collection;
use Laravelito\Field\Type\NumberTrait;

class Decimal extends Type {
    use NumberTrait;

    /** @var int */
    private $decimals;

    /**
     * Constructor de esta clase.
     * @param int $decimals
     * @return void
     */

    public function __construct(int $decimals = 2,bool $unsigned = false,float $step = 0.01)
    {
        $this->unsigned = $unsigned;
        $this->step = $step;
        $this->decimals = $decimals;
    }

    /**
     * Tipo de dato Decimal.
     *
     * @return string
     */

    public function name(): string
    {
        return 'decimal';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     *
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }

    /**
     * Determina la cantidad de decimales a imprimir.
     * @return int
     */

    public function decimals(): int
    {
        return $this->decimals;
    }
}
