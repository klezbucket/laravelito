<?php

namespace Laravelito\Field\Type;

use Illuminate\Support\Collection;
use Laravelito\Field\Type\Type;

class Date extends Type {
    /**
     * Tipo de dato Date.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'date';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }
}