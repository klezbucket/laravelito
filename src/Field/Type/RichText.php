<?php

namespace Laravelito\Field\Type;

use Illuminate\Support\Collection;
use Laravelito\Field\Type\Type;

class RichText extends Type {
    /**
     * Tipo de dato RichText.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'rich_text';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }

    /**
     * Label del link de descarga.
     * 
     * @return string
     */

    public function link(): string 
    {
        return 'looper.summernote.link';
    }
}