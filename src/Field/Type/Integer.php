<?php

namespace Laravelito\Field\Type;

use Laravelito\Field\Type\Type;
use Illuminate\Support\Collection;
use Laravelito\Field\Type\NumberTrait;

class Integer extends Type {
    use NumberTrait;

    /**
     * Tipo de dato Integer.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'integer';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        $this->numberConstraints($constraints);
        return $constraints;
    }
}
