<?php

namespace Laravelito\Field\Type;

use Laravelito\Field\Type\Map;
use Laravelito\Field\Type\Type;
use Illuminate\Support\Collection;

class MarkerMap extends Map {
    /**
     * Tipo de dato MarkerMap.
     * Permite establecer un marker en el mapa.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'marker_map';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }

    /**
     * Transforma un JSON de GPs en PHP array
     * @param string
     * @return array
     */

    public static function decode(string $json): array
    {
        $locationJSON = preg_replace('/:\s*(\-?\d+(\.\d+)?([e|E][\-|\+]\d+)?)/', ': "$1"', $json);
        $location = json_decode($locationJSON, true);
        return $location;
    }
}