<?php

namespace Laravelito\Field\Type;

use Illuminate\Support\Collection;
use Laravelito\Field\Type\Text;
use Laravelito\Field\Constraint\Email as EC;

class Email extends Text {
    /**
     * Tipo de dato Email.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'email';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        $constraints[] = new EC();
        return $constraints;
    }
}