<?php

namespace Laravelito\Field\Type;

use Illuminate\Support\Collection;
use Laravelito\Field\Constraint\Minlength;

class Password extends Type {
    /**
     * Tipo de dato Password.
     * 
     * @return string
     */

    public function name(): string 
    {
        return 'password';
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * 
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }
}