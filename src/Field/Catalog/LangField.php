<?php

namespace Laravelito\Field\Catalog;

use Laravelito\Field\Field;
use Laravelito\Field\Type\Enum;
use Laravelito\Field\Type\Text;
use Laravelito\Field\Type\Type;
use Laravelito\Field\Type\Email;
use Laravelito\Auth\Facades\Auth;
use Illuminate\Support\Collection;
use Laravelito\Field\Constraint\Unique;
use Laravelito\Field\Constraint\Required;
use Laravelito\Field\Constraint\Maxlength;

class LangField extends Field {
    /**
     * Se debe establecer el placeholder del campo
     * @return string
     */

    public function placeholder(): string
    {
        return 'me.fields.lang.placeholder';
    }

    /**
     * Se debe establecer el label del campo
     * @return string
     */

    public function label(): string
    {
        return 'me.fields.lang.label';
    }

    /**
     * Se debe establecer el nombre del campo
     * @return string
     */

    public function name(): string
    {
        return 'lang';
    }

    /**
     * Se debe establecer el tipo del campo
     * @return Type
     */

    public function type(): Type
    {
        return new Enum([
            'en' => 'me.fields.lang.options.en',
            'es' => 'me.fields.lang.options.es',
        ]);
    }

    /**
     * Se debe establecer una coleccion de Constaints.
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        $constraints[] = new Required();
        return $constraints;
    }
}