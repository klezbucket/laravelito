<?php

namespace Laravelito\Field\Catalog;

use Laravelito\Field\Field;
use Laravelito\Field\Type\Enum;
use Laravelito\Field\Type\Text;
use Laravelito\Field\Type\Type;
use Laravelito\Field\Type\Email;
use Laravelito\Auth\Facades\Auth;
use Illuminate\Support\Collection;
use Laravelito\Field\Constraint\Unique;
use Laravelito\Field\Constraint\Required;
use Laravelito\Field\Constraint\Maxlength;

class TimezoneField extends Field {
    /**
     * Se debe establecer el placeholder del campo
     * @return string
     */

    public function placeholder(): string
    {
        return 'me.fields.timezone.placeholder';
    }

    /**
     * Se debe establecer el label del campo
     * @return string
     */

    public function label(): string
    {
        return 'me.fields.timezone.label';
    }

    /**
     * Se debe establecer el nombre del campo
     * @return string
     */

    public function name(): string
    {
        return 'timezone';
    }

    /**
     * Se debe establecer el tipo del campo
     * @return Type
     */

    public function type(): Type
    {
        $timezones = timezone_identifiers_list();
        $opts = [];

        foreach($timezones as $timezone){
            $opts[$timezone] = $timezone;
        }

        return new Enum($opts, false);
    }

    /**
     * Se debe establecer una coleccion de Constaints.
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        $constraints[] = new Required();
        return $constraints;
    }
}