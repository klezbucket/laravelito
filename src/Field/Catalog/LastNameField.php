<?php

namespace Laravelito\Field\Catalog;

use Illuminate\Support\Collection;
use Laravelito\Field\Field;
use Laravelito\Field\Type\Text;
use Laravelito\Field\Type\Type;
use Laravelito\Field\Constraint\Required;
use Laravelito\Field\Constraint\Maxlength;

class LastNameField extends Field {
    /**
     * Se debe establecer el placeholder del campo
     * @return string
     */

    public function placeholder(): string
    {
        return 'me.fields.last_name.placeholder';
    }
    
    /**
     * Se debe establecer el label del campo
     * @return string
     */

    public function label(): string
    {
        return 'me.fields.last_name.label';
    }
    
    /**
     * Se debe establecer el nombre del campo
     * @return string
     */

    public function name(): string
    {
        return 'last_name';
    }

    /**
     * Se debe establecer el tipo del campo
     * @return Type
     */

    public function type(): Type
    {
        return new Text();
    }

    /**
     * Se debe establecer una coleccion de Constaints.
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        $constraints[] = new Required();
        #$constraints[] = new Maxlength(191);
        return $constraints;
    }
}