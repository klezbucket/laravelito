<?php

namespace Laravelito\Field;

use Illuminate\Support\Collection;
use Laravelito\Core\Provider\Provider;
use Laravelito\Field\FieldDependences;

interface FieldProvider extends Provider {
    /**
     * Obtiene la coleccion de campos.
     *
     * @return Collection of Field
     */

    function fields(): Collection;

    /**
     * Obtiene las dependencias de campos.
     *
     * @return FieldDependences
     */

    function dependences(): ?FieldDependences;

    /**
     * Obtiene el namespace de las locales de los campos. 
     *
     * @return string
     */

    function localesAt(): string;
}