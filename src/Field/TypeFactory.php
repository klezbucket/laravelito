<?php

namespace Laravelito\Field;

use Laravelito\Field\Type\Type;
use Laravelito\Field\Type\Text;
use Laravelito\Field\Type\Email;
use Laravelito\Field\Type\Integer;

class TypeFactory {
    /** @var string */
    private $type;

    /**
     * Devuelve una instancia de factory a partir de un nombre.
     * @param string|null $type
     * @return TypeFactory
     */

    public static function with(?string $type = null): TypeFactory
    {
        return new TypeFactory($type);
    }

    /**
     * Constructor
     * @param string|null $type
     * @return void
     */

    private function __construct(?string $type = null)
    {
        $this->type = $type;
    }

    /**
     * Devuelve la instancia de generic.
     * @return Type
     */

    public function build(): Type
    {
        switch($this->type){
            case 'email': 
                return new Email();
            case 'numeric': 
            case 'integer': 
            case 'int': 
                return new Integer();
            case 'text': 
            default:
                return new Text();
        }
    }
}