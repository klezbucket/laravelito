<?php

namespace Laravelito\Field;

use Laravelito\Field\Field as Field;
use Laravelito\Field\Type\Type;
use Illuminate\Support\Collection;

class Generic extends Field {
    /** @var string $placeholder */
    private $placeholder = '';

    /** @var string $label */
    private $label = '';

    /** @var string $name */
    private $name = '';

    /** @var string $help */
    private $help = '';

    /** @var Type $type */
    private $type;

    /** @var Collection $constraints */
    private $constraints;

    /**
     * Constructor
     * @param Collection|null $constraints
     * @return void
     */

    public function __construct(?Type $type = null,?Collection $constraints = null)
    {
        $this->type = $type;
        $this->constraints = $constraints;
    }

    /**
     * Se debe establecer el placeholder del campo
     * @return string
     */

    public function placeholder(): string
    {
        return $this->placeholder;
    }

    /**
     * Se debe establecer el label del campo
     * @return string
     */

    public function label(): string
    {
        return $this->label;
    }

    /**
     * Se debe establecer el nombre del campo
     * @return string
     */

    public function name(): string
    {
        return $this->name;
    }

    /**
     * Se debe establecer el tipo del campo
     * @return Type
     */

    public function type(): Type
    {
        return $this->type;
    }

    /**
     * Se debe establecer una coleccion de Constaints.
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        return $this->constraints;
    }

    /**
     * Set name
     * @param string $name
     * @return void
     */

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Set label
     * @param string $label
     * @return void
     */

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    /**
     * Set help
     * @param string $help
     * @return void
     */

    public function setHelp(string $help): void
    {
        $this->help = $help;
    }

    /**
     * Set placeholder
     * @param string $placeholder
     * @return void
     */

    public function setPlaceholder(string $placeholder): void
    {
        $this->placeholder = $placeholder;
    }

    /**
     * Set type
     * @param Type $type
     * @return void
     */

    public function setType(Type $type): void
    {
        $this->type = $type;
    }

    /**
     * Set constraints
     * @param Collection $constraints
     * @return void
     */

    public function setConstraints(Collection $constraints): void
    {
        $this->constraints = $constraints;
    }

    /**
     * Realiza la traduccion.
     * 
     * @return bool
     */

    public function translate(): bool
    {
        return false;
    }

    /**
     * Devuelve el nombre del campo para el namespace de validacion.
     * Por defecto el mismo nombre.
     * 
     * @return string|null
     */

    public function validationsName(): ?string
    {
        return null;
    }
}
