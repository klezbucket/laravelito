<?php

namespace Laravelito\Field;

use Laravelito\Field\Upload\UploadTrait;

class Upload implements Constraint{
    use UploadTrait;

    /**
     * En laravel, la validacion es 'email'.
     * 
     * @return string
     */

    public function validates(): string
    {
        return 'upload.size:' . $this->size() . '|upload.mimes:' . implode(',', $this->mimes());
    }

    /** 
     * Devuelve el nombre formal.
     * 
     * @return string
     */

    public function name(): string
    {
        return 'upload';
    }
}