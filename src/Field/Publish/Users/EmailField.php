<?php

namespace App\Backend\Field\Users;

use Laravelito\Field\Field;
use Illuminate\Support\Collection;
use Laravelito\Field\Type\Text;
use Laravelito\Field\Type\Type;
use Laravelito\Field\Type\Email;
use Laravelito\Field\Constraint\Unique;
use Laravelito\Field\Constraint\Required;
use Laravelito\Field\Constraint\Maxlength;

class EmailField extends Field {
    /**
     * Se debe establecer el placeholder del campo
     * @return string
     */

    public function placeholder(): string
    {
        return 'users.fields.email.placeholder';
    }

    /**
     * Se debe establecer el label del campo
     * @return string
     */

    public function label(): string
    {
        return 'users.fields.email.label';
    }

    /**
     * Se debe establecer el nombre del campo
     * @return string
     */

    public function name(): string
    {
        return 'email';
    }

    /**
     * Se debe establecer el tipo del campo
     * @return Type
     */

    public function type(): Type
    {
        return new Email();
    }

    /**
     * Se debe establecer una coleccion de Constaints.
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        $constraints[] = new Required();
        $constraints[] = new Unique('users','email',request()->route()->parameter('id') ?? null);
        $constraints[] = new Maxlength(60);
        return $constraints;
    }
}
