<?php

namespace App\Backend\Field\Users;

use Laravelito\Field\Field;
use Laravelito\Field\Type\Text;
use Laravelito\Field\Type\Type;
use Laravelito\Field\Type\Email;
use Illuminate\Support\Collection;
use Laravelito\Field\Type\Boolean;
use Laravelito\Field\Type\Datetime;
use Laravelito\Field\Constraint\Unique;
use Laravelito\Field\Constraint\Required;
use Laravelito\Field\Constraint\Maxlength;

class DeletedAtField extends Field {
    /** @var bool */
    private $edit = false;

    /**
     * Se debe establecer el placeholder del campo
     * @return string
     */

    public function placeholder(): string
    {
        return 'users.fields.deleted_at.placeholder';
    }

    /**
     * Se debe establecer el label del campo
     * @return string
     */

    public function label(): string
    {
        return 'users.fields.deleted_at.label';
    }

    /**
     * Se debe establecer el nombre del campo
     * @return string
     */

    public function name(): string
    {
        return 'deleted_at';
    }

    /**
     * Se debe establecer el tipo del campo
     * @return Type
     */

    public function type(): Type
    {
        if($this->edit){
            return new Boolean('users.fields.status.boolean.true','users.fields.status.boolean.false');
        }

        return new Datetime();
    }

    /**
     * Se debe establecer una coleccion de Constraints.
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }

    /**
     * Determina si el campo es para el form de edit/delete
     * @return Field
     */

    public function edit(): Field
    {
        $this->edit = true;
        return $this;
    }
}
