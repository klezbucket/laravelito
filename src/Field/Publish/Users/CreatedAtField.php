<?php

namespace App\Backend\Field\Users;

use Laravelito\Field\Field;
use Laravelito\Field\Type\Text;
use Laravelito\Field\Type\Type;
use Illuminate\Support\Collection;
use Laravelito\Field\Type\Datetime;
use Laravelito\Field\Constraint\Required;
use Laravelito\Field\Constraint\Maxlength;

class CreatedAtField extends Field {
    /**
     * Se debe establecer el label del campo
     * @return string
     */

    public function label(): string
    {
        return 'users.fields.created_at.label';
    }

    /**
     * Se debe establecer el nombre del campo
     * @return string
     */

    public function name(): string
    {
        return 'created_at';
    }

    /**
     * Se debe establecer el tipo del campo
     * @return Type
     */

    public function type(): Type
    {
        return new Datetime();
    }

    /**
     * Se debe establecer una coleccion de Constaints.
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }
}
