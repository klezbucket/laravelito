<?php

namespace App\Backend\Field\Users;

use App\Model\Platform;
use Laravelito\Field\Field;
use Laravelito\Field\Type\Type;
use Illuminate\Support\Collection;
use Laravelito\Field\Type\Foreign;
use Laravelito\Core\Facades\Router;
use Laravelito\Field\Constraint\Required;

class PlatformsField extends Field {
    /**
     * Se debe establecer el placeholder del campo
     * @return string
     */

    public function placeholder(): string
    {
        return 'users.fields.platforms.placeholder';
    }

    /**
     * Se debe establecer el label del campo
     * @return string
     */

    public function label(): string
    {
        return 'users.fields.platforms.label';
    }

    /**
     * Se debe establecer el nombre del campo
     * @return string
     */

    public function name(): string
    {
        return 'platforms';
    }

    /**
     * Se debe establecer el tipo del campo
     * @return Type
     */

    public function type(): Type
    {
        return new Foreign(Router::route('platforms.autocomplete'), Platform::class, true);
    }

    /**
     * Se debe establecer una coleccion de Constaints.
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        return $constraints;
    }
}
