<?php

namespace App\Backend\Field\Users;

use Laravelito\Field\Field;
use Illuminate\Support\Collection;
use Laravelito\Field\Type\Type;
use Laravelito\Field\Type\Password;
use Laravelito\Field\Constraint\Nullable;
use Laravelito\Field\Constraint\Required;
use Laravelito\Field\Constraint\Maxlength;
use Laravelito\Field\Constraint\Minlength;

class PasswordField extends Field {

    /**
     * Se debe establecer el placeholder del campo
     * @return string
     */

    public function placeholder(): string
    {
        return 'users.fields.password.placeholder.create';
    }

    /**
     * Se debe establecer el label del campo
     * @return string
     */

    public function label(): string
    {
        return 'users.fields.password.label';
    }

    /**
     * Se debe establecer el nombre del campo
     * @return string
     */

    public function name(): string
    {
        return 'password';
    }

    /**
     * Se debe establecer el tipo del campo
     * @return Type
     */

    public function type(): Type
    {
        return new Password();
    }

    /**
     * Se debe establecer una coleccion de Constaints.
     * @return Collection of Constraint
     */

    public function constraints(): Collection
    {
        $constraints = collect();
        $constraints[] = new Required();
        $constraints[] = new Minlength(6);
        return $constraints;
    }
}
