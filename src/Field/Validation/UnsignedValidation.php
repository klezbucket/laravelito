<?php

namespace Laravelito\Field\Validation;

use Exception;
use Laravelito\Picture\Picture;
use Laravelito\Upload\File\File;

class UnsignedValidation {
    
    /**
     * Valida que el archivo sea entero positivo
     */

    public static function validate($attribute, $value, $parameters)
    {
        return $value >= 0;
    }
}