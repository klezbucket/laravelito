<?php

namespace Laravelito\Field;

use Laravelito\Field\Generic;
use Laravelito\Field\Type\Type;
use Illuminate\Support\Collection;

class Factory {
    /** @var Generic */
    private $field;

    /**
     * Devuelve una instancia de factory a partir de Type y Constraints.
     * @param Type|null $type
     * @param Collection|null $constraints
     * @return Factory
     */

    public static function with(?Type $type = null,?Collection $constraints = null): Factory
    {
        return new Factory($type,$constraints);
    }

    /**
     * Constructor
     * @param Type|null $type
     * @param Collection|null $constraints
     * @return void
     */

    private function __construct(?Type $type = null,?Collection $constraints = null)
    {
        $this->generic = new Generic($type,$constraints);
    }

    /**
     * Devuelve la instancia de generic.
     * @return Generic
     */

    public function build(): Generic
    {
        return $this->generic;
    }

    /**
     * Setea nombre.
     * @param string $name
     * @return Factory
     */

    public function name(string $name): Factory
    {
        $this->generic->setName($name);
        return $this;
    }

    /**
     * Setea label.
     * @param string $label
     * @return Factory
     */

    public function label(string $label): Factory
    {
        $this->generic->setLabel($label);
        return $this;
    }

    /**
     * Setea placeholder.
     * @param string $placeholder
     * @return Factory
     */

    public function placeholder(string $placeholder): Factory
    {
        $this->generic->setPlaceholder($placeholder);
        return $this;
    }

    /**
     * Setea help.
     * @param string $help
     * @return Factory
     */

    public function help(string $help): Factory
    {
        $this->generic->setHelp($help);
        return $this;
    }

    /**
     * Setea type.
     * @param Type $type
     * @return Factory
     */

    public function type(Type $type): Factory
    {
        $this->generic->setType($type);
        return $this;
    }

    /**
     * Setea constraints.
     * @param Collection $constraints
     * @return Factory
     */

    public function constraints(Collection $constraints): Factory
    {
        $this->generic->setConstraints($constraints);
        return $this;
    }
}