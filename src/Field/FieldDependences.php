<?php

namespace Laravelito\Field;

use Laravelito\Field\FieldProvider;
use Laravelito\Field\Dependences\When;
use Laravelito\CRUD\Model\ModelProvider;
use Laravelito\Field\Dependences\WhenNot;

class FieldDependences {
    /** @var array */
    private $deps = [];

    /**
     * Determina la dependencia de un campo sobre los posibles valores de otro.
     * @param string $master
     * @param string $slave
     * @return When
     */

    public function when(string $master,array $options): When
    {
        return new When($this,$master,$options);
    }

    /**
     * Devuelve el array de dependencias.
     * @param array|null $deps
     * @return array
     */

    public function deps(?array $deps = null): array
    {
        if(isset($deps)){
            $this->deps = $deps;
        }

        return $this->deps;
    }

    /**
     * Dado el field provider, se deducen los campos a deshabilitar por las reglas.
     * @param FieldProvider $provider
     * @param array $values
     * @return array la coleccion de campos a deshabilitar
     */

    public static function shutdown(FieldProvider $provider,array $values = []): array
    {
        $slaves = [];

        if($deps = $provider->dependences()){
            $deps = $deps->deps();

            foreach($deps as $field => $conditions){
                $value = $values[$field] ?? request()->get($field);

                foreach($conditions as $condition => $options){
                    switch($condition){
                        case 'when':
                            if(isset($options[$value])){
                                foreach($options[$value] as $action => $fields){
                                    switch($action){
                                        case 'disable':
                                            $slaves += $fields; 
                                    }
                                }
                            }
                            break;
                    }
                }
            }
        }

        return array_unique($slaves);
    }
}
