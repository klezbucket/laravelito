<?php

namespace Laravelito\Field\Dependences;

use Laravelito\Field\Dependences\Condition;

abstract class Action {
    /** @var Condition */
    private $condition;

    /** @var string */
    private $slave;

    /**
     * Instancia la acction.
     * 
     * @param Condition $condition
     * @param string $slave
     * @return void
     */

    public function __construct(Condition $condition,string $slave)
    {
        $this->condition = $condition;
        $this->slave = $slave;
    }

    /**
     * Getter condition
     * @return Condition
     */

    public function condition(): Condition
    {
        return $this->condition;
    }

    /**
     * Getter slave
     * @return string
     */

    public function slave(): string
    {
        return $this->slave;
    }

    /**
     * Provee la accion
     * @return string
     */

    public abstract function name(): string;

    /**
     * Aplica a la configuracion las nuevas entradas.
     * @return array
     */

    public function apply(): array
    {
        return $this->condition()->apply($this);
    }
}