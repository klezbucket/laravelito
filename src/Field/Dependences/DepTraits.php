<?php
namespace Laravelito\Field\Dependences;

use Laravelito\Field\FieldDependences;
use Laravelito\Field\Dependences\Enable;
use Laravelito\Field\Dependences\Disable;

trait DepTraits {
    /**
     * Habilita el esclavo
     * @param string $slave
     * @return FieldDependences
     */

    public function enable(string $slave): FieldDependences
    {
        $disabler = new Enable($this, $slave);
        $this->deps()->deps($disabler->apply());
        return $this->deps();
    }

    /**
     * Deshabilita el esclavo
     * @param string $slave
     * @return FieldDependences
     */

    public function disable(string $slave): FieldDependences
    {
        $disabler = new Disable($this, $slave);
        $this->deps()->deps($disabler->apply());
        return $this->deps();
    }
}