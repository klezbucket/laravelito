<?php

namespace Laravelito\Field\Dependences;

use Laravelito\Field\FieldDependences;
use Laravelito\Field\Dependences\Action;
use Laravelito\Field\Dependences\DepTraits;

abstract class Condition {
    use DepTraits;

    /** @var string */
    private $master;

    /** @var FieldDependences */
    private $deps;

    /**
     * Instancia el WHEN.
     * @param string $master
     * @return void
     */

    public function __construct(FieldDependences $deps,string $master)
    {
        $this->deps = $deps;
        $this->master = $master;
    }

    /**
     * Getter master
     * @return string
     */

    public function master(): string
    {
        return $this->master;
    }

    /**
     * Getter deps
     * @return FieldDependences
     */

    public function deps(): FieldDependences
    {
        return $this->deps;
    }

    /**
     * Determina el nombre de la condicion.
     * @return string
     */

    public abstract function name(): string;

    /**
     * Devuelve la configuracion
     * @return array
     */

    public abstract function apply(Action $action): array;
}