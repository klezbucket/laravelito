<?php

namespace Laravelito\Field\Dependences;

use Laravelito\Field\FieldDependences;
use Laravelito\Field\Dependences\Action;
use Laravelito\Field\Dependences\Disable;
use Laravelito\Field\Dependences\Condition;

class When extends Condition {
    /** @var array */
    private $options;

    /**
     * Instancia el WHEN.
     * @param FieldDependences $deps
     * @param string $master
     * @param array $options
     * @return void
     */

    public function __construct(FieldDependences $deps,string $master,array $options)
    {
        parent::__construct($deps,$master);
        $this->options = $options;
    }

    /**
     * Getter options
     * @return array
     */

    public function options(): array
    {
        return $this->options;
    }


    /**
     * Determina el nombre de la condicion.
     * @return string
     */

    public function name(): string
    {
        return 'when';
    }

    /**
     * Devuelve la configuracion
     * @return array
     */

    public function apply(Action $action): array{
        $deps = $this->deps()->deps();
        $master = $this->master();
        $condition = $this->name();
        $act = $action->name();

        foreach($this->options() as $option){
            if(!isset($deps[$master][$condition][$option][$act])){
                $deps[$master][$condition][$option][$act] = [];
            }

            if(! in_array($action->slave(), $deps[$master][$condition][$option][$act])){
                $deps[$master][$condition][$option][$act][] = $action->slave();
            }
        }
    
        return $deps;
    }
}