<?php

namespace Laravelito\Field\Dependences;

use Laravelito\Field\Dependences\Action;

class Disable extends Action {
    /**
     * Provee la accion
     * @return string
     */

    public function name(): string
    {
        return 'disable';
    }
}