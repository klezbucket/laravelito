<?php

namespace Laravelito\Field\Dependences;

use Laravelito\Field\Dependences\Action;

class Enable extends Action {
    /**
     * Provee la accion
     * @return string
     */

    public function name(): string
    {
        return 'enable';
    }
}