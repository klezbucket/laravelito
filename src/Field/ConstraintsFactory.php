<?php

namespace Laravelito\Field;

use Laravelito\Field\Type\Text;
use Laravelito\Field\Type\Type;
use Laravelito\Field\Type\Email;
use Illuminate\Support\Collection;
use Laravelito\Field\Type\Integer;
use Laravelito\Field\Constraint\Required;

class ConstraintsFactory {
    /** @var string */
    private $config;

    /** @var Collection */
    private $contraints;

    /**
     * Devuelve una instancia de factory a partir de un array de config.
     * @param string|null $type
     * @return TypeFactory
     */

    public static function with(array $config): ConstraintsFactory
    {
        return new ConstraintsFactory($config);
    }

    /**
     * Constructor
     * @param string|null $type
     * @return void
     */

    private function __construct(array $config)
    {
        $this->constraints = collect();
        $this->config = $config;
    }

    /**
     * Devuelve la instancia de generic.
     * @return Collection
     */

    public function build(): Collection
    {
        $this->constraints = collect();

        return $this->required()->get();
    }

    /**
     * Determina si el campo posee required.
     * @return ConstraintsFactory
     */

    private function required(): ConstraintsFactory
    {   
        if($required = ($this->config['required'] ?? false)){
            $this->constraints->push(new Required());
        }

        return $this;
    }

    /**
     * Obtiene los constraints.
     * @return Collection
     */

     public function get(): Collection
     {
        return $this->constraints;
     }
}