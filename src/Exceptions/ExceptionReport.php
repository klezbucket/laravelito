<?php
namespace Laravelito\Exceptions;

use Throwable;
use Laravelito\Core\Logs\Logging;
use Illuminate\Support\Facades\Log;

class ExceptionReport {
    /**
     * Reporta la excepcion en los logs pertinentes.
     * 
     * @param Throwable $e
     * @return void
     */

    public static function log(Throwable $e): void
    {
        Logging::section('CATCH');
        Logging::output('Exception: ' . $e->getMessage());
        Log::critical($e->getMessage());
        Log::critical($e->getTraceAsString());
    }

    /**
     * Public message, si DEBUG esta activado, o NULL si es production.
     * 
     * @param Throwable $e
     * @return string|null
     */

    public static function message(Throwable $e): ?string
    {
        if(config('app.debug')){
            return $e->getMessage();
        }

        return null;
    }
}