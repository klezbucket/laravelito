<?php

/**
 * Utiliza la traduccion de laravelito.
 * 
 * @param string $key
 * @return string|array
 */

if (! function_exists('t')) {
    function t(string $key,array $replace = [],string $locale = null)
    {
        if(empty($key)) return '';
        
        return Translator::t($key,$replace,$locale);
    }
}