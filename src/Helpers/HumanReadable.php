<?php

namespace Laravelito\Helpers;

class HumanReadable {
    /**
     * Devuelve una cantidad de bytes en humano.
     *
     * @param int $bytes
     * @return string
     */
    public static function bytesToHuman(int $bytes): string
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
        for ($i = 0; $bytes >= 1000; $i++) $bytes /= 1000;
        return round($bytes, 2) . ' ' . $units[$i];
    }
}