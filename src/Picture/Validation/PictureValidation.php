<?php

namespace Laravelito\Picture\Validation;

use Exception;
use Laravelito\Picture\Picture;
use Laravelito\Upload\File\File;

class PictureValidation {
    
    /**
     * Valida el archivo como una imagen.
     */

    public static function validate($attribute, $value, $parameters)
    {
        if($value){
            $file = new File($value);
            $image = null;

            try{
                $image = Picture::raw($file->content());
            }
            catch(Exception $e){
                return false;
            }
            finally{
                unset($file);
                unset($image);
            }
        }

        return true;
    }
}