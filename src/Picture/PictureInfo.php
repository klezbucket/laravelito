<?php

namespace Laravelito\Picture;

use Exception;
use Laravelito\Download\File\File;
use Laravelito\Picture\PictureInfo;
use Laravelito\Upload\LocalStorage;
use Illuminate\Database\Eloquent\Model;

class PictureInfo {
    /** @var string */
    const UNKNOWN_EXTENSION = 'unknown';

    /** @var array $info */
    private $info = [];

    /**
     * Constructor recibo el info de gd.
     * @param array $info
     * @return void
     */

    private function __construct(array $info)
    {
        $this->info = $info;
    }

    /**
     * Devuelve el array de info gd desde un path.
     * 
     * @param string $path
     * @return PictureInfo
     * @throws Exception si el archivo no es accesible.
     */

    public static function file(string $path): PictureInfo
    {
        if(File::isReadable($path)){
            $info = getimagesize($path);
        }

        return new PictureInfo($info ?? []);
    }

    /**
     * Devuelve el array de info gd desde un modelo y campo determinados.
     * 
     * @param Model $model
     * @param string $field
     * @param PictureInfo
     */

    public static function model(Model $model,string $field): PictureInfo
    {
        $path = LocalStorage::fullpath($model,$field);
        return PictureInfo::file($path);
    }

    /**
     * Devuelve la extension del archivo.
     * @return string
     */

    public function extension(): string
    {
        if($ext = (image_type_to_extension($this->info[2] ?? -1))){
            return $ext;
        }

        return PictureInfo::UNKNOWN_EXTENSION;
    }
}