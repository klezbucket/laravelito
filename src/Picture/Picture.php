<?php

namespace Laravelito\Picture;

use Exception;

class Picture {
    /** @var $image la imagen cargada con gd */
    private $image = null;

    /**
     * Devuelve una instancia de la imagen gd.
     * @param string $picture
     */

    public static function raw(string $picture): Picture
    {
        return new Picture($picture);
    }

    /**
     * Constructor de picture.
     * 
     * @param string $picture
     * @return void
     * @throws Exception si gd falla en cargar la imagen.
     */

    private function __construct(string $picture)
    {
        $this->image = imagecreatefromstring($picture);

        if($this->image === false){
            throw new Exception('Cannot load picture');
        }
    }
}