<?php 
namespace Laravelito\Report\Type;

use Closure;
use Laravelito\Report\Report;

abstract class CSVReport extends Report {
    /**
     * Establece el nombre de archivo.
     * 
     * @return string
     */

    abstract public function filename(): string;

    /**
     * Establece las columnas del csv.
     * 
     * @return array
     */

    abstract public function cols(): array;

    /**
     * Callback para proceso de la coleccion.
     * @return Closure|null
     */

    public function callback(): ?Closure
    {
        return null;
    }
}