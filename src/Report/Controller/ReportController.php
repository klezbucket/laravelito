<?php

namespace Laravelito\Report\Controller;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Laravelito\Core\Facades\Flash;
use Laravelito\Core\Facades\Theme;
use Laravelito\Report\Service\ReportService;
use Symfony\Component\HttpFoundation\Response;

abstract class ReportController extends Controller {
    /** @var ReportService */
    private $Report;

    /**
     * Devuelve el nombre del module.
     * @return string
     */
    public abstract function module(): string;

    /**
     * Create a new controller instance.
     *
     * @param ReportService $report
     * @return void
     */

    public function __construct(ReportService $report)
    {
        $this->Report = $report;
    }

    /**
     * Reporte con filtro y tabla.
     * 
     * @param Request $request
     * @param string $report
     * @return View|Response
     */

     public function csv(Request $request)
     {
        $view = Theme::controller('report.filter','backend');
        $view->with('form', $this->Report->form($this->module()));

        switch($this->Report->csv($request, $this->module())){
            case ReportService::SUCCESS:
                return $this->Report->response();
            case ReportService::FAILURE:
                Flash::danger(t('report.' . $this->module() . '.failure'));
                break;
            case ReportService::INVALID:
                Flash::danger(t('report.' . $this->module() . '.invalid'));
                break;
            case ReportService::DRY:
                break;
        }

        return $view;
     }
}

