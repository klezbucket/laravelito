<?php

namespace Laravelito\Report\Logs;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Laravelito\Core\Logs\Logging as LC;

class Logging {
    /** @var int */
    public static $TAB = 10;

    /** @var string */
    public static $ID;

    /** @var string */
    public static $CHANNEL = 'report';

    /**
     * Changes the Log's Channel.
     * @param string $ch the new channel
     */

    public static function channel(string $ch)
    {
        self::$CHANNEL = $ch;
    }

    /**
     * Writes out the message into the web log file.
     * @param string $message
     * @param int $blocks the # blocks to prepend into the message
     */

    public static function output(string $message,int $blocks = 1)
    {
        Log::channel(self::$CHANNEL)->info(LC::id() . ' ' . str_repeat('#',$blocks) . ' ' . $message);
    }

    /**
     * Writes out the message into the web log file.
     * @param string $message
     * @param int $strips the arrow lenght to prepend into the message
     */

    public static function section(string $message,int $strips = 25,string $symbol = '-')
    {
        self::output(str_repeat($symbol,$strips) . '# ' . $message . ' #' . str_repeat($symbol,$strips));
    }


    /**
     * Writes out the message among # bars.
     * @param string $title
     * @param int $blocks the bar length after and before the message
     * @return void
     */

    public static function separator(string $title, int $blocks = 25): void
    {
        $message = strtoupper($title);
        self::output(str_repeat('#',$blocks) . ' ' .  $message . ' ' . str_repeat('#',$blocks));
    }

    /**
     * Loguea la excepcion y su traza
     * @param Throwable $exception
     * @return void
     */

    public static function exception(Throwable $exception): void
    {
        self::output('Exception: ' . $exception->getMessage());
        self::section('TRACE');

        $trace = explode("\n",$exception->getTraceAsString());

        foreach($trace as $msg){
            self::output($msg);
        }
    }
}
