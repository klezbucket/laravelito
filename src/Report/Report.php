<?php

namespace Laravelito\Report;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\Response;

abstract class Report {
    /** @var Request */
    private $Request;

    /**
     * Constructor debe recibir un Request validado.
     * 
     * @param Request $request
     * @return void
     */

    public function __construct(Request $request)
    {
        $this->Request = $request;
    }

    /**
     * Expone el request.
     * 
     * @return Request
     */

    public function request(): Request
    {
        return $this->Request;
    }

    /**
     * Expone la query del reporte.
     * 
     * @return Builder
     */

    abstract public function query(): Builder;
}