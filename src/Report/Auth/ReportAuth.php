<?php

namespace Laravelito\Report\Auth;

use Illuminate\Http\Request;
use Laravelito\Auth\Facades\Auth;
use Laravelito\CRUD\Context\Context;
use Illuminate\Validation\UnauthorizedException;
use Laravelito\Core\Exceptions\ForbiddenException;

class ReportAuth {

    /**
     * Verifica permisos para acceder al modulo de Reporte.
     *
     * @param Request $request
     * @param array $permits
     * @return void
     * @throws ForbiddenException si no se posee permisos
     * @throws UnauthorizedException si no esta autorizado
     */

    public static function ensure(Request $request, array $permits): void
    {
        self::permits($permits);
        self::authorize($request);
    }

    /**
     * Verifica que el usuario autorizado disponga de los permisos para ejecutar el Reporte.
     *
     * @param Context $context
     * @throws ForbiddenException si la autorizacion falla.
     */

    public static function permits(array $permits)
    {
        foreach($permits as $permit){
            if(! Auth::permit($permit)){
                throw new ForbiddenException();
            }
        }
    }

    /**
     * Ejecuta la autorizacion Request.
     *
     * @param Request $request
     * @throws UnauthorizedException si la autorizacion falla.
     */

    public static function authorize(Request $request)
    {
        if(! $request->authorize()){
            throw new UnauthorizedException();
        }
    }
}
