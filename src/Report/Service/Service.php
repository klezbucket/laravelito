<?php

namespace Laravelito\Report\Service;

use Illuminate\Http\Request;
use Laravelito\Report\Report;
use Laravelito\Report\Service\ReportService;
use Symfony\Component\HttpFoundation\Response;
use Laravelito\Core\Exceptions\InternalServerException;

abstract class Service{
    /**
     * Instancia un reporte
     * 
     * @param Request $request
     * @param string $module
     * @return Report
     */

    public static function report(Request $request, string $module): report
    {
        $class = ReportService::config($module . '.report');
        $parent = Report::class;

        if(! is_a($class, $parent, true)){
            throw new InternalServerException("{$class} is not a {$parent} instance");
        }

        return new $class($request);
    }
    
    /**
     * La funcion que ejecuta el reporte.
     *
     * @return void
     */

    abstract public function run(): void;

    /**
     * Expone la respuesta del reporte
     * @return Response
     */

    abstract public function response(): Response;
}