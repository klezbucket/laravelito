<?php

namespace Laravelito\Report\Service;

use Closure;
use Throwable;
use Laravelito\Form\Form;
use Illuminate\Http\Request;
use Laravelito\Core\Logs\Sanitize;
use Laravelito\Report\Logs\Logging;
use Illuminate\Support\Facades\Config;
use Laravelito\Report\Auth\ReportAuth;
use Laravelito\Report\Service\Service;
use Laravelito\Core\Facades\Form as FF;
use Laravelito\CRUD\Request\BaseRequest;
use Laravelito\Report\Request\FilterRequest;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Laravelito\Report\Service\CSVReportService;
use Laravelito\Core\Exceptions\InternalServerException;

class ReportService {
    /** @var int no pasa nada */
    const IGNORE = 0;

    /** @var int success */
    const SUCCESS = 1;

    /** @var int failure */
    const FAILURE = 2;

    /** @var int invalid */
    const INVALID = 3;

    /** @var int dry */
    const DRY = 4;

    /** @var FilterRequest */
    private $Request;

    /** @var Service */
    private $Service;

    /**
     * Lee una configuracion del archivo report.
     * 
     * @param string $path
     * @return string|array
     * @throws InternalServerException si no se encuentra la configuracion especificada
     */

    public static function config(string $path)
    {
        $fullpath = 'report.' . $path;

        if($value = Config::get($fullpath)){
            return $value;
        }

        throw new InternalServerException('Report config path not found: ' . $fullpath);
    }

    /**
     * Obtiene el nombre del formulario
     * @param string $module
     * @return string
     */

    public function form(string $module): string
    {
        return self::config($module . '.form');
    }

    /**
     * Obtiene los permisos para ver el reporte.
     * @param string $module
     * @return array
     */

    public function permits(string $module)
    {
        return self::config($module . '.permits');
    }


    /**
     * Procesa el request filter. Ejecuta el closure proveido.
     * 
     * @param Closure $closure
     * @return int
     */

    public function filter(Closure $closure, Request $request, string $module): int
    {
        try{
            $this->Request = $this->request($request, $module);
            $permits = $this->permits($module);
            ReportAuth::ensure($this->Request, $permits);
            return $closure();
        }
        catch(Throwable $e){
            return $this->exception($module,$e);
        }
    }

    /**
     * Procesa una tabla con filtros.
     * 
     * @param Request $request
     * @param string $module
     * @return int
     */

    public function csv(Request $request, string $module): int
    {
        return $this->filter(function() use ($request,$module){
            $form = $this->form($module);
            Logging::section('REPORT CSV');
            Logging::output('Filter: ' . $form);

            if($request->isMethod('POST')){
                Logging::output('Data: ' . serialize(Sanitize::body($this->Request->all())));

                $this->Request->validate();
                $this->Service = CSVReportService::instantiate($this->Request, $module);
                $this->Service->run();
                Logging::output('Status: success');
                return self::SUCCESS;
            }

            return self::IGNORE;
        }, $request, $module);
    }

    /**
     * Maneja la exception generada en el servicios.
     * @param string $module
     * @param Throwable $exception
     * @return int
     */

    private function exception(string $module, Throwable $exception): int
    {
        if($exception instanceof ValidationException){
            $errors = $this->Request->errors();
            FF::invalid($errors,$this->form($module));
    
            Logging::output('Errors: ' . serialize($errors));
            Logging::output('Status: invalid');
            return self::INVALID;
        }
        else{       
            Logging::exception($exception);
            Logging::output('Status: failure');
            return self::FAILURE;    
        }
    }

    /**
     * Carga el FilterRequest y prepara su validacion
     * @return FilterRequest
     */

    private function request(Request $request, string $module): FilterRequest
    {
        if($req = $this->Request){
            return $req;
        }

        $form = Form::instantiate($this->form($module));
        $this->Request = new FilterRequest($form);
        BaseRequest::copyData($form, $request, $this->Request);
        return $this->Request;
    }

    /**
     * Expone la respuesta del reporte.
     * @return Response
     */

    public function response(): Response
    {
        return $this->Service->response();
    }
}