<?php

namespace Laravelito\Report\Service;

use Throwable;
use Illuminate\Http\Request;
use Laravelito\Report\Report;
use Laravelito\Report\Type\CSVReport;
use Laravelito\Report\Service\Service;
use Illuminate\Database\Eloquent\Model;
use Laravelito\Report\Request\FilterRequest;
use Symfony\Component\HttpFoundation\Response;
use Laravelito\Download\Service\DownloadService;

class CSVReportService extends Service {
    /** @var DownloadService */
    private $Service;

    /** @var CSVReport */
    private $Report;

    /**
     * Instancia la clase.
     * Descarga un CSV segun los filtros establecidos.
     * 
     * @param Report $report
     * @return void
     */

    private function __construct(DownloadService $service,CSVReport $report){
        $this->Report = $report;
        $this->Service = $service;
    }

    /**
     * Instancia un servicio de TABLE with Filter
     * 
     * @param FilterRequest $request
     * @param string $module
     * @return CSVReportService
     */

    public static function instantiate(FilterRequest $request,string $module): CSVReportService
    {
        $report = Service::report($request,$module);
        $parent = CSVReport::class;
        $class = get_class($report);

        if(! is_a($class, $parent, true)){
            throw new InternalServerException("{$class} is not a {$parent} instance");
        }

        return new CSVReportService(new DownloadService(), $report);
    }

    /**
     * Realiza la operacion LIST con validacion de datos del Request.
     *
     * @return void el estado de la operacion
     */

    final public function run(): void
    {
        $collection = $this->Report->query()->cursor();
        $filename = $this->Report->filename();
        $cols = $this->Report->cols();

        $download = $this->Service->csv($filename,$collection,$cols);

        if($cb = $this->Report->callback()){
            $download->setCallback($cb);
        }
    }

    /**
     * Expone el request del reporte.
     * @return Request
     */

    public function request(): Request
    {
        return $this->Report->request();
    }

    /**
     * Expone la respuesta del reporte
     * @return Response
     */

    public function response(): Response
    {
        return $this->Service->response();
    }
}