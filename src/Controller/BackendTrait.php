<?php
namespace Laravelito\Controller;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravelito\Auth\Facades\Auth;
use Laravelito\Core\Facades\Flash;
use Laravelito\Core\Facades\Theme;
use Laravelito\Core\Facades\Router;
use Laravelito\CRUD\Service\CRUDService;
use Laravelito\Auth\Services\AuthService;
use Laravelito\Locales\Services\LocaleService;
use Symfony\Component\HttpFoundation\RedirectResponse;

trait BackendTrait {
    /** @var AuthService */
    private $Auth;

    /** @var CRUDService */
    private $CRUD;

    /** @var LocaleService */
    private $Locale;

    /**
     * Create a new controller instance.
     *
     * @param AuthService $auth
     * @return void
     */

    public function __construct(AuthService $auth, CRUDService $crud,LocaleService $locale)
    {
        $this->Auth = $auth;
        $this->CRUD = $crud;
        $this->Locale = $locale;
    }

    /**
     * Renderea el Dashboard de este Backend.
     *
     * @param Request $request
     * @return mixed
     */

    public function home(Request $request)
    {
        return Theme::controller('home','backend');
    }

    /**
     * Renderea y Gestiona el Login de este Backend.
     *
     * @param Request $request
     * @return mixed
     */

    public function login(Request $request)
    {
        if($request->isMethod('POST')){
            if($this->Auth->authenticate($request)){
                $locale = $this->Locale->loginCallback($request,AuthService::getAuthorized());
                return $this->Auth->redirect();
            }
        }

        return Theme::controller('login','login');
    }

    /**
     * Gestiona el Logout de este Backend.
     *
     * @param Request $request
     * @return Response
     */

    public function logout(Request $request): RedirectResponse
    {
        $this->Auth->forget($request);
        return $this->Auth->login();
    }

    /**
     * Permite editar la cuenta del Usuario autenticado.
     *
     * @param Request $request
     * @return Response|View
     */

    public function me(Request $request)
    {
        switch($this->CRUD->edit($request, 'me', false)){
            case CRUDService::SUCCESS:
                Flash::success(t('me.success',[],Auth::user('lang')));
                return Router::redirect('backend.home');
            case CRUDService::FAILURE:
                Flash::danger(t('me.failure'));
                break;
            case CRUDService::INVALID:
                Flash::danger(t('me.invalid'));
                break;
        }

        return Theme::controller('me','backend');
    }
}
