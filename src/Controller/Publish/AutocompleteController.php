<?php

namespace App\Backend\Controller;

use App\Model\Platform;
use App\Model\Role;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Laravelito\Controller\AutocompleteTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Laravelito\Autocomplete\Model\AutocompleteModelInterface;

class AutocompleteController extends Controller {
    use AutocompleteTrait;

    /**
     * AUTOCOMPLETE platforms
     *
     * @param Request $request
     * @return JsonResponse
     */

    public function platforms(Request $request): JsonResponse
    {
        return $this->autocomplete($request, Platform::class);
    }

    /**
     * AUTOCOMPLETE roles
     *
     * @param Request $request
     * @return JsonResponse
     */

    public function roles(Request $request): JsonResponse
    {
        return $this->autocomplete($request, Role::class);
    }
}
