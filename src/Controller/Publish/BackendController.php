<?php

namespace App\Backend\Controller;

use App\Backend\Controller\AppController;
use Illuminate\Http\Request;
use Laravelito\Auth\Facades\Auth;
use Laravelito\Core\Facades\Flash;
use Laravelito\Core\Facades\Theme;
use Laravelito\Core\Facades\Router;
use Laravelito\Controller\BackendTrait;
use Laravelito\CRUD\Service\CRUDService;
use Laravelito\Auth\Services\AuthService;
use Laravelito\View\Breadcrumb\Breadcrumb;
use Laravelito\Locales\Services\LocaleService;
use Laravelito\View\Breadcrumb\Item\LinkBreadcrumbItem;

class BackendController extends AppController {
    use BackendTrait;

    /** @var AuthService */
    private $Auth;

    /** @var CRUDService */
    private $CRUD;

    /** @var LocaleService */
    private $Locale;


    /**
     * Constructor con servicios inyectados
     * @param CustomerizeService $customerize
     * @param AuthService $auth
     * @param CRUDService $crud
     * @param LocaleService $locale
     * @return void
     */

    public function __construct(AuthService $auth, CRUDService $crud,LocaleService $locale)
    {
        $this->Auth = $auth;
        $this->CRUD = $crud;
        $this->Locale = $locale;
    }
}
