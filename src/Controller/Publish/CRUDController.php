<?php
namespace App\Backend\Controller;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Laravelito\View\Page\Title;
use App\Backend\Menu\BackendMenu;
use Laravelito\Core\Facades\Router;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Service\CRUDService;
use Laravelito\Auth\Services\AuthService;
use Laravelito\View\Breadcrumb\Breadcrumb;
use App\Backend\Controller\ControllerTrait;
use Laravelito\Download\Service\DownloadService;
use Laravelito\CRUD\Controller\CRUDController as CC;
use Laravelito\View\Breadcrumb\Item\LinkBreadcrumbItem;
use Laravelito\View\Breadcrumb\Item\LabelBreadcrumbItem;

abstract class CRUDController extends CC {
    use ControllerTrait;

    /**
     * Create a new controller instance.
     *
     * @param AuthService $auth
     * @param CRUDService $crud
     * @param DownloadService $download
     * @return void
     */

    public function __construct(AuthService $auth, CRUDService $crud,DownloadService $download)
    {
        parent::__construct($auth,$crud,$download);
    }

    /**
     * Implementa onFetch para setear el titulo y el breadcrumb
     *
     * @param Model $model
     * @return void
     */

    public function onFetch(Model $model): void
    {
        Breadcrumb::push(new LinkBreadcrumbItem($this->label($model), Router::route($this->module() . '.fetch', $model)));
        Title::set($this->label($model));
    }

    /**
     * CRUD COLLECTION
     *
     * @param Request $request
     * @return Response|View
     */

    protected function collection(Request $request,string $flavor,int $limit = 10)
    {
        $this->initialize($request);
        Breadcrumb::push(new LinkBreadcrumbItem(t($this->module() . '.collections.' . $flavor . '.title'), Router::here()));
        $response = parent::collection($request, $flavor, $limit);
        return $response;
    }

    /**
     * CRUD CREATE
     *
     * @param Request $request
     * @param bool $dry
     * @return Response|View
     */

    public function create(Request $request)
    {
        $this->initialize($request);
        Breadcrumb::push(new LinkBreadcrumbItem(t($this->module() . '.create.title'), Router::here()));
        return parent::create($request);
    }

    /**
     * CRUD FETCH
     *
     * @param Request $request
     * @return Response|View
     */

    public function fetch(Request $request)
    {
        $this->initialize($request);
        Breadcrumb::push(new LinkBreadcrumbItem(t($this->module() . '.fetch.title'), Router::here()));
        return parent::fetch($request);
    }

    /**
     * CRUD EDIT
     *
     * @param Request $request
     * @return Response|View
     */

    public function edit(Request $request)
    {
        $this->initialize($request);
        Breadcrumb::push(new LinkBreadcrumbItem(t($this->module() . '.edit.title'), Router::here()));
        return parent::edit($request);
    }

    /**
     * INITIALIZATION.
     * Pone el menu activo y el breadcrumb del modulo actual.
     * @param Request $request
     * @return void
     */

    protected function initialize(Request $request): void
    {
        Breadcrumb::push(new LabelBreadcrumbItem(t($this->module() . '.module.name')));
        BackendMenu::active($this->module());
    }

    /**
     * Dado el modelo devuelve el label del mismo.
     *
     * @param Model $model
     * @return string
     */

    public abstract function label(Model $model): string;

    /**
     * No devuelve una vista custom pero sobrecargamos el metodo para
     * devolver el request con fakers.
     *
     * @param Request $request
     * @return View|null
     */

    public function view(Request $request): ?View
    {
        if($request->isMethod('POST')){
            if($data = $request->original){
                $request->merge($data);
                unset($request->original);
            }
        }

        return null;
    }
}
