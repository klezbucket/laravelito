<?php

namespace App\Backend\Controller;

use App\Backend\Controller\ControllerTrait;
use Illuminate\Routing\Controller as BaseController;

class AppController extends BaseController
{
    use ControllerTrait;
}
