<?php

namespace Laravelito\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Laravelito\Autocomplete\AutocompleteCollection;
use Laravelito\Autocomplete\Model\AutocompleteModelInterface;

trait AutocompleteTrait {
    /**
     * Dada la interface de autocompletado, realiza el query con el Request proveido.
     * @param Request $request
     * @param string $class debe implementar AutocompleteModelInterface
     * @return JsonResponse
     */

    protected final function autocomplete(Request $request,string $class): JsonResponse
    {
        $items = $this->autocompleteCollect($request, $class);
        return response()->json($items, 200);
    }

    /**
     * Obtiene solo la coleccion.
     * @return AutocompleteCollection
     */

    protected final function autocompleteCollect(Request $request,string $class): Collection
    {
        if(! is_a($class, AutocompleteModelInterface::class, true)){
            return response()->json([], 500);
        }

        $term = $request->get('q') ?? '';
        $page = $request->get('page') ?? 1;
        $context = $request->get('context') ?? [];
        $items = call_user_func($class . '::autocomplete',$term,$context,$page)->items();
        return $items;
    }
}