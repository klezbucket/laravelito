<?php

namespace App\Backend\Controller;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravelito\View\Page\Title;
use App\Backend\Menu\BackendMenu;
use Laravelito\Core\Facades\Router;
use Illuminate\Database\Eloquent\Model;
use App\Backend\Controller\CRUDController;
use Laravelito\View\Breadcrumb\Breadcrumb;
use Laravelito\View\Breadcrumb\Item\LinkBreadcrumbItem;

class PlaceholdersController extends CRUDController {
    /**
     * Devuelve el modulo.
     *
     * @return string
     */

    public function module(): string
    {
        return BackendMenu::$PLACEHOLDERS;
    }

    /**
     * Devuelve el label de este modulo.
     * @param Model $model
     * @return string
     */

    public function label(Model $model): string
    {
        return $model->LABELHOLDER;
    }

    /**
     * Coleccion de habilitados.
     *
     * @param Request $request
     * @return Response|View
     */

    public function enabled(Request $request)
    {
        return $this->collection($request, 'enabled', 10);
    }

    /**
     * Coleccion de eliminados.
     *
     * @param Request $request
     * @return Response|View
     */

    public function disabled(Request $request)
    {
        return $this->collection($request, 'disabled', 10);
    }
}
