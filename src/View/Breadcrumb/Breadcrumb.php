<?php

namespace Laravelito\View\Breadcrumb;

use Illuminate\Support\Collection;
use Laravelito\View\Breadcrumb\Item\BreadcrumbItem;

abstract class Breadcrumb {
    /** @var Collection singleton para almacenar BreadcrumbItem */
    private static $items;

    /** 
     * Debe devolver la coleccion de BreadcrumbItem.
     * @return Collection
     **/

    abstract public function items(): Collection;

    /**
     * Un singleton Collection para los BreadcrumbItem.
     * 
     * @return Collection
     */

    public static function push(BreadcrumbItem $item): Collection
    {
        $items = self::get();
        $items[] = $item;
        self::$items = $items;
        return $items;
    }

    /**
     * Elimina los breadcrumbs
     * 
     * @return Collection
     */

    public static function flush(): Collection
    {
        $items = collect();
        self::$items = $items;
        return $items;
    }
    
    /**
     * Obtiene los breadcrumbs.
     * 
     * @return Collection
     */

    public static function get(): Collection
    {
        if(! ($items = self::$items)){
            $items = collect();
        }

        self::$items = $items;
        return $items;
    }
}