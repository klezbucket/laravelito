<?php

namespace Laravelito\View\Breadcrumb\Item;

use Laravelito\View\Breadcrumb\Item\BreadcrumbItem;

class LabelBreadcrumbItem extends BreadcrumbItem{

    /** @var string el icono del item */
    private $icon;

    /** @var string el texto del item */
    private $title;

    /**
     * Devuelve una nueva instancia de LabelBreadcrumbItem.
     * 
     * @param string $title
     * @param string|null $icon
     * @return void
     */

    public function __construct(string $title, ?string $icon = null)
    {
        $this->icon = $icon;
        $this->title = $title;
    }

    /**
     * Devuelve el texto.
     * @return string
     */

    public function title(): string {
        return $this->title;
    }

    /**
     * Devuelve el icono.
     * @return string|null
     */

    public function icon(): ?string {
        return $this->icon;
    }

    /**
     * Devuelve el parcial a renderear.
     * @override BreadcrumbItem
     * @return string
     */

    public function partial(): string {
        return 'label';
    }

    /**
     * Devuelve la data del parcial
     * @override BreadcrumbItem
     * @return array
     */

    public function data(): array {
        return [
            'title' => $this->title(),
            'icon' => $this->icon(),
        ];
    }
}