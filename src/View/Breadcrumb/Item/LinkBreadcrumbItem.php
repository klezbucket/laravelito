<?php

namespace Laravelito\View\Breadcrumb\Item;

use Laravelito\View\Breadcrumb\Item\BreadcrumbItem;

class LinkBreadcrumbItem extends BreadcrumbItem{

    /** @var string|null el icono del item */
    private $icon;

    /** @var string el texto del item */
    private $title;

    /** @var string el enlace del item */
    private $url;

    /**
     * Devuelve una nueva instancia de LinkBreadcrumbItem.
     * 
     * @param string $title
     * @param string $url
     * @param string|null $icon
     * @return void
     */

    public function __construct(string $title,string $url,?string $icon = null)
    {
        $this->icon = $icon;
        $this->title = $title;
        $this->url = $url;
    }

    /**
     * Devuelve el enlace.
     * @return string
     */

    public function url(): string {
        return $this->url;
    }

    /**
     * Devuelve el texto.
     * @return string
     */

    public function title(): string {
        return $this->title;
    }

    /**
     * Devuelve el icono.
     * @return string|null
     */

    public function icon(): ?string {
        return $this->icon;
    }

    /**
     * Devuelve el parcial a renderear.
     * @override BreadcrumbItem
     * @return string
     */

    public function partial(): string {
        return 'link';
    }

    /**
     * Devuelve la data del parcial
     * @override BreadcrumbItem
     * @return array
     */

    public function data(): array {
        return [
            'title' => $this->title(),
            'icon' => $this->icon(),
            'url' => $this->url()
        ];
    }
}