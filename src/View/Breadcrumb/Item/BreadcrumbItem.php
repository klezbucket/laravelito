<?php

namespace Laravelito\View\Breadcrumb\Item;

abstract class BreadcrumbItem {
    /**
     * El partial a renderear.
     * @return string
     */

    abstract function partial(): string;

    /**
     * La data a entregar al partial
     * @return array
     */

    abstract function data(): array;
}