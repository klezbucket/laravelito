<?php

namespace Laravelito\View\Breadcrumb;

use Illuminate\Support\Collection;
use Laravelito\Core\Facades\Router;
use Laravelito\View\Breadcrumb\Breadcrumb;
use Laravelito\View\Breadcrumb\Item\LinkBreadcrumbItem;
use Laravelito\View\Breadcrumb\Item\LabelBreadcrumbItem;

class BackendBreadcrumb extends Breadcrumb {
    /**
     * Devuelve los breadcrumbs configurados.
     * Este es un ejemplo de prueba del package. Implementa tu propio Breadcrumb.
     *
     * @return Collection
     */
    
    public function items(): Collection
    {
        $items = Breadcrumb::get();
        $items->prepend(new LinkBreadcrumbItem(t('looper.site'),url(route('backend.home')),'fa-angle-right'));

        switch(Router::me()){
            case 'backend.dashboard':
                break;
            case 'backend.me':
                $items->push(new LinkBreadcrumbItem(t('me.title'), url(route(Router::me()))));
                break;
        }

        return $items;
    }
}
