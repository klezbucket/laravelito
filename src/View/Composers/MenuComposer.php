<?php

namespace Laravelito\View\Composers;

use Illuminate\View\View;
use Laravelito\View\Menu\Menu;
use Laravelito\Core\Facades\Site;
use Illuminate\Support\Facades\Config;
use Laravelito\Core\Exceptions\InternalServerException;

class MenuComposer
{
    /**
     * Creamos el menu desde la configuracion especificada.
     *
     * @param View $view
     * @return void
     */
    
    public function compose(View $view): void
    {
        $site = Site::resolv(request());
        $class = Config::get('laravelito.menu.' . $site);
        $menu = new $class();

        if(! ($menu instanceof Menu)){
            throw new InternalServerException('Class provided by laravelito.menu.' . $site . ' is not a Menu instance');
        }

        $view->with('menu', new $class());
    }
}