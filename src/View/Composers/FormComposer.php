<?php

namespace Laravelito\View\Composers;

use Illuminate\View\View;
use Laravelito\Form\Form;
use Laravelito\Core\Facades\Site;
use Laravelito\Form\Injector;
use Illuminate\Support\Facades\Config;
use Laravelito\Core\Exceptions\InternalServerException;

class FormComposer
{
    /**
     * Inyectamos el formulario con la configuracion especificada.
     *
     * @param View $view
     * @return void
     */
    
    public function compose(View $view): void
    {
        $name = $view->getData()['name'] ?? null;
        $form = Form::instantiate($name);
        $view->with('form', $form);
    }
}