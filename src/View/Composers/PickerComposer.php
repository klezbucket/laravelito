<?php

namespace Laravelito\View\Composers;

use Illuminate\View\View;
use Laravelito\Table\Table;
use Laravelito\Table\Picker;
use Laravelito\Table\Injector;
use Laravelito\Core\Facades\Site;
use Illuminate\Support\Facades\Config;
use Laravelito\Core\Exceptions\InternalServerException;

class PickerComposer
{
    /**
     * Inyectamos la tabla con la configuracion especificada.
     *
     * @param View $view
     * @return void
     */
    
    public function compose(View $view): void
    {
        $name = $view->getData()['name'] ?? null;
        $table = Picker::instantiate($name);
        $view->with('table', $table);
    }
}