<?php

namespace Laravelito\View\Composers;

use Illuminate\View\View;
use Laravelito\View\Page\Page;
use Laravelito\Core\Facades\Site;
use Illuminate\Support\Facades\Config;
use Laravelito\Core\Exceptions\InternalServerException;

class PageComposer
{
    /**
     * Inyectamos la info de la pagina desde la configuracion especificada.
     *
     * @param View $view
     * @return void
     */
    
    public function compose(View $view): void
    {
        $site = Site::resolv(request());
        $class = Config::get('laravelito.pages.' . $site);
        $page = new $class();

        if(! ($page instanceof Page)){
            throw new InternalServerException('Class provided by laravelito.pages.' . $site . ' is not a Page instance');
        }

        $view->with('page', $page);
    }
}