<?php

namespace Laravelito\View\Composers;

use Closure;
use Illuminate\View\View;
use Laravelito\View\Link\Link;
use Laravelito\Auth\Facades\Auth;
use Illuminate\Support\Collection;

class UserExtrasComposer {
    /**
     * Inyectamos los datos del User en la vista.
     *
     * @param View $view
     * @return void
     */

    public function compose(View $view): void
    {
        $user = $view->getData()['detail']->model();

        $view->with([
            'roles' => $user->roles()->get()->sort(self::orderByDescription()),
            'platforms' => $user->platforms()->get()->sort(self::orderByDescription()),
        ]);
    }

    /**
     * Ordena la coleccion por description.
     * @return Closure
     */

    public static function orderByDescription(): Closure
    {
        return function($a,$b){
            return strcmp($a->description, $b->description);
        };
    }
}
