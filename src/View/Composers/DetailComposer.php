<?php

namespace Laravelito\View\Composers;

use Illuminate\View\View;
use Laravelito\Form\Form;
use Laravelito\Detail\Detail;
use Laravelito\Form\Injector;
use Laravelito\Core\Facades\Site;
use Illuminate\Support\Facades\Config;
use Laravelito\Core\Exceptions\InternalServerException;

class DetailComposer
{
    /**
     * Inyectamos el formulario con la configuracion especificada.
     *
     * @param View $view
     * @return void
     */
    
    public function compose(View $view): void
    {
        $name = $view->getData()['name'] ?? null;
        $detail = Detail::instantiate($name);
        $view->with('detail', $detail);
    }
}