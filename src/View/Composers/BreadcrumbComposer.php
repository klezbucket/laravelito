<?php

namespace Laravelito\View\Composers;

use Illuminate\View\View;
use Laravelito\Core\Facades\Site;
use Laravelito\View\Breadcrumb\Breadcrumb;
use Illuminate\Support\Facades\Config;
use Laravelito\Core\Exceptions\InternalServerException;

class BreadcrumbComposer
{
    /**
     * Inyecta la informacion para el layout backend del tema looper.
     *
     * @param View $view
     * @return void
     */
    
    public function compose(View $view): void
    {
        $site = Site::resolv(request());
        $class = Config::get('laravelito.breadcrumb.' . $site);
        $menu = new $class();

        if(! ($menu instanceof Breadcrumb)){
            throw new InternalServerException('Class provided by laravelito.breadcrumb.' . $site . ' is not a Breadcrumb instance');
        }

        $view->with('breadcrumb', new $class());
    }
}