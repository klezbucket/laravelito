<?php

namespace Laravelito\View\Link;

use Illuminate\Support\Collection;
use Laravelito\Core\Provider\Provider;
use Laravelito\CRUD\Model\ModelProvider;

interface LinkProvider extends Provider, ModelProvider {
    /**
     * Obtiene la coleccion de links.
     *
     * @return Collection of Link
     */

    function links(): Collection;
}