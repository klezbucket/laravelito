<?php

namespace Laravelito\View\Link;

use Closure;

class Link {
    /** @var string  */
    private $route;

    /** @var string  */
    private $label;

    /** @var string  */
    private $icon;

    /** @var bool  */
    private $resolv = true;

    /** @var Closure|null  */
    private $resolver = null;

    /**
     * Instancia un Link.
     *
     * @param string $route
     * @param string $label
     * @param string $icon
     * @param Closure|null $resolver
     * @return void
     */

    public function __construct(string $route,string $label,string $icon,Closure $resolver = null)
    {
        $this->route = $route;
        $this->label = $label;
        $this->icon = $icon;
        $this->resolver = $resolver;
    }

    /**
     * Setea el flag resolv a FALSE
     * @return Link
     */

    public function resolved(): Link
    {
        $this->resolv = false;
        return $this;
    }

    /**
     * Devuelve el closure que resuelve la ruta.
     * @return Closure|null
     */

    public function resolver(): ?Closure
    {
        return $this->resolver;
    }

    /**
     * Resolv determina si la ruta debe ser resuelta por el modelo del provedor, sino se pasa el string de la ruta
     * directamente como url para el HREF.
     *
     * @return string
     */

    public function resolv(): bool
    {
        return $this->resolv;
    }

    /**
     * Getter para route
     *
     * @return string
     */

    public function route(): string
    {
        return $this->route;
    }

    /**
     * Getter para label
     *
     * @return string
     */

    public function label(): string
    {
        return $this->label;
    }

    /**
     * Getter para icono
     *
     * @return string
     */

    public function icon(): string
    {
        return $this->icon;
    }
}
