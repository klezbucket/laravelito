<?php

namespace Laravelito\View\Menu;

use Laravelito\View\Menu\Menu;
use Illuminate\Support\Collection;
use Laravelito\View\Menu\Item\LinkMenuItem;
use Laravelito\View\Menu\Item\LabelMenuItem;
use Laravelito\View\Menu\Item\ParentMenuItem;

class BackendMenu extends Menu {
    /**
     * Devuelve la coleccion de MenuItem para el Backend.
     * Este es un ejemplo de prueba del package. Implementa tu propio Menu.
     * @return Collection
     */

    public function items(): Collection{
        $items = collect();
        $items->add(new LabelMenuItem(t('menu.title')));
        $items->add(new LinkMenuItem(t('menu.home'), 'fa-home', url(route('backend.home'))));
        // $items->add(new ParentMenuItem('Example', 'fa-file', collect([
        //     new LinkMenuItem(__('logout.title'), 'fa-ban', url(route('backend.logout'))),
        //     new LinkMenuItem(__('dashboard.title'), 'fa-home', url(route('backend.dashboard'))),
        //     new LinkMenuItem(__('dashboard.title'), 'fa-home', url(route('backend.dashboard'))),
        // ])));

        return $items;
    }
}
