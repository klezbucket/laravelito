<?php

namespace Laravelito\View\Menu\Item;

abstract class MenuItem {
    /**
     * El partial a renderear.
     * @return string
     */

    abstract function partial(): string;

    /**
     * La data a entregar al partial
     * @return array
     */

    abstract function data(): array;
}