<?php

namespace Laravelito\View\Menu\Item;

use Laravelito\View\Menu\Item\MenuItem;

class LinkMenuItem extends MenuItem {
    /** @var string el icono del item */
    private $icon;

    /** @var string el texto del item */
    private $title;

    /** @var string el enlace del item */
    private $url;

    /**
     * Devuelve una nueva instancia de LinkMenuItem.
     * 
     * @param string $title
     * @param string $icon
     * @param string $url
     * @return void
     */

    public function __construct(string $title, string $icon, string $url)
    {
        $this->icon = $icon;
        $this->title = $title;
        $this->url = $url;
    }

    /**
     * Devuelve el enlace.
     * @return string
     */

    public function url(): string {
        return $this->url;
    }

    /**
     * Devuelve el texto.
     * @return string
     */

    public function title(): string {
        return $this->title;
    }

    /**
     * Devuelve el icono.
     * @return string
     */

    public function icon(): string {
        return $this->icon;
    }

    /**
     * Devuelve el parcial a renderear.
     * @override MenuItem
     * @return string
     */

    public function partial(): string {
        return 'link';
    }

    /**
     * Devuelve la data del parcial
     * @override MenuItem
     * @return array
     */

    public function data(): array {
        return [
            'title' => $this->title(),
            'icon' => $this->icon(),
            'url' => $this->url()
        ];
    }
}