<?php

namespace Laravelito\View\Menu\Item;

use Laravelito\View\Menu\Item\MenuItem;

class LabelMenuItem extends MenuItem {
    /** @var string el texto del item */
    private $title;

    /**
     * Devuelve una nueva instancia de LabelMenuItem.
     * 
     * @param string $title
     * @return void
     */

    public function __construct(string $title)
    {
        $this->title = $title;
    }

    /**
     * Devuelve el texto.
     * @return string
     */

    public function title(): string {
        return $this->title;
    }

    /**
     * Devuelve el parcial a renderear.
     * @override MenuItem
     * @return string
     */

    public function partial(): string {
        return 'label';
    }

    /**
     * Devuelve la data del parcial
     * @override MenuItem
     * @return array
     */

    public function data(): array {
        return [
            'title' => $this->title()
        ];
    }
}