<?php

namespace Laravelito\View\Menu\Item;

use Illuminate\Support\Collection;
use Laravelito\View\Menu\Item\MenuItem;

class ParentMenuItem extends MenuItem {

    /** @var string el icono del item */
    private $icon;

    /** @var string el texto del item */
    private $title;

    /** @var Collection items del submenu */
    private $childs;

    /** @var bool si esta activo */
    private $active = false;

    /**
     * Devuelve una nueva instancia de ParentMenuItem.
     * 
     * @param string $title
     * @param string $icon
     * @param Collection $childs
     * @return void
     */

    public function __construct(string $title, string $icon, Collection $childs)
    {
        $this->icon = $icon;
        $this->title = $title;
        $this->childs = $childs;
    }

    /**
     * Set/Get de active.
     * @param bool|null $active
     * @return bool
     */

    public function active(?bool $active = null): bool
     {
        if(isset($active)){
            $this->active = $active;
        }

        return $this->active;
    }

    /**
     * Devuelve los items del submenu.
     * @return Collection
     */

    public function childs(): Collection 
    {
        return $this->childs;
    }

    /**
     * Devuelve el texto.
     * @return string
     */

    public function title(): string {
        return $this->title;
    }

    /**
     * Devuelve el icono.
     * @return string
     */

    public function icon(): string {
        return $this->icon;
    }

    /**
     * Devuelve el parcial a renderear.
     * @override MenuItem
     * @return string
     */

    public function partial(): string {
        return 'parent';
    }

    /**
     * Devuelve la data del parcial
     * @override MenuItem
     * @return array
     */

    public function data(): array {
        return [
            'title' => $this->title(),
            'icon' => $this->icon(),
            'childs' => $this->childs()
        ];
    }
}