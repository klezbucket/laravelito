<?php

namespace Laravelito\View\Menu;

use Illuminate\Support\Collection;

abstract class Menu {

    /** 
     * Debe devolver la coleccion de MenuItem.
     * @return Collection
     **/

    abstract public function items(): Collection;
}