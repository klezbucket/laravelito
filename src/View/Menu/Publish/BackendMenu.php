<?php

namespace App\Backend\Menu;

use App\Model\Role;
use Laravelito\View\Menu\Menu;
use Laravelito\Auth\Facades\Auth;
use Illuminate\Support\Collection;
use Laravelito\View\Menu\Item\LinkMenuItem;
use Laravelito\View\Menu\Item\LabelMenuItem;
use Laravelito\View\Menu\Item\ParentMenuItem;

class BackendMenu extends Menu {
    /** @var string */
    private static $ACTIVE = '';

    /** @var string */
    public static $USERS = 'users';

    /**
     * Devuelve la coleccion de MenuItem para el Backend.
     * Cada Rol posee su propio menu.
     *
     * @return Collection
     */

    public function items(): Collection
    {
        $items = collect();
        $items->add(new LabelMenuItem(t('menu.title')));
        $items->add(new LinkMenuItem(t('menu.home'), 'fa-home', url(route('backend.home'))));
        $this->admin($items);

        return $items;
    }

    /**
     * Agrega al menu los items relevantes al rol Admin.
     *
     * @param Collection $items
     * @return void
     */

    private function admin(Collection $items): void
    {
        if(Auth::permit(Role::backendAdmin()->code)){
            $items->add(new LabelMenuItem(t('menu.management')));
            $items->add(self::users());
        }
    }

    /**
     * Obtiene los menues para el modulo de users.
     * @return ParentMenuItem
     */

    private static function users(): ParentMenuItem
    {
        $item = new ParentMenuItem(t('users.module.name'), 'fa-users', collect([
            new LinkMenuItem(t('users.create.title'), 'fa-user-plus', url(route('users.create'))),
            new LinkMenuItem(t('users.collections.enabled.title'), 'fa-th-list', url(route('users.collections.enabled'))),
            new LinkMenuItem(t('users.collections.disabled.title'), 'fa-trash', url(route('users.collections.disabled'))),
        ]));

        $item->active(self::$ACTIVE === self::$USERS);
        return $item;
    }

    /**
     * Flaguea el modulo activo del menu.
     * @param string $module
     * @return void
     */

    public static function active(string $module): void
    {
        self::$ACTIVE = $module;
    }
}
