<?php

namespace Laravelito\View\Page;

use Laravelito\View\Page\Page;
use Laravelito\View\Page\Title;
use Laravelito\Core\Facades\Site;
use Laravelito\Core\Facades\Router;
use  Laravelito\View\Page\Facades\Page as PS;

class BackendPage extends Page {
    /**
     * Devuelve el titulo de la pagina.
     * Implementa tu propia instancia de BackendPage.
     *
     * @return string
     */

    public function title(): string
    {
        if(! ($title = Title::get())){
            $site = Site::resolv();
            $me = preg_replace("/^{$site}\./",'',Router::me());

            return t($me . '.title');
        }

        return $title;
    }
}
