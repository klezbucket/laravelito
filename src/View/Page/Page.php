<?php

namespace Laravelito\View\Page;

use Illuminate\Session\Store;
use Laravelito\View\Page\Page;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

abstract class Page {
    /** 
     * El titulo de la pagina
     * 
     * @return string
     */
    abstract function title(): string;
}