<?php

namespace Laravelito\View\Page;

class Title {
    /** @var string|null */
    private static $title;

    /**
     * Setea el titulo
     *
     * @param string $title
     * @return void
     */

    public static function set(string $title): void
    {
        self::$title = $title;
    }

    /**
     * Getter del titulo
     *
     * @return string|null
     */

    public static function get(): ?string
    {
        return self::$title;
    }
}
