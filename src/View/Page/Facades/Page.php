<?php

namespace Laravelito\View\Page\Facades;

class Page {
    /** @var Page */
    private static $me;

    /** @var string */
    private $title;

    /** 
     * Obtiene la instancia del singleton
     * 
     * @param string|null $title
     * @return Page
     */

    public static function with(?string $title): Page
    {
        if(! ($me = self::$me)){
            $me = new Page();
        }

        if(isset($title)){
            $me->title = $title;
        }

        self::$me = $me;
        return $me;
    }

    /** 
     * El titulo de la pagina
     * 
     * @param string|null $title
     * @return string
     */
    public static function title(?string $title = null): string
    {
        $me = self::with($title);
        return $me->title;
    }
}
