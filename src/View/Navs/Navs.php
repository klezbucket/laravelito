<?php

namespace Laravelito\View\Navs;

use Illuminate\Support\Collection;

class Navs {
    /** @var Collection $panels */
    private $panels;

    /**
     * Constructor.
     * @param Collection|null $panels
     * @return void
     */

    public function __construct(?Collection $panels = null)
    {
        if($panels){
            $this->panels = $panels;
        }
        else{
            $this->panels = collect();
        }
    }

    /**
     * Devuelve la coleccion de panels.
     * @return Collection
     */

    public function panels(): Collection
    {
        return $this->panels;
    }
}