<?php

namespace Laravelito\View\Navs;

class Panel {
    /** @var string $id */    
    private $id;

    /** @var string $label */
    private $label;

    /** @var bool $active */
    private $active;

    /** @var string $partial */
    private $partial;

    /** @var array $params */
    private $params;

    /**
     * Constructor
     * @param string $id
     * @param string $label
     * @param bool $active
     * @param string $partial
     * @param array $params
     * @return void
     */

    public function __construct(string $id,string $label,bool $active,string $partial,array $params)
    {
        $this->id = $id;
        $this->label = $label;
        $this->active = $active;
        $this->partial = $partial;
        $this->params = $params;
    }

    /**
     * Getter ID
     * @return string
     */

    public function id(): string
    {
        return $this->id;
    }

    /**
     * Getter LABEL
     * @return string
     */

    public function label(): string
    {
        return $this->label;
    }

    /**
     * Getter ACTIVE
     * @return bool
     */

    public function active(): bool
    {
        return $this->active;
    }

    /**
     * Getter PARTIAL
     * @return string
     */

    public function partial(): string
    {
        return $this->partial;
    }

    /**
     * Getter PARAMS
     * @return array
     */

    public function params(): array
    {
        return $this->params;
    }
}