<?php

namespace App\Backend\Form\CRUD\Users;

use Laravelito\Form\Form;
use Laravelito\Field\Field;
use Laravelito\Auth\Facades\Auth;
use Illuminate\Support\Collection;
use Laravelito\Core\Facades\Router;
use Laravelito\Field\FieldDependences;
use App\Backend\Field\Users\EmailField;
use App\Backend\Field\Users\RolesField;
use Laravelito\Field\Catalog\LangField;
use App\Backend\Field\Users\StatusField;
use App\Backend\Field\Users\LastNameField;
use App\Backend\Field\Users\DeletedAtField;
use App\Backend\Field\Users\FirstNameField;
use App\Backend\Field\Users\PlatformsField;
use Laravelito\Field\Catalog\PasswordField;
use Laravelito\Field\Catalog\TimezoneField;

class EditForm extends Form {
    /**
     * Obtiene el context del formulario.
     *
     * @return string
     */

    public function context(): string
    {
        return 'users.edit';
    }

    /**
     * Devuelve los campos de este formulario.
     *
     * @return Collection
     */

    public function fields(): Collection
    {
        $fields = collect();
        $fields[] = new FirstNameField();
        $fields[] = new LastNameField();
        $fields[] = new EmailField();
        $fields[] = new PasswordField();
        $fields[] = new TimezoneField();
        $fields[] = new LangField();
        $fields[] = (new DeletedAtField())->edit();
        $fields[] = new RolesField();
        $fields[] = new PlatformsField();
        return $fields;
    }

    /**
     * Obtiene el texto del submit.
     *
     * @return string
     */

    public function submit(): string
    {
        return 'users.edit.submit';
    }

    /**
     * Obtiene el valor del campo.
     *
     * @param Field $field
     * @return string
     */

    public function value(Field $field): string
    {
        if($field->type()->name() === 'password'){
            if(request()->isMethod('GET')){
                return '';
            }
        }

        return parent::value($field);
    }
}
