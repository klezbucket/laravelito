<?php

namespace App\Backend\Form\CRUD\Users;

use Laravelito\Form\Form;
use Laravelito\Field\Field;
use Laravelito\Auth\Facades\Auth;
use Illuminate\Support\Collection;
use Laravelito\Core\Facades\Router;
use Laravelito\Field\FieldDependences;
use App\Backend\Field\Users\EmailField;
use App\Backend\Field\Users\RolesField;
use App\Backend\Field\Users\TokenField;
use Laravelito\Field\Catalog\LangField;
use App\Backend\Field\Users\LastNameField;
use App\Backend\Field\Users\PasswordField;
use App\Backend\Field\Users\ProductsField;
use App\Backend\Field\Users\FirstNameField;
use App\Backend\Field\Users\PlatformsField;
use Laravelito\Field\Catalog\TimezoneField;

class CreateForm extends Form {

    /**
     * Obtiene el context del formulario.
     *
     * @return string
     */

    public function context(): string
    {
        return 'users.create';
    }

    /**
     * Devuelve los campos de este formulario.
     *
     * @return Collection
     */

    public function fields(): Collection
    {
        $fields = collect();
        $fields[] = new FirstNameField();
        $fields[] = new LastNameField();
        $fields[] = new EmailField();
        $fields[] = new PasswordField();
        $fields[] = new TimezoneField();
        $fields[] = new LangField();
        $fields[] = new RolesField();
        $fields[] = new PlatformsField();
        return $fields;
    }

    /**
     * Obtiene el texto del submit.
     *
     * @return string
     */

    public function submit(): string
    {
        return 'users.create.submit';
    }
}
