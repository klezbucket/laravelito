<?php
namespace App\Backend\Form;

use Laravelito\Form\Form;
use App\Backend\Form\MeForm;
use Laravelito\Form\Injector;
use Laravelito\Field\Type\Text;
use Illuminate\Support\Collection;
use App\Backend\Loader\Loader as L;
use Laravelito\Core\Facades\Loader;
use Illuminate\Support\Facades\Config;
use Laravelito\Core\Exceptions\NotFoundException;

class BackendForm implements Injector {
    /** @var string|null */
    private static $LOAD = null;

    /** @var string|null */
    private static $NAME = null;

    /**
     * Determina la clase de Form a utilizar.
     *
     * @param string $class
     * @param string|null $name
     * @return void
     */

    public static function load(string $class,?string $name = null): void
    {
        self::$LOAD = $class;
        self::$NAME = $name;
    }

    /**
     * Devuelve el Form requerido.
     *
     * @param string $name
     * @return Form
     * @throws NotFoundException si el $name especificado no resuelve un formulario valido.
     */

    public function get(string $name): Form
    {
        if($form = self::$LOAD){
            $class = self::$LOAD;
            return new $class(self::$NAME ?? $name);
        }

        switch($name){
            case 'me.edit':
                return new MeForm($name);
        }

        $class = Loader::getClass($this->namespace($name), L::get($name) . 'Form', Form::class);
        return new $class($name);
    }

    /**
     * Resuelve el namespace del form.
     * Si es report el namespace es diferente, etc.
     *
     * @param string $name
     * @return string
     */

    public function namespace(string $name): string
    {
        if(preg_match('/^report\./',$name)){
            return 'App\\Backend\\Form';
        }

        return 'App\\Backend\\Form\\CRUD';
    }
}
