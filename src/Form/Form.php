<?php

namespace Laravelito\Form;

use Illuminate\Support\Str;
use Laravelito\Field\Field;
use Laravelito\Core\Facades\Site;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Laravelito\Core\Facades\Router;
use Laravelito\Field\FieldProvider;
use Illuminate\Support\Facades\Config;
use Laravelito\Field\FieldDependences;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Model\ModelProvider;
use Laravelito\CRUD\Context\ContextProvider;
use Laravelito\Core\Exceptions\InternalServerException;
 
abstract class Form implements FieldProvider, ContextProvider, ModelProvider {
    /** @var string nombre del formulario */
    public $name;

    /** @var string modelo */
    private $model;

    /** @var array */
    private $data = [];

    /** @var array */
    protected $defaults = [];

    /**
     * Constructor del formulario.
     * @return void
     */

    public function __construct(string $name)
    {
        $this->name = $name;
    }
    
    /**
     * Obtiene el metodo HTTP.
     *
     * @return string
     */

    public function method(): string
    {
        return 'POST';
    }
    
    /**
     * Obtiene las dependencias de campos.
     * Sobreescribe este metodo para inyectar tus propias 
     *
     * @return FieldDependences
     */

    public function dependences(): ?FieldDependences
    {
        return null;
    }
    
    /**
     * Obtiene el nombre del formulario.
     *
     * @return string
     */

    public function name(): string
    {
        return $this->name;
    }

    /**
     * Obtiene el namespace de los campos. 
     * Por defecto es el nombre mismo del formulario hasta el primer punto.
     *
     * @return string
     */

    public function localesAt(): string
    {
        return explode('.',$this->name())[0] ?? $this->name();
    }

    /**
     * Obtiene el texto del submit.
     *
     * @return string
     */

    public function submit(): string
    {
        return t('submit');
    }

    /**
     * Obtiene el action del formulario.
     *
     * @return string
     */

    public function action(): string
    {
        return Router::here();
    }

    /**
     * Setter/Getter para el modelo.
     *
     * @param Model|null $model
     * @return Model
     */
    
    public function model(?Model $model = null): ?Model
    {
        if(isset($model)){
            $this->model = $model;
        }

        return $this->model;
    }

    /**
     * Obtiene el valor del campo dado.
     *
     * @param Field $field
     * @return string|null
     */

    public function value(Field $field): ?string
    {
        $value = $this->foreignValue($field);
        $name = $field->name();
        
        if(isset($value)){
            return $value;
        }
        
        if($value = $this->model->{$name} ?? null){
            return $value;
        }

        return $this->requestValue($field) ?? $this->defaults[$name] ?? '';
    }

    /**
     * Devuelve el valor desde el request.
     * @param Field
     * @return string|NULL
     */

    public function requestValue(Field $field): ?string
    {
        $name = $field->name();
        $all = request()->all();
        $value = $all[$name] ?? (array_key_exists($name, $all) ? '' : null);

        if(is_array($value)){
            return implode(',', $value);
        }

        return $value;
    }

    /**
     * Resuelve el valor de los campos foreign.
     * @param Field
     * @return string|null
     */

    public function foreignValue(Field $field): ?string
    {
        if($field->type()->name() === 'foreign'){
            $camelized = Str::camel($field->name());

            if($value = $this->model->{$field->name()} ?? $this->model->{$camelized} ?? null){
                if($value instanceof Collection){
                    $collection = $value->map(function($item){
                        return $item->toArray()['id'];
                    });

                    $value = $collection->implode(',');
                }
                else{
                    $array = $this->model->toArray();
                    return $array[$field->name()];
                }
    
                return $value;
            }
        }

        return null;
    }
    
    /**
     * Devuelve la instancia de un formulario segun su nombre, a traves del inyector.
     * @param string $name
     * @return Form
     */

    public static function instantiate(string $name): Form
    {
        if(! ($form = self::$BAG[$name] ?? null)){
            $site = Site::resolv(request());
            $class = Config::get('laravelito.forms.' . $site);

            if(! isset($class)){
                throw new InternalServerException('No Form injector resolved for '. $site);
            }

            $injector = new $class($name);

            if(! ($injector instanceof Injector)){
                throw new InternalServerException('Class provided by laravelito.forms.' . $site . ' is not an Injector instance');
            }

            $form = $injector->get($name);

            if(! ($form instanceof Form)){
                throw new InternalServerException("Class provided by {$class} is not a Form instance");
            }

            self::$FIELDS = [];

            foreach($form->fields() as $field){
                self::$FIELDS[$field->name()] = $field;
            }
        }

        self::$BAG[$name] = $form;
        self::$CURRENT = $form;
        return $form;
    }

    /**
     * Al isntanciar un formulario se va a una bolsa de cache.
     *
     * @var array
     */

    private static $BAG = [];

    /**
     * Al instanciar un formulario los campos se van a una bolsa cache
     *
     * @var array
     */

    private static $FIELDS = [];

    /**
     * El Formulario siendo procesado.
     * @var Form
     */

    private static $CURRENT;

    /**
     * Devuelve el formulario actual en proceso.
     * @return Form
     */

    public static function current(): Form
    {
        return self::$CURRENT;
    }

    /**
     * Devuelve un campo segun su nombre
     * @param string $name
     * @return Field|null
     */

    public static function field(string $name): ?Field
    {
        return self::$FIELDS[$name] ?? null;
    }
}
