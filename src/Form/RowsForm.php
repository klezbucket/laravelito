<?php

namespace Laravelito\Form;


use Laravelito\Core\Facades\Form as FC;
use Laravelito\Form\Form;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

abstract class RowsForm extends Form  {
    /** @var array las data */
    private static $data;

    /** @var array las validaciones */
    private static $validations = [];

    /**
     * Setter para data.
     * @param array $data
     * @return void
     */

    public static function setData(array $data): void
    {
        self::$data = $data;
    }

    /**
     * Setter para validations.
     * @param string $name
     * @param array $validations
     * @return void
     */

    public static function setValidations(string $name,array $validations): void
    {
        self::$validations[$name] = $validations;
    }

    /**
     * Obtiene las filas de esta tabla.
     * @return array
     */

    public function rows(): Collection
    {
        return [];
    }

    /**
     * Obtiene la data para este rows form.
     * Si existe data en el request, se utiliza este sino el implementador debera inyectar su data.
     * @return array
     */

    public function data(): array
    {
        return $this->requestData() ?? self::$data ?? [];
    }

    /**
     * Obtiene la attributes para anexar al campo
     * @return array
     */

    public function attrs(): array
    {
        return [];
    }

    /**
     * Obtiene las validaciones.
     * El implementador debera inyectar su data.
     * @return array
     */

    public function validations(): array
    {
        return self::$validations[$this->name()] ?? [];
    }

    /**
     * Obtiene la data del request para este rows form.
     * @return array|null
     */

    protected function requestData(): ?array
    {
        if(request()->isMethod('POST')){
            $name = $this->name();

            if($data = request()->get($name)){
                return $data;
            }
        }

        return null;
    }

    /**
     * Obtiene el texto del submit.
     *
     * No es necesario para un RowsForm.
     *
     * @return string
     */

    public function submit(): string
    {
        return '';
    }

    /**
     * Obtiene el texto cuando la tabla esta vacia.
     * @return string
     */

    public function empty(): string
    {
        return '';
    }

    /**
     * Obtiene el nombre del contexto CRUD.
     *
     * No es necesario para un RowsForm.
     *
     * @return string
     */

    public function context(): string
    {
        return '';
    }

    /**
     * Devuelve la cantidad de registros permitidos.
     * NULL = infinito
     *
     * @return int|null
     */

    public function max(): ?int
    {
        return null;
    }

    /**
     * Determina el archivo de traducciones.
     * @return string
     */

    public abstract function lang(): string;

    /**
     * Dada una coleccion devuelve el array en modo rows.
     * @param Collection $collection
     * @param array $fields
     * @return array
     */

    public function normalize(Collection $collection): array
    {
        if($collection->isEmpty()){
            return [];
        }

        $items = [];
        $data = $collection->toArray();
        foreach($this->fields() as $field){
            $name = $field->name();
            $items[$name] = Arr::pluck($data,$name);

            switch($field->type()->name()){
                case 'foreign':
                    $autocomplete = FC::autocompleteHidden($this,$field);
                    $items[$autocomplete] = [];
                    
                    foreach($items[$name] as $j => $id){
                        $items[$autocomplete][] = $field->type()->text($id);
                    }

                    break;
            }
        }

        return $items;
    }

    /**
     * Dado un request devuelve la data en modo 'normal'.
     * 
     * @param Request $request
     * @param string $name
     * @return array
     */

    public static function bundle(Request $request, string $name): array
    {
        $raw = $request->get($name);
        $bundle = [];

        foreach($raw as $field => $values){
            foreach($values as $i => $value){
                $bundle[$i][$field] = $value;
            }
        }

        return $bundle;
    }

    /**
     * Cantida de filas por default.
     * @return int
     */

     public function emptyRows(): int
     {
         return 1;
     }
}
