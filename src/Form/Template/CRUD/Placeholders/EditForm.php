<?php

namespace App\Backend\Form\CRUD\Placeholders;

use Laravelito\Form\Form;
use Laravelito\Field\Field;
use Laravelito\Auth\Facades\Auth;
use Illuminate\Support\Collection;
use Laravelito\Core\Facades\Router;
use Laravelito\Field\FieldDependences;

class EditForm extends Form {
    /**
     * Obtiene el context del formulario.
     *
     * @return string
     */

    public function context(): string
    {
        return 'placeholders.edit';
    }

    /**
     * Devuelve los campos de este formulario.
     *
     * @return Collection
     */

    public function fields(): Collection
    {
        $fields = collect();
        return $fields;
    }

    /**
     * Obtiene el texto del submit.
     *
     * @return string
     */

    public function submit(): string
    {
        return 'placeholders.edit.submit';
    }
}
