<?php

namespace App\Backend\Form\CRUD\Relations;

use Laravelito\Form\Form;
use Laravelito\Field\Field;
use App\Model\Placeholder;
use Illuminate\Support\Collection;
use Laravelito\Form\RowsForm as RF;

class RowsForm extends RF {
    /**
     * Devuelve los campos de este formulario.
     *
     * @return Collection
     */

    public function fields(): Collection
    {
        $fields = collect();
        return $fields;
    }

    /**
     * Determina el archivo de traducciones.
     * @return string
     */

    public function lang(): string
    {
        return 'relations';
    }

    /**
     * Obtiene el texto cuando la tabla esta vacia.
     * @return string
     */

    public function empty(): string
    {
        return 'placeholders.fields.relations.empty';
    }

    /**
     * Obtiene las filas de esta tabla.
     * @return Collection
     */

    public function rows(): Collection
    {
        $id = request()->route()->parameter('id');

        if($placeholdeR = Placeholder::find($id)){
            return $placeholdeR->relationSRows()->get();
        }

        return collect();
    }

    /**
     * Obtiene la data para este rows form.
     * Si existe data en el request, se utiliza este sino el implementador debera inyectar su data.
     * @return array
     */

    public function data(): array
    {
        if($data = $this->requestData()){
            return $data;
        }
        else{
            return $this->normalize($this->rows());
        }
    }
}
