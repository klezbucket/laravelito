<?php

namespace App\Backend\Form;

use Laravelito\Form\Form;
use Laravelito\Field\Field;
use Laravelito\Auth\Facades\Auth;
use Illuminate\Support\Collection;
use Laravelito\Core\Facades\Router;
use Laravelito\Field\Catalog\LangField;
use Laravelito\Field\Catalog\EmailField;
use Laravelito\Field\Catalog\LastNameField;
use Laravelito\Field\Catalog\PasswordField;
use Laravelito\Field\Catalog\TimezoneField;
use Laravelito\Field\Catalog\FirstNameField;

class MeForm extends Form {
    /**
     * Obtiene el namespace de validacion
     * 
     * @return string
     */

    public function localesAt(): string
    {
        return 'me';
    }

    /**
     * Obtiene el context del formulario.
     * 
     * @return string
     */

    public function context(): string
    {
        return 'me.edit';
    }

    /**
     * Devuelve los campos de este formulario.
     * 
     * @return Collection
     */

    public function fields(): Collection
    {
        $fields = collect();
        $fields[] = new FirstNameField();
        $fields[] = new LastNameField();
        $fields[] = new EmailField();
        $fields[] = new PasswordField();
        $fields[] = new TimezoneField();
        $fields[] = new LangField();
        return $fields;
    }
    /**
     * Obtiene el texto del submit.
     * 
     * @return string
     */

    public function submit(): string 
    {
        return 'me.submit';
    }

    /**
     * Obtiene el action del formulario.
     * 
     * @return string
     */

    public function action(): string
    {
        return Router::here();
    }

    /**
     * Obtiene el valor del campo dado.
     * 
     * @param Field $field
     * @return string
     */

    public function value(Field $field): string
    {
        if($field->type()->name() === 'password'){
            return request()->get('password') ?? '';
        }
        
        $name = $field->name();
        
        if(request()->isMethod('POST')){
            return request()->get($name) ?? '';
        }

        return Auth::user($name) ?? '';
    }
}