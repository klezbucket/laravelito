<?php

namespace Laravelito\Form\Validation;

use Throwable;
use Laravelito\Form\Form;
use Illuminate\Http\Request;
use Laravelito\Form\RowsForm;
use Laravelito\Field\FieldProvider;
use Laravelito\Field\FieldDependences;
use Laravelito\Form\Validation\Validation;
use Illuminate\Validation\ValidationException;
use Laravelito\Core\Facades\Form as FF;

class Validation {

    /**
     * Rutina generica para validar row forms.
     * Usar en el beforeValidate del callback edit/create a implementar.
     *
     * @param Request $request
     * @param RowsForm $form
     * @param bool $throw
     * @throws ValidationException si es invalido
     * @return ValidationException|null
     */

    public static function rows(Request $request,RowsForm $form,bool $throw = true): ?ValidationException
    {
        $name = $form->name();
        $raw = $request->get($name) ?? [];
        $validations = [];
        $fields = [];

        foreach($raw as $field => $bundle){
            foreach($bundle as $i => $value){
                $fields[$i][$field] = $value;
            }
        }

        foreach($fields as $i => $field){
            $validator = validator($field, Validation::generate($form));

            try{
                $validator->validate();
            }
            catch(ValidationException $e){
                $validations[$i] = $validator->errors();
            }
        }

        if(! empty($validations)){
            call_user_func(get_class($form) . '::setValidations', $name, $validations);

            if($exception = new ValidationException($validator)){
                if($throw){
                    throw $exception;
                }

                return $exception;
            }
        }

        return null;
    }

    /**
     * Dado un formulario devuelve el array laravel de validaciones.
     *
     * @param FieldProvider $provider
     * @return array
     */

    public static function generate(FieldProvider $provider): array
    {
        $validations = [];

        foreach($provider->fields() as $field){
            $name = $field->name();
            $rules = [];

            foreach($field->type()->constraints() as $constraint){
                $rules[] = $constraint->validates();
            }

            foreach($field->constraints() as $constraint){
                $rules[] = $constraint->validates();
            }

            $validations[$name] = implode('|',$rules);
        }

        return self::resolvDependeces($provider,$validations);
    }

    /**
     * Elimina ciertas validations si las depencias del formulario asi lo indican.
     *
     * @param FieldProvider $provider
     * @param array $validations
     * @return array
     */

    private static function resolvDependeces(FieldProvider $provider,array $validations): array
    {
        $shutdown = FieldDependences::shutdown($provider);

        foreach($shutdown as $field){
            unset($validations[$field]);
        }

        return $validations;
    }

    /**
     * Preuba las validacion del request, contra el provider.
     * @param Request $request
     * @param FieldProvider $provider
     * @param bool $throw determina si lanza la excepcion.
     * @return bool
     */

    public static function test(Request $request,FieldProvider $provider,bool $throw = false): bool
    {
        $rules = Validation::generate($provider);
        $validator = validator($request->all(), $rules);

        try{
            $validator->validate();
        }
        catch(Throwable $e){
            FF::invalid($validator->errors()->messages(),$provider->name());

            if($throw){
                throw $e;
            }

            return false;
        }

        return true;
    }
}
