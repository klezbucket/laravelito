<?php

namespace Laravelito\Form;

use Laravelito\Form\Form;

interface Injector {
    /**
     * Dado el nombre del form deseado, devuelve una instancia del mismo.
     * 
     * @param string $name
     * @return Form
     */

    function get(string $name): Form;
}