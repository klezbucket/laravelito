<?php

namespace Laravelito\Autocomplete\Model;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Laravelito\Autocomplete\AutocompleteCollection;

interface AutocompleteModelInterface {

    /**
     * Devuelve una coleccion de objetos filtrados desde el Request.
     * 
     * @param string $term
     * @param array $context
     * @param int $page
     * @return Collection
     */

    static function autocomplete(string $term,array $context = [],int $page = 1): AutocompleteCollection;

    /**
     * Devuelve una coleccion de objetos filtrados desde el Request.
     * 
     * @param mixed $value
     * @param mixed $parameters
     * @return bool
     */
    
    static function autocompleteValidation($value,$parameters): bool;

    /**
     * Devuelve un id devuelve la representacion textual del objeto.
     * 
     * @param mixed $id
     * @return string
     */
    
    static function autocompleteText($id): string;
}