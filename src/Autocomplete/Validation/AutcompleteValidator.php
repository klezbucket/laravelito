<?php

namespace Laravelito\Autocomplete\Validation;

use Laravelito\Form\Form;

class AutcompleteValidator {
    
    /**
     * Valida el autocomplete segun los parametros establecidos.
     */

    public static function validate($attribute, $value, $parameters)
    {
        if($value){
            if($field = Form::field($attribute)){
                $class = $field->type()->model();

                foreach(explode(',', $value) as $val){
                    return call_user_func($class . '::autocompleteValidation',$val,$parameters);
                }
            }
        }

        return true;
    }
}