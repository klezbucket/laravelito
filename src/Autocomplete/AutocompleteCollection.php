<?php

namespace Laravelito\Autocomplete;

use Illuminate\Support\Collection;

class AutocompleteCollection {
    /** @var Collection */
    private $collection;

    /** @var string */
    private $value;

    /** @var Colstringlection */
    private $label;

    /**
     * Devuelve una nueva instancia.
     * @param Collection $collection
     * @return void
     */

    public function __construct(Collection $collection,string $valueField, string $labelField)
    {
        $this->collection = $collection;
        $this->value = $valueField;
        $this->label = $labelField;
    }

    /**
     * Devuelve los items de la coleccion, listos para el plugin js select2.
     * @return Collection
     */

    public function items(): Collection
    {
        $items = collect($this->collection)->map(function ($item) {
            return [
                'id' => $item[$this->value],
                'text' => $item[$this->label],
            ];
        });

        return $items->values();
    }
}