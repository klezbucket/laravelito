<?php

namespace Laravelito\Console\Command;

use Closure;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Laravelito\Routing\LaravelitoRouter;
use Laravelito\View\Menu\Item\LinkMenuItem;
use Laravelito\Console\Command\CommandTrait;
use Laravelito\View\Menu\Item\ParentMenuItem;

class BackendRowsFormCommand extends Command
{
    use CommandTrait;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backend:rowsform {table} {relation}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea un rows form de forma semi-automatica';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($table = $this->myTable()){
            $this->replacement('Relations',self::capitalsFor($this->myRelation()));
            $this->replacement('relations',self::lowersFor($this->myRelation()));
            $this->replacement('relationS',self::camelsFor($this->myRelation()));
            $this->replacement('relatioN',self::camelFor($this->myRelation()));
            $this->replacement('Relation',self::capitalFor($this->myRelation()));
            $this->info('Preparing files for ' . $table);
            $this->field();
            $this->form();
            $this->model();
            $this->snippets();
            $this->advice();
        }
        else{
            $this->error('You must specify table');
        }
    }

    /**
     * Prepara el campo RowsField.
     * @return void
     */

    private function field(): void
    {
        $capitals = $this->capitals();
        $capitals2 = self::capitalsFor($this->myRelation());

        $path = app_path("Backend/Field/{$capitals}/{$capitals2}RowsField.php");
        $source = $this->laravelito('src/Field/Template/RelationRowsField.php');

        $dir = dirname($path);
        $this->mkdir($dir);

        $this->info('Rows Field: ' . $path);
        $this->single($source,$path);
    }

    /**
     * Prepara el form rows.
     * @return void
     */

    private function form(): void
    {
        $capitals = self::capitalsFor($this->myRelation());
        $path = app_path("Backend/Form/CRUD/{$capitals}");
        $source = $this->laravelito('src/Form/Template/CRUD/Relations');
        $this->info('Form Rows: ' . $path);
        $this->directory($source,$path);
    }

    /**
     * Prepara el snippet para el modelo
     * @return void
     */

    private function model(): void
    {
        $capital = self::capitalFor($this->myTable());
        $capital2 = self::capitalFor($this->myRelation());

        $path = app_path("Model/{$capital2}.php");
        $source = $this->laravelito('src/Model/Template/Relation.php');

        $this->info('Relation Model: ' . $path);
        $this->single($source,$path);

        $this->error("\nAllocate the following snippet into your $capital model file, you can tune the builder to suit your needs");
        $this->warn($this->replace('
                /**
                 * Devuelve los registros que alimentan el RowsForm
                 * @return Builder
                 */
            
                public function relationSRows(): Builder
                {
                    return Relation::join(\'placeholders\',\'relations.placeholder_id\',\'placeholders.id\')
                                            ->select(\'relations.*\')
                                            ->where(\'relations.placeholder_id\', $this->id)
                                            ->whereNull(\'relations.deleted_at\')
                                            ->orderBy(\'relations.id\', \'ASC\');
                }
        '));
    }

    /**
     * Prepara los snippets.
     * @return void
     */

    private function snippets(): void
    {
        $capitals = self::capitalsFor($this->myTable());
        $capital = self::capitalFor($this->myRelation());
        $this->error("\nAllocate the following snippet into your {$capitals}CallbackTrait callback file, within the beforeValidate method");
        $this->warn($this->replace('
            Validation::rows($request,new \App\Backend\Form\CRUD\Relations\RowsForm(\'relations\'));
        '));
        
        $this->error("\nAllocate the following snippet into your {$capitals}CallbackTrait callback file, within the afterSave method");
        $this->warn($this->replace('
            $this->relationS($model,$request);
        '));
        
        $this->error("\nAllocate the following method into your {$capitals}CallbackTrait callback file");
        $this->warn($this->replace('
            /**
             * Saves the Relation registers.
             *
             * @param Model $model
             * @param Request $request
             */
        
            private function relationS(Model $model, Request $request)
            {
                $ids = [];
                $bundle = \Laravelito\Form\RowsForm::bundle($request,\'relations\');
        
                foreach($bundle as $data){
                    if(! ($relatioN = \App\Model\Relation::alloc($model, $data))){
                        throw new InternalServerException(\'Cannot alloc Relation\');
                    }
        
                    $ids[] = $relatioN->id;
                }
        
                \App\Model\Relation::shutdown($model, $ids);
            }
        '));
    }

    /**
     * Recordatorio de agregar el field a la coleccion del form edit/create
     * @return void
     */

    private function advice(): void
    {
        $capitals = self::capitalsFor($this->myRelation());
        $capital = self::capitalFor($this->myRelation());
        $this->error("\nCheck your shutdown and alloc methods within the $capital model file and suit it to your needs!");
        $this->error("\nDon't forget to fill the \$fillable array aswell!");
        $this->error("\nDon't forget to add {$capitals}RowsField into your Create/Edit forms!");
    }
}
