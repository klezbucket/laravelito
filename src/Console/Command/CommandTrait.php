<?php
namespace Laravelito\Console\Command;

use Closure;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

trait CommandTrait {


    /**
     * Obtiene el path en laravelito.
     * @return string|null
     */

    private function laravelito(string $path): string
    {
        return base_path('vendor/klezbucket/laravelito/' . $path);
    }

    /**
     * Obtiene el relation.
     * @return string|null
     */

    private function myRelation(): ?string
    {
        return $this->safeArg('relation');
    }

    /**
     * Obtiene el label.
     * @return string|null
     */

    private function myLabel(): ?string
    { 
        return $this->safeArg('label');
    }

    /**
     * Obtiene el argumento sin explotar sino exise.
     * @param string
     * @return string|null
     */

    private function safeArg(string $arg): ?string
    {
        $args = $this->arguments();
        return $args[$arg] ?? null;
    }

    /**
     * Obtiene la tabla.
     * @return string|null
     */

    private function myTable(): ?string
    {
        return $this->safeArg('table');
    }

    /**
     * Obtiene el nombre todo en mayus.
     * @return string 
     */

    private function allcaps(): string
    {
        $table = $this->myTable();
        return strtoupper($table);
    }

    /**
     * Obtiene el nombre singular capitalizado camel.
     * @param string|null $append
     * @return string 
     */

    private function capital(?string $append = null): string
    {
        $table = $this->myTable();
        return self::capitalFor($table,$append);
    }

    /**
     * Obtiene el nombre plural capitalizado camel.
     * @param string|null $append
     * @return string 
     */

    private function capitals(?string $append = null): string
    {
        $table = $this->myTable();
        return self::capitalsFor($table, $append);
    }

    /**
     * Obtiene el camel singular
     * @param string|null $append
     * @return string 
     */

    private function camel(?string $append = null): string
    {
        $table = $this->myTable();
        return self::camelFor($table, $append);
    }

    /**
     * Obtiene el camel plural
     * @param string|null $append
     * @return string 
     */

    private function camels(?string $append = null): string
    {
        $table = $this->myTable();
        return self::camelsFor($table, $append);
    }

    /**
     * Estatico de camel
     * @param string $what
     * @param string|null $append
     * @return string
     */

    private function camelFor(string $what, ?string $append = null): string
    {
        $caps = Str::camel(Str::singular($what));

        if(isset($append)){
            $caps .= $append;
        }

        return $caps;
    }

    /**
     * Estatico de camels
     * @param string $what
     * @param string|null $append
     * @return string
     */

    private function camelsFor(string $what, ?string $append = null): string
    {
        $caps = Str::camel(Str::plural($what));

        if(isset($append)){
            $caps .= $append;
        }

        return $caps;
    }

    /**
     * Estatico de capital
     * @param string $what
     * @param string|null $append
     * @return string
     */

    private function capitalFor(string $what, ?string $append = null): string
    {
        $caps = ucfirst(Str::camel(Str::singular($what)));

        if(isset($append)){
            $caps .= $append;
        }

        return $caps;
    }

    /**
     * Estatico de capitals
     * @param string $what
     * @param string|null $append
     * @return string
     */

    private function capitalsFor(string $what, ?string $append = null): string
    {
        $caps = ucfirst(Str::camel(Str::plural($what)));

        if(isset($append)){
            $caps .= $append;
        }

        return $caps;
    }

    /**
     * Obtiene el nombre plural lowers.
     * @return string 
     */

    private function lowers(): string
    {
        $table = $this->myTable();
        return self::lowersFor($table);
    }

    /**
     * Estatico de lowers
     * @param string $what
     * @return string
     */

    private static function lowersFor(string $what): string
    {
        return strtolower(Str::plural($what));
    }

    /**
     * Obtiene el nombre singular lowers.
     * @return string 
     */

    private function lower(): string
    {
        $table = $this->myTable();
        return strtolower(Str::singular($table));
    }

    /**
     * Obtiene la clase del modulo.
     * @param string|null $append
     * @param string|null $module
     * @return string 
     */

    private function classy(?string $append = null,?string $module = null): string
    {
        if(is_null($module)){
            $module = $this->myTable();
        }

        $classy = ucfirst(Str::camel(Str::singular($module)));

        if(isset($append)){
            $classy .= $append;
        }

        return $classy;
    }

    /**
     * Reemplazador de directorio.
     * @param string $source
     * @param string $path
     * @return void
     */

    private function directory(string $source,string $path): void
    {
        $confirm = true;

        if(file_exists($path)){
            $this->warn('The path exists!');

            if($confirm = $this->confirm('Do you want to replace the content at ' . $path . '?')){
                rename($path, $path . '_' . time() . '_' . uniqid());
            }
        }

        if($confirm){
            File::copyDirectory($source, $path);
            $this->scanner($path);
        }
    }

    /**
     * Scan de directorio recursivo.
     * @param string $path
     * @return void
     */

    private function scanner(string $path): void
    {
        $objects = scandir($path, SCANDIR_SORT_NONE);

        foreach ($objects as $obj) {
            if ($obj === '.' || $obj === '..')
                continue;
                
            $file = "{$path}/{$obj}";

            if(is_dir($file)){
                $this->scanner($file);
            }
            else{
                $this->write($file);
                $this->rename($file);
            }
        }
    }

    /**
     * Reemplaza el nombre del archivo de ser necesario.
     * @param string $file
     * @return void
     */

    private function rename(string $file): void
    {
        $filename = basename($file);
        $filename = str_replace('Placeholders', $this->capitals(), $filename);
        $move = dirname($file) . '/' . $filename;

        if(strcmp($file, $move) !== 0){
            rename($file, $move);
        }
    }

    /**
     * Reemplazador de archivo
     * @param string $source
     * @param string $path
     * @return void
     */

    private function single(string $source,string $file): void
    {
        $confirm = true;

        if(file_exists($file)){
            $this->warn('The file exists!');

            if($confirm = $this->confirm('Do you want to replace the file at ' . $file . '?')){
                rename($file, $file . '_' . time() . '_' . uniqid());
            }
        }

        if($confirm){
            copy($source, $file);
            $this->write($file);
        }
    }

    /**
     * Reemplaza el placeholder por los datos del modulo.
     * @param string $file
     * @return mixed
     */

    private function write(string $file)
    {
        $data = $this->replace(file_get_contents($file));
        return file_put_contents($file, $data);
    }

    /**
     * Reemplaza el placeholder por los datos del modulo.
     * @param string $text
     * @return mixed
     */

    private function replace(string $text)
    {
        $text = str_replace('LABELHOLDER', $this->myLabel(), $text);
        $text = str_replace('PLACEHOLDERS', $this->allcaps(), $text);
        $text = str_replace('Placeholders', $this->capitals(), $text);
        $text = str_replace('placeholderS', $this->camels(), $text);
        $text = str_replace('placeholders', $this->lowers(), $text);
        $text = str_replace('Placeholder', $this->classy(), $text);
        $text = str_replace('placeholder', $this->lower(), $text);
        $text = str_replace('placeholdeR', $this->camel(), $text);

        foreach($this->replaces as $search => $replacement){
            $text = str_replace($search, $replacement, $text);
        }

        return $text;
    }

    /** @var array */
    private $replaces = [];

    /**
     * Agrega una entrada al vector de reemplazos.
     * @param string $search
     * @param string $replacement
     * @return void
     */

    private function replacement(string $search, string $replacement): void
    {
        $this->replaces[$search] = $replacement;
    }

    /**
     * Crea el directorio si no existe
     * @param string $dir
     * @return void
     */

    private function mkdir(string $dir): void
    {
        if(! file_exists($dir)){
            mkdir($dir);
        }
    }
}