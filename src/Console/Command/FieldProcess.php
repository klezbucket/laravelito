<?php

namespace Laravelito\Console\Command;

trait FieldProcess {

    /**
     * Pre process for Picture
     * @param string $table
     * @param string $field
     * @param array $properties
     * @return void
     */

    private function picturePre(string $table,string $field,array $properties): void
    {

    }

    /**
     * Post process for Picture
     * @param string $table
     * @param string $field
     * @param array $properties
     * @return void
     */

    private function picturePost(string $table,string $field,array $properties): void
    {
        
    }
}