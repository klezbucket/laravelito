<?php
namespace Laravelito\Console\Command;

trait FieldSignatures {
    /**
     * Genera la firma para el tipo boolean.
     * @param string $table
     * @param string $field
     * @param array $properties
     * @return string
     */

    private function booleanSignature(string $table,string $field,array $properties): string
    {
        return '$this->table . \'.fields.\' . $this->field . \'.boolean.true\',$this->table . \'.fields.\' . $this->field . \'.boolean.false\'';
    }

    /**
     * Genera la firma para el tipo foreign.
     * @param string $table
     * @param string $field
     * @param array $properties
     * @return string
     */

    private function foreignSignature(string $table,string $field,array $properties): string
    {
        $model = $properties['foreign'];
        $modelClass = $this->classy(null,$model);
        $multi = array_key_exists('multi',$properties) ? 'true' : 'false';
        return "Router::route('{$model}.autocomplete'), \\App\\Model\\{$modelClass}::class, $multi";
    }

    /**
     * Genera la firma para el tipo marker map.
     * @param string $table
     * @param string $field
     * @param array $properties
     * @return string
     */

    private function markerMapSignature(string $table,string $field,array $properties): string
    {
        return "env('GMAP_API_KEY'),env('GMAP_LAT'),env('GMAP_LON'),env('GMAP_ZOOM')";
    }

    /**
     * Genera la firma para el tipo enum.
     * @param string $table
     * @param string $field
     * @param array $properties
     * @return string
     */

    private function enumSignature(string $table,string $field,array $properties): string
    {
        $translate = array_key_exists('translate', $properties) ? 'true' : 'false';
        $options = $properties['options'];
        return "t('{$options}'),$translate";
    }

    /**
     * Genera la firma para el tipo picture.
     * @param string $table
     * @param string $field
     * @param array $properties
     * @return string
     */

    private function pictureSignature(string $table,string $field,array $properties): string
    {
        $route = $properties['route'] ?? "{$table}.{$field}";
        $size = json_encode(array_key_exists('size', $properties) ? (int) $properties['size'] : null);
        $mimes = json_encode(array_key_exists('mimes', $properties) ? explode('|', $properties['mimes']): null);

        return "new \Laravelito\Field\Upload\Catalog\PictureUpload('$route','$field',$mimes,$size)";
    }
}