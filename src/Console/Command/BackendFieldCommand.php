<?php

namespace Laravelito\Console\Command;

use Closure;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Laravelito\Routing\LaravelitoRouter;
use Laravelito\View\Menu\Item\LinkMenuItem;
use Laravelito\Console\Command\CommandTrait;
use Laravelito\Console\Command\FieldProcess;
use Laravelito\View\Menu\Item\ParentMenuItem;
use Laravelito\Console\Command\FieldSignatures;

class BackendFieldCommand extends Command
{
    use CommandTrait;
    use FieldSignatures;
    use FieldProcess;
    
    /**
     * The name and signature of the console command.
     * 
     * backend:field producers name type=text,required,unique
     *
     * @var string
     */
    protected $signature = 'backend:field {table} {field} {properties}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea un field de forma semi-automatica';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($table = $this->myTable()){
            if($field = $this->safeArg('field')){
                if($properties = $this->safeArg('properties')){
                    $props = $this->parseProperties($properties);
                    $this->pre($table,$field,$props);
                    $this->generator($table,$field,$props);
                    $this->localization($table,$field,$props);
                    $this->post($table,$field,$props);
                }
                else{
                    $this->error('You must specify properties');
                }
            }
            else{
                $this->error('You must specify field');
            }
        }
        else{
            $this->error('You must specify table');
        }
    }

    /**
     * Pre process
     * @param string $table
     * @param string $field
     * @param array $properties
     * @return void
     */

    private function pre(string $table,string $field,array $properties): void
    {
        $type = $properties['type'];  
        $method = $type . 'Pre';

        if(method_exists($this, $method)){
            $this->{$method}($table,$field,$properties);
        }
    }

    /**
     * Post process
     * @param string $table
     * @param string $field
     * @param array $properties
     * @return void
     */

    private function post(string $table,string $field,array $properties): void
    {
        $type = $properties['type'];  
        $method = $type . 'Post';

        if(method_exists($this, $method)){
            $this->{$method}($table,$field,$properties);
        }
    }

    /**
     * Hint para localizacion.
     * @param string $table
     * @param string $field
     * @param array $properties
     * @return void
     */

    private function localization(string $table,string $field,array $properties): void
    {
        $validation = $this->getValidationHint($properties);
        $null = $this->getNullHint($properties);
        $type = $this->getTypeHint($properties);
        $placeholder = $this->getPlaceholderHint($properties);
        $help = $this->getHelpHint($properties);
        $type = $this->getTypeHint($properties);

        $this->error("\nAllocate the following snippet into your modules/{$table}.php locales file, within the fields array");
        $this->error("Adapt it to your needs!");
        $this->warn($this->replace("
            '$field' => [
                'label' => 'Label',{$placeholder}{$help}{$null}
                'validation' => [{$validation}
                ],{$type}
            ],
        "));
    }

    /**
     * Retorna el hint de configuracion para placeholder.
     * @param array $properties
     * @return string;
     */

    private function getPlaceholderHint(array $properties): string
    {
        $placeholder = '';

        if(array_key_exists('placeholder',$properties)){
            $placeholder = "
                'placeholder' => 'Placeholder',";
        }

        return $placeholder;
    }

    /**
     * Retorna el hint de configuracion para help.
     * @param array $properties
     * @return string;
     */

    private function getHelpHint(array $properties): string
    {
        $help = '';
        
        if(array_key_exists('help',$properties)){
            $help = "
                'help' => 'Help',";
        }

        return $help;
    }

    /**
     * Retorna el hint de configuracion para type.
     * @param array $properties
     * @return string;
     */

    private function getTypeHint(array $properties): string
    {
        $type = '';

        switch($properties['type']) {
            case 'enum': 
                $type = "
                'options' => [
                    'op1' => 'Label 1',
                    'op2' => 'Label 2'
                ]";
                break;
            case 'boolean':
                $type = "
                'boolean' => [
                    'true' => 'True',
                    'false' => 'False'
                ]";
                break;
        }

        return $type;
    }

    /**
     * Retorna el hint de configuracion para validaciones.
     * @param array $properties
     * @return string;
     */

    private function getValidationHint(array $properties): string
    {
        $validation = '';

        if (array_key_exists('unique', $properties)){
            $validation .= "
                    'unique' => 'Unique',";
        }

        if (array_key_exists('required', $properties)){
            $validation .= "
                    'required' => 'Required',";
        }

        return $validation;
    }

    /**
     * Retorna el hint de configuracion para tipos null.
     * @param array $properties
     * @return string;
     */

    private function getNullHint(array $properties): string
    {
        $null = '';

        if (! array_key_exists('required', $properties)){
            $null = "
                'null' => 'NULL',";
        }

        return $null;
    }

    /**
     * Genera el archivo de Field.
     * @param string $table
     * @param string $field
     * @param array $properties
     * @return void
     */

    private function generator(string $table,string $field,array $properties): void
    {
        $folder = $this->capitalsFor($table);
        $filename = ucfirst(Str::camel($field)) . 'Field';
        $path = app_path("Backend/Field/{$folder}/{$filename}.php");
        $source = $this->laravelito('src/Field/Template/PhieldField.php');
        $type = $properties['type'] ?? 'text';

        $this->replacement('Phield',ucfirst(Str::camel($field)));
        $this->replacement('phield',$field);
        $this->replacement('tables',$table);
        $this->replacement('Tables',$folder);
        $this->replacement('PRType',$this->classy(null, $type));
        $this->replacement('PRSignature',$this->signature($table,$field,$properties));
        $this->replacement('PRrequired',array_key_exists('required', $properties) ? 'true' : 'false');
        $this->replacement('PRunique',array_key_exists('unique', $properties) ? 'true' : 'false');
        $this->replacement('PRplaceholder',array_key_exists('placeholder', $properties) ? 'true' : 'false');
        $this->replacement('PRhelp',array_key_exists('help', $properties) ? 'true' : 'false');
        $this->replacement('PRuniqcomp',$this->uniqcomp($properties));
        $this->replacement('PRarguments',$this->getFullArgs());

        $this->mkdir(dirname($path));
        $this->info('Field: ' . $path);
        $this->single($source,$path);
    }

    /**
     * Obtiene la lista de argumentos de la consola en formato string.
     * 
     * @return string
     */

    private function getFullArgs(): string 
    {
        return implode(' ', $this->arguments());
    }

    /**
     * Obtiene el array(string) uniqcomp desde las propiedades.
     * @param array $properties
     * @return string
     */

    private function uniqcomp(array $properties): string
    {
        $fields = ($properties['comp'] ?? '');

        if(strlen($fields) > 0){
            $comp = explode('|',$fields);
        } else {
            $comp = [];
        }

        return str_replace('"','\'',json_encode($comp));
    }

    /**
     * Genera el archivo de Field.
     * @param string $table
     * @param string $field
     * @param array $properties
     * @return string
     */

    private function signature(string $table,string $field,array $properties): string
    {
        $type = $properties['type'] ?? 'text';
        $method = $this->camelFor($type,'Signature');

        if(method_exists($this, $method)){
            return $this->{$method}($table,$field,$properties);
        }

        return '';
    }

    /**
     * Reemplaza el placeholder por los datos del modulo.
     * @param string $text
     * @return mixed
     */

    private function replace(string $text)
    {
        foreach($this->replaces as $search => $replacement){
            $text = str_replace($search, $replacement, $text);
        }

        return $text;
    }

    /**
     * Parse the properties.
     * @param string $properties
     * @return array
     */

    private function parseProperties(string $properties): array
    {
        $props = [];
        $set = explode(',', $properties);

        foreach($set as $tuple){
            $pair = explode('=', $tuple);
            $property = $pair[0] ?? null;
            $value = $pair[1] ?? null;
            $props[$property] = $value;
        }

        return $props;
    }
}
