<?php

namespace Laravelito\Console\Command;

use Closure;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Laravelito\Routing\LaravelitoRouter;
use Laravelito\View\Menu\Item\LinkMenuItem;
use Laravelito\Console\Command\CommandTrait;
use Laravelito\View\Menu\Item\ParentMenuItem;

class BackendModuleCommand extends Command
{
    use CommandTrait;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backend:module {table} {label}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea un modulo de forma semi-automatica';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($table = $this->myTable()){
            $this->info('Preparing files for ' . $table);
            $this->forms();
            $this->links();
            $this->details();
            $this->tables();
            $this->crud();
            $this->routes();
            $this->menu();
            $this->controller();
            $this->autocomplete();
            $this->lang();
            $this->model();
            $this->permits();
        }
        else{
            $this->error('You must specify table');
        }
    }

    /**
     * Prepara los formularios.
     * @return void
     */

    private function forms(): void
    {
        $capitals = $this->capitals();
        $path = app_path("Backend/Form/CRUD/{$capitals}");
        $source = $this->laravelito('src/Form/Template/CRUD/Placeholders');
        $this->info('Forms: ' . $path);
        $this->directory($source,$path);
    }

    /**
     * Prepara los links.
     * @return void
     */

    private function links(): void
    {
        $filename = $this->classy('Links');
        $path = app_path("Backend/Links/{$filename}.php");
        $source = $this->laravelito('src/Links/Template/PlaceholderLinks.php');
        $this->info('Links: ' . $path);
        $this->single($source,$path);
    }

    /**
     * Prepara los detalles.
     * @return void
     */

    private function details(): void
    {
        $capitals = $this->capitals();
        $path = app_path("Backend/Detail/CRUD/{$capitals}");
        $source = $this->laravelito('src/Detail/Template/CRUD/Placeholders');
        $this->info('Details: ' . $path);
        $this->directory($source,$path);
    }

    /**
     * Prepara las tablas.
     * @return void
     */

    private function tables(): void
    {
        $capitals = $this->capitals();
        $path = app_path("Backend/Table/CRUD/{$capitals}");
        $source = $this->laravelito('src/Table/Template/CRUD/Placeholders');
        $this->info('Tables: ' . $path);
        $this->directory($source,$path);
    }

    /**
     * Prepara los crud y su config.
     * @return void
     */

    private function crud(): void
    {
        $config = function(){
            $lowers = $this->lowers();
            $path = config_path("crud/{$lowers}.php");
            $source = $this->laravelito('config/template/crud/placeholders.php');
            $this->info('CRUD Config: ' . $path);
            $this->single($source,$path);

            $this->error('Append the following line into your crud.php config file\'s response array');
            $this->info("");
            $this->warn($this->replace("'placeholders' => require(__DIR__ . '/crud/placeholders.php'),"));
        };

        $files = function(){
            $capitals = $this->capitals();
            $path = app_path("Backend/CRUD/{$capitals}");
            $source = $this->laravelito('src/CRUD/Template/Placeholders');
            $this->info('CRUD Files: ' . $path);
            $this->directory($source,$path);
        };

        $files();
        $config();
    }

    /**
     * Prepara las rutas.
     * @return void
     */

    private function routes(): void
    {
        $this->error("\nAllocate the following lines into your backend.php routes file");
        $this->info("");
        $this->warn($this->replace("LaravelitoRouter::form('PlaceholdersController@create', 'placeholders.create', Permits::placeholderS());"));
        $this->warn($this->replace("LaravelitoRouter::get('PlaceholdersController@fetch', 'placeholders.fetch', Permits::placeholderS());"));
        $this->warn($this->replace("LaravelitoRouter::form('PlaceholdersController@edit', 'placeholders.edit', Permits::placeholderS());"));
        $this->warn($this->replace("LaravelitoRouter::form('PlaceholdersController@enabled', 'placeholders.collections.enabled', Permits::placeholderS());"));
        $this->warn($this->replace("LaravelitoRouter::form('PlaceholdersController@disabled', 'placeholders.collections.disabled', Permits::placeholderS());"));
        
        $this->error("\nAllocate the following line into your autocomplete.php routes file");
        $this->info("");
        $this->warn($this->replace("LaravelitoRouter::post('AutocompleteController@placeholderS', 'placeholders.autocomplete', PermitsAutocomplete::placeholderS());"));
    }

    /**
     * Prepara el menu.
     * @return void
     */

    private function menu(): void
    {
        $this->error("\nAllocate the following snippet into your BackendMenu injector to annex the menu option");
        $this->warn($this->replace('
            /** @var string */
            public static $PLACEHOLDERS = \'placeholders\';

            /**
             * Menu entry for placeholders module, change the fa-truck to whatever you want!
             * @return ParentMenuItem
             */
        
            private static function placeholderS(): ParentMenuItem
            {
                $item = new ParentMenuItem(t(\'placeholders.module.name\'), \'fa-truck\', collect([
                    new LinkMenuItem(t(\'placeholders.create.title\'), \'fa-plus-circle\', url(route(\'placeholders.create\'))),
                    new LinkMenuItem(t(\'placeholders.collections.enabled.title\'), \'fa-th-list\', url(route(\'placeholders.collections.enabled\'))),
                    new LinkMenuItem(t(\'placeholders.collections.disabled.title\'), \'fa-trash\', url(route(\'placeholders.collections.disabled\'))),
                ]));

                $item->active(self::$ACTIVE === self::$PLACEHOLDERS);
                return $item;
            }
        '));
    }

    /**
     * Prepara el controlador.
     * @return void
     */

    private function controller(): void
    {
        $capitals = $this->capitals('Controller');
        $path = app_path("Backend/Controller/{$capitals}.php");
        $source = $this->laravelito('src/Controller/Template/PlaceholdersController.php');
        $this->info('Controller: ' . $path);
        $this->single($source,$path);
    }

    /**
     * Prepara la funcion para autocomplete controller.
     * @return void
     */

    private function autocomplete(): void
    {
        $this->error("\nAllocate the follwing function into your AutcompleteController file");
        $this->warn($this->replace('
            /**
             * AUTOCOMPLETE placeholders
             *
             * @param Request $request
             * @return JsonResponse
             */
        
            public function placeholderS(Request $request): JsonResponse
            {
                return $this->autocomplete($request, \App\Model\Placeholder::class);
            }
        '));
    }

    /**
     * Acomoda el equeleto de los archivos lang. Para es y en.
     * @return void
     */

    private function lang(): void
    {
        $enModule = function(){
            $lowers = $this->lowers();
            $path = resource_path("lang/en/modules/{$lowers}.php");
            $source = $this->laravelito('resources/template/lang/en/modules/placeholders.php');
            $this->info('EN Module Lang: ' . $path);
            $this->single($source,$path);

            $this->error('Append the following line into your lang/en/backend.php config file\'s response array');
            $this->info("");
            $this->warn($this->replace("'placeholders' => require(__DIR__ . '/modules/placeholders.php'),"));
            $this->info("");
        };

        $esModule = function(){
            $lowers = $this->lowers();
            $path = resource_path("lang/es/modules/{$lowers}.php");
            $source = $this->laravelito('resources/template/lang/es/modules/placeholders.php');
            $this->info('ES Module Lang: ' . $path);
            $this->single($source,$path);

            $this->error('Append the following line into your lang/es/backend.php config file\'s response array');
            $this->info("");
            $this->warn($this->replace("'placeholders' => require(__DIR__ . '/modules/placeholders.php'),"));
            $this->info("");
        };

        $enRoutes = function(){
            $lowers = $this->lowers();
            $path = resource_path("lang/en/routes/{$lowers}.php");
            $source = $this->laravelito('resources/template/lang/en/routes/placeholders.php');
            $this->info('EN Routes Lang: ' . $path);
            $this->single($source,$path);

            $this->error('Append the following line into your lang/en/routes.php config file\'s response array');
            $this->info("");
            $this->warn($this->replace("'placeholders' => require(__DIR__ . '/routes/placeholders.php'),"));
            $this->info("");
        };

        $esRoutes = function(){
            $lowers = $this->lowers();
            $path = resource_path("lang/es/routes/{$lowers}.php");
            $source = $this->laravelito('resources/template/lang/es/routes/placeholders.php');
            $this->info('ES Routes Lang: ' . $path);
            $this->single($source,$path);

            $this->error('Append the following line into your lang/es/routes.php config file\'s response array');
            $this->info("");
            $this->warn($this->replace("'placeholders' => require(__DIR__ . '/routes/placeholders.php'),"));
            $this->info("");
        };

        $enModule();
        $esModule();
        $enRoutes();
        $esRoutes();
    }

    /**
     * Prepara el modelo.
     * @return void
     */

    private function model(): void
    {
        $capital = $this->capital();
        $path = app_path("Model/{$capital}.php");
        $source = $this->laravelito('src/Model/Template/Placeholder.php');
        $this->info('Model: ' . $path);
        $this->single($source,$path);
    }

    /**
     * Prepara el snippet para el archivo de permisos.
     * @return void
     */

    private function permits(): void
    {
        $this->error("\nAllocate the following function into your Permits file, suit it to your needs");
        $this->warn($this->replace('

                /**
                 * Permits required for placeholderS module
                 * @return array
                 */
            
                public static function placeholderS(): array
                {
                    return [
                        \'ADMIN\'
                    ];
                }
        '));
    }
}
