<?php

namespace Laravelito\Locales\Services;

use Cookie;
use Illuminate\Http\Request;
use Laravelito\Locales\Locale;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Laravelito\Auth\Model\AuthModelInterface;

class LocaleService {
    /**
     * Setea las locales desde el Store proveido.
     *
     * @param Request $request
     * @return Locale
     */

    public function fromSession(AuthModelInterface $authorized): Locale
    {
        return $authorized->locale()->apply();
    }

    /**
     * Setea las locales desde las cookies recibidas.
     *
     * @param Request $request
     * @return Locale
     */

    public function fromCookie(Request $request): Locale
    {
        $langCookie = Locale::langCookie($request);
        $timezoneCookie = Locale::timezoneCookie($request);

        $lang = $request->cookie($langCookie);
        $tz = $request->cookie($timezoneCookie);
        return Locale::with($lang,$tz)->apply();
    }

    /**
     * Dada una configuracion de locale, guarda en una cookie estos datos.
     *
     * @param Request $request
     * @param Locale $locale
     * @return Collection
     */

    public function cookies(Request $request, Locale $locale): Collection
    {
        $langCookie = Locale::langCookie($request);
        $timezoneCookie = Locale::timezoneCookie($request);
        $lang = $locale->lang();
        $tz = $locale->timezone();
        $lifetime = 1000 * Config::get('session.lifetime');

        Cookie::queue($timezoneCookie, $tz, $lifetime);
        Cookie::queue($langCookie, $lang, $lifetime);

        $cookies = collect();
        $cookies[] = cookie($timezoneCookie,$tz,$lifetime);
        $cookies[] = cookie($langCookie,$lang,$lifetime);

        return $cookies;
    }
    
    /**
     * Setea las locales after login.
     * 
     * @param Request $request
     * @param AuthModelInterface $user 
     * @return Locale
     */
    
    public function loginCallback(Request $request, AuthModelInterface $user): Locale
    {
        $locale = $this->fromSession($user);
        $this->cookies($request,$locale);
        return $locale;
    }
}
