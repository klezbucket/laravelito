<?php

namespace Laravelito\Locales;

use Throwable;
use DateTimeZone;
use Illuminate\Http\Request;
use Laravelito\Core\Facades\Site;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Config;

class Locale {
    /** @var array Idiomas disponibles */
    private static $langs;

    /** @var Locale actual deducido de las cookies. */
    private static $current;

    /** @var string */
    private $lang;

    /** @var string */
    private $timezone;

    /** @var string */
    private $envlocale;

    /**
     * Setter/getter for the $lang var
     * @param ?string $lang
     * @return string
     */

    public function lang(?string $lang = null): string
    {
        if(isset($lang)){
            $this->lang = $lang;
        }

        return $this->lang;
    }
    /**
     * Setter/getter for the $timezone var
     * @param ?string $timezone
     * @return string
     */

    public function timezone(?string $timezone = null): string
    {
        if(isset($timezone)){
            $this->timezone = $timezone;
        }

        return $this->timezone;
    }
    /**
     * Setter/getter for the $envlocale var
     * @param ?string $envlocale
     * @return string
     */

    public function envlocale(?string $envlocale = null): string
    {
        if(isset($envlocale)){
            $this->envlocale = $envlocale;
        }

        return $this->envlocale;
    }

    /**
     * Construye la Locale segun los datos solicitados.
     *
     * @param ?string $lang
     * @param ?string $timezone
     * @return Locale
     */

    public static function with(?string $lang, ?string $timezone): Locale
    {
        if(! in_array($lang, Config::get('laravelito.locale.available'))){
            $lang = Config::get('app.locale');
        }

        if (! in_array($timezone, DateTimeZone::listIdentifiers())) {
            $timezone = Config::get('app.timezone');
        }

        $envlocale = Config::get('laravelito.locale.env.' . $lang);

        $Locale = new Locale();
        $Locale->lang($lang);
        $Locale->timezone($timezone);
        $Locale->envlocale($envlocale);

        return $Locale;
    }

    /**
     * Aplica la Locale actual a los parametros de PHP y Laravel.
     * @return Locale
     */

    public function apply(): Locale
    {

        config(['app.timezone' => $this->timezone()]);
        date_default_timezone_set($this->timezone());
        DB::statement("SET time_zone='{$this->timezone()}'");

        App::setLocale($this->lang());
        setlocale(LC_TIME, $this->envlocale());
        return $this;
    }
    
    /**
     * Obtiene el nombre de la coookie de Timezone.
     * 
     * @param Request $request
     * @return string
     */

    public static function timezoneCookie(Request $request): string
    {
        #$site = Site::resolv($request);
        return "LaravelitoTimezone";
    }

    /**
     * Obtiene el nombre de la coookie de Lang.
     * 
     * @param Request $request
     * @return string
     */

    public static function langCookie(Request $request): string
    {
        #$site = Site::resolv($request);
        return "LarvelitoLang";
    }

    /**
     * Obtiene los idiomas disponibles.
     *
     * @return array
     */

    public static function langs(): array
    {
        if(! ($langs = self::$langs)){
            $langs = Config::get('laravelito.locale.available');
        }

        self::$langs = $langs;
        return $langs;
    }

    /**
     * Obtiene el idioma actual desde las rutas.
     * 
     * @return Locale
     */

    public static function current(): Locale
    {
        if(! ($current = self::$current)){
            $timezoneCookie = Locale::timezoneCookie(request());
            $langCookie = Locale::langCookie(request());
            $lang = request()->cookie($langCookie);
            $timezone = request()->cookie($timezoneCookie);

            try{
                $lang = Crypt::decrypt($lang, false);
                $timezone = Crypt::decrypt($timezone, false);
            }
            catch(Throwable $e){

            }

            $current = Locale::with($lang,$timezone);
        }


        self::$current = $current;
        return $current;
    }
}
