<?php

namespace Laravelito\Locales\Middlewares;

use Closure;
use Throwable;
use Illuminate\Http\Request;
use Laravelito\Core\Logs\Logging;
use Laravelito\Auth\Services\AuthService;
use Laravelito\Core\Middlewares\Middleware;
use Laravelito\Locales\Facades\LocaleRouter;
use Laravelito\Locales\Services\LocaleService;

class LocaleMiddleware extends Middleware {

    /** @var LocaleService servicio que gestiona las locales. */
     private $Locale;

    /** @var AuthService servicio que gestiona las sesion. */
    private $Auth;

    /**
     * Devuelve una instancia del Middleware
     *
     * @param AuthService $auth
     * @param LocaleService $locale
     * @return void
     */

    public function __construct(AuthService $auth, LocaleService $locale)
    {
        $this->Auth = $auth;
        $this->Locale = $locale;
    }

    /**
     * Aplica las locales personalizadas del usuario autenticado, o de las cookies seteadas.
     *
     * Utiliza las defaults declaradas en la configuracion del framework como fallback.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function hook(Request $request, Closure $next)
    {
        if($this->Auth->isAuth($request)){
            Logging::section('LOCALE@session');
            $locale = $this->Locale->fromSession(AuthService::getAuthorized());
        }
        else{
            Logging::section('LOCALE@cookie');
            $locale = $this->Locale->fromCookie($request);
        }

        Logging::output('Lang: ' . $locale->lang() . '@' . $locale->envlocale());
        Logging::output('Timezone: ' . $locale->timezone());
        $this->Locale->cookies($request,$locale);
        return $next($request);
    }
}
