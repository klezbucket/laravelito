<?php

namespace Laravelito\Locales\Facades;

use Throwable;
use Illuminate\Http\Request;
use Laravelito\Locales\Locale;
use Laravelito\Core\Facades\Site;
use Laravelito\Core\Facades\Router;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\RedirectResponse;

class LocaleRouter {

    /** @var array Buffer de rutas para evitar sobreescrituras */
    private static $buffer = [];

    /**
     * Configura la ruta GET con todas las traducciones posibles.
     *
     * @param string $action
     * @param string $name
     * @return void
     */

    public static function get(string $action,string $name): void
    {
        foreach(Locale::langs() as $lang){
            $url = __('routes.' . $name, [], $lang);
            $routeName = self::name($name, 'get', $lang);
            
            if(isset(self::$buffer['get'][$url]) && $name !== $routeName){
                continue;
            }

            self::$buffer['get'][$url] = true;
            Route::get($url, $action)->name($routeName);
        }
    }

    /**
     * Configura la ruta POST con todas las traducciones posibles.
     *
     * @param string $action
     * @param string $name
     * @return void
     */

    public static function post(string $action,string $name): void
    {
        foreach(Locale::langs() as $lang){
            $url = __('routes.' . $name, [], $lang);
            $routeName = self::name($name, 'get', $lang);

            if(isset(self::$buffer['post'][$url]) && $name !== $routeName){
                continue;
            }

            self::$buffer['post'][$url] = true;
            Route::post($url, $action)->name($routeName);
        }
    }

    /**
     * Obtiene el nombre completo de la ruta.
     *
     * @param string $name
     * @param string $method
     * @param string $lang
     * @return string
     */

    public static function name(string $name,string $method, string $lang): string
    {
        $current = Locale::current();

        if($current->lang() === $lang){
            return $name;
        }

        return $name . '#' . strtolower($method) .'@' . $lang;
    }
}
