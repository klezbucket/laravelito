<?php

namespace Laravelito\Locales\Facades;

use Laravelito\Core\Facades\Site;

class Translator {
    /**
     * Resuelve la traduccion especificada utilizando el Site actual como archivo.
     * 
     * @param string $key
     * @param array $replace
     * @param string|null $locale
     * @return string|array
     */
    public static function t(string $key, array $replace = [],string $locale = null)
    {
        return __(Site::resolv() . '.' . $key, $replace,$locale);
    }
}