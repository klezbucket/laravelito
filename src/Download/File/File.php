<?php
namespace Laravelito\Download\File;

use Illuminate\Http\Request;

class File {
    /**
     * Determina si un archivo puede abrirse para lectura.
     * @param string $path
     * @return bool
     */

    public static function isReadable(string $path): bool
    {
        
        return file_exists($path) && is_readable($path) && !is_dir($path);
    }

    /**
     * Determina si el archivo es distinto al request segun cabeceras
     * @param Request $request
     * @param string
     * @return bool
     */

    public static function isCached(Request $request,string $path): bool
    {
        if(! File::isReadable($path)){
            return false;
        }

        $lastModified = File::lastModified($path);
        $etag = File::etag($path);

        $ifNoneMatch = $request->header('If-None-Match') ?? null;
        $ifModifiedSince = strtotime($request->header('If-Modified-Since') ?? null);

        if(strcmp($etag, $ifNoneMatch) === 0){
            if($ifModifiedSince === $lastModified){
                return true;
            }
        }

        return false;
    }

    /**
     * Obtiene el ETag del archivo
     * @param string
     * @return string
     */
    public static function etag(string $path): string
    {
        if(! File::isReadable($path)){
            return '';
        }

        return md5_file($path);
    }

    /**
     * Obtiene el Last Mod del archivo
     * @param string
     * @return int
     */
    public static function lastModified(string $path): int
    {
        if(! File::isReadable($path)){
            return -1;
        }

        return filemtime($path);
    }
}