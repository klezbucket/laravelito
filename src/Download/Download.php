<?php

namespace Laravelito\Download;

use Symfony\Component\HttpFoundation\StreamedResponse;

abstract class Download {
    /** @var array */
    private $headers = [];

    /** @var bool */
    private $notModified = false;

    /** @var bool */
    private $notFound = false;

    /**
     * Agrega la cabecera al array de cabeceras.
     * @param string $name
     * @param string $value
     * @return Download
     */

    public function header(string $name, string $value): Download
    {
        $this->headers[$name] = $value;
        return $this;
    }

    /**
     * Devuelve la respuesta de la descarga.
     * @return StreamedResponse
     */

    public function response(): StreamedResponse
    {
        if($this->notFound){
            return (new StreamedResponse(function(){}, 404));
        }

        if($this->notModified){
            return (new StreamedResponse(function(){}, 304));
        }

        $callback = function() {
            $fd = fopen('php://output', 'w');
            $this->writer($fd);
            fclose($fd);
        };

        return (new StreamedResponse($callback, 200, $this->headers));
    }

    /**
     * Determina que no existe una version nueva del archivo en el servidor.
     * @return void
     */

    public function notModified(): void
    {
        $this->notModified = true;
    }

    /**
     * Determina que no existe el archivo en el servidor.
     * @return void
     */

    public function notFound(): void
    {
        $this->notFound = true;
    }

    /**
     * Establece el callback que escribira en el descriptor la respuesta.
     * 
     * @param mixed $fd
     * @return void
     */

    abstract public function writer($fd): void;
}