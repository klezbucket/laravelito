<?php
namespace Laravelito\Download\LocalStorage;

use Laravelito\Download\Download;
use Laravelito\Download\File\File;
use Laravelito\Upload\LocalStorage;
use Illuminate\Database\Eloquent\Model;

class LocalStorageDownload extends Download {
    
    /** @var string */
    private $path;

    /**
     * Constructor recibe el path del archivo.
     * @param string $path
     * @return void
     */

    public function __construct(string $path)
    {
        $this->path = $path;

        if(! File::isReadable($path)){
            $this->notFound();
            return;
        }

        if(File::isCached(request(), $path)){
            $this->notModified();
            return;
        }

        $this->header('ETag', File::etag($path));
        $this->header('Last-Modified', gmdate("D, d M Y H:i:s", File::lastModified($path)) . " GMT");
        $this->header('Cache-Control', 'private');

        $mime = $path . '.' . LocalStorage::MIME;
        $this->header('Content-Type', file_get_contents($mime));
    }

    /**
     * Establece el callback que escribira en el descriptor la respuesta.
     * 
     * @param mixed $fd
     * @return void
     */

    public function writer($fd): void
    {
        $file = fopen($this->path, 'r');
        stream_copy_to_stream($file, $fd);
    }
}