<?php

namespace Laravelito\Download\CSV;

use Closure;
use Laravelito\Download\Download;
use Illuminate\Support\LazyCollection;
use Illuminate\Database\Eloquent\Model;

class CSVGenerator extends Download {
    /** @var string */
    private $name;

    /** @var array */
    private $cols = [];

    /** @var LazyCollection */
    private $collection;

    /** @var Closure
     *  @param Model $data
     *  @return array
     */
    private $callback;

    /**
     * Constructor.
     * 
     * @param string $name
     * @param array $cols
     * @param array $data
     * @return void 
     */

    public function __construct(string $name,LazyCollection $collection,array $cols = [])
    {
        $this->name = $name;
        $this->cols = $cols;
        $this->collection = $collection;
    
        $this->header('Content-Type','application/vnd.ms-excel; charset=utf-8');
        $this->header('Cache-Control','must-revalidate, post-check=0, pre-check=0');
        $this->header('Content-Disposition','attachment; filename=' . $this->name);
        $this->header('Expires','0');
        $this->header('Pragma','public');
    }

    /**
     * Establece el callback que escribira en el descriptor la respuesta.
     * 
     * @param mixed $fd
     * @return void
     */

    public function writer($fd): void
    {
        $this->write($fd, $this->cols);

        foreach($this->collection as $data){
            $row = $this->row($data);
            $this->write($fd, $row);
        }
    }

    /**
     * Resuelve la fila de datos.
     * @param Model $model
     * @return array
     */

    private function row(Model $data): array
    {
        if($cb = $this->callback){
            $row = $cb($data);
        }
        else{
            $row = $data->toArray();
        }

        return $row;
    }

    /**
     * Escribe el array en el desriptor del archivo CSV, si no es vacio.
     * @param mixed $fd
     * @param array $data 
     * @return void
     */

    private function write($fd, array $data): void
    {
        if(! empty($data)){
            fputcsv($fd, $data);
        }
    }

    /**
     * Setter de callback
     * 
     * @return CSVGenerator
     */

    public function setCallback(Closure $cb): CSVGenerator
    {
        $this->callback = $cb;
        return $this;
    }
}