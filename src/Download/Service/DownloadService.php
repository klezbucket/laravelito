<?php

namespace Laravelito\Download\Service;

use Laravelito\Download\Download;
use Laravelito\Upload\LocalStorage;
use Illuminate\Support\LazyCollection;
use Illuminate\Database\Eloquent\Model;
use Laravelito\Download\CSV\CSVGenerator;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Laravelito\Download\LocalStorage\LocalStorageDownload;

class DownloadService {
    /** @var Download */
    private $download;

    /**
     * Instancia el csv generator.
     * 
     * @param string $name
     * @param LazyCollection $collection
     * @param array $cols
     * @return CSVGenerator
     */

    public function csv(string $name,LazyCollection $collection,array $cols = []): CSVGenerator
    {
        $this->download = new CSVGenerator($name,$collection,$cols);
        return $this->download;
    }
    
    /**
     * Prepara la descarga desde el local storage.
     * @param Model $model
     * @param string $field
     * @return LocalStorageDownload
     */

    public function localModel(Model $model,string $field): LocalStorageDownload
    {
        $path = LocalStorage::fullpath($model,$field);
        $this->download = new LocalStorageDownload($path);
        return $this->download;
    }

    /**
     * Devuelve la respuesta de la descarga.
     * @param array $headers
     * @return StreamedResponse
     */

    public function response(array $headers = []): StreamedResponse
    {
        foreach($headers as $header => $value) {
            $this->download->header($header, $value);
        }
        
        return $this->download->response();
    }
}