<?php

namespace Laravelito\Detail;

use Laravelito\Detail\Detail;

interface Injector {
    /**
     * Dado el nombre del detail deseado, devuelve una instancia del mismo.
     * 
     * @param string $name
     * @return Detail
     */

    function get(string $name): Detail;
}