<?php

namespace App\Backend\Detail\CRUD\Users;

use Laravelito\Field\Field;
use Laravelito\Detail\Detail;
use Laravelito\View\Link\Link;
use App\Backend\Links\UserLinks;
use Illuminate\Support\Collection;
use App\Backend\Field\Users\EmailField;
use App\Backend\Field\Users\StatusField;
use App\Backend\Field\Users\CreatedAtField;
use App\Backend\Field\Users\DeletedAtField;

class FetchDetail extends Detail
{
    /**
     * Obtiene la coleccion de campos.
     *
     * @return Collection of Field
     */

    public function fields(): Collection
    {
        $fields = collect();
        $fields[] = new CreatedAtField();
        $fields[] = new EmailField();
        $fields[] = new DeletedAtField();
        return $fields;
    }

    /**
     * Obtiene la coleccion de links.
     *
     * @return Collection of Link
     */

    public function links(): Collection
    {
        return UserLinks::provide();
    }

    /**
     * Obtiene el contexto que alimenta el detalle
     *
     * @return string
     */

    public function context(): string
    {
        return 'users.fetch';
    }
}
