<?php

namespace App\Backend\Detail;

use Laravelito\Detail\Detail;
use Laravelito\Detail\Injector;
use Illuminate\Support\Collection;
use Laravelito\Core\Facades\Loader;
use App\Backend\Loader\Loader as L;
use Illuminate\Support\Facades\Config;
use Laravelito\Core\Exceptions\NotFoundException;

class BackendDetail implements Injector {
    /**
     * Devuelve el Detail requerido.
     *
     * @param string $name
     * @return Detail
     * @throws NotFoundException si el $name especificado no resuelve un detail valido.
     */

    public function get(string $name): Detail
    {
        $class = Loader::getClass('App\\Backend\\Detail\\CRUD', L::get($name) . 'Detail', Detail::class);
        return new $class($name);
    }
}
