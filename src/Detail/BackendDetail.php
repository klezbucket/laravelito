<?php

namespace Laravelito\Detail;

use Laravelito\Detail\Detail;
use Laravelito\Detail\Injector;
use Illuminate\Support\Collection;
use Laravelito\Core\Facades\Loader;
use Illuminate\Support\Facades\Config;
use Laravelito\Core\Exceptions\NotFoundException;

class BackendDetail implements Injector {
    /**
     * Devuelve el Detail requerido.
     * Implementa tu propio Inyector.
     *
     * @param string $name
     * @return Detail
     * @throws NotFoundException si el $name especificado no resuelve un detail valido.
     */

    public function get(string $name): Detail
    {
        null;
    }
}
