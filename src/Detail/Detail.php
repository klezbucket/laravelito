<?php

namespace Laravelito\Detail;

use Laravelito\Field\Field;
use Laravelito\Detail\Detail;
use Laravelito\Detail\Injector;
use Laravelito\Core\Facades\Site;
use Illuminate\Support\Collection;
use Laravelito\Field\FieldProvider;
use Illuminate\Support\Facades\Config;
use Laravelito\Field\FieldDependences;
use Laravelito\View\Link\LinkProvider;
use Illuminate\Database\Eloquent\Model;
use Laravelito\CRUD\Model\ModelProvider;
use Laravelito\CRUD\Context\ContextProvider;
use Laravelito\Core\Exceptions\InternalServerException;

abstract class Detail implements FieldProvider, ContextProvider, ModelProvider, LinkProvider {
    /** @var string nombre del detail */
    private $name;

    /** @var string modelo */
    private $model;

    /**
     * Constructor del detail.
     * @return void
     */

    public function __construct(string $name)
    {
        $this->name = $name;
    }
    
    /**
     * Obtiene las dependencias de campos.
     * Sobreescribe este metodo para inyectar tus propias 
     *
     * @return FieldDependences
     */

    public function dependences(): ?FieldDependences
    {
        return null;
    }

    /**
     * Obtiene el namespace de los campos. 
     * Por defecto es el nombre mismo del detalle hasta el primer punto.
     *
     * @return string
     */

    public function localesAt(): string
    {
        return explode('.',$this->name())[0] ?? $this->name();
    }
    
    /**
     * Obtiene el nombre del detail.
     *
     * @return string
     */

    public function name(): string
    {
        return $this->name;
    }

    /**
     * Setter/Getter para el modelo.
     *
     * @param Model|null $model
     * @return Model
     */
    
    public function model(?Model $model = null): ?Model
    {
        if(isset($model)){
            $this->model = $model;
        }

        return $this->model;
    }

    /**
     * Obtiene el valor del campo dado.
     *
     * @param Field $field
     * @return string
     */

    public function value(Field $field): ?string
    {
        if($model = $this->model){
            $name = $field->name();
            return $model->{$name};
        }

        return null;
    }

    /**
     * Devuelve la instancia de un detail segun su nombre, a traves del inyector.
     * @param string $name
     * @return Detail
     */

    public static function instantiate(string $name): Detail
    {
        if(! ($detail = self::$BAG[$name] ?? null)){
            $site = Site::resolv(request());
            $class = Config::get('laravelito.details.' . $site);
            $injector = new $class($name);

            if(! ($injector instanceof Injector)){
                throw new InternalServerException('Class provided by laravelito.details.' . $site . ' is not an Injector instance');
            }

            $detail = $injector->get($name);

            if(! ($detail instanceof Detail)){
                throw new InternalServerException("Class provided by {$class} is not a Detail instance");
            }
        }

        self::$BAG[$name] = $detail;
        return $detail;
    }

    /**
     * Al instanciar un detail se va a una bolsa de cache.
     *
     * @var array
     */

    private static $BAG = [];
}
