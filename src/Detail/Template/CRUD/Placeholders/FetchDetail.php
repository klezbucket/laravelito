<?php

namespace App\Backend\Detail\CRUD\Placeholders;

use Laravelito\Field\Field;
use Laravelito\Detail\Detail;
use Laravelito\View\Link\Link;
use App\Backend\Links\UserLinks;
use Illuminate\Support\Collection;
use App\Backend\Links\PlaceholderLinks;

class FetchDetail extends Detail
{
    /**
     * Obtiene la coleccion de campos.
     *
     * @return Collection of Field
     */

    public function fields(): Collection
    {
        $fields = collect();
        return $fields;
    }

    /**
     * Obtiene la coleccion de links.
     *
     * @return Collection of Link
     */

    public function links(): Collection
    {
        return PlaceholderLinks::provide();
    }

    /**
     * Obtiene el contexto que alimenta el detalle
     *
     * @return string
     */

    public function context(): string
    {
        return 'placeholders.fetch';
    }
}
