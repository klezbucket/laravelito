<a href="{{ Link::url($provider,$link) }}" class="@yield(Link::yield($provider,'link_class',$link))">
    @include(Link::icon($provider,$link)) 
    @include(Link::text($provider,$link)) 
</a> 