<a href="{{ Link::url($provider,$link) }}" title="@include(Link::text($provider,$link))" class="@yield(Link::yield($provider,'link_class',$link))">
    <i class="fa {{ $link->icon() }}"></i> <span class="sr-only">@include(Link::text($provider,$link))</span>
</a> 
