@if($icon)
    <i class="breadcrumb-icon fa {{ $icon }} mr-2"></i>
@endif

{{ $title }}