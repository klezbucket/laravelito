@if (strcmp(request()->url(), $url) === 0)
    <li class="breadcrumb-item active">
@else
    <li class="breadcrumb-item">
@endif
    <a href="{{ $url }}">
        @include(Theme::partial('backend.breadcrumb.__label'), $item->data())
    </a>
</li>