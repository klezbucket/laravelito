<a class="dropdown-item" href="{{ route('backend.me') }}"><span class="dropdown-icon oi oi-person"></span>  {{ t('me.title') }}</a>
<a class="dropdown-item" href="{{ route('backend.logout') }}"><span class="dropdown-icon oi oi-account-logout"></span> {{ t('logout.title') }}</a>
