@if($flash = Flash::unserialize())    
  <div class="row">
    <div class="col-lg-12">
      <div class="alert alert-{{ $flash->type() }} alert-dismissible fade show @if($flash->icon()) has-icon @endif">
        <button type="button" class="close" data-dismiss="alert">×</button> 

        @if($flash->icon())
          <div class="alert-icon">
              <span class="oi oi-{{ $flash->icon() }}"></span>
          </div>
        @endif
        
        @if($flash->title())
          <h4 class="alert-heading"> {{ $flash->title() }} </h4>
              
        @endif

        <p class="mb-0">
          {{ $flash->message() }}
        </p>
      </div>
    </div>
  </div>
@endif