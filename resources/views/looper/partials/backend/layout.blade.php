@section('account')
    @include(Theme::partial('backend/account'))
@endsection

@section('top')
    @include(Theme::partial('backend/top'))
@endsection

@section('logo')
    @include(Theme::partial('backend/logo'))
@endsection

@section('account_aside')
    @include(Theme::partial('backend/account_aside'))
@endsection

@section('menu')
    @include(Theme::partial('backend/menu'))
@endsection

@section('header')
    @include(Theme::partial('backend/header'))
@endsection

@section('flash')
    @include(Theme::partial('backend/flash'))
@endsection

@section('footer')
    @include(Theme::partial('backend/footer'))
@endsection