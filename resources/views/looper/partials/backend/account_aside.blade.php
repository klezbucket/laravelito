<button class="btn-account" type="button" data-toggle="collapse" data-target="#dropdown-aside">
    <span class="user-avatar user-avatar-lg">
        @include(Theme::partial('backend/account/avatar'))
    </span>
    <span class="account-icon"><span class="fa fa-caret-down fa-lg"></span></span>
    <span class="account-summary">
        @include(Theme::partial('backend/account/summary'))
    </span>
</button>
<div id="dropdown-aside" class="dropdown-aside collapse" style="">
    <!-- dropdown-items -->
    <div class="pb-3">
        @include(Theme::partial('backend/account/links'))
    </div><!-- /dropdown-items -->
</div>
