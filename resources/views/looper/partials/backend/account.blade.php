<div class="dropdown d-flex">
<button class="btn-account d-none d-md-flex" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="user-avatar user-avatar-md">
        @include(Theme::partial('backend/account/avatar'))
    </span>
    <span class="account-summary pr-lg-4 d-none d-lg-block">
        @include(Theme::partial('backend/account/summary'))
    </span>
</button> <!-- .dropdown-menu -->
<div class="dropdown-menu">
<div class="dropdown-arrow ml-3"></div>
<h6 class="dropdown-header d-none d-md-block d-lg-none"> {{ Auth::user('full_name') }} </h6>
    @include(Theme::partial('backend/account/links'))
</div><!-- /.dropdown-menu -->
</div><!-- /.btn-account -->
