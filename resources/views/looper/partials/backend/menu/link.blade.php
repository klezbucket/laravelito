@if (strcmp(request()->url(), $url) === 0)
    <li class="menu-item has-active">
@else
    <li class="menu-item">
@endif
    <a href="{{ $url }}" class="menu-link">
        @include(Theme::partial('backend.menu.__link'), $item->data())
    </a>
</li>