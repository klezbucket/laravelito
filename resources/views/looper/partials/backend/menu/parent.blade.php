@component(Theme::partial('backend.menu.childs'), [ 'item' => $item ])
    @include(Theme::core('__menu'), [
        'items' => $childs
    ])
@endcomponent