<li class="menu-item has-child {{ $item->active() ? 'has-active' : '' }}">
    <a href="#" class="menu-link">
        @include(Theme::partial('backend.menu.__link'), $item->data())
    </a> <!-- child menu -->
    <ul class="menu">
        {{ $slot }}
    </ul>
</li>