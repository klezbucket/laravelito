<div class="card-deck-xl">
    @include(Detail::before($detail))
    <!-- .card -->
    <div class="card card-fluid">
        <div class="card-body">
            @foreach (Detail::fields($detail) as $field)
                @include(Detail::field($detail,$field))
            @endforeach
        </div>
        <div class="card-footer">
            @foreach ($detail->links() as $link)
                @include(Link::provide($detail,$link,'card'),[
                    'provider' => $detail
                ])
            @endforeach
        </div>
    </div><!-- /.card -->
    @include(Detail::after($detail))
</div>

