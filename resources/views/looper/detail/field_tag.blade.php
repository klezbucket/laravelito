<div class="@yield(Detail::yield($detail,'field_class',$field))">
    @include(Detail::label($detail,$field))
    @include(Detail::value($detail,$field))
</div>
