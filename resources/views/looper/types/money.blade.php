@php $value = Type::decimalValue($provider,$field) @endphp
{{ t($field->type()->currency()) }}
@include(Type::print($provider,$field,$value),[
    'value' => $value
])