@if($url = Upload::url($provider,$field))
    <a target="_blank" href="{{ $url }}">{{ Upload::link($provider,$field) }}</a>
@else
    @include(Type::printNull($provider,$field))
@endif