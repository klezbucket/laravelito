@if($html = Type::textValue($provider,$field))
    <span><span>    
    <a href="#" data-toggle="modal" data-target="#{{ Type::id($provider,$field) }}">{{ Summernote::link($provider,$field) }}</a>

    <div class="modal fade" id="{{ Type::id($provider,$field) }}" tabindex="-1" role="dialog" aria-hidden="true">
            <!-- .modal-dialog -->
            <div class="modal-dialog modal-lg" role="document">
                <!-- .modal-content -->
                <div class="modal-content">
                <!-- .modal-header -->
                <!-- .modal-body -->
                <div class="modal-body">{!! $html !!}</div><!-- /.modal-body -->
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
@else
    @include(Type::printNull($provider,$field))
@endif