@php $value = Type::agoValue($provider,$field) @endphp
@php $title = Type::datetimeValue($provider,$field) @endphp

<a title="{{ $title }}">
    @include(Type::print($provider,$field,$value),[
        'value' => $value
    ])
</a>