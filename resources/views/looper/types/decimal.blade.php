@php $value = Type::decimalValue($provider,$field) @endphp

@include(Type::print($provider,$field,$value),[
    'value' => $value
])