<label class="switcher-control switcher-control-danger switcher-control-lg">
    <input 
        type="hidden" 
        name="@yield(Form::yield($form,'name',$field))"  />
    <input
        @yield(Form::yield($form,'extra_attributes',$field))
        type="checkbox" 
        id="@yield(Form::yield($form,'id',$field))" 
        name="@yield(Form::yield($form,'name',$field))" 
        @yield(Form::yield($form,'checked',$field))
        aria-describedby="@yield(Form::yield($form,'described',$field))" 
        class="switcher-input">
    <span class="switcher-indicator"></span>
</label> <!-- /.switcher-control -->
    