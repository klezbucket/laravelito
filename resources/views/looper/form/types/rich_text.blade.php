@extends(Form::textarea($form,$field))
@include(Form::attributes($form,$field))
@section(Form::yield($form,'extra_attributes',$field))
    data-toggle="summernote"
    data-lang="{{ Summernote::locale() }}"
@endsection
@php
    Theme::script("form/summernote");
@endphp