@extends(Form::switcher($form,$field))
@include(Form::attributes($form,$field))
@section(Form::yield($form,'checked',$field))
    @if($form->value($field))
        {{ 'checked="checked"'}}
    @endif
@endsection