<div dropzone-for="@include(Form::attribute($form,'name',$field))" class="picture fileinput-dropzone @unless(Form::isValid($form,$field))is-invalid @endunless">
    <span class="placeholder">@include(Form::attribute($form,'placeholder',$field))</span>
    <span class="loader"><i class="fa fa-4x fa-spin fa-cogs"></i></span>
</div>

@extends(Form::input($form,$field))
@include(Form::attributes($form,$field))
@section(Form::yield($form,'extra_attributes',$field))
    data-picture-clear="{{ Form::uploadClear($form,$field) }}"
    data-picture-url="{{ Upload::url($form,$field) }}"
@endsection
@section(Form::yield($form,'type',$field)){{ 'hidden' }}@endsection

<input file-for="@include(Form::attribute($form,'name',$field))" type="file" />
<input size-for="@include(Form::attribute($form,'name',$field))" value="{{ $field->type()->size() }}" type="hidden" />

<input lang-for="@include(Form::attribute($form,'name',$field))" value="{{ $field->type()->invalidSize() }}" lang-entry="invalid.size" type="hidden" />
<input lang-for="@include(Form::attribute($form,'name',$field))" value="{{ $field->type()->invalidMime() }}" lang-entry="invalid.mime" type="hidden" />

@foreach($field->type()->mimes() as $mime)
    <input mime-for="@include(Form::attribute($form,'name',$field))" value="{{ $mime }}" type="hidden" />
@endforeach

@php
    Theme::script("form/picture");
@endphp