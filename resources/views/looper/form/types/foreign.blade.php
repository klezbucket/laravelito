@extends(Form::select($form,$field))
@include(Form::selectAttributes($form,$field))
@section(Form::yield($form,'extra_attributes',$field))
    data-foreign-url="{{ $field->type()->url() }}"
    data-foreign-bearer="{{ Auth::user()->bearer() }}"
    data-foreign-text="{{ Form::autocompleteText($form,$field) }}"
    data-foreign-id="{{ Form::value($form,$field) }}"
    data-foreign-clear="{{ Form::autocompleteClear($form,$field) }}"
    data-foreign-hidden="{{ Form::autocompleteHidden($form,$field) }}"
    lang="{{ app()->getLocale() }}"
    @if($field->type()->multiple())
        multiple
    @endif
@endsection
@section(Form::yield($form,'class',$field))@include(Form::attribute($form,'class',$field))@endsection
@php
    Theme::script('form/foreign');
@endphp

@extends(Form::hidden($form,$field),[
    'name' => Form::autocompleteHidden($form,$field),
    'value' => Form::autocompleteText($form,$field)
])