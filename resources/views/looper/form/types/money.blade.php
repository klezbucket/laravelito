@extends(Form::input($form,$field))
@include(Form::attributes($form,$field))
@section(Form::yield($form,'extra_attributes',$field))
    step={{ $field->type()->step() }}
    @if($field->type()->unsigned())
        min=0
    @endif
@endsection
@section(Form::yield($form,'type',$field)){{ 'number' }}@endsection