@extends(Form::input($form,$field))
@include(Form::attributes($form,$field))
@section(Form::yield($form,'extra_attributes',$field))
    data-toggle="flatpickr"
    data-mode="range"
    data-alt-input="true"
    data-alt-format="F j, Y"
    data-date-format="Y-m-d"
    data-show-months="2"
    lang="{{ app()->getLocale() }}"
@endsection
@section(Form::yield($form,'class',$field))@include(Form::attribute($form,'class',$field))@endsection