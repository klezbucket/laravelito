
<div class="table-responsive rows-form">
    <table class="table table-bordered" id="{{ $field->type()->form()->name() }}">
        @include(RowsForm::thead($field->type()->form()),[
            'form' => $field->type()->form(),
            'rows' => $field
        ])

        @include(RowsForm::tbody($field->type()->form()),[
            'form' => $field->type()->form(),
            'rows' => $field
        ])
    </table>
</div>

<textarea style="display: none;" data-rowsform-lang="{{ $field->type()->form()->name() }}">@php echo json_encode(__(Site::resolv() . '.' . $field->type()->form()->lang())) @endphp</textarea>
<textarea style="display: none;" data-rowsform-data="{{ $field->type()->form()->name() }}">@php echo json_encode($field->type()->form()->data()) @endphp</textarea>
<textarea style="display: none;" data-rowsform-validations="{{ $field->type()->form()->name() }}">@php echo json_encode($field->type()->form()->validations()) @endphp</textarea>
<textarea style="display: none;" data-rowsform-attrs="{{ $field->type()->form()->name() }}">@php echo json_encode($field->type()->form()->attrs()) @endphp</textarea>
<textarea style="display: none;" data-rowsform-empty-rows="{{ $field->type()->form()->name() }}">{{ $field->type()->form()->emptyRows() }}</textarea>

@php
    Theme::script('form/rows');
    Theme::stylesheet('form/rows');
@endphp