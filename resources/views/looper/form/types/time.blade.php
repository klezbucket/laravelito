@extends(Form::input($form,$field))
@include(Form::attributes($form,$field))
@section(Form::yield($form,'extra_attributes',$field))
    data-toggle="flatpickr"
    data-enable-time="true"
    data-no-calendar="true"
    data-date-format="H:i"
    lang="{{ app()->getLocale() }}"
@endsection
@section(Form::yield($form,'class',$field))@include(Form::attribute($form,'class',$field))@endsection