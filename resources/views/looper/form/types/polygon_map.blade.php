<div class="gmap gmap-polygon" data-gmap-canvas="{{ $field->name() }}">{{ t('looper.gmaps.loading') }}</div>

@extends(Form::input($form,$field))
@include(Form::attributes($form,$field))
@section(Form::yield($form,'type',$field)){{ 'hidden' }}@endsection

<textarea style="display: none;" data-gmap-latitude="{{ $field->name() }}">{{ $field->type()->latitude()  }}</textarea>
<textarea style="display: none;" data-gmap-longitude="{{ $field->name() }}">{{ $field->type()->longitude()  }}</textarea>
<textarea style="display: none;" data-gmap-zoom="{{ $field->name() }}">{{ $field->type()->zoom()  }}</textarea>
<textarea style="display: none;" data-gmap-type="{{ $field->name() }}">{{ $field->type()->name()  }}</textarea>

@php
    Theme::script("form/gmap");
    Theme::stylesheet('gmap/gmap');
    Theme::externalScript("https://maps.googleapis.com/maps/api/js?key={$field->type()->apiKey()}&libraries=drawing");
@endphp