<script>
    if(typeof $_FORMS === 'undefined'){
        $_FORMS = {};
    }

    $_FORMS[<?=json_encode(Form::id($form))?>] = {
        deps: <?= json_encode($form->dependences()->deps()) ?>
    }
</script>

@php
    Theme::script('form/dependences');
@endphp