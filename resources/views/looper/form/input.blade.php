<input      
     @yield(Form::yield($form,'extra_attributes',$field))
     type="@yield(Form::yield($form,'type',$field))" 
     class="@yield(Form::yield($form,'class',$field))"
     id="@yield(Form::yield($form,'id',$field))" 
     name="@yield(Form::yield($form,'name',$field))" 
     value="@yield(Form::yield($form,'value',$field))" 
     aria-describedby="@yield(Form::yield($form,'described',$field))" 
     placeholder="@yield(Form::yield($form,'placeholder',$field))" /> 
