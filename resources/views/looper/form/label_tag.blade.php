<label data-type="{{ $field->type()->name() }}" class="@yield(Form::yield($form,'label_class',$field))" for="{{ Form::id($form,$field) }}">
    @include(Form::labelText($form,$field)) 
    <span>@include(Form::labelExtras($form,$field))</span>
</label> 