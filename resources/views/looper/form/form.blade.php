<form id="{{ Form::id($form) }}" method="{{ $form->method() }}" action="{{ $form->action() }}">
    <div class="card-deck-xl">
    @include(Form::before($form))
    
    <div id="base-style" class="card card-fluid">
        @include(Form::header($form))
        <!-- .card-body -->
        <div class="card-body">
            <!-- .form -->
            <!-- .fieldset -->
            <fieldset>
                @if($form->method() === 'POST')
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                @endif
                
                @foreach ($form->fields() as $field)
                    @include(Form::group($form,$field))
                @endforeach

                <div class="form-actions">
                    @include(Form::submit($form))
                </div>
            </fieldset><!-- /.fieldset -->
        </div><!-- /.card-body -->
    </div>

    @include(Form::after($form))
    </div>
</form><!-- /.form -->

@include(Form::assets($form))
@include(Form::dependences($form))