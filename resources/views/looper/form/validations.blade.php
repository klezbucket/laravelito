<div class="invalid-feedback">
    <i class="fa fa-exclamation-circle fa-fw"></i> {{ Form::errors($form,$field) }}
</div>