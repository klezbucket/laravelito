<div class="section-block">
        <!-- grid row -->
        <div class="row">
            <!-- grid column -->
            <div class="col-lg-12">
            <!-- .list-group -->
            <div class="table-picker list-group list-group-bordered mb-3">
                @foreach($table->collection()->collection() as $item)
                    <a data-id="{{ $item->toArray()['id'] }}" class="list-group-item list-group-item-action">
                        @include(Picker::icon($table))
                        @include(Picker::body($table))
                        @include(Picker::angle($table))
                    </a>
                @endforeach
            </div><!-- /.list-group -->

            @include(Table::summary($table))
            @include(Table::pagination($table))
            </div><!-- /grid column -->
        </div><!-- /grid row -->
    </div>
    
    <form id="form" method="post">
        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
        <input id="input" type="hidden" name="{{ $table->model()->getKeyName() }}" />
    </form>
    
    @php
        Theme::script('table/picker');
        Theme::stylesheet('table/picker');
    @endphp
    