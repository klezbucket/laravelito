<td>
    @foreach($table->links() as $link)
        @include(Link::provide($table,$link,'table'),[
            'provider' => $table
        ])
    @endforeach
</td>