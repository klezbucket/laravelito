<tr>
    @foreach($table->fields() as $field)
        @include(Table::td($table,$field))
    @endforeach
    
    @include(Table::actionsTd($table))
</tr>