<ul class="pagination justify-content-center mt-4">
    @include(Table::beforePageLink($table))
    @include(Table::pageLinks($table))
    @include(Table::afterPageLink($table))
</ul><!-- /.pagination -->