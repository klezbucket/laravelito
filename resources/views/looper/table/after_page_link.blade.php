@if($table->collection()->current() < $table->collection()->pages() - 1)
    @if($table->collection()->current() < $table->collection()->pages() - 2)   
        @include(Table::pageDots($table))
    @endif

    @include(Table::pageLink($table), [
        'page' => $table->collection()->pages()
    ])
@endif

@if($table->collection()->current() >= $table->collection()->pages())
    <li class="page-item disabled">
@else
    <li class="page-item">
@endif
    <a class="page-link" href="{{ Table::page($table->collection()->current() + 1) }}" tabindex="-1">
        <i class="fa fa-lg fa-angle-right"></i>
    </a>
</li>