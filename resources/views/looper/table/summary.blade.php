<div class="text-muted"> {{ t( $table->name() . '.summary', [
    'total' => $table->collection()->total(),
    'lower' => $table->collection()->lower(),
    'upper' => $table->collection()->upper(),
]) }} </div>