@if($table->collection()->current() > 1)
    @include(Table::pageLink($table), [
        'page' => $table->collection()->current() - 1
    ])
@endif

@include(Table::pageLink($table), [
    'page' => $table->collection()->current()
])

@if($table->collection()->current() < $table->collection()->pages())
    @include(Table::pageLink($table), [
        'page' => $table->collection()->current() + 1
    ])
@endif