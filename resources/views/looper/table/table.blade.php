@if($filters = $table->filters())
  @include(Theme::core('form'), [
      'name' => $filters
  ])
@endif

<div class="card card-fluid">
    <div class="card-body">
      <div class="table-responsive">
        <!-- .table -->
        <table class="table">
          <!-- thead -->
          <thead>
            <tr>
              @foreach($table->fields() as $field)
                @include(Table::th($table,$field))
              @endforeach

              @include(Table::actionsTh($table))
            </tr>
          </thead><!-- /thead -->
          <!-- tbody -->
          <tbody>
              @foreach($table->collection()->collection() as $data)
                @include(Table::tr($table,$data))
              @endforeach

          </tbody><!-- /tbody -->
        </table><!-- /.table -->
      </div><!-- /.table-responsive -->
      <!-- .pagination -->
      @include(Table::summary($table))
      @include(Table::pagination($table))
    </div><!-- /.card-body -->
  </div>