@if($table->collection()->current() === $page)
    <li class="page-item active">
@else
    <li class="page-item">
@endif
    <a class="page-link" href="{{ Table::page($page) }}">{{ $page }}</a>
</li>