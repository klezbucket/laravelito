@if($table->collection()->current() === 1)
    <li class="page-item disabled">
@else
    <li class="page-item">
@endif
    <a class="page-link" href="{{ Table::page($table->collection()->current() - 1) }}" tabindex="-1">
        <i class="fa fa-lg fa-angle-left"></i>
    </a>
</li>

@if($table->collection()->current() > 2)
    @include(Table::pageLink($table), [
        'page' => 1
    ])

    @if($table->collection()->current() > 3)
        @include(Table::pageDots($table))
    @endif
@endif