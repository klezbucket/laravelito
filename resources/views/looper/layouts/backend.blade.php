<!DOCTYPE html>
<html lang="en">
  <head>
    @include(Theme::core('head'))

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- End Required meta tags -->

    <!-- FAVICONS -->
    <link rel="apple-touch-icon" sizes="144x144" href="{{ Theme::asset('apple-touch-icon.png') }}">
    <link rel="shortcut icon" href="{{ Theme::asset('favicon.ico') }}">
    <meta name="theme-color" content="#3063A0">

    <!-- GOOGLE FONT -->
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600" rel="stylesheet"><!-- End GOOGLE FONT -->

    <!-- BEGIN PLUGINS STYLES -->
    <link rel="stylesheet" href="{{ Theme::asset('vendor/fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('vendor/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('vendor/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('vendor/open-iconic/css/open-iconic-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('vendor/flatpickr/flatpickr.min.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('vendor/summernote/summernote-bs4.css') }}">

    <!-- END PLUGINS STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" href="{{ Theme::asset('stylesheets/theme.min.css') }}" data-skin="default">
    <link rel="stylesheet" href="{{ Theme::asset('stylesheets/theme-dark.min.css') }}" data-skin="dark">
    <link rel="stylesheet" href="{{ Theme::asset('stylesheets/custom.css') }}"><!-- Disable unused skin immediately -->

    @foreach(Theme::stylesheets() as $stylesheet)
      <link rel="stylesheet" href="{{ Theme::asset("stylesheets/{$stylesheet}.css") }}">
    @endforeach

    @foreach(Theme::externalStylesheets() as $stylesheet)
      <link rel="stylesheet" href="{{!! $stylesheet !!}}">
    @endforeach

    <script>
      var skin = localStorage.getItem('skin') || 'default';
      var unusedLink = document.querySelector('link[data-skin]:not([data-skin="' + skin + '"])');
      unusedLink.setAttribute('rel', '');
      unusedLink.setAttribute('disabled', true);
    </script><!-- END THEME STYLES -->
  </head>
  <body>
    <!-- .app -->
    <div class="app">
      <!--[if lt IE 10]>
      <div class="page-message" role="alert">You are using an <strong>outdated</strong> browser. Please <a class="alert-link" href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</div>
      <![endif]-->
      <!-- .app-header -->
      <header class="app-header app-header-dark">
        <!-- .top-bar -->
        <div class="top-bar">
          <!-- .top-bar-brand -->
          <div class="top-bar-brand">
              @yield('logo')
          </div><!-- /.top-bar-brand -->
          <!-- .top-bar-list -->
          <div class="top-bar-list">
            <!-- .top-bar-item -->
            <div class="top-bar-item px-2 d-md-none d-lg-none d-xl-none">
              <!-- toggle menu -->
              <button class="hamburger hamburger-squeeze" type="button" data-toggle="aside" aria-label="toggle menu"><span class="hamburger-box"><span class="hamburger-inner"></span></span></button> <!-- /toggle menu -->
            </div><!-- /.top-bar-item -->
            <!-- .top-bar-item -->
            <div class="top-bar-item top-bar-item-full">
              <!-- .top-bar-search -->
            </div><!-- /.top-bar-item -->
            <!-- .top-bar-item -->
            <div class="top-bar-item top-bar-item-right px-0 d-none d-sm-flex">
              <!-- .nav -->
              <ul class="header-nav nav">
                  @yield('top')
              </ul><!-- /.nav -->
              <!-- .btn-account -->

              @yield('account')

            </div><!-- /.top-bar-item -->
          </div><!-- /.top-bar-list -->
        </div><!-- /.top-bar -->
      </header><!-- /.app-header -->
      <!-- .app-aside -->
      <aside class="app-aside app-aside-expand-md app-aside-light">
        <!-- .aside-content -->
        <div class="aside-content">
          <!-- .aside-header -->
          <header class="aside-header d-block d-md-none">
            <!-- .btn-account -->
            @yield('account_aside')
          </header><!-- /.aside-header -->
          <!-- .aside-menu -->
          <div class="aside-menu overflow-hidden">
            <!-- .stacked-menu -->
            <nav id="stacked-menu" class="stacked-menu">
              <!-- .menu -->
              <ul class="menu">
                @yield('menu')
              </ul><!-- /.menu -->
            </nav><!-- /.stacked-menu -->
          </div><!-- /.aside-menu -->
          <!-- Skin changer -->
          <footer class="aside-footer border-top p-3">
            <button class="btn btn-light btn-block text-primary" data-toggle="skin">{{ t('looper.nightmode') }} <i class="fas fa-moon ml-1"></i></button>
          </footer><!-- /Skin changer -->
        </div><!-- /.aside-content -->
      </aside><!-- /.app-aside -->
      <!-- .app-main -->
      <main class="app-main">
        <!-- .wrapper -->
        <div class="wrapper">
          <!-- .page -->
          <div class="page @yield('page')">
            <!-- .page-inner -->
            <div class="page-inner">
              <!-- .page-title-bar -->
              <header class="page-title-bar">
                @yield('header')
              </header><!-- /.page-title-bar -->
              <!-- .page-section -->
              <div class="page-section">
                @yield('flash')
                @yield('content')
              </div><!-- /.page-section -->
            </div><!-- /.page-inner -->

            <div class="page-sidebar">
              @yield('sidebar')
            </div>
          </div><!-- /.page -->
        </div><!-- .app-footer -->
        {{-- <footer class="app-footer">
          @yield('footer')
        </footer><!-- /.app-footer --> --}}
        <!-- /.wrapper -->
      </main><!-- /.app-main -->
    </div><!-- /.app -->

    @foreach(Theme::deferred() as $view)
      @include($view)
    @endforeach

    <!-- BEGIN BASE JS -->
    <script src="{{ Theme::asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script> <!-- END BASE JS -->
    <script src="{{ Theme::asset('vendor/moment/moment-with-locales.min.js') }}"></script>
    <!-- BEGIN PLUGINS JS -->
    <script src="{{ Theme::asset('vendor/pace/pace.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/toastr/toastr.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/stacked-menu/stacked-menu.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/flatpickr/plugins/monthSelect/index.js') }}"></script>

    <script src="{{ Theme::asset('vendor/flot/jquery.flot.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/flot/jquery.flot.resize.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/flot/jquery.flot.categories.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/flot/jquery.flot.time.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/flot/jquery.flot.time.min.js') }}"></script>

    <script src="{{ Theme::asset('vendor/select2/js/select2.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/select2/js/i18n/' . app()->getLocale() . '.js') }}"></script>

    <script src="{{ Theme::asset('vendor/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/flatpickr/l10n/' . app()->getLocale() .'.js') }}"></script>
    <script> flatpickr.localize(flatpickr.l10ns.{{ app()->getLocale() }}); </script>

    @if($locale = Summernote::locale())
      <script src="{{ Theme::asset("vendor/summernote/lang/summernote-{$locale}.min.js") }}"></script>
    @endif
    <script src="{{ Theme::asset('vendor/easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/chart.js/Chart.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/color-hash/color-hash.dist.js') }}"></script> <!-- END PLUGINS JS -->
    <!-- BEGIN THEME JS -->
    <script src="{{ Theme::asset('javascript/theme.min.js') }}"></script> <!-- END THEME JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    {{-- <script src="{{ Theme::asset('javascript/pages/dashboard-demo.js') }}"></script> <!-- END PAGE LEVEL JS --> --}}

    @foreach(Theme::externalScripts() as $script)
      <script src="{{ $script }}"></script>
    @endforeach

    @foreach(Theme::scripts() as $script)
      <script src="{{ Theme::asset("javascript/{$script}.js") }}"></script>
    @endforeach
     <!-- END PAGE LEVEL JS -->
  </body>
</html>
