<!DOCTYPE html>
<html lang="en">
  <head>
    @include(Theme::core('head'))
    
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- End Required meta tags -->

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="144x144" href="{{ Theme::asset('apple-touch-icon.png') }}">
    <link rel="shortcut icon" href="{{ Theme::asset('favicon.ico') }}">
    <meta name="theme-color" content="#3063A0">

    <!-- GOOGLE FONT -->
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600" rel="stylesheet">
    <!-- End Google font -->

    <!-- BEGIN PLUGINS STYLES -->
    <link rel="stylesheet" href="{{ Theme::asset('vendor/fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('vendor/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('vendor/open-iconic/css/open-iconic-bootstrap.min.css') }}">
    <!-- END PLUGINS STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" href="{{ Theme::asset('stylesheets/theme.min.css') }}" data-skin="default">
    <link rel="stylesheet" href="{{ Theme::asset('stylesheets/theme-dark.min.css') }}" data-skin="dark">
    <link rel="stylesheet" href="{{ Theme::asset('stylesheets/custom.css') }}"><!-- Disable unused skin immediately -->

    @foreach(Theme::stylesheets() as $stylesheet)
      <link rel="stylesheet" href="{{ Theme::asset("stylesheets/{$stylesheet}.css") }}">
    @endforeach

    @foreach(Theme::externalStylesheets() as $stylesheet)
      <link rel="stylesheet" href="{{!! $stylesheet !!}}">
    @endforeach

    <script>
      var skin = localStorage.getItem('skin') || 'default';
      var unusedLink = document.querySelector('link[data-skin]:not([data-skin="' + skin + '"])');
      unusedLink.setAttribute('rel', '');
      unusedLink.setAttribute('disabled', true);
    </script><!-- END THEME STYLES -->
  </head>
  <body>
    <!--[if lt IE 10]>
    <div class="page-message" role="alert">You are using an <strong>outdated</strong> browser. Please <a class="alert-link" href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</div>
    <![endif]-->
    <!-- .auth -->
    <main class="auth">
      <header id="auth-header" class="auth-header">
        <h1>
            {{ t('looper.site') }}
            <h6>{{ t('login.title') }}</h6>
          <span class="sr-only">{{ t('login.sign_in') }}</span>
        </h1>
      </header><!-- form -->

      @yield('content')

    </main><!-- /.auth -->
    <!-- BEGIN BASE JS -->
    <script src="{{ Theme::asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script> <!-- END BASE JS -->
    <!-- BEGIN PLUGINS JS -->
    <script src="{{ Theme::asset('vendor/particles.js/particles.min.js') }}"></script>
    <script src="{{ Theme::asset('vendor/toastr/toastr.min.js') }}"></script>
    <script>
      /**
       * Keep in mind that your scripts may not always be executed after the theme is completely ready,
       * you might need to observe the `theme:load` event to make sure your scripts are executed after the theme is ready.
       */
      $(document).on('theme:init', () =>
      {
        /* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
        particlesJS.load('auth-header', '{{ Theme::asset('javascript/pages/particles.json') }}');
      })
    </script> <!-- END PLUGINS JS -->
    <!-- BEGIN THEME JS -->
    <script src="{{ Theme::asset('javascript/theme.js') }}"></script> <!-- END THEME JS -->
    <script>
    </script>
  </body>
</html>
