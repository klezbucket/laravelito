<thead class="thead-">
    <tr>
        @foreach($form->fields() as $field)
            @include(RowsForm::th($form,$field))
        @endforeach

        <th>@include(RowsForm::append($form))</th>
    </tr>
</thead>