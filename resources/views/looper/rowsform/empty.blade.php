<tr data-empty="true" style="display: none;">
    <td colspan="{{ RowsForm::cols($form) }}">
        <div class="alert alert-info alert-dismissible fade show">
            {{ t($form->empty()) }}
        </div>
    </td>
</tr>