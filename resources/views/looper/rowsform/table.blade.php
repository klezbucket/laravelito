@if($form->rows()->isEmpty())
    <div class="alert alert-info alert-dismissible fade show">
        {{ t($form->empty()) }}
    </div>
@else
    <div class="table-responsive">
        <table id="{{ $form->name() }}" class="table">
            <thead>
                <tr>
                    @foreach($form->fields() as $field)
                        <th>{{ Field::label($field) }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($form->rows() as $row)
                    <tr>
                        @foreach($form->fields() as $field)
                            @php $form->model($row) @endphp
                            <td>@include(Type::value($form, $field), [
                                'provider' => $form
                            ])</td>
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endif