<form method="post" class="auth-form">
    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

    <!-- .form-group -->
    <div class="form-group">
        @unless ((request())->isMethod('get'))
            <div class="alert alert-secondary has-icon alert-dismissible fade show">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <div class="alert-icon">
                    <span class="oi oi-flag"></span>
                </div>
                <h4 class="alert-heading">{{ t('login.invalid.title') }}</h4>
                <p class="mb-0">{{ t('login.invalid.message') }}.</p>
            </div>
        @endunless

        <div class="form-label-group">
            <input value="{{request()->get('email') }}" name="email" type="email" id="inputUser" class="form-control" placeholder="{{ t('login.email') }}" autofocus=""> <label for="inputUser">{{ t('login.email') }}</label>
        </div>
    </div><!-- /.form-group -->
    <!-- .form-group -->
    <div class="form-group">
        <div class="form-label-group">
            <input name="password" type="password" id="inputPassword" class="form-control" placeholder="{{ t('login.password') }}"> <label for="inputPassword">{{ t('login.password') }}</label>
        </div>
    </div><!-- /.form-group -->
    <!-- .form-group -->
    <div class="form-group">
        <button class="btn btn-lg btn-primary btn-block" type="submit">{{ t('login.sign_in') }}</button>
    </div><!-- /.form-group -->
</form>