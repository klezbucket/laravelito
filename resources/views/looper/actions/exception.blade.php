<div class="wrapper">
    <!-- .empty-state -->
    <div class="empty-state">
      <!-- .empty-state-container -->
      <div class="empty-state-container">
        <div class="state-figure">
          <img class="img-fluid" src="{{ Theme::asset('images/illustration/setting.svg') }}" alt="" style="max-width: 320px">
        </div>
        <h3 class="state-header"> {{ $exception->getStatusCode() }} {{ $exception->getMessage() }} </h3>
      </div><!-- /.empty-state-container -->
    </div><!-- /.empty-state -->
  </div>