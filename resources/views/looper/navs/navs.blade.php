<div class="card card-fluid">
    <!-- .card-header -->
    <div class="card-header">
      <!-- .nav-tabs -->
      <ul class="nav nav-tabs card-header-tabs">
        <li class="nav-item">
            @foreach($navs->panels() as $panel)
                <a class="nav-link @if($panel->active()) {{ 'show active' }} @endif" data-toggle="tab" href="#{{ $panel->id() }}">{{ $panel->label() }}</a>
            @endforeach
        </li>
      </ul><!-- /.nav-tabs -->
    </div><!-- /.card-header -->
    <!-- .card-body -->
    <div class="card-body">
      <!-- .tab-content -->
      <div class="tab-content">
        @foreach($navs->panels() as $panel)
            <div class="tab-pane fade @if($panel->active()) {{ 'show active' }} @endif" id="{{ $panel->id() }}">
                @include($panel->partial())
            </div>
        @endforeach
      </div><!-- /.tab-content -->
    </div><!-- /.card-body -->
  </div>
