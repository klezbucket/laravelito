@extends(Theme::layout($layout))
@include(Theme::partial($partial))
@section('content')
    @include(Theme::action($action))
@endsection
@section('sidebar')
    @include(Theme::optional('sidebars.' . $action))
@endsection
@section('page')
    @if(Theme::exists('sidebars.' . $action))
        has-sidebar has-sidebar-expand-xl
    @endif
@endsection