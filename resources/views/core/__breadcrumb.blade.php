<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        @foreach ($items as $item)
            @include(Theme::partial(Theme::currentLayout() . '.breadcrumb.' . $item->partial()), $item->data() + [ 'item' => $item ])
        @endforeach
    </ol>
</nav>