@foreach ($items as $item)
    @include(Theme::partial(Theme::currentLayout() . '.menu.' . $item->partial()), $item->data() + [ 'item' => $item ])
@endforeach
