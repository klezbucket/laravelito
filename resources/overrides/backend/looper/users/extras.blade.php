<div class="card card-fluid">
    <!-- .card-header -->
    <div class="card-header">
      <!-- .nav-tabs -->
      <ul class="nav nav-tabs card-header-tabs">
        <li class="nav-item">
        <a class="nav-link show active" data-toggle="tab" href="#roles">{{ t('users.fields.roles.label') }}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#platforms">{{ t('users.fields.platforms.label') }}</a>
        </li>
      </ul><!-- /.nav-tabs -->
    </div><!-- /.card-header -->
    <!-- .card-body -->
    <div class="card-body">
      <!-- .tab-content -->
      <div class="tab-content">
        <div class="tab-pane fade active show" id="roles">
            @if($roles->isEmpty())
                <div class="alert alert-info alert-dismissible fade show">
                    {{ t('users.fields.roles.empty') }}
                </div>
            @endif

            @foreach($roles as $role)
                <div class="p-sm-2">
                    <h6>{{ $role->description }}</h6>
                </div>
            @endforeach
        </div>
        <div class="tab-pane fade" id="platforms">
            @if($platforms->isEmpty())
                <div class="alert alert-info alert-dismissible fade show">
                    {{ t('users.fields.platforms.empty') }}
                </div>
            @endif

            @foreach($platforms as $platform)
                <div class="p-sm-2">
                    <h6>{{ $platform->description }}</h6>
                </div>
            @endforeach
        </div>
      </div><!-- /.tab-content -->
    </div><!-- /.card-body -->
  </div>
