<?php

return [
    'module' => [
        'name' => 'Usuarios'
    ],
    'edit' => [
        'title' => 'Editar',
        'submit' => 'Editar Usuario',
        'invalid' => 'Los datos de usuario no son válidos, revisa el formulario e inténtalo nuevamente',
        'failure' => 'Ha ocurrido un error al editar el usuario',
        'success' => 'Se ha editado el usuario',
    ],
    'fetch' => [
        'title' => 'Detalles',
    ],
    'collections' => [
        'enabled' => [
            'title' => 'Listar Usuarios',
            'summary' => 'Mostrando usuarios del :lower al :upper, de un total de :total'
        ],
        'disabled' => [
            'title' => 'Usuarios Eliminados',
            'summary' => 'Mostrando usuarios eliminados del :lower al :upper, de un total de :total'
        ],
    ],
    'create' => [
        'side' => [
            'title' => 'Asignar a Usuario'
        ],
        'title' => 'Crear Usuario',
        'submit' => 'Crear Usuario',
        'invalid' => 'Los datos de usuario no son válidos, revisa el formulario e inténtalo nuevamente',
        'failure' => 'Ha ocurrido un error al crear un nuevo usuario',
        'success' => 'Se ha creado un nuevo usuario',
    ],
    'fields' => [
        'full_name' => [
            'label' => 'Nombre Completo',
        ],
        'first_name' => [
            'label' => 'Nombre',
            'placeholder' => 'Debes espeficicar nombre',
            'validation' => [
                'required' => 'Nombre es requerido'
            ]
        ],
        'last_name' => [
            'label' => 'Apellido',
            'placeholder' => 'Debes espeficicar apellido',
            'validation' => [
                'required' => 'Apellido es requerido'
            ]
        ],
        'email' => [
            'label' => 'Correo electrónico',
            'placeholder' => 'Debes espeficicar correo electrónico',
            'validation' => [
                'required' => 'Correo electrónico es requerido',
                'email' => 'Debe ser un correo electrónico válido',
                'unique' => 'Este correo electrónico ya ha sido registrado por otro usuario',
                'max' => [
                    'string' => 'No puede superar los 60 caracteres'
                ]
            ]
        ],
        'password' => [
            'label' => 'Contraseña',
            'placeholder' => [
                'update' => 'No se actualizará',
                'create' => 'Debes especificar contraseña'
            ],
            'validation' => [
                'min' => [
                    'string' => 'Debe poseer al menos 6 caracteres'
                ],
                'required' => 'Contraseña es requerida'
            ]
        ],
        'deleted_at' => [
            'label' => 'Eliminado',
            'null' => 'No está eliminado',
            'boolean' => [
                'true' => 'Habilitado',
                'false' => 'Eliminado'
            ]
        ],
        'timezone' => [
            'label' => 'Zona horaria',
            'placeholder' => 'Debes especificar una zona horaria',
            'validation' => [
                'required' => 'Zona horaria es requerida',
            ]
        ],
        'lang' => [
            'label' => 'Idioma',
            'placeholder' => 'Debes especificar un idioma',
            'validation' => [
                'required' => 'Idioma es requerido',
            ],
            'options' => [
                'en' => 'English',
                'es' => 'Español'
            ]
        ],
        'roles' => [
            'label' => 'Roles',
            'placeholder' => 'Selecciona uno o varios roles',
            'validation' => [
                'required' => 'Debes especificar al menos un rol',
            ],
            'empty' => 'No se han especificado roles'
        ],
        'platforms' => [
            'label' => 'Plataformas',
            'placeholder' => 'Selecciona uno o varias plataaformas',
            'validation' => [
                'required' => 'Debes especificar al menos una plataforma',
            ],
            'empty' => 'No se han especificado plataformas'
        ],
        'created_at' => [
            'label' => 'Fecha alta'
        ],
    ]
];
