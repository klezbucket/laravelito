<?php

return [
    'login' => [
        'sign_in' => 'Ingresa a tu cuenta',
        'password' => 'Contraseña',
        'email' => 'Correo Electrónico',
        'title' => 'Acceso',
        'invalid' => [
            'title' => 'Advertencia!',
            'message' => 'Las credenciales no son válidas',
        ]
    ],
    'logout' => [
        'title' => 'Cerrar sesión'
    ],
    'me' => [
        'title' => 'Mi cuenta',
        'submit' => 'Guardar Cambios',
        'invalid' => 'Los datos no son válidos, verifica el formulario e inténtalo nuevamente',
        'success' => 'Datos de la cuenta actualizados correctamente',
        'failure' => 'Ha ocurrido un error al actualizar los datos de la cuenta',
        'fields' => [
            'first_name' => [
                'label' => 'Nombre',
                'placeholder' => 'Debe espeficicar nombre',
                'validation' => [
                    'required' => 'Nombre es requerido'
                ]
            ],
            'last_name' => [
                'label' => 'Apellido',
                'placeholder' => 'Debe espeficicar apellido',
                'validation' => [
                    'required' => 'Apellido es requerido'
                ]
            ],
            'email' => [
                'label' => 'Correo electrónico',
                'placeholder' => 'Debe espeficicar correo electrónico',
                'validation' => [
                    'required' => 'Correo electrónico es requerido',
                    'email' => 'Debe ser un correo electrónico válido'
                ]
            ],
            'password' => [
                'label' => 'Contraseña',
                'placeholder' => 'No se actualizará',
                'validation' => [
                    'min' => [
                        'string' => 'Debe poseer al menos 6 caracteres'
                    ]
                ]
            ],
            'timezone' => [
                'label' => 'Zona horaria',
                'placeholder' => 'Debe especificar una zona horaria',
                'validation' => [
                    'required' => 'Zona horaria es requerida',
                ]
            ],
            'lang' => [
                'label' => 'Idioma',
                'placeholder' => 'Debe especificar un idioma',
                'validation' => [
                    'required' => 'Idioma es requerido',
                ],
                'options' => [
                    'en' => 'English',
                    'es' => 'Español'
                ]
            ],
        ]
    ],
    'menu' => [
        'title' => 'Menú',
        'home' => 'Inicio',
    ],
    'errors' => [
        'not_found' => 'No encontrado',
        'unauthorized' => 'Requiere autenticación',
        'forbidden' => 'No posee permisos',
        'internal_error' => 'Error interno',
    ],
    'home' => [
        'title' => 'Bienvenido'
    ],
    'error' => [
        'title' => 'Ha ocurrido un error'
    ],
    'looper' => [
        'site' => 'Backend',
        'nightmode' => 'Modo Noche',
        'copyright' => 'Copyright © 2020',
        'required' => 'Requerido',
        'gmaps' => [
            'loading' => 'Esperando por Google Maps...'
        ],
        'autocomplete' => [
            'clear' => 'Limpiar selección'
        ],
        'upload' => [
            'invalid' => [
                'size' => 'El archivo supera el tamaño permitido de :max (:bytes bytes)',
                'mime' => 'Tipo de archivo no válido'
            ],
            'clear' => 'Limpiar archivo'
        ],
        'download' => [
            'link' => 'Descargar archivo'
        ],
        'picture' => [
            'link' => 'Ver imagen'
        ],
        'summernote' => [
            'link' => 'Ver contenido'
        ]
    ],
    'menu' => require(__DIR__ . '/menu.php'),
    'users' => require(__DIR__ . '/modules/users.php'),
    'roles' => require(__DIR__ . '/modules/roles.php'),
    'platforms' => require(__DIR__ . '/modules/platforms.php'),
];
