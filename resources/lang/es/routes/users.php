<?php

return [
    'create' => '/usuarios/crear.html',
    'fetch' => '/usuarios/{id}/detallar/{full_name}.html',
    'edit' => '/usuarios/{id}/editar/{full_name}.html',
    'collections' => [
        'enabled' => '/usuarios/listar.html',
        'disabled' => '/usuarios/listar/eliminados.html',
    ],
];
