<?php

return [
    'backend' => [
        'login' => '/iniciar-sesion.html',
        'logout' => '/cerrar-sesion.html',
        'me' => '/mi-cuenta.html',
        'home' => '/',
    ],
    'users' => require(__DIR__ . '/routes/users.php'),
    'platforms' => require(__DIR__ . '/routes/platforms.php'),
    'roles' => require(__DIR__ . '/routes/roles.php'),
];
