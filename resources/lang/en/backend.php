<?php

return [
    'login' => [
        'sign_in' => 'Login into your account',
        'password' => 'Password',
        'email' => 'E-mail',
        'title' => 'Login',
        'invalid' => [
            'title' => 'Warning!',
            'message' => 'The credentials are not valid!',
        ]
    ],
    'logout' => [
        'title' => 'Log out'
    ],
    'me' => [
        'title' => 'My account',
        'submit' => 'Save changes',
        'invalid' => 'Invalid data, check the form for any errors reported',
        'success' => 'Account data updated',
        'failure' => 'Unexpected error',
        'fields' => [
            'first_name' => [
                'label' => 'First Name',
                'placeholder' => 'You must specify a first name',
                'validation' => [
                    'required' => 'First name is required'
                ]
            ],
            'last_name' => [
                'label' => 'Last name',
                'placeholder' => 'You must specify a last name',
                'validation' => [
                    'required' => 'Last name is required'
                ]
            ],
            'email' => [
                'label' => 'E-mail',
                'placeholder' => 'You must specify an e-mail address',
                'validation' => [
                    'required' => 'E-mail is required',
                    'email' => 'It must be a valid e-mail address',
                    'unique' => 'This email has been taken by other User',
                ]
            ],
            'password' => [
                'label' => 'Password',
                'placeholder' => 'It will not be changed',
                'validation' => [
                    'min' => [
                        'string' => 'It must have at least 6 characters'
                    ]
                ]
            ],
            'timezone' => [
                'label' => 'Timezone',
                'placeholder' => 'You must specify a timezone for your account',
                'validation' => [
                    'required' => 'Timezone is required',
                ]
            ],
            'lang' => [
                'label' => 'Language',
                'placeholder' => 'You must specify a language',
                'validation' => [
                    'required' => 'Language is required',
                ],
                'options' => [
                    'en' => 'English',
                    'es' => 'Español'
                ]
            ],
        ]
    ],
    'menu' => [
        'title' => 'Menu',
        'home' => 'Home',
    ],
    'errors' => [
        'not_found' => 'Not found',
        'unauthorized' => 'Unahtorized',
        'forbidden' => 'Forbidden',
        'internal_error' => 'Internal error',
    ],
    'home' => [
        'title' => 'Welcome'
    ],
    'error' => [
        'title' => 'Error'
    ],
    'looper' => [
        'site' => 'Backend',
        'menu' => 'Menu',
        'nightmode' => 'Night Mode',
        'copyright' => 'Copyright © 2019',
        'required' => 'Required',
        'gmaps' => [
            'loading' => 'Waiting for Google Maps...'
        ],
        'autocomplete' => [
            'clear' => 'Clear selection'
        ],
        'upload' => [
            'invalid' => [
                'size' => 'File size too long, should not be larger than :max (:bytes bytes)',
                'mime' => 'File type is not valid'
            ],
            'clear' => 'Drop file'
        ],
        'download' => [
            'link' => 'Download file'
        ],
        'picture' => [
            'link' => 'Display image'
        ]
    ],
    'menu' => require(__DIR__ . '/menu.php'),
    'users' => require(__DIR__ . '/modules/users.php'),
    'roles' => require(__DIR__ . '/modules/roles.php'),
    'platforms' => require(__DIR__ . '/modules/platforms.php'),
];
