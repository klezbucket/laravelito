<?php

return [
    'backend' => [
        'login' => '/login.html',
        'logout' => '/logout.html',
        'me' => '/my-account.html',
        'home' => '/',
    ],
    'users' => require(__DIR__ . '/routes/users.php'),
    'platforms' => require(__DIR__ . '/routes/platforms.php'),
    'roles' => require(__DIR__ . '/routes/roles.php'),
];
