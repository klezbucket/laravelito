<?php

return [
    'module' => [
        'name' => 'Users'
    ],
    'edit' => [
        'title' => 'Edit',
        'submit' => 'Edit User',
        'invalid' => 'Invalid data, check the form for any errors reported',
        'success' => 'User data updated',
        'failure' => 'Unexpected error',
    ],
    'fetch' => [
        'title' => 'Details',
    ],
    'collections' => [
        'enabled' => [
            'title' => 'List Users',
            'summary' => 'Showing users from :lower to :upper, total :total'
        ],
        'disabled' => [
            'title' => 'Deleted Users',
            'summary' => 'Showing deleted users from :lower to :upper, total :total'
        ],
    ],
    'create' => [
        'side' => [
            'title' => 'Assign to User'
        ],
        'title' => 'Create User',
        'submit' => 'Create User',
        'invalid' => 'Invalid data, check the form for any errors reported',
        'success' => 'User successfully created',
        'failure' => 'Unexpected error',
    ],
    'fields' => [
        'full_name' => [
            'label' => 'Full Name',
        ],
        'first_name' => [
            'label' => 'First Name',
            'placeholder' => 'You must specify a first name',
            'validation' => [
                'required' => 'First name is required'
            ]
        ],
        'last_name' => [
            'label' => 'Last name',
            'placeholder' => 'You must specify a last name',
            'validation' => [
                'required' => 'Last name is required'
            ]
        ],
        'email' => [
            'label' => 'E-mail',
            'placeholder' => 'You must specify an e-mail address',
            'validation' => [
                'required' => 'E-mail is required',
                'email' => 'It must be a valid e-mail address',
                'unique' => 'This email has been taken by other User',
            ]
        ],
        'password' => [
            'label' => 'Password',
            'placeholder' => [
                'update' => 'It will not be updated',
                'placeholder' => 'You must specify a password',
            ],
            'validation' => [
                'min' => [
                    'string' => 'It must have at least 6 characters'
                ],
                'required' => 'Password is required'
            ]
        ],
        'deleted_at' => [
            'label' => 'Deleted',
            'null' => 'Not deleted',
            'boolean' => [
                'true' => 'Available',
                'false' => 'Deleted'
            ]
        ],
        'timezone' => [
            'label' => 'Timezone',
            'placeholder' => 'You must specify a timezone',
            'validation' => [
                'required' => 'Timezone is required'
            ]
        ],
        'lang' => [
            'label' => 'Language',
            'placeholder' => 'You must specify a language',
            'validation' => [
                'required' => 'Language is required'
            ],
            'options' => [
                'en' => 'English',
                'es' => 'Español'
            ]
        ],
        'roles' => [
            'label' => 'Roles',
            'placeholder' => 'Select one or more roles',
            'validation' => [
                'required' => 'You must specify at least one role',
            ],
            'empty' => 'Roles not specified'
        ],
        'platforms' => [
            'label' => 'Plataforms',
            'placeholder' => 'Select one or more platforms',
            'validation' => [
                'required' => 'You must specify at least one platform',
            ],
            'empty' => 'Platforms not specified'
        ],
        'created_at' => [
            'label' => 'Create date'
        ]
    ]
];
