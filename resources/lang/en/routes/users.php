<?php

return [
    'create' => '/users/create.html',
    'fetch' => '/users/{id}/detail/{full_name}.html',
    'edit' => '/users/{id}/edit/{full_name}.html',
    'collections' => [
        'enabled' => '/users/list.html',
        'disabled' => '/users/list/deleted.html',
    ],
];
