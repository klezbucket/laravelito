<?php

return [
    'create' => '/placeholders/create.html',
    'fetch' => '/placeholders/{id}/detail/{LABELHOLDER}.html',
    'edit' => '/placeholders/{id}/edit/{LABELHOLDER}.html',
    'collections' => [
        'enabled' => '/placeholders/list.html',
        'disabled' => '/placeholders/list/deleted.html',
    ],
    'autocomplete' => '/placeholders/autocomplete.html',
];
