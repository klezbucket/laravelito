<?php

return [
    'module' => [
        'name' => 'Placeholders'
    ],
    'edit' => [
        'title' => 'Edit',
        'submit' => 'Edit Placeholder',
        'invalid' => 'Invalid data, check the form for any errors reported',
        'success' => 'Placeholder data updated',
        'failure' => 'Unexpected error',
    ],
    'fetch' => [
        'title' => 'Details',
    ],
    'collections' => [
        'enabled' => [
            'title' => 'List Placeholders',
            'summary' => 'Showing placeholders from :lower to :upper, total :total'
        ],
        'disabled' => [
            'title' => 'Deleted Placeholders',
            'summary' => 'Showing deleted placeholders from :lower to :upper, total :total'
        ],
    ],
    'create' => [
        'title' => 'Create Placeholder',
        'submit' => 'Create Placeholder',
        'invalid' => 'Invalid data, check the form for any errors reported',
        'success' => 'Placeholder successfully created',
        'failure' => 'Unexpected error',
    ],
    'fields' => [

    ]
];
