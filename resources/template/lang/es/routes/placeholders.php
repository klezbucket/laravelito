<?php

return [
    'create' => '/placeholders/crear.html',
    'fetch' => '/placeholders/{id}/detallar/{LABELHOLDER}.html',
    'edit' => '/placeholders/{id}/editar/{LABELHOLDER}.html',
    'collections' => [
        'enabled' => '/placeholders/listar.html',
        'disabled' => '/placeholders/listar/eliminados.html',
    ],
    'autocomplete' => '/placeholders/autocompletar.html',
];
