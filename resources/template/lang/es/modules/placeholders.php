<?php

return [
    'module' => [
        'name' => 'Placeholders'
    ],
    'edit' => [
        'title' => 'Editar',
        'submit' => 'Editar Placeholder',
        'invalid' => 'Los datos de placeholder no son válidos, revisa el formulario e inténtalo nuevamente',
        'failure' => 'Ha ocurrido un error al editar el placeholder',
        'success' => 'Se ha editado el placeholder',
    ],
    'fetch' => [
        'title' => 'Detalles',
    ],
    'collections' => [
        'enabled' => [
            'title' => 'Listar Placeholders',
            'summary' => 'Mostrando placeholders del :lower al :upper, de un total de :total'
        ],
        'disabled' => [
            'title' => 'Placeholders Eliminados',
            'summary' => 'Mostrando placeholders eliminados del :lower al :upper, de un total de :total'
        ],
    ],
    'create' => [
        'title' => 'Crear Placeholder',
        'submit' => 'Crear Placeholder',
        'invalid' => 'Los datos de placeholder no son válidos, revisa el formulario e inténtalo nuevamente',
        'failure' => 'Ha ocurrido un error al crear un nuevo placeholder',
        'success' => 'Se ha creado un nuevo placeholder',
    ],
    'fields' => [

    ]
];
